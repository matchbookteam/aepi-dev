<?php
/**
 * Extension-Boilerplate
 * @link https://github.com/ReduxFramework/extension-boilerplate
 *
 * Radium Importer - Modified For ReduxFramework
 * @link https://github.com/FrankM1/radium-one-click-demo-install
 *
 * @package     WBC_Importer - Extension for Importing demo content
 * @author      Webcreations907
 * @version     1.0.1
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Don't duplicate me!
if ( !class_exists( 'ReduxFramework_wbc_importer' ) ) {

    /**
     * Main ReduxFramework_wbc_importer class
     *
     * @since       1.0.0
     */
    class ReduxFramework_wbc_importer {

        /**
         * Field Constructor.
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        function __construct( $field = array(), $value ='', $parent ) {
            $this->parent = $parent;
            $this->field = $field;
            $this->value = $value;

            $class = ReduxFramework_extension_wbc_importer::get_instance();

            if ( !empty( $class->demo_data_dir ) ) {
                $this->demo_data_dir = $class->demo_data_dir;
                $this->demo_data_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->demo_data_dir ) );
            }

            if ( empty( $this->extension_dir ) ) {
                $this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
                $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
            }
        }

        /**
         * Field Render Function.
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function render() {

            echo '</fieldset></td></tr><tr><td colspan="2"><fieldset class="redux-field wbc_importer">';

            $nonce = wp_create_nonce( "redux_{$this->parent->args['opt_name']}_wbc_importer" );

            // No errors please
            $defaults = array(
                'id'        => '',
                'url'       => '',
                'width'     => '',
                'height'    => '',
                'thumbnail' => '',
            );

            $this->value = wp_parse_args( $this->value, $defaults );

            $imported = false;

            $this->field['wbc_demo_imports'] = apply_filters( "redux/{$this->parent->args['opt_name']}/field/wbc_importer_files", array() );

            echo '<div class="theme-browser">';
            
            // Filters
            
            echo '<div class="demo-filters-container"><ul id="demo-filters">';
            
            //echo '<li class="filter-title">' . esc_html('Filter demos:', 'crexis') . '</li>';
            echo '<li class="demo-filter active" data-type="*">' . esc_html('Show All', 'crexis') . '</li>';
            echo '<li class="demo-filter" data-type="classic-home">' . esc_html('Classic Homepage', 'crexis') . '</li>';
            echo '<li class="demo-filter" data-type="onepager">' . esc_html('One Pager', 'crexis') . '</li>';
            echo '<li class="demo-filter" data-type="fullpage">' . esc_html('Full Page', 'crexis') . '</li>';
            
            echo '</ul></div>';
            
            echo '<div class="themes">';

            if ( !empty( $this->field['wbc_demo_imports'] ) ) {

                foreach ( $this->field['wbc_demo_imports'] as $section => $imports ) {

                    if ( empty( $imports ) ) {
                        continue;
                    }

                    if ( !array_key_exists( 'imported', $imports ) ) {
                        $extra_class = 'not-imported';
                        $imported = false;
                        $import_message = esc_html__( 'Import Demo', 'framework' );
                    }else {
                        $imported = true;
                        $extra_class = 'active imported';
                        $import_message = esc_html__( 'Demo Imported', 'framework' );
                    }
                    
                    $demo_name = apply_filters( 'wbc_importer_directory_title', $imports['directory'] );
                    
                    $demo_type = 'classic-home';
                    
                    if( substr( $demo_name, 0, 9 ) === "onepagerf" ) {
                    
                    	$demo_type = 'onepager fullpage';
                    	
                    } elseif( substr( $demo_name, 0, 8 ) === "onepager" ) {
                    
                    	$demo_type = 'onepager';
                    }
                    
                    echo '<div class="wrap-importer theme theme-visible '.$extra_class.'" data-demo-id="'.esc_attr( $section ).'"  data-nonce="' . $nonce . '" data-demo-type="' . esc_attr( $demo_type ) . '" id="' . $this->field['id'] . '-custom_imports">';

                    echo '<div class="theme-screenshot">';
                    
                    if( substr( $demo_name, 0, 9 ) === "onepagerf" ) {
                    
                    	echo '<div class="demo-type demo-type-fullpage">' . esc_html__('FullPage', 'crexis') .'</div>';
                    	
                    } elseif( substr( $demo_name, 0, 8 ) === "onepager" ) {
                    
                    	echo '<div class="demo-type demo-type-onepager">' . esc_html__('OnePager', 'crexis') .'</div>';
                    }

                    if ( isset( $imports['image'] ) ) {
                        echo '<img class="wbc_image" src="'.esc_attr( esc_url( $this->demo_data_url.$imports['directory'].'/'.$imports['image'] ) ).'"/>';

                    }
                    
                    //if($demo_name == '0demo-main') {
                    	//echo '<div class="theme-overlay"><p>' . esc_html__('Average remaining time', 'crexis') . ': <span class="count-container"><span class="count-value">100</span>s</span></p></div>';
                    	//echo '<div class="theme-overlay"><p>' . esc_html__('Average remaining time: 2 minutes.', 'crexis') . '</div>';
                    //}
                    
                    echo '</div>';

                    echo '<span class="more-details">'.$import_message.'</span>';
                    echo '<h3 class="theme-name">';
                    
                    
                    
                    if($demo_name == '0demo-main') {
                    	
                    	$demo_name = 'Base Demo Content - <span class="red-color">import first!</span>';
                    	
                    } else {
                    
	                    $demo_name = str_replace('__home', ': ', $demo_name);
	                    $demo_name = str_replace('onepagerf', 'onepager', $demo_name);
	                    $demo_name = str_replace('-', ' ', $demo_name);
	                    //$demo_name = str_replace('0', '', $demo_name);
	                    $demo_name = str_replace('onepager', 'OnePager', $demo_name);
	                    
	                    $demo_name = esc_html( ucwords( $demo_name ) );
	                    
	                }
                    
                    echo $demo_name;
                    
                    echo '</h3>';

                    echo '<div class="theme-actions">';
                    if ( false == $imported ) {
                        echo '<div class="wbc-importer-buttons"><span class="spinner">'.esc_html__( 'Please Wait...', 'framework' ).'</span><span class="button-primary importer-button import-demo-data">' . __( 'Import', 'framework' ) . '</span></div>';
                    }else {
                        echo '<div class="wbc-importer-buttons button-secondary importer-button">'.esc_html__( 'Imported', 'framework' ).'</div>';
                        echo '<span class="spinner">'.esc_html__( 'Please Wait...', 'framework' ).'</span>';
                        echo '<div id="wbc-importer-reimport" class="wbc-importer-buttons button-primary import-demo-data importer-button">'.esc_html__( 'Re-Import', 'framework' ).'</div>';
                    }
                    echo '</div>';
                    echo '</div>';


                }

            } else {
                echo "<h5>".esc_html__( 'No Demo Data Provided', 'framework' )."</h5>";
            }

            echo '</div></div>';
            echo '</fieldset></td></tr>';

        }

        /**
         * Enqueue Function.
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function enqueue() {

            $min = Redux_Functions::isMin();

            wp_enqueue_script(
                'redux-field-wbc-importer-js',
                $this->extension_url . '/field_wbc_importer' . $min . '.js',
                array( 'jquery' ),
                time(),
                true
            );

            wp_enqueue_style(
                'redux-field-wbc-importer-css',
                $this->extension_url . 'field_wbc_importer.css',
                time(),
                true
            );

        }
    }
}
