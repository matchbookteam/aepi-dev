<?php

// Blog Posts Carousel

function vntd_button_slider($atts, $content = null) {
	extract(shortcode_atts(array(
		"slides" 		=> '',
		"bg_overlay"	=> 'none',
		"arrow_navigation" => 'yes',
		"height" => '600',
		"parallax" => 'yes'
	), $atts));
	
	wp_enqueue_script('superslides', '', '', '', true);
	//wp_enqueue_script( 'vc_jquery_skrollr_js' );
	wp_enqueue_script('vntd-parallax', '', '', '', true);
	//wp_enqueue_script('vntd-parallax', '', '', '', true);
	
	$height = str_replace('px', '', $height);
	
	$random_id = rand(1,9999);
	
	ob_start();
	
	$extra_class = '';
	
	$values = (array) vc_param_group_parse_atts( $slides );
	
	if( sizeof( $values ) == 1 ) $extra_class = ' button-no-slider';
   
	echo '<div class="fullscreen-slider button-slider' . esc_attr( $extra_class ) . '" style="height: ' . esc_attr( $height ) . 'px !important">';
	
	echo '<div class="slides-container relative">';
	
	foreach ( $values as $data ) {
		$new_line = $data;
		
		$new_line['image'] = isset( $data['image'] ) ? $data['image'] : '';
		$new_line['button_label'] = isset( $data['button_label'] ) ? $data['button_label'] : '';
		$new_line['button_url'] = isset( $data['button_url'] ) ? $data['button_url'] : '';
		$new_line['button_url_target'] = isset( $data['button_url_target'] ) ? $data['button_url_target'] : '';
		
		$img_url = wp_get_attachment_image_src($new_line['image'], 'crexis-bg-image');
		
		echo '<div class="parallax2" style="background-image: url(' . $img_url[0] . ');">';
		
		if($bg_overlay != 'none') {
			echo '<div class="bg-overlay bg-overlay-' . esc_attr($bg_overlay) . '"></div>';
		}
		
		echo '<div class="button-slider-holder"><a href="' . esc_url($new_line['button_url']) . '" class="button-slider-btn font-primary">' . esc_html( $new_line['button_label'] ) . '</a></div>';
		
		echo '</div>';
	}
	
	echo '</div>';
	
	if($arrow_navigation != 'no') {
	?>
	<nav class="slides-navigation">
		<a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
		<a href="#" class="next"><i class="fa fa-angle-right"></i></a>
	</nav>
	<?php
	
	}
	
	echo '</div>';

	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('vntd_button_slider');
add_shortcode('vntd_button_slider', 'vntd_button_slider');