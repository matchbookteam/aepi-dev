<?php

// List Shortcode Processing

function crexis_list($atts, $content=null) {
	extract(shortcode_atts(
		array(
		"icon" => 'angle-right',
		"color" => 'accent',
		"background" => '',
		"colored" => 'yes'
		), $atts)
	);
	
	$background_class = '';
	
	if($background == 'yes') {
		$background_class = 'vntd-list-bg';
	}
	
	$colored_class = 'vntd-list-colored';
	
	if($colored == 'no') $colored_class = '';
	
	$content = str_replace( '[li]', '[li icon="' . $icon . '"]' , $content );
	
	return '<ul class="vntd-list vntd-list-'.$color.' '.$background_class.' list-icon-'.$icon.' ' . $colored_class . '">'.crexis_do_shortcode($content).'</ul>';
}

function crexis_li($atts, $content=null) {	
	extract(shortcode_atts(
		array(
		"icon" => 'angle-right',
		), $atts)
	);
	
	return '<li><i class="fa fa-' . $icon . '"></i> '.$content.'</li>';
}

remove_shortcode('list');
remove_shortcode('li');
add_shortcode('list', 'crexis_list');
add_shortcode('li', 'crexis_li');