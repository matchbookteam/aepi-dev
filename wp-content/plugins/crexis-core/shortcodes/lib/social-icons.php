<?php

// Shortcode Processing

function crexis_social_icons($atts, $content = null) {
	$attss = extract(shortcode_atts(array(
		"style" => 'square',
		"facebook" => '',
		"twitter" => '',
		"googleplus" => '',
		"rss" => '',
		"linkedin" => '',
		"pinterest" => '',
		"email" => '',
		"dribbble" => '',
		"instagram" => '',
		"youtube" => '',
		"el_class" => '',
		"tumblr" => '',
		"css" => ''
	), $atts));
	
	$icons = '';
	$icon_arr = array('facebook','twitter','google_plus','tumblr','linkedin','vimeo','pinterest','instagram','dribbble','skype','flickr','dropbox','youtube','mail','dribbble','soundcloud','rss');
	
	$style_class = '';
	if($style == 'round') {
		$style_class = ' round';
	}
	foreach($icon_arr as $icon_name) {	
		if(array_key_exists($icon_name,$atts)) {		
			if( $icon_name == 'google_plus' ) {
				$icon_name = 'google-plus';	
			} elseif( $icon_name == 'email' ) {
				$icon_name = 'envelope';
			}
			$icons .= '<a href="'.$atts[$icon_name].'" class="social '.$icon_name.$style_class.'" target="_blank"><i class="fa fa-'.$icon_name.'"></i></a>';
		}
	}	
	
	$custom_css = '';
	
	if(function_exists('vc_shortcode_custom_css_class')) {
		$custom_css = vc_shortcode_custom_css_class( $css );
	}
			
	return '<div class="vntd-social-icons vntd-social-icons-'.$style . ' ' . $custom_css . '">'.$icons.'</div>';
}
remove_shortcode('social_icons');
add_shortcode('social_icons', 'crexis_social_icons');