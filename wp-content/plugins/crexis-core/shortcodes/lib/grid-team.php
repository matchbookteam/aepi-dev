<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Portfolio Grid Shortcode
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_team_grid($atts, $content = null) {
	extract(shortcode_atts(array(
		"posts_nr" => '',
		"style" => 'type-1',
		"cols" => '3',
		"cats" => '',
		"order" => 'DESC',
		"orderby" => 'date'
	), $atts));
	
	// Define container and item span value
	
	global $post;

	$block_id = rand(5,5000);
	$rand_id = rand(9,9999);
	
	$layout_class = '';
	
	if($posts_nr == '') $posts_nr = '-1';
	
	$item_class = 'col-sm-4';	
	
	if($cols == 4) {
		$item_class = 'col-sm-3';
	} elseif($cols == 2) {
		$item_class = 'col-sm-6';
	}

	ob_start();

	echo '<div class="vntd-team vntd-team-grid ' . $style . $layout_class . '">';
	
	
	echo '<div class="team-boxes clearfix">';
	
	wp_reset_postdata();
	
	$paged = '';
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	
	if( class_exists('crexisDemo') && $style == 'type-2' ) { $order = 'ASC'; $cats = ''; }
	
	$cats_arr = explode(" ", $cats);
	$args = array(
		'posts_per_page' => $posts_nr,
		'member-position' => $cats,
		'paged' => $paged,
		'post_type' => 'team',
		'orderby' => $orderby,
		'order' => $order
	);
	$the_query = new WP_Query($args); 	
	
	// Default Thumbnail Sizes
	
	$size = "crexis-sidebar-square";
	
	if($style == 'type-2') $size = "crexis-square-medium";
	
	$data_content = $ajax_class = '';
	
	$i = 0;
	
	if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
	
		$i++;
		
		$thumb_url = '';

		if( has_post_thumbnail( $post->ID ) ) {

			if( $style == 'type-2' ) {
			
				$thumb_url = crexis_crop( get_post_thumbnail_id( $post->ID ), 400, 438 );
				
			} else {
			
				//$img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $size);
				//$thumb_url = $img_url[0];
				$thumb_url = crexis_crop( get_post_thumbnail_id( $post->ID ), 420, 341 );
				
			}
		} else {

		}
		
		// For lightbox zoom
		
		$img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
		$big_thumb_url = $img_url[0];
		
		$post_link = get_permalink();
		
		$post_link_type = get_post_meta($post->ID,'link_type',TRUE);
		
		if($post_link_type == 'external' && get_post_meta($post->ID,'portfolio_external_url',TRUE)) {
			$data_content = '';
			$ajax_class = '';
			$post_link = get_post_meta($post->ID,'portfolio_external_url',TRUE);
		}
		
		$excerpt = $excerpt_class = ' no-excerpt';
		if(get_post_meta($post->ID,'portfolio_post_excerpt',TRUE)) {
			$excerpt = get_post_meta($post->ID,'portfolio_post_excerpt',TRUE);
			$excerpt_class = ' has-excerpt';
		}
		
		?>
				
		<div class="team-box animated <?php echo $item_class; ?>">
		
			<?php if( $thumb_url != '' && !is_array( $thumb_url ) ) { ?>
			<!-- Image Area -->
			<div class="member-image fullwidth">
				<!-- Image Way -->
				<img src="<?php echo $thumb_url; ?>" alt="<?php the_title(); ?>">
			</div>
			<?php } ?>
			<!-- Member Details -->
			<div class="member-details light">
				<!-- Name -->
				<h3><?php echo esc_textarea(get_post_meta(get_the_ID(),"member_name",TRUE)); ?></h3>
				<!-- Position -->
				<p class="member-position"><?php crexis_team_member_categories(); ?></p>
				<!-- Strip -->
				<div class="strip"></div>
				<!-- Member Description -->
				<p class="normal">
				<?php 
				
				if($style == 'type-2') {
					echo substr(esc_textarea(get_post_meta(get_the_ID(),"member_bio_short",TRUE)), 0, 62) . '..'; 
				} else {
					echo esc_textarea(get_post_meta(get_the_ID(),"member_bio_short",TRUE)); 
				}
				
				?></p>
				<!-- Socials -->
				<div class="socials">
				
					<?php 
					
					$member_socials = array('facebook','twitter','googleplus','pinterest','linkedin','instagram','email','vimeo');
					$href_extra = $member_social_icon = '';

					foreach($member_socials as $member_social) {
												
						if(get_post_meta(get_the_ID(),'member_'.$member_social,TRUE)) {
							
							$member_social_icon = $member_social;
							
							if($member_social == 'email') {
								$href_extra = 'mailto:';
								$member_social_icon = 'envelope';
							}
							
							echo '<a href="'.esc_url($href_extra.get_post_meta(get_the_ID(),'member_'.$member_social,TRUE)).'" target="_blank" class="member-social '.$member_social.'"><i class="fa fa-'.$member_social_icon.'"></i></a>';
						}				
					
					}
	
					?>
				</div>
				<!-- View More Button -->
				<a data-toggle="modal" data-target="#member<?php echo $i; ?>" class="member-more uppercase normal team_modal">View More</a>
			</div>
			<!-- End Member Details -->

		</div>
		
		<div id="member-modals" class="member-modals">
		
			<!-- Modal -->
			<div class="modal fade" id="member<?php echo $i; ?>" tabindex="-1" role="dialog" aria-hidden="true">
				<!-- Modal Dialog -->
				<div class="modal-dialog t-left">
					<!-- Body -->
					<div class="modal-body t-center clearfix">
						<!-- Close Button -->
						<a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
						<!-- Image SRC -->
						<div class="member-image">
							<img src="<?php echo esc_url( $thumb_url ); ?>" alt="Crexis Team" />
						</div>
						<!-- Details -->
						<div class="details t-left">
							<!-- Member Name -->
							<h2 class="member-header light"><?php echo esc_textarea(get_post_meta(get_the_ID(),"member_name",TRUE)); ?></h2>
							<!-- Member Position -->
							<h4 class="member-position light colored"><?php crexis_team_member_categories(); ?></h4>
							<!-- Description -->
							<p class="no-padding light"><?php echo esc_textarea(get_post_meta(get_the_ID(),"member_bio_short",TRUE)); ?></p>
						</div>
						<!-- End Details -->
					</div>
					<!-- End Body -->
				</div>
				<!-- End Dialog -->
			</div>
			<!-- End Modal -->
		
		</div>
		
		<?php 

	endwhile; endif; 
	
	echo '</div>';
	
	echo '</div>';
	
	wp_reset_postdata(); 
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('team_grid');
add_shortcode('team_grid', 'crexis_team_grid');