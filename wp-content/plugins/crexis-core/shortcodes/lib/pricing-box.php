<?php

// Shortcode Processing

function crexis_pricing_box($atts, $content) {
	extract(shortcode_atts(array(
		"featured" 		=> 'no',
		"title" 		=> 'Standard',
		"features" 		=> '',
		"button_label" 	=> 'Buy Now',
		"period" 		=> 'Month',
		"price" 		=> '$99',
		"button_url" 	=> '#',
		"animated" 		=> 'yes',
		"animation_delay" => '100'
	), $atts));
	
	$animated_class = $animation_data = $featured_class = $info_class = $color_class = $color_style = $color_bg_style = $button_color_style = $button_color_class = $color_style_h3 = '';
	
	if($animated != 'no') {
		$animated_class = Crexis_Core::get_animated_class();
		$animated_data = ' data-animation="fadeIn" data-animation-delay="'.$animation_delay.'"';
	}
	
	if($featured == 'yes') $featured_class = " active";;
	
	$output = '<div class="pricing-box box' . $animated_class . $featured_class . '"' . $animated_data . '>';
	
	$output .= '<h2 class="head georgia">' . esc_textarea($title) . '</h2>';
	
	$output .= '<h1 class="price georgia colored">' . esc_textarea($price) . '<span class="raleway">/' . esc_textarea($period) . '</span></h1>';
	
	$output .= '<ul class="price_list light">';
	
	// features loop
	
	$features_arr = explode( ',', $features );	
	
	foreach( $features_arr as $single_feature ) {
		if ( strpos( $single_feature, '(/icon)' ) !== false ) {
			$split = explode( '(/icon)', $single_feature, 2 );
			$icon = str_replace( '(icon)' , '', $split[0] );
			$single_feature = '<i class="fa ' . esc_html( $icon ) . '"></i>' . esc_html( $split[1] );  
		}
		$output .= '<li>' . $single_feature . '</li>';
	}
	
	// end loop
	
	$output .= '</ul>';
	
	// button
	
	if($button_label) {
	
		$output .= '<a href="' . esc_url( $button_url ) . '" class="price_button uppercase">' . esc_textarea( $button_label ) . '</a>';
	
	}
	
	$output .= '</div>';
	
	return $output;
	
}
remove_shortcode('pricing_box');
add_shortcode('pricing_box', 'crexis_pricing_box');  