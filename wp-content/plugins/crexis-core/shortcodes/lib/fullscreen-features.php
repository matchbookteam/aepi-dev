<?php

// Blog Posts Carousel

function vntd_fullscreen_features($atts, $content = null) {
	extract(shortcode_atts(array(
		"boxes" 	=> '',
		"bg_image"		=> '',
		"heading"	=> 'We create (rotate)awesome,stunning(/rotate) themes!',
		"heading_secondary"	=> 'Super Responsive, clean and creative theme - (b)Crexis!(/b)',
		"scroll_button" => 'yes',
		"scroll_url" 	=> '#second',
		"scroll_label" 	=> 'Learn More',
	), $atts));
	
	wp_enqueue_script('owl-carousel', '', '', '', true);
	wp_enqueue_style('owl-carousel');
	
	$random_id = rand(1,9999);
	
	$bg_img = wp_get_attachment_image_src($bg_image, 'crexis-bg-image');
	$bg_img = $bg_img[0];
	
	ob_start();
   
    ?>
    
    <div class="vntd-fullscreen-features">
    
    	<div class="absolute page_bg fullscreen cover" style="background-image:url(<?php echo $bg_img; ?>)"></div>
    
		<!-- Intro -->
		<div class="intro relative white t-center">
			<!-- Header -->
			<h1 class="normal uppercase big text-rotator">
				
				<?php
				
				$heading = esc_html($heading);
				$heading = str_replace("(rotate)",'<span class="rotate">', $heading);
				$heading = htmlspecialchars_decode(str_replace("(/rotate)",'</span>', $heading));
				
				echo $heading;
				
				?>

			</h1>
			<a class="arrow jump">
				<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-bottom.png" alt="arrow down" />
			</a>
			<!-- Boxes  -->
			<div class="feature-boxes type-5 boxed box-carousel-dragable three-items">
			
				<?php
				
				$values = (array) vc_param_group_parse_atts( $boxes );
				
				foreach ( $values as $data ) {
					$new_line = $data;
					
					$new_line['box_heading'] = isset( $data['box_heading'] ) ? $data['box_heading'] : '';
					$new_line['box_content'] = isset( $data['box_content'] ) ? $data['box_content'] : '';
					$new_line['icon_fontawesome'] = isset( $data['icon_fontawesome'] ) ? $data['icon_fontawesome'] : '';
					
					echo '<div class="feature-box clearfix">';
					echo '<div class="f-left feature-icon"><i class="' . esc_attr( $new_line['icon_fontawesome'] ) . '"></i></div>';
					echo '<div class="feature-text f-left t-left white">';
					echo '<h4 class="no-margin thin no-padding">' . esc_html( $new_line['box_heading'] ) . '</h4>';
					echo '<p>' . esc_html( $new_line['box_content'] ) . '</p>';
					echo '</div></div>';
				}
				
				?>

			</div>
			<!-- End Boxes -->

			<h4 class="light antialiased">
				<?php 
				
				$heading_secondary = esc_html($heading_secondary);
				$heading_secondary = str_replace("(b)",'<span class="colored">', $heading_secondary);
				$heading_secondary = htmlspecialchars_decode(str_replace("(/b)",'</span>', $heading_secondary));
				
				echo $heading_secondary;
				
				?>

			</h4>

		</div>
		<!-- End Intro -->

		<!-- Bottom Note -->
		<div class="page_bottom_note t-center">
			<!-- Bottom Note Link -->
			<a href="<?php echo esc_url( $scroll_url ); ?>" class="bottom_note uppercase normal antialiased">
				<!-- Mouse Icon -->
				<?php 
				
				echo '<img src="'. get_template_directory_uri() . '/img/mouse-icon.png" alt="mouse_icon">';
				echo esc_html( $scroll_label );
				
				?>
			</a>
		</div>
		
	</div>
    
    <?php

	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('vntd_fullscreen_features');
add_shortcode('vntd_fullscreen_features', 'vntd_fullscreen_features');