<?php

// Shortcode Processing

function crexis_portfolio_details($atts, $content) {
	extract(shortcode_atts(array(
		"title_type" => 'post_title',
		"title" => '',
		"skills" => 'Design,Photography,HTML,jQuery',
		"categories" => 'yes',
		"budget" => '$800 - $1200',
		"likes" => 'yes',
		"date" => 'no',
		"client_website" => 'www.clientsite.com',
		"client" => 'Client Name',
		"button1_label" => 'Live Preview',
		"button1_url" => '#',
		"button2_label" => '',
		"button2_url" => '',
		"style" => 'default2'
	), $atts));
	
	global $post;
	
	$theTitle = get_the_title($post->ID);
	
	if($title_type == 'custom' && $title) $theTitle = $title;
	
	$output = '<div class="vntd-portfolio-details portfolio-details-'.$style.'">';
	
	if($style == 'default2') {
		$output .= '<div class="row"><div class="col-md-8"><h2 class="project-head">'.$theTitle.'</h2><p class="project-desc">'.$content.'</p>';
		
		if($button1_label & $button1_url) {
			$output .= '<a href="'.$button1_url.'" target="_blank" class="btn btn-accent project-button medium-btn">'.$button1_label.'</a>';
		}
		
		if($button2_label & $button2_url) {
			$output .= '<a href="'.$button2_url.'" class="btn btn-green vntd-accent-bgcolor-hover project-button project-button-secondary">'.$button2_label.'</a>';
		}
		
		$output .= '</div><div class="col-md-4 portfolio-details-side">';
	} elseif($style == 'default1') {	
		$output .= '<h2 class="project-head">'.$theTitle.'</h2>';
		$output .= '<p class="project-desc">'.$content.'</p>';
		
	} else { // Minimal Style
		$output .= '<h2 class="project-head">'.$theTitle.'</h2>';
		
		if($categories != 'no') {
			$output .= '<div class="project-head-categories">';
			$terms = wp_get_object_terms($post->ID, "project-type");
			foreach ( $terms as $term ) {
				$output .= $term->name;
				if(end($terms) !== $term){
					$output .= ", ";
				}
			}
			$output .= '</div>';
		}
		
		$output .= '<p class="project-desc">'.$content.'</p>';

	}

	if($style != 'minimal') {
		$output .= '<ul class="project-features">';
		
		// Client Name
		
		if($client) {
			$output .= '<li class="project-feature"><h6>'.__('Client','crexis').': </h6><p class="normal">'.$client.'</p></li>';
		}
		
		// Budget
		
		if($budget != '') {
			$output .= '<li class="project-feature project-feature-budget"><h6>'.__('Budget','crexis').': </h6><p class="normal">' . $budget . '</p></li>';
		}
		
		// Likes
		
		if($likes != 'no') {
			$output .= '<li class="project-feature project-feature-likes"><h6>'.__('Likes','crexis').': </h6><p class="normal">'.getPostLikeLink( $post->ID ).'</p></li>';
		}
		
		// Client Website
		
		if($client_website) {
			$output .= '<li class="project-feature project-feature-website"><h6>'.__('Live Demo','crexis').': </h6><p class="normal"><a href="http://'.$client_website.'" target="_blank" title="'.$theTitle.'" alt="'.$theTitle.'">'.$client_website.'</a></p></li>';
		}
		
		// Date
		
		if($date != 'no') {
			$output .= '<li class="project-feature project-feature-date"><h6>'.__('Date','crexis').': </h6><p class="normal">'.get_the_time('F d, Y').'</p></li>';
		}
		
		// Skills
		
		if($skills) {
			$output .= '<li class="project-feature"><h6>'.__('Skills','crexis').': </h6><ul class="project-feature-skills">';
			
			$skills = explode(',',$skills);
			
			foreach($skills as $skill) {
				$output .= '<li class="project-skill">'.$skill.'</li>';
			}
			
			$output .= '</ul></li>';
		}
		
		
		
		$output .= '</ul>';
	
	} // end if style == minimal

	
	
	if($style != 'default2') {
	
		if($button1_label & $button1_url) {
			$output .= '<a href="'.$button1_url.'" target="_blank" class="btn btn-accent project-button medium-btn btn-hover-accent2">'.$button1_label.'</a>';
		}
		
		if($button2_label & $button2_url) {
			$output .= '<a href="'.$button2_url.'" class="btn btn-green vntd-accent-bgcolor-hover project-button project-button-secondary medium-btn btn-hover-accent2">'.$button2_label.'</a>';
		}
	
	}
	
	$output .= '</div>';
	
	if($style == 'default2') {
		$output .= '</div></div>';
	}
	
	return $output;
	
}
remove_shortcode('portfolio_details');
add_shortcode('portfolio_details', 'crexis_portfolio_details');  