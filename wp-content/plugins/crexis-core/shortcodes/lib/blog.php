<?php

// Blog Shortcode


function vntd_blog($atts, $content = null) {
	extract(shortcode_atts(array(
		"cats" => '',
		"blog_style" => 'classic',
		"grid_cols" => '3',
		"grid_masonry" => 'yes',
		"ajax" => 'no',
		"posts_nr" => '',
		"el_position" => '',
		"width" => '',
	), $atts));
	
	// Get Blog Style

	$blog_grid_style = '';
	
	$grid_classes = '';
	$wide = false;
	
	if($blog_style == "grid") {
		
		$grid_classes = ' masonry-blog bl-' . $grid_cols . '-col';
		
		if(crexis_option("blog_fullwidth") == true) $wide = true;
	
	}
	
	global $post;
	
	$layout = get_post_meta($post->ID, 'page_layout', true);
	if($layout != "fullwidth") $sidebar_class = "page-sidebar";

	// Define grid item size
	
	ob_start();
	
	echo '<div class="blog blog-index blog-style-'.esc_attr($blog_style) . $grid_classes . '">';
	
	// The Loop
	
	wp_reset_query(); 
	
	global $more; $more = 0; // Reset the More Tage
	
	echo '<div class="blog-inner blog-style-' . esc_attr($blog_style) . ' blog-items">';
	
	
	wp_reset_postdata();
	
	$paged = crexis_query_pagination();

	$args = array(
		'posts_per_page' => $posts_nr,
		'cat'		=> $cats,
		'orderby'	=> 'slug',
		'paged' => $paged
	);	
	
	$the_query = new WP_Query($args); 	
	
	if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
		
		crexis_blog_post_content($layout, $blog_style);
		
	endwhile; endif; 
	
	// Loop END
	
	echo '</div>';
		
	crexis_pagination($the_query); // Pagination
	
	if($ajax == 'yes' && $the_query->max_num_pages > 1) {
	
		crexis_ajax_pagination($the_query,"blog");
		
		echo '<div id="ajax-load-posts" class="pagination-wrap"><a href="#" class="ajax-load-more-text">' . esc_html__('Load More Posts','crexis') . '</a></div>';
		
	}
	
	wp_reset_query(); 
	
	echo '</div>';
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('vntd_blog');
add_shortcode('vntd_blog', 'vntd_blog');