<?php

// Blog Posts Carousel

function vntd_boxes_carousel($atts, $content = null) {
	extract(shortcode_atts(array(
		"boxes" 	=> '',
		"bg_image"		=> '',
	), $atts));
	
	wp_enqueue_script('owl-carousel', '', '', '', true);
	wp_enqueue_style('owl-carousel');
	
	$random_id = rand(1,9999);
	
	ob_start();
   
    ?>
    
    <div class="vntd-boxes-carousel categories_full_screen">
    
		<!-- Boxes  -->
		<div class="boxes fullscreen no-padding t-center box-carousel-dragable four-items bg-image-<?php echo esc_attr( $bg_image ); ?>">
		
			<?php
			
			$values = (array) vc_param_group_parse_atts( $boxes );
			
			foreach ( $values as $data ) {
				$new_line = $data;
				
				$new_line['bg_image'] = isset( $data['bg_image'] ) ? $data['bg_image'] : '';
				$new_line['box_heading_top'] = isset( $data['box_heading_top'] ) ? $data['box_heading_top'] : '';
				$new_line['box_heading'] = isset( $data['box_heading'] ) ? $data['box_heading'] : '';
				$new_line['box_content'] = isset( $data['box_content'] ) ? $data['box_content'] : '';
				$new_line['box_url'] = isset( $data['box_url'] ) ? $data['box_url'] : '';
				$new_line['box_url_label'] = isset( $data['box_url_label'] ) ? $data['box_url_label'] : '';
				
				$img_url = wp_get_attachment_image_src($new_line['bg_image'], 'crexis-bg-image');
				
				echo '<div class="box">';
				
				echo '<div class="category-image translated_image" data-image-position="center">';
				
				if($bg_image != 'no') {
					echo '<img src="' . $img_url[0] . '" alt="' . esc_html( $new_line['box_heading'] ) . '">';
				}
				
				echo '</div>';
				
				echo '<div class="texts t-left">';
				echo '<h1 class="category-number uppercase antialiased">' . esc_html( $new_line['box_heading_top'] ) . '</h1>';
				echo '<h2 class="category-header uppercase antialiased">' . esc_html( $new_line['box_heading'] ) . '</h2>';
				echo '<p class="category-detail">' . esc_html( $new_line['box_content'] ) . '</p>';
				if($new_line['box_url']) echo '<a href="' . esc_url( $new_line['box_url'] ) . '" class="read_more ex-link" data-text="' . esc_html( $new_line['box_url_label'] ) . '"><i class="fa fa-arrow-right"></i></a>';
				echo '</div></div>';
			}
			
			?>

		</div>
		<!-- End Boxes  -->
	</div>
    
    <?php

	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('vntd_boxes_carousel');
add_shortcode('vntd_boxes_carousel', 'vntd_boxes_carousel');