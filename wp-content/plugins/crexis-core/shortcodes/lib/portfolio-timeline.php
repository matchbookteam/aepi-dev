<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Portfolio Timeline Shortcode
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function vntd_portfolio_timeline($atts, $content = null) {
	extract(shortcode_atts(array(
		"posts_nr" => '8',
		"cats" => '',
		"cols" => '4',
	), $atts));
	
	wp_enqueue_script('magnific-popup');
	wp_enqueue_style('magnific-popup');
	
	wp_enqueue_script('crexis-timeline');
	wp_enqueue_style('crexis-timeline');
	
	ob_start();
	
	?>

	<section id="timeline" class="container background42 parallax7 light-bg">
	
	<div class="timelineFlat timeline tl3  animated" data-animation="fadeIn" data-animation-delay="400" data-first="19/01/2016">
	
	<?php

	wp_reset_query();
	
	$orderby = 'date';
	$order = 'ASC';
	$post_in = '';
	
	if( class_exists('crexisDemo') ) {
		$post_in = crexisDemo::crexisTimelinePosts();
		$orderby = 'post__in';
		$posts_nr = 11;
	}
	
	$cats_arr = explode(" ", $cats);
	$args = array(
		'posts_per_page' => $posts_nr,
		'project-type'		=> $cats,
		'post_type' => 'portfolio',
		'orderby' => $orderby,
		'order' => $order,
		'post__in' => $post_in
	);
	$the_query = new WP_Query($args); 	
	
	// Default Thumbnail Sizes
	
	$size = "crexis-portfolio-square";	
	
	$temp_day = 0;
	
	if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
		
		$img_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), $size);
		$thumb_url = $img_url[0];
		
		// For lightbox zoom
		
		$img_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large');
		$big_thumb_url = $img_url[0];
		
		$temp_day = $temp_day + 2;
		
		?>
		
		<div class="item" data-id="<?php the_time('d/m/Y'); ?>" data-description="<?php the_title(); ?>">
			<!-- Item Image -->
			<a data-description="ZOOM IN" href="<?php echo get_permalink(); ?>">
				<img src="<?php echo $thumb_url; ?>" alt="<?php the_title(); ?>">
			</a>
			<!-- Timeline Details -->
			<div class="timeline-item-details clearfix">
				<!-- Left Area -->
				<div class="left_details f-left">
					<!-- Header -->
					<h3><?php the_title(); ?></h3>
					<!-- Date -->
					<p><?php the_time('Y, d F'); ?></p>
				</div>
				<!-- Right Area -->
				<div class="right-buttons f-right">
					<!-- Detail Page Link -->
					<a href="<?php echo get_permalink(); ?>" class="detail_page ex-link">
						<i class="fa fa-plus"></i>
					</a>
					<!-- Read More -->
					<a class="read_more" data-id="<?php the_time('d/m/Y'); ?>">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
			</div>
			<!-- End Details -->
		</div>
		
		<div class="item_open" data-id="<?php the_time('d/m/Y'); ?>">
			<div class="item_open_content">
				<div class="timeline_open_content">
				
					<!-- Header -->
					<h2 class="no-marg-top timeline_content_header"><?php the_title(); ?></h2>
					
					<?php 
					
					if( get_post_meta( get_the_ID(), 'portfolio_post_excerpt', true) ) {
					
						echo get_post_meta( get_the_ID(), 'portfolio_post_excerpt', true);
					
					} else {
					
						echo crexis_excerpt( 90, false);
						
					} 
					
					?>
			
					<a href="<?php echo get_permalink(); ?>" class="item_link colored">
						<?php esc_html_e('See More Details', 'crexis'); ?> <i class="fa fa-long-arrow-right"></i>
					</a>
			
				</div>
			</div>
		</div>
		
		<?php 


	endwhile; endif; 
	
	wp_reset_postdata(); 
	
	?>
	
	</div>
	
	</section>
	
	<?php
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('vntd_portfolio_timeline');
add_shortcode('vntd_portfolio_timeline', 'vntd_portfolio_timeline');