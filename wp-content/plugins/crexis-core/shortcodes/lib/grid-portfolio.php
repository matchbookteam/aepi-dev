<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Portfolio Grid Shortcode
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_portfolio_grid($atts, $content = null) {
	extract(shortcode_atts(array(
		"filter" => 'yes',
		"filter_style" => 'simple',
		"filter_counter" => 'yes',
		"animation" => 'quicksand',
		"posts_nr" => '',
		"cats" => '',
		"thumb_style" => 'classic',
		"order" => '',
		"el_position" => '',
		"paged_posts" => '',
		"thumb_space" => 'yes',
		"cols" => '4',
		"masonry" => 'no',
		"ajax" => 'no',
		"love" => 'yes',
		"pagination" => 'no',
		"order" => 'DESC',
		"orderby" => 'date',
	), $atts));
	

	wp_enqueue_script('cubePortfolio');
	wp_enqueue_script('cubeConfig');
	wp_enqueue_style('cubePortfolio');
	
	global $post;

	if(!$posts_nr) $posts_nr = "-1";
	$block_id = rand(5,5000);
	
	$layout_class = $post_in = '';
	
	$portfolio_style = 'type2';
	$item_class = ' cbp-caption-zoom';
	
	if($thumb_style == 'minimal') {
		$portfolio_style = 'type1';
		$item_class .= ' cbp-caption-active';
	}
	
	if( class_exists('crexisDemo') ) {
		$post_in = crexisDemo::crexisGridPosts($thumb_style, $masonry);
		$orderby = 'post__in';
	}
	
	$thumb_gap = 10;
	if($thumb_space == "no") $thumb_gap = 0;
	
	$rand_id = rand(1,9999);
	ob_start();

	echo '<div class="portfolio vntd-portfolio-grid ' . $layout_class . ' portfolio-'.$thumb_style.' portfolio-cols-'.$cols.' portfolio-counter-' . $filter_counter . ' portfolio-love-' . $love . '">';
	
		if($filter == "yes") crexis_filters('project-type',$cats,$filter_style,$rand_id,$filter_counter);
		
		echo '<div id="portfolio-'.$rand_id.'" class="portfolio-items ' . $portfolio_style . $item_class . ' grid-items" data-cols="'.$cols.'" data-animation="'.$animation.'" data-gap="'.$thumb_gap.'" data-filters="portfolio-filters-'.$rand_id.'">';
		
		wp_reset_query();
		
		$paged = crexis_query_pagination();
		
		$cats_arr = explode(" ", $cats);
		$args = array(
			'posts_per_page' => $posts_nr,
			'project-type'		=> $cats,
			'paged' => $paged,
			'post_type' => 'portfolio',
			'order' => $order,
			'orderby' => $orderby,
			'post__in' => $post_in,
		);
		$the_query = new WP_Query($args); 	
		
		// Default Thumbnail Sizes
		
		$size = "crexis-portfolio-square";	
		if($masonry == "yes") $size = "crexis-portfolio-auto";
		
		$data_content = $ajax_class = '';
		
		if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
			
			$img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $size);
			$thumb_url = $img_url[0];
			
			// For lightbox zoom
			
			$img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
			$big_thumb_url = $img_url[0];
			
			$post_link = get_permalink();
			
			$post_link_type = get_post_meta($post->ID,'link_type',TRUE);
			
			if($post_link_type == 'external' && get_post_meta($post->ID,'portfolio_external_url',TRUE)) {
				$data_content = '';
				$ajax_class = '';
				$post_link = get_post_meta($post->ID,'portfolio_external_url',TRUE);
			}
			
			$excerpt = $excerpt_class = ' no-excerpt';
			if(get_post_meta($post->ID,'portfolio_post_excerpt',TRUE)) {
				$excerpt = get_post_meta($post->ID,'portfolio_post_excerpt',TRUE);
				$excerpt_class = ' has-excerpt';
			}

			?>
						
			<div class="item cbp-item <?php echo crexis_portfolio_item_class(); ?>"<?php echo $data_content; ?>>
			
				<a href="<?php echo esc_url($post_link); ?>" class="cbp-caption ex-link">
				
	            	<!-- Item Image -->
	                <div class="cbp-caption-defaultWrap">
	                	<!-- Image Src -->
	                    <img src="<?php echo esc_url($thumb_url); ?>" alt="<?php the_title(); ?>">

						<?php if($thumb_style != 'minimal') { ?>
						
	                    <!-- Item Note -->
		                <div class="item_icon">
		                 	<!-- Icon -->
		                 	<p><i class="fa <?php echo crexis_get_category_icon(); ?>"></i></p>
	                    	<p><?php crexis_portfolio_overlay_categories(); ?></p>
	                    </div>
	                    <!-- End Item Note -->
	                     
	                    <?php } ?>
	                </div>
	                <!-- End Item Image -->

	                <!-- Item Details -->
	                <div class="cbp-caption-activeWrap">
	                	<!-- Centered Details -->
	                    <div class="center-details">
	                        <div class="details">
	                        	<!-- Item Name -->
	                            <h2 class="name">
	                            	<?php the_title(); ?>
	                            </h2>
	                            <!-- Tags -->
	                            <p class="tags">
	                            	<?php crexis_portfolio_overlay_categories(); ?>
	                            </p>
	                        </div>
	                    </div>
	                    <!-- End Center Details Div -->
	                </div>
	                <!-- End Item Details -->
	            </a>	
				
			</div>
			
			<?php 
	
	
		endwhile; endif; 
		
		echo '</div>';
		
		if($ajax == 'yes' && $posts_nr > 1) {
		
			crexis_ajax_pagination($the_query,"portfolio");
			
			$button_class = 'circle-button georgia t-center light-type antialiased normal';
			$load_more_content = '<i class="fa fa-plus"></i>';	
				
			if($thumb_style == 'minimal') {
				$button_class = 'semibold t-center light-type antialiased minimal-button';
				$load_more_content = esc_html__('Load More','crexis');
			}
			
			echo '<div id="portfolio-load-posts" class="pagination-wrap"><a href="#" class="load-more-button cbp-l-loadMore-link ' . esc_attr( $button_class ) . '">' . $load_more_content . '</a></div>';
			
			
		} elseif( $pagination == 'yes' ) {
			crexis_pagination( $the_query );
		}

		
		echo '</div>';
		
		wp_reset_postdata(); 
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('portfolio_grid');
add_shortcode('portfolio_grid', 'crexis_portfolio_grid');