<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Contact Block
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_contact_form($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"animated" => '',
		"color_scheme" => ''
	), $atts));
	
	//If the form is submitted	
	
	$output = '<div class="vntd-contact-form contact-form-'.$color_scheme.' contact font-primary">';
	$output .= do_shortcode('[contact-form-7 id="'.$id.'"]');
	$output .= '</div>';
	
	return $output;
	
}
remove_shortcode('crexis_contact_form');
add_shortcode('crexis_contact_form', 'crexis_contact_form');