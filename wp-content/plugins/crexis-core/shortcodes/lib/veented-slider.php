<?php

// Blog Posts Carousel

function crexis_veented_slider($atts, $content = null) {
	extract(shortcode_atts(array(
		"style" => 'centered',
		"height" => 'fullscreen',
		"height_custom" => '',
		"offset" => '0px',
		"autoplay" => '7000',
		"scroll_button" => 'no',
		"scroll_button_label" => 'View More',
		"slider_effect" => 'fade',
		"slides" => '',
		"animated" => 'yes',
		"button_color" => 'dark',
		"heading_size" => 'default',
		"heading_size_custom" => '72px',
		"heading_top_size" => 'default',
		"heading_top_size_custom" => '18px',
		"text_size" => 'default',
		"text_size_custom" => '15px',
		"arrow_nav" => 'yes',
		"bullet_nav" => 'yes'
	), $atts));
	
	wp_enqueue_script('swiper', '', '', '', true);
	wp_enqueue_style('swiperCSS');	
	wp_enqueue_script('vntd-skrollr', '', '', '', true);
	
	$fullscreen = 0;
	$fullscreen_class = '';
	if($height != 'custom') $fullscreen_class = ' veented-slider-fullscreen';
	
	$random_id = rand(1,99999);

	$scroll_class = '';
	if($scroll_button == 'yes') {
		$scroll_class = ' veented-slider-with-scroll';
	}

	ob_start();
	
	?>
    
    <div id="veented-slider-<?php echo $random_id; ?>" class="veented-slider-holder<?php echo $fullscreen_class . $scroll_class; ?> veented-slider-<?php echo esc_attr( $style ); ?> slider-arrow-<?php echo esc_attr($arrow_nav); ?> slider-bullet-<?php echo esc_attr($bullet_nav); ?>" data-slider-autoplay="<?php echo esc_attr($autoplay); ?>" data-slider-effect="<?php echo esc_attr($slider_effect); ?>">
	
		<div class="veented-slider-loader">
			<div class="spinner">
			  <div class="dot1"></div>
			  <div class="dot2"></div>
			</div>
		</div>
		
		<div class="veented-slider swiper-containers">
		
			<div class="veented-slider-inner swiper-wrapper">
			
				<?php
				
				$values = (array) vc_param_group_parse_atts( $slides );
				
				$i = 0;
				
				$animated_class = ' animated animatedSlider';
				
				if($animated == 'no') {
					$animated_class = '';
				}
				
				foreach ( $values as $data ) {
				
					$i++;
					$new_line = $data;
					
					$new_line['image'] = isset( $data['image'] ) ? $data['image'] : '';
					$new_line['heading_top'] = isset( $data['heading_top'] ) ? $data['heading_top'] : '';
					$new_line['heading'] = isset( $data['heading'] ) ? $data['heading'] : '';
					$new_line['text'] = isset( $data['text'] ) ? $data['text'] : '';
					$new_line['button_label'] = isset( $data['button_label'] ) ? $data['button_label'] : '';
					$new_line['button_action'] = isset( $data['button_action'] ) ? $data['button_action'] : '';
					$new_line['button_url'] = isset( $data['button_url'] ) ? $data['button_url'] : '';
					$new_line['button_url_target'] = isset( $data['button_url_target'] ) ? $data['button_url_target'] : '';
					
					$img_url = wp_get_attachment_image_src($new_line['image'], 'crexis-bg-image');
				
				?>
				
				<div class="swiper-slide veented-slide swiper-lazy veented-slide-<?php echo $i; ?>" style="background-image:url(<?php echo esc_url($img_url[0]); ?>);" data-background="<?php echo esc_url($img_url[0]); ?>">
				
					<div class="inner">
						<div class="veented-slide-inner skrollable skrollable-between" data-0="opacity:1;margin-top:0px;" data-600="opacity:0;margin-top:220px;">
							<?php
							
							$h3_style = $h2_style = $p_style = '';
							
							if($new_line['heading_top'] != '') {
							
								if($heading_top_size == 'custom' && $heading_top_size_custom) {
									$h3_style = ' style="font-size: ' . str_replace('px','',$heading_top_size_custom) . 'px;"';
								}
								
								?>
								<h3 class="veented-slide-secondary-heading<?php echo $animated_class; ?>"<?php if($h3_style!= '') echo $h3_style; ?>><?php echo esc_html( $new_line['heading_top'] ); ?></h3>
								<?php
							}
							
							if($heading_size == 'custom' && $heading_size_custom) {
								$h2_style = ' style="font-size: ' . str_replace('px','',$heading_size_custom) . 'px;"';
							}
							
							if($text_size == 'custom' && $text_size_custom) {
								$p_style = ' style="font-size: ' . str_replace('px','',$text_size_custom) . 'px;"';
							}
							
							//if(get_post_meta(get_the_ID(),"heading_size",TRUE) != 56) $heading_style = 'style="font-size:'.esc_attr(get_post_meta(get_the_ID(),"heading_size",TRUE)).'px;"';
							
							?>
							<h2 class="veented-slide-heading<?php echo $animated_class; ?>"<?php if($h2_style!= '') echo $h2_style; ?>> <?php echo esc_html( $new_line['heading'] ); ?></h2>
							<?php if($new_line['text']) { ?>
							<p class="veented-slide-paragraph<?php echo $animated_class; ?>"<?php if($p_style!= '') echo $p_style; ?>><?php echo esc_html( $new_line['text'] ); ?></p>
							<?php }
							
							if($new_line['button_label']) { 
							
								echo '<div class="veented-slide-buttons'.$animated_class.'">';	
								
								$button_class = '';
								$button_href = esc_url( $new_line['button_url'] );
								
								if($new_line['button_action'] == 'scroll') {
									$button_class = ' scroll-after-slider';
									$button_href = '';
								}
								
								$button_color_class = 'dark-button';
								if($button_color == 'white') $button_color_class = 'white-button';

								echo '<a class="home-button ' . $button_color_class . ' thin veented-slide-button1' . $button_class . '" href="' . $button_href . '">' . esc_html( $new_line['button_label'] ) . '</a>';	
								
								echo '</div>';
							} 
							
							?>
						</div>
						
					</div>
				
				</div>
									
				<?php	
					 
				}
				

				if($height_custom > 0 && $height == 'custom' || $offset != 0) {
					$height_custom = str_replace('px','',$height_custom);
					echo '<style type="text/css">';
					if($height_custom > 0 && $height == 'custom') {
						echo '#veented-slider-'.$random_id.' { height: '.esc_attr($height_custom).'px; }';	
					}
					if($offset != 0) {
						echo '#veented-slider-'.$random_id.' .inner { padding-top: '.esc_attr($offset).'px; }';
					}
					echo '</style>';			
				}			
				
				?>    
		
			</div>
		
			<!-- Slider Pagination -->
		
			<div class="veented-slider-pagination swiper-pagination"></div>
			
			<!-- Slider Arrows -->
			
			<div class="swiper-button-next"><i class="fa fa-angle-right"></i></div>
			<div class="swiper-button-prev"><i class="fa fa-angle-left"></i></div>

			<?php

			if($scroll_button == 'yes') {
				echo '<div class="veented-slider-scroll-button-holder"><a href="#second" class="scroll veented-slider-scroll-button"><span class="vntd-mouse-dot"></span></a></div>';
			}

			?>
		
		</div>
	
	</div>
	
	<?php
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('veented_slider');
add_shortcode('veented_slider', 'crexis_veented_slider');