<?php

// Counter Shortcode Processing

function crexis_counter($atts, $content=null){
	
	extract(shortcode_atts(array(
		"title" => 'Days',
		"number" => '100',
		"color" => 'light',
		"color_custom" => '',
		"style" => 'default'
	), $atts));
	
	// End Icon related
	
	$rand_id = rand(1,1000);
	
	$extra_style = '';
	
	$return = '<div id="counter-'.$rand_id.'" class="vntd-counter counter-color-'.$color.' counter-style-'.$style.'" data-perc="'.$number.'">';
	
	$return .= '<div class="counter-value georgia"><div class="counter-number"'.$extra_style.'>0</div></div>';
	
	$return .= '<div class="counter-title"><h6'.$extra_style.'>'.$title.'</h6></div>';
	
	$return .= '</div>';
	
	if($color == 'custom' && $color_custom) {
		$return .= '<style type="text/css">.counter-color-dark,
		#counter-'.$rand_id.' h6,
		#counter-'.$rand_id.' .counter-number,
		#counter-'.$rand_id.' .counter-icon {
			color: '.$color_custom.' !important;
		}
		
		#counter-'.$rand_id.' .counter-number:after,
		#counter-'.$rand_id.' .counter-number:before,
		#counter-'.$rand_id.' .counter-value {
			border-color: '.$color_custom.' !important;
		}
	}</style>';
	}
	
	return $return;
}
remove_shortcode('counter');
add_shortcode('counter', 'crexis_counter');