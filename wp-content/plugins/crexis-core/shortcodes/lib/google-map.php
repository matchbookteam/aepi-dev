<?php

// Google Map Shortcode


function crexis_gmap($atts, $content = null) {
	extract(shortcode_atts(array(
		"height" => '400',
		"zoom" 	=> '15',		
		"label" => '',
		"fullscreen" => 'no',	
		"lat" 	=> '41.862274',
		"long" 	=> '-87.701328',
		"map_style" => 'grayscale',
		"marker1_title"	=> 'Office 1',
		"marker1_text"	=> 'Your description goes here.',
		"marker1_location"	=> 'center',
		"marker1_location_custom"	=> '41.863774,-87.721328',
		"marker2_title"	=> '',
		"marker2_text"	=> '',
		"marker2_location"	=> '41.858774,-87.685328',
		"map_scroll" => 'false'
	), $atts));
	
	$rand_id = rand(1,9999);
	
	
	wp_enqueue_script('google-map-sensor', '', '', '', true);
	wp_enqueue_script('google-map-label', '', '', '', true);
	
	if(!$lat || !$long) {
		return 'Error: no location lat and/or long data found';
	}
	
	$map_center = $lat.','.$long;
	
	$marker1_center = $map_center;
	
	if($marker1_location == 'custom') {
		$marker1_center = $marker1_location_custom;
	}		

	ob_start();	
	?>
	<script type="text/javascript">
	
	jQuery(document).ready(function() {
	
		'use strict';

		// Map Coordination

		var latlng = new google.maps.LatLng(<?php echo esc_attr($map_center); ?>);
		<?php
			$style_class = '';
			if($map_style == "grayscale") {
				$style_class = 'styles: [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},
				{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},
				{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},
				{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},
				{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}]';
			}

			
		?>
		// Map Options
		var myOptions = {
			zoom: <?php echo esc_attr($zoom); ?>,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: false,
			scrollwheel: <?php echo esc_attr($map_scroll); ?>,
			<?php if($style_class) echo $style_class; ?>
		};

		var map = new google.maps.Map(document.getElementById('google-map-<?php echo $rand_id; ?>'), myOptions);

		// Marker Image
		var image = '<?php echo get_template_directory_uri() . '/img/marker.png'; ?>';
		
	  	/* ========= First Marker ========= */

	  	// First Marker Coordination
		
		var myLatlng = new google.maps.LatLng(<?php echo esc_attr($marker1_center); ?>);

		// Your Texts 

		 var contentString = '<div id="content">'+
		  '<div id="siteNotice">'+
		  '</div>'+
		  '<h4>' +

		  '<?php echo esc_textarea($marker1_title); ?>'+

		  '</h4>'+
		  <?php if($marker1_text) { ?>
		  	
		  '<p>' +
		 		
  		  '<?php echo esc_textarea($marker1_text); ?>' +
  
  		  '</p>'+
		  	
		  <?php	} ?>
		  
		  '</div>';
	
		
		var marker = new MarkerWithLabel({
			position: myLatlng,
			draggable: true,
			raiseOnDrag: true,
			icon: ' ',
			map: map,
			labelContent: '<i class="fa fa-map-marker vntd-gmap-marker vntd-gmap-marker1"></i>',
			labelAnchor: new google.maps.Point(22, 50),
			labelClass: "labels" // the CSS class for the label
		});
		
		marker.setMap( map );

		var infowindow = new google.maps.InfoWindow({
		  content: contentString
		  });

		  
		 google.maps.event.addListener(marker, 'click', function(e) {
			infowindow.open(map,this);
		  });

		 /* ========= End First Marker ========= */



		<?php if($marker2_title) { ?>

		 /* ========= Second Marker ========= */

		 // Second Marker Coordination

		 var myLatlngSecond = new google.maps.LatLng(<?php echo esc_attr($marker2_location); ?>);

		 // Your Texts

		 var contentStringSecond = '<div id="content">'+
		  '<div id="siteNotice">'+
		  '</div>'+
		  '<h4>' +

		  '<?php echo esc_textarea($marker2_title); ?>'+

		  '</h4>'+
		  <?php if($marker2_text) { ?>
		  		  	
  		  '<p>' +
  		 		
  		  '<?php echo esc_textarea($marker2_text); ?>' +
  
  		  '</p>'+
  		  	
  		  <?php	} ?>
		  '</div>';
		  
		var marker2 = new MarkerWithLabel({
			position: myLatlngSecond,
			draggable: true,
			raiseOnDrag: true,
			icon: ' ',
			map: map,
			labelContent: '<i class="fa fa-map-marker vntd-gmap-marker vntd-gmap-marker2"></i>',
			labelAnchor: new google.maps.Point(22, 50),
			labelClass: "labels" // the CSS class for the label
		});
			
		marker2.setMap( map );
			
		var infowindow2 = new google.maps.InfoWindow({
			content: contentStringSecond
		});
			
			
		google.maps.event.addListener(marker2, 'click', function(e) {
			infowindow2.open(map,this);
		});
		  
		  

		 /* ========= End Second Marker ========= */
	
		<?php } ?>
	});
	
	</script>
	<div class="vntd-gmap">	
		<?php
			
			$height = str_replace('px','',$height);
			
			if(is_null($height)) $height = 400;
		?>
	    <div id="google-map-<?php echo $rand_id; ?>" style="height:<?php echo esc_attr($height); ?>px;"></div>
	    
	</div>
	<?php
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('gmap');
add_shortcode('gmap', 'crexis_gmap');