<?php

// Blog Posts Carousel

function vntd_hero_section($atts, $content = null) {
	extract(shortcode_atts(array(
		"media_type" 		=> 'images',
		"images" 			=> '',
		"youtube_id"		=> 'nKAxHHTxXIU',
		"style" 			=> 'default',
		"boxes_style"		=> 'default',
		"heading_dynamic" 	=> 'We Create Awesome Themes!,Crexis Creative Design Studio',
		"heading" 			=> 'Design Your Life',
		"heading_small" 	=> 'never give up!',
		"heading_top"		=> '',
		"subtitle" 			=> 'Contrary to popular belief, Lorem Ipsum is not simply random text. Piece of classical Latin literature.',
		"bg_overlay" 		=> 'none',
		"height" 			=> 'fullscreen',
		"height_custom" 	=> '',
		
		"button1_label" 	=> 'Button Text',
		"button1_color" 	=> 'light',
		"button1_url" 		=> '#second',
		"scroll_button" 	=> 'no',
		"scroll_button_style" => 'circle',
		"scroll_label" 		=> '',
		"scroll_url" 		=> '#second',
		
		"arrow_navigation" 	=> 'yes',
		"arrow_navigation_style" => 'side',
		"effect" 			=> 'none',
		"video_autoplay" 	=> 'true',
		"video_controls" 	=> 'true',
		"video_controls_style" => 'bottom',
		"video_mute" 		=> 'true',
		"video_img_placeholder"	=> '',
		"boxes"				=> '',
		"separator_line"	=> 'no',
		"content_align"		=> 'center',
		"heading_font_size" => 'default',
		"subtitle_width"	=> 'default',
		
		"tooltips_enable" 	=> 'no',
		"tooltips" 			=> '',
		
		"parallax" 			=> 'yes',
		"slider_speed" 		=> '12000'
	), $atts));
	
	wp_enqueue_script('superslides', '', '', '', true);
	
	$parallax_class = $parallax_data = '';
	
	if($media_type == "images" && $effect == 'rainyday') {
		wp_enqueue_script('rainyday', '', '', '', true);
	} elseif($media_type == "images" && $effect == 'particles') {
		wp_enqueue_script('vntd-greensock', '', '', '', true);
		wp_enqueue_script('vntd-particle', '', '', '', true);
		
	} elseif($media_type == "images") {
		
		//wp_enqueue_script( 'vc_jquery_skrollr_js' );
		
		//wp_enqueue_style('swiperCSS');
	} elseif($media_type == "youtube") {
		wp_enqueue_script('YTPlayer', '', '', '', true);
		wp_enqueue_style('YTPlayer');
	}
	
	if($style != 'style2') {
		wp_enqueue_script('textrotator', '', '', '', true);
		//wp_enqueue_script('vntd-skrollr', '', '', '', true);
	} else {
		wp_enqueue_script('owl-carousel', '', '', '', true);
	}
	
	if($parallax == 'yes') {
		wp_enqueue_script('vntd-parallax', '', '', '', true);
		
		$parallax_class = 'skrollable';
		$parallax_data = ' data-top-bottom="background-position: 50% -340px;" data-top="background-position: 50% 0px;" data-bottom-top="background-position: 50% 200px;"';
		$parallax_class = 'parallax';
		$parallax_data = '';
	}
	
	$fullscreen = 0;
	$fullscreen_class = '';
	if($height != 'custom') $fullscreen_class = ' veented-slider-fullscreen';
	
	$random_id = rand(1,9999);

	$scroll_class = '';
	if($scroll_button == 'yes') {
		$scroll_class = ' veented-slider-with-scroll';
	}
	
	$no_slider = false;
	$hero_section_class = 'veented-slider'; // Image Slider
	
	if(sizeof($images) == 1 || $media_type == 'video') {
		$no_slider = true;
		$hero_section_class = 'hero-section-video';
	}

	$overlay_class = '';
	$animated_class = '';
	
	if($height == 'custom') {
	
		if(!$height_custom) {
			$height_custom = '700';
		} else {
			$height_custom = str_replace('px','',$height_custom);
		}
		echo '<style type="text/css">';
		if($height_custom > 0 && $height == 'custom') {
			echo '#hero-section-'.$random_id.' .hero-fullscreen { height: '.esc_attr($height_custom).'px !important; }';	
		}
//		if($offset != 0) {
//			echo '#hero-section-'.$random_id.' .hero-fullscreen .inner { padding-top: '.esc_attr($offset).'px; }';
//		}
		echo '</style>';			
	}	
	
	ob_start();
	
	echo '<div id="hero-section-' . $random_id . '" class="vntd-hero hero-media-' . esc_attr( $media_type ) . ' hero-effect-' . esc_attr( $effect ) . ' arrow-navigation-' . esc_attr( $arrow_navigation_style ) . ' video-controls-' . esc_attr( $video_controls_style ) . '">';
	
	?>
	
		<?php
		
		if($media_type == 'images' && $effect != 'none') {
		
			$bg_img = '';

			if (strpos($images,',') !== false) {
				$images = explode(',',$images);
				//echo "Image: " . array_shift(array_slice($images, 0, 1));
				$bg_img = array_shift(array_slice($images, 0, 1));
			} else {
				$bg_img = $images;
			}
			
			$img_url = wp_get_attachment_image_src($bg_img, 'crexis-bg-image');
			
			
			
			if($effect == 'rainyday') {
			
				echo '<div class="hero-fullscreen ' . esc_attr( $effect ) . '">';
				
					echo '<img id="rainyday" data-image="' . $img_url[0] . '" src="" alt="background" />';
					
				echo '</div>';
				
			} elseif($effect == 'animated') {
			
				echo '<div class="hero-fullscreen animated-bg" style="background-image: url(' . esc_url( $img_url[0] ) . ');"></div>';
				
			} else {
			
				echo '<div class="hero-fullscreen ' . esc_attr( $effect ) . ' ' . $parallax_class . '" style="background-image: url(' . esc_url( $img_url[0] ) . ');">';
				
					echo '<canvas id="particle"></canvas>';
					
				echo '</div>';
				
			}
			
		} elseif($media_type == 'images') {
				
			$slider = true;
			
			if (strpos($images,',') !== false) {
			    // More than one image
			    
			    echo '<div class="hero-fullscreen" data-slider-speed="' . esc_attr( $slider_speed ) . '">';
			} else {
				$slider = false;
				// Single image
				$img_url = wp_get_attachment_image_src($images, 'full');
				
				echo '<div class="hero-fullscreen ' . $parallax_class . '"' . $parallax_data . ' style="background-image: url(' . esc_url($img_url[0]) . ');">';
			}
			
			if($bg_overlay != 'none') {
				echo '<div class="bg-overlay bg-overlay-' . esc_attr($bg_overlay) . '"></div>';
			}
			
			if($slider == true) {
			
				echo '<div class="slides-container relative">';
				
				$img_arr = explode(',',$images);
				
				
									
				foreach($img_arr as $img) {
					$img_url = wp_get_attachment_image_src($img, 'full');
					echo '<div class="superslides-slide ' . $parallax_class . '" ' . $parallax_data . ' style="background-image: url(' . $img_url[0] . '); display:block !important; opacity: 1 !important; z-index:2 !important;"></div>';

				}
				
				echo '</div>';
				
				if($arrow_navigation != 'no') {
				?>
				<nav class="slides-navigation">
					<a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
					<a href="#" class="next"><i class="fa fa-angle-right"></i></a>
				</nav>
				<?php
				}
			
			}
			
			echo '</div>';
			
		} elseif($media_type == "youtube") {
		
			$rand_id = rand(9,9999);
			
			if($bg_overlay != 'none') {
				echo '<div class="bg-overlay bg-overlay-' . esc_attr($bg_overlay) . '"></div>';
			}
			
			$placeholder_img = '';
			$placeholder_img_url = '';
			
			if($video_img_placeholder) {
				$placeholder_img = wp_get_attachment_image_src($video_img_placeholder, 'full');
				$placeholder_img_url = $placeholder_img[0];
			}
			
			$rand_id = rand(1,99999);
			$quality = "default";
			
			echo '<div id="fullscreen-' . $rand_id . '" class="hero-fullscreen" style="background-image:url('. $placeholder_img_url .')">';
			
			echo '<div id="p1" class="player video-container" data-property="{videoURL:\'' . esc_textarea( $youtube_id ) .'\', containment:\'#fullscreen-' . $rand_id . '\', autoPlay:' . esc_attr( $video_autoplay ) . ', showControls:' . esc_attr( $video_controls ) . ', mute:'. esc_attr( $video_mute ) . ', startAt:0, opacity:1, quality:\'' . $quality . '\'}"></div>';
			
			echo '</div>'; // End fullscreen
			
		
		}
		
		if($style == 'style2') {
		?>
		
		<div class="vntd-hero-style2 home-inner hero-boxes-<?php echo esc_attr( $boxes_style ); ?>">
		
			<!-- Home Boxes -->
			<div class="home_boxes t-left">
					
				<!-- Texts -->
				<div class="home_boxes_texts">
					<!-- First -->
					
					<?php if($heading_top) { ?>
					<h2 class="white thin no-margin">
						<?php echo esc_html($heading_top); ?>
					</h2>
					<?php } ?>
					<?php if($heading) { ?>
					<!-- Second, Big Text -->
					<h1 class="white thin">
						<?php echo esc_textarea($heading); if($heading_small) echo ' <span>' . esc_html($heading_small) . '</span>'; ?>
					</h1>
					
					<?php } ?>
					
					<?php if($subtitle) { ?>
					<!-- Third, Description -->
					<p class="white thin no-margin">
						<?php echo esc_html($subtitle); ?>
					</p>
					<?php } ?>
				</div>
				<!-- End Texts -->
				
				<?php 
				
				$classes = 'white-boxes three-items';
				
				if($boxes_style == 'minimal') {
					$classes = 'type-6 boxed four-items';
				}
				
				?>
				
				<!-- Boxes -->
				<div class="boxes box-carousel-dragable <?php echo $classes; ?>">
				
					<?php
					
					$values = (array) vc_param_group_parse_atts( $boxes );
					
					foreach ( $values as $data ) {
						$new_line = $data;
						
						$new_line['box_heading'] = isset( $data['box_heading'] ) ? $data['box_heading'] : '';
						$new_line['box_content'] = isset( $data['box_content'] ) ? $data['box_content'] : '';
						$new_line['box_url'] = isset( $data['box_url'] ) ? $data['box_url'] : '';
						$new_line['box_dark'] = isset( $data['box_dark'] ) ? $data['box_dark'] : '';
						
						$extra_class = '';
						
						if( $new_line['box_dark'] == 'yes') $extra_class = ' box-dark';
						
						$holder = 'a';
						if( $new_line['box_url'] == '' ) $holder = 'div';
						
						echo '<' . $holder .' href="' . esc_url($new_line['box_url']) . '" class="scroll box inline-block' . esc_attr( $extra_class ) . '">';
						
						echo '<h2 class="white thin">' . esc_html($new_line['box_heading']);
						
						if($boxes_style == 'minimal') echo '<i class="fa fa-plus-circle"></i>';
						
						echo '</h2>';
						
						if($boxes_style != 'minimal') {
						
						echo '<p class="white thin">';
						
						if( $new_line['box_content'] == '[social-icons]' ) {
						
							crexis_print_social_icons();
							
						} else {
						
							echo esc_html( $new_line['box_content'] );
							
						}
						
						echo '</p>';

						}
						
						echo '</' . $holder .'>';
					}
					
					
					?>
	
				</div>
				<!-- End Boxes -->
				
			</div>
			
		</div>	
			
		
		<?php
		
		} else { // Default style
		
		$extra_class = '';

		if($separator_line != 'no') $extra_class = ' home-inner-seperator-' . esc_attr($separator_line);
		
		if($content_align == 'left') $extra_class .= ' hero-align-left';
		
		if($subtitle_width == 'narrow') $extra_class .= ' hero-subtitle-narrow';
		
		?>

		<!-- Home Inner Details -->
		<div class="vntd-hero-style1 home-inner <?php echo $extra_class; ?>" data-0="opacity:1;" data-600="opacity:0;">
		
			<?php if($content_align == 'left') echo '<div class="home_boxes">'; ?>

			<!-- Home Text Slider -->
			<div class="home-text-slider relative">
			
				<?php if($heading_top) { ?>
				<h2 class="white thin no-margin">
					<?php echo esc_html( $heading_top ); ?>
				</h2>
				<?php } 
				
				$font_size_class = '';
				
				if($heading_font_size != 'default') {
					$font_size_class = ' fs' . $heading_font_size;
				}
				
				?>

				<!-- Home Text Slider -->
				<h1 class="white thin text-rotator<?php echo esc_attr( $font_size_class ); ?>">

					<!-- Home Texts -->
					<span class="rotate<?php if ( strpos( $heading_dynamic, ',' ) === false ) { echo "-no-rotate"; } ?>">
					
					<?php
					
					$dynamic_lines = explode(',',$heading_dynamic);
					
					$numItems = count($dynamic_lines);
					$i = 0;
					
					foreach ( $dynamic_lines as $dynamic_line ) {
					
						$title = esc_html( $dynamic_line );
						$title = str_replace( "(b)", '<span class="highlight">', $title );
						$title = htmlspecialchars_decode( str_replace( "(/b)", '</span>', $title ) );
						
						echo $title;
						
					  	if(++$i === $numItems) {

					  	} else {
					  		echo  ',';
					  	}
					}  
					
					?>
					
					</span>
					<!-- End Home Texts -->

				</h1>
				<!-- End Home Text Slider -->
				
				<!-- Home Fixed Text -->		
				<p class="home-fixed-text white"><?php echo str_replace("(br)","<br>", esc_html( $subtitle ) ); ?></p>

			</div>
			<!-- End Home Text Slider -->
			
			<?php
			
			if($button1_label != '') {
			
				$button1_color_class = 'light';
				
				if($button1_color == 'dark') {
					$button1_color_class = 'dark';
				}
				
				echo '<a href="' . esc_url($button1_url) . '" class="home-button '. $button1_color_class .'-button thin scroll">' . esc_textarea($button1_label) . '</a>';
			}
			
			?>
			
			<?php if($content_align == 'left') echo '</div>'; ?>
			
		</div>
		<!-- End Home Inner Details -->
		
		<?php } // End content that depends on specific style ?>
		
		
		<?php if($scroll_button == 'yes') { 
		
		$extra_class = ' scroll-button-' . $scroll_button_style;
		
		if($scroll_label == '') $extra_class .= ' jump';
		
		if($scroll_button_style == 'circle') {
			$extra_class .= ' thin';
		}
		?>

		<!-- Home Bottom Note -->
		<div class="home-extra-note fullwidth t-center white font-primary absolute<?php echo $extra_class; ?>" data-0="opacity:1;" data-600="opacity:0;">
			<!-- Text Link -->
			<a href="<?php echo esc_url($scroll_url); ?>" class="scroll">
				<!-- Bottom Text -->
				<?php 
				
				if($scroll_button_style == 'circle') {
				
					if($scroll_label != '') echo '<p>' . esc_html($scroll_label) . '</p>';
					echo '<span class="home-button dark-button t-center home-circle-button fa fa-angle-down"></span>';
					
				} else {
					echo '<img src="'. get_template_directory_uri() . '/img/mouse-icon.png" alt="mouse_icon">' . esc_html( $scroll_label );
				}
				
				?>

			</a>
			<!-- End Text Link -->
		</div>
		<!-- End Home Bottom Note -->
		
		<?php } ?>
	
	<?php
	
	if($tooltips_enable == 'yes') {
	
	//wp_enqueue_script('vntd-parallax', '', '', '', true);
	wp_enqueue_script('vntd-parallax-conf', '', '', '', true);
	
	?>
	
	<ul id="parallax-tooltips" class="vntd-parallax-tooltips boxed moving-items">
	
	<?php
	
	$values = (array) vc_param_group_parse_atts( $tooltips );
	
	foreach ( $values as $data ) {
	
		$new_line = $data;
		
		$new_line['tooltip_title'] = isset( $data['tooltip_title'] ) ? $data['tooltip_title'] : '';
		$new_line['tooltip_url'] = isset( $data['tooltip_url'] ) ? $data['tooltip_url'] : '';
		$new_line['tooltip_placement'] = isset( $data['tooltip_placement'] ) ? $data['tooltip_placement'] : '';
		$new_line['tooltip_depth'] = isset( $data['tooltip_depth'] ) ? $data['tooltip_depth'] : '';
		
		echo '<li class="box layer inactive" data-depth="' . esc_attr( $new_line['tooltip_depth'] ) . '">';
		echo '<a href="' . esc_url( $new_line['tooltip_url'] ) . '" data-toggle="tooltip" data-placement="bottom" title="' . esc_html( $new_line['tooltip_title'] ) . '"><span class="fa fa-plus-circle icon"></span></a>';
		echo '</li>';
		
	}
	
	?>

	</ul>
				
	<?php
	
	}
	
	echo '</div>';
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('vntd_hero_section');
add_shortcode('vntd_hero_section', 'vntd_hero_section');