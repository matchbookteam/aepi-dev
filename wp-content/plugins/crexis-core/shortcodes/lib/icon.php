<?php

// - - - - - - - - - -
// FontAwesome Icon
// - - - - - - - - - -

function crexis_icon($atts, $content = null) {
	extract(shortcode_atts(array(
		"icon" => '',
		"color" => '',
		"style" => '',
		"float" => '',
		"size" => 'small',
		"icon_style" => 'font-awesome'
	), $atts));
	
	$icon_style_class = 'fa fa-';
	
	if($icon_style == 'simple-line-icons') {
		$icon_style_class = 'icon-';
		if($icon == "phone") {
			$icon = "screen-smartphone";
		}
	}
	
	$icon_class = $extra_styling = '';
	if($style == "bordered") {
		$icon_class .= ' vntd-icon-style-bordered';
		if(strpos($color,'#') !== false) {
			$extra_styling = 'style="color:'.$color.';border-color:'.$color.'"';
		} else {
			if(!$color) $color = "accent";
			$icon_class .= ' vntd-bordercolor-'.$color.' vntd-color-'.$color;
		}
	} elseif ($style == "colored") {
		$icon_class .= ' vntd-icon-style-colored';	

		if(strpos($color,'#') !== false) {
			$extra_styling .= 'style="background-color:'.$color.';"';
		} else {
			if(!$color) $color = "accent";
			$icon_class .= ' vntd-bgcolor-'.$color;
		}
		
	} elseif(strpos($color,'#') !== false) {
		$extra_styling .= 'style="color:'.$color.';"';
	} elseif($color) {
		$icon_class .= ' vntd-color-'.$color;
	}
	
	if($float) {
		$icon_class .= ' vntd-icon-float-'.$float;
	}
	
	return '<i class="'.$icon_style_class.$icon.' vntd-icon vntd-icon-'.$size.$icon_class.'"'.$extra_styling.'></i>';
}
remove_shortcode('icon');
add_shortcode('icon', 'crexis_icon');