<?php

// Counter Shortcode Processing

function vntd_product_features($atts, $content=null){
	
	extract(shortcode_atts(array(
		"img" 				=> '',
		"left_box1_title"	=> 'Feature 1 Title',
		"left_box1_text"	=> 'This is a short description of the feature. Please feel free to change me!',
		"left_box1_icon"	=> 'fa fa-info-circle',
		"left_box1_tooltip_label"	=> 'This is a simple tooltip label.',
		"left_box1_offset_top"	=> '50px',
		"left_box1_offset_left"	=> '160px',
		"left_box1_url"	=> '',
		"left_box2_title"	=> 'Feature 2 Title',
		"left_box2_text"	=> 'This is a short description of the feature. Please feel free to change me!',
		"left_box2_icon"	=> 'fa fa-info-circle',
		"left_box2_tooltip_label"	=> 'This is a simple tooltip label.',
		"left_box2_offset_top"	=> '20px',
		"left_box2_offset_left"	=> '180px',
		"left_box2_url"	=> '',
		"right_box1_title"	=> 'Feature 3 Title',
		"right_box1_text"	=> 'This is a short description of the feature. Please feel free to change me!',
		"right_box1_icon"	=> 'fa fa-info-circle',
		"right_box1_tooltip_label"	=> 'This is a simple tooltip label.',
		"right_box1_offset_top"	=> '60px',
		"right_box1_offset_left"	=> '140px',
		"right_box1_url"	=> '',
		"right_box2_title"	=> 'Feature 4 Title',
		"right_box2_text"	=> 'This is a short description of the feature. Please feel free to change me!',
		"right_box2_icon"	=> 'fa fa-info-circle',
		"right_box2_tooltip_label"	=> 'This is a simple tooltip label.',
		"right_box2_offset_top"	=> '50px',
		"right_box2_offset_left"	=> '160px',
		"right_box2_url"	=> '',
		"plus_icon"		=> "yes",
		"toggle_heading" => "Clean, Responsive and Professional design with powerful code!",
		"toggle_link_title" => "View all our features",
		"toggle_link_url" => "http://yoursite.com"
	), $atts));
	
	// End Icon related
	
	$rand_id = rand(1,1000);
	
	// Background Image
	
	$img_attr = '';
	
	if($img) {
		
		$imgurl = wp_get_attachment_image_src( $img, 'full');
		
		$img_attr = ' style="background-image: url(' . esc_url( $imgurl[0] ) . ');"';
		
	}
	
	ob_start();	
	
	?>
	
	<section class="product-features">
	
	<ul class="mobile-boxes clearfix animated t-center" <?php if($img_attr) echo $img_attr;  ?> data-animation="fadeIn" data-animation-delay="300">
	
		<?php 
		
		if($left_box1_title) {
		
		?>
		
		<li class="clearfix animated box-left" data-animation="fadeIn" data-animation-delay="600">
		
			<div class="details">
				<div class="mobile-icon">

					<?php 
					
					$anchor = '';
					
					if($left_box1_url) {
						$anchor = ' href="' . esc_url($left_box1_url) . '" '; 
					}
					
					?>
					
					<a<?php echo $anchor; ?> class="mobile-icon">
						<i class="<?php echo esc_textarea($left_box1_icon); ?>"></i>
					</a>
				</div>
				<h4 class="antialiased minimal-head"><?php echo esc_textarea($left_box1_title); ?></h4>
				<p><?php echo esc_textarea($left_box1_text); ?></p>
			</div>

			<div class="buttons">

				<span class="first-icon">
					<i class="fa fa-plus"></i>
				</span>

				<div class="strips" data-width="<?php echo str_replace("px","",esc_attr($left_box1_offset_left)); ?>" data-height="<?php echo str_replace("px","",esc_attr($left_box1_offset_top)); ?>">
					<span class="second-icon" data-toggle="tooltip" data-placement="top" title="<?php echo esc_textarea($left_box1_tooltip_label); ?>">
						<i class="fa fa-plus"></i>
					</span>
				</div>
				
			</div>

		</li>
		
		<?php
		
		}
		
		if($right_box1_title) {
				
		?>
		
		<li class="clearfix animated box-right" data-animation="fadeIn" data-animation-delay="800">
		
			<div class="details">
				<div class="mobile-icon">
					<?php 
					
					$anchor = '';
					
					if($right_box1_url) {
						$anchor = ' href="' . esc_url($right_box1_url) . '" '; 
					}
					
					?>
					
					<a<?php echo $anchor; ?> class="mobile-icon">
						<i class="<?php echo esc_textarea($right_box1_icon); ?>"></i>
					</a>
				</div>
				<h4 class="antialiased minimal-head"><?php echo esc_textarea($right_box1_title); ?></h4>
				<p><?php echo esc_textarea($right_box1_text); ?></p>
			</div>

			<div class="buttons">

				<span class="first-icon">
					<i class="fa fa-plus"></i>
				</span>

				<div class="strips" data-width="<?php echo str_replace("px","",esc_attr($right_box1_offset_left)); ?>" data-height="<?php echo str_replace("px","",esc_attr($right_box1_offset_top)); ?>">
					<span class="second-icon" data-toggle="tooltip" data-placement="top" title="<?php echo esc_textarea($right_box1_tooltip_label); ?>">
						<i class="fa fa-plus"></i>
					</span>
				</div>
				
			</div>

		</li>
		
		<?php
		
		}
		
		if($left_box2_title) {
				
		?>
		
		<li class="clearfix animated box-left" data-animation="fadeIn" data-animation-delay="1000">
		
			<div class="details">
				<div class="mobile-icon">
					<?php 
					
					$anchor = '';
					
					if($left_box2_url) {
						$anchor = ' href="' . esc_url($left_box2_url) . '" '; 
					}
					
					?>
					
					<a<?php echo $anchor; ?> class="mobile-icon">
						<i class="<?php echo esc_textarea($left_box2_icon); ?>"></i>
					</a>
				</div>
				<h4 class="antialiased minimal-head"><?php echo esc_textarea($left_box2_title); ?></h4>
				<p><?php echo esc_textarea($left_box2_text); ?></p>
			</div>

			<div class="buttons">

				<span class="first-icon">
					<i class="fa fa-plus"></i>
				</span>

				<div class="strips" data-width="<?php echo str_replace("px","",esc_attr($left_box2_offset_left)); ?>" data-height="<?php echo str_replace("px","",esc_attr($left_box2_offset_top)); ?>">
					<span class="second-icon" data-toggle="tooltip" data-placement="top" title="<?php echo esc_textarea($left_box2_tooltip_label); ?>">
						<i class="fa fa-plus"></i>
					</span>
				</div>
				
			</div>

		</li>
		
		<?php
		
		}
		
		if($right_box2_title) {
						
		?>
		
		<li class="clearfix animated box-right" data-animation="fadeIn" data-animation-delay="1200">
		
			<div class="details">
				<div class="mobile-icon">
					<?php 
					
					$anchor = '';
					
					if($right_box2_url) {
						$anchor = ' href="' . esc_url($right_box2_url) . '" '; 
					}
					
					?>
					
					<a<?php echo $anchor; ?> class="mobile-icon">
						<i class="<?php echo esc_textarea($right_box2_icon); ?>"></i>
					</a>
				</div>
				<h4 class="antialiased minimal-head"><?php echo esc_textarea($right_box2_title); ?></h4>
				<p><?php echo esc_textarea($right_box2_text); ?></p>
			</div>

			<div class="buttons">

				<span class="first-icon">
					<i class="fa fa-plus"></i>
				</span>

				<div class="strips" data-width="<?php echo str_replace("px","",esc_attr($right_box2_offset_left)); ?>" data-height="<?php echo str_replace("px","",esc_attr($right_box2_offset_top)); ?>">
					<span class="second-icon" data-toggle="tooltip" data-placement="top" title="<?php echo esc_textarea($right_box2_tooltip_label); ?>">
						<i class="fa fa-plus"></i>
					</span>
				</div>
				
			</div>

		</li>
		
		<?php
		
		}
		
		if($plus_icon != 'no') {
		
		?>
		<li class="features-button">
			<a class="f-button black-gradient"></a>
		</li>
		<?php
		
		}
		
		?>

	</ul>
	<!-- End Boxes -->
	
	<?php if($plus_icon != 'no') { ?>

	<!-- Collapse For features-button -->
	<div class="f-collapse t-center">
		<!-- Inner -->
		<div class="oswald uppercase antialiased">
			<?php echo esc_textarea($toggle_heading); ?>
			<p>
				<a href="<?php echo esc_url($toggle_link_url); ?>"><?php echo esc_textarea($toggle_link_title); ?> <i class="fa fa-arrow-right"></i></a>
			</p>
		</div>
		<!-- End Collapse Inner -->
	</div>
	
	<?php } ?>
	
</section>
	
	
	<?php
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('vntd_product_features');
add_shortcode('vntd_product_features', 'vntd_product_features');