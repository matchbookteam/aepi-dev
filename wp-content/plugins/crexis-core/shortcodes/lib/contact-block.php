<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Contact Block
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_contact_block($atts, $content = null) {
	extract(shortcode_atts(array(
		"title" 		=> 'Keep in touch',
		"description" 	=> '',
		"style" 		=> 'horizontal',
		"field1_title" 	=> 'Phone',
		"field1_text" 	=> '0123 456 789 - 0123 456 788',
		"field1_url"	=> 'tel:123456789',
		"field1_icon"	=> 'fa fa-mobile',
		"field2_title" 	=> 'E-Mail',
		"field2_text" 	=> 'hello@mysite.com',
		"field2_url"	=> 'mailto:hello@mysite.com',
		"field2_icon"	=> 'fa fa-envelope',
		"field3_title" 	=> 'Address',
		"field3_text" 	=> 'Street 1352, Melbourne Australia',
		"field3_url"	=> '',
		"field3_icon"	=> 'fa fa-map-marker',
	), $atts));
	
	$plus_button = '<div class="button"></div>';
	
	$box_class = 'clearfix block f-left';
	
	if($style == 'vertical') {
		$box_class = 'box light hover';
	}
	
	if( $field1_title == '' && $field2_title != '' && $field3_title != '' || $field1_title != '' && $field2_title == '' && $field3_title != '' || $field1_title != '' && $field2_title != '' && $field3_title == '' ) {
		$extra_class = ' blocks-number-2';
	}
	
	ob_start();	
	
	echo '<div class="vntd-contact-block contact-block-' . esc_attr( $style ) . $extra_class . '">';
	
	if($style == 'vertical' && $title) {
		echo '<h3>' . esc_html( $title ) . '</h3>';
	}
	
	
	?>
	<?php if( $field1_title != '' ) { 
	
	if( $field1_url != '' ) {
		$field1_url = ' href="' . $field1_url . '"';
	}
	
	?>
	<a class="box <?php echo $box_class; ?>"<?php echo $field1_url; ?>>
		<!-- Icon -->
		<div class="icon">
			<i class="<?php echo $field1_icon; ?>"></i>
		</div>
		<!-- Texts -->
		<div class="texts">
			<!-- Arrow -->
			<span class="arrow"></span>
			<!-- Header -->
			<h3><?php echo esc_html($field1_title); ?></h3>
			<!-- Detail -->
			<p><?php echo esc_html($field1_text); ?></p>
		</div>
		<?php if($style == 'vertical' && $field1_url != '' ) echo $plus_button; ?>
		<!-- End Texts -->
	</a>
	<?php
	}
	if( function_exists( 'eae_encode_str' ) ) {
		$field2_text = eae_encode_str( $field2_text );
		$field2_url = eae_encode_str( $field2_url );
	}
	if( $field2_title != '' ) {
	
		if( $field2_url != '' ) {
			$field2_url = ' href="' . $field2_url . '"';
		}
	
	?>
	<a class="box <?php echo $box_class; ?>"<?php echo $field2_url; ?>>
		<!-- Icon -->
		<div class="icon">
			<i class="<?php echo $field2_icon; ?>"></i>
		</div>
		<!-- Texts -->
		<div class="texts">
			<!-- Arrow -->
			<span class="arrow"></span>
			<!-- Header -->
			<h3><?php echo esc_html($field2_title); ?></h3>
			<!-- Detail -->
			<p><?php echo $field2_text; ?></p>
		</div>
		<?php if($style == 'vertical' && $field2_url != '' ) echo $plus_button; ?>
		<!-- End Texts -->
	</a>
	<?php 
	}
	if( $field3_title != '' ) { 
	
		if( $field3_url != '' ) {
			$field3_url = ' href="' . $field3_url . '"';
		}
	
	?>
	<a class="box <?php echo $box_class; ?>"<?php echo $field3_url; ?>>
		<!-- Icon -->
		<div class="icon">
			<i class="<?php echo $field3_icon; ?>"></i>
		</div>
		<!-- Texts -->
		<div class="texts">
			<!-- Arrow -->
			<span class="arrow"></span>
			<!-- Header -->
			<h3><?php echo esc_html($field3_title); ?></h3>
			<!-- Detail -->
			<p><?php echo esc_html($field3_text); ?></p>
		</div>
		<?php if($style == 'vertical' && $field3_url != '' ) echo $plus_button; ?>
		<!-- End Texts -->
	</a>
	
	<?php
	}
	echo '</div>';
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('contact_block');
add_shortcode('contact_block', 'crexis_contact_block');