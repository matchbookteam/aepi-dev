<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Testimonials Carousel
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_logos($atts, $content = null) {
	extract(shortcode_atts(array(
		"images" => '',
		"onclick" => 'link_no',
		"custom_links" => '',
		"cols" => 4,
		"autoplay" => 'true',
		"dots" => "false",
		"style" => 'default',
		"nav" => "false",
		"nav_position" => "bottom",
		"carousel_title" => '',
		"logos_height" => 'regular',
		"carousel_speed" => 300,
	), $atts));
	
	wp_enqueue_script('owl-carousel', '', '', '', true);
	wp_enqueue_style('owl-carousel');

	ob_start();	
	
	$link_href = '';
	
	if($onclick == 'custom_link') {
		$custom_links = explode(',',$custom_links);
	}
					 			
	$images = explode( ',', $images );
	$i = - 1;
	
	echo '<div class="vntd-carousel-holder">';
		
		echo '<div class="client-logos vntd-carousel client-logos-' . $style . '" data-cols="' . $cols . '" data-autoplay="' . esc_attr( $autoplay ) . '" data-dots="' . esc_attr( $dots ) . '" data-nav="' . esc_attr( $nav ) . '" data-speed="' . esc_attr( $carousel_speed ) . '">';	
			
		foreach ( $images as $attach_id ) {
			$i++;
			$link_href = '';
			if($onclick == 'custom_link') {
				$link_href = ' href="' . esc_url( $custom_links[$i] ) . '"';
			}
			$img = wp_get_attachment_image_src($attach_id, 'full');
			$alt = get_post_meta($attach_id, '_wp_attachment_image_alt', true);
			$alt_text = 'alt';
			if( count($alt) ) $alt_text ='alt="' . esc_textarea( $alt ) . '"';
			
			?>
			<div class="client-logo">
				<!-- Logo Link -->
				<a <?php if($link_href) echo $link_href; ?>>
					<!-- Logo Image SRC -->
					<img src="<?php echo esc_url($img[0]); ?>"<?php echo $alt_text; ?>>
				</a>
			</div>			
			<?php

		}
			
	echo '</div></div>';
	
	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('logos_carousel');
add_shortcode('logos_carousel', 'crexis_logos');