<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Testimonials Carousel
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_testimonials($atts, $content = null) {
	extract(shortcode_atts(array(
		"posts_nr" => '8',
		"style" => 'simple',
		"cols" => 3,
		"orderby" => 'date',
		"title_font" => 'georgia',
		"autoplay" => 'true',
		"dots" => 'false'
	), $atts));
	
	$holder_class = 'vntd-carousel-holder';
	
	wp_enqueue_script('owl-carousel', '', '', '', true);
	wp_enqueue_style('owl-carousel');
	
	$extra_class = ' boxes-type-4';
	$extra_class2 = '';
	
	if($style == 'fullwidth') {
		$cols = 1;
		$extra_class = $extra_class2 = ' testimonials type-1';
	} elseif($style == 'fullwidth2') {
		$cols = 1;
		$extra_class = $extra_class2 = ' testimonials type-2';
	}
	
	
	
	ob_start();		
	
	echo '<div class="vntd-carousel-holder t-center clients ' . $extra_class2 . '">';
	
	if($style == 'fullwidth') {
		echo '<div class="quote white t-center circle light"><i class="fa fa-quote-left"></i></div>';
		
	}
		
		echo '<div class="vntd-carousel vntd-testimonials-carousel testimonial-style-' . $style . $extra_class . ' boxes light box-carousel ' . crexis_column_items( $cols ) . ' clearfix" data-autoplay="' . esc_attr( $autoplay ) . '" data-dots="' . esc_attr( $dots ) . '">';
		
		wp_reset_postdata();
		
		$post_in = '';
		
		if( class_exists('crexisDemo') ) {
			$post_in = crexisDemo::crexisTestimonials($style);
			$orderby = 'post__in';
		}
		
		$args = array(
			'posts_per_page' => $posts_nr,
			'post_type' => 'testimonials',
			'orderby' => $orderby,
			'post__in' => $post_in
		);
		
		$i = 0;
		$extra_class = '';
		$the_query = new WP_Query($args);
		

		if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
		$i++;
	
		?>
		
		<div class="box<?php if($style != 'fullwidth' && $style != 'fullwidth2') echo ' georgia'; ?>">
		
			<?php if($style != 'fullwidth' && $style != 'fullwidth2') { ?>
			<!-- Box Image -->
			<div class="box-image fullwidth t-center normal">
				<!-- Image -->
				<?php 
				
				$holder = '<span ';
				$holder_end = '</span>';
				
				if( get_post_meta( get_the_ID(), 'website_url', TRUE ) && get_post_meta( get_the_ID(), 'website_url', TRUE ) != '#') { 
					$holder = '<a href="' . esc_url( get_post_meta( get_the_ID(), 'website_url', TRUE ) ) . '" ';
					$holder_end = '</a>';
				}
				
				echo $holder . ' class="changeable-image">';
				
				?> 
					<img src="<?php echo crexis_thumb(120,120); ?>" alt="crexis_image">
				<?php 
				
				echo $holder_end;
				
				?></a>
			</div>
			<!-- End Box Icon -->
			<!-- Box Header -->
			
			<?php
			
			$font_family = 'georgia';
			
			if( $title_font == 'primary' ) {
				$font_family = 'font-primary';
			}
			
			?>
			
			<h4 class="box-header no-padding uppercase <?php echo esc_attr( $font_family ); ?>">
				<?php echo esc_textarea(get_post_meta(get_the_id(), 'name', true)); ?>
			</h4>
			<!-- Position -->
			<h5 class="colored ">
				<?php echo esc_textarea(get_post_meta(get_the_id(), 'role', true)); ?>
			</h5>
			<?php } ?>
			<!-- Box Description -->
			<p class="no-padding no-margin raleway testimonial-content"><?php echo esc_textarea(get_post_meta(get_the_id(), 'testimonial_content', true)); ?></p>
			
			<?php if($style == 'fullwidth' || $style == 'fullwidth2') { ?>
			<p class="uppercase light t-center testimonial-author">
			<?php
			
			if($style == 'fullwidth2' && get_post_meta(get_the_id(), 'website_url', true) != '') {
			
				echo esc_html__('See more at', 'crexis') . ' <span class="colored">' . esc_html(get_post_meta(get_the_id(), 'website_url', true)) . '</span>';
				
			} else {
			
				echo esc_textarea(get_post_meta(get_the_id(), 'name', true));
				
			}
				
			?>
			</p>
			<?php } ?>
		</div>
		
		<?php
		
		endwhile; endif; wp_reset_postdata();
		
		
	echo '</div></div>';

	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('testimonials');
add_shortcode('testimonials', 'crexis_testimonials');