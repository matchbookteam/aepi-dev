<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Video Lightbox
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_video_lightbox($atts, $content = null) {
	extract(shortcode_atts(array(
		"link" => '',
		"description" => 'This is our video!',
		"title" => "Our Video",
		"length" => "03:46"
	), $atts));	
	
	wp_enqueue_script('magnific-popup', '', '', '', true);
	wp_enqueue_style('magnific-popup');
	
	
	$output = '<div class="video-lightbox"><div class="video-lightbox-inner"><a href="'.$link.'" class="video-link mp-video"><i class="fa fa-play"></i></a>';

	if($title) {
		$output .= '<h1 class="video-lightbox-title">'.$title.'</h1>';
	}

	if($description) {
		$output .= '<p class="video-lightbox-description">'.$description.'</p>';
	}
	
	if($length) {
		$output .= '<p class="video-lightbox-length">'.$length.'</p>';
	}

	$output .= '</div></div>';

	
	return $output;
	
}
remove_shortcode('video_lightbox');
add_shortcode('video_lightbox', 'crexis_video_lightbox');