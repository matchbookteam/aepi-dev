<?php

// Recent Work Shortcode

if( !function_exists("crexis_portfolio_carousel") ) {

	function crexis_portfolio_carousel($atts, $content = null) {
		extract(shortcode_atts(array(
			"cats" => '',
			"posts_nr" => '8',
			"type" => "classic",
			"hover_style" => 'dark',
			"cols" => 3,
			"cols_category" => 5,
			"thumb_space" => 'yes',
			"thumb_size" => 'portrait',
			"hover_style" => 'dark',
			"autoplay" => '',
			"dots" => "false",
			"nav" => "false",
			"nav_position" => "top_right",
			"carousel_title" => '',
			"love" => 'yes',
			"orderby" => 'date',
			"order" => 'DESC',
			"title_font" => 'georgia',
			"autoplay" => 'false'
		), $atts));
		
		wp_enqueue_script('owl-carousel', '', '', '', true);
		wp_enqueue_style('owl-carousel');
	
		ob_start();
		
		if($thumb_space == 'no') {
			$margin = 0;
		}
		
		$hover_class = ' dark-hover';
		
		if($hover_style == 'light') {
			$hover_class = ' white-hover';
		}
		
		echo '<div class="vntd-carousel-holder">';
		
		if($type == 'category') {
		
			$size = 'crexis-portrait';
			
			if($thumb_size == 'landscape') {
				$size = 'crexis-portfolio-square';
			}
		
			?>
			
			<div class="categories fullwidth">
				<!-- Boxes -->
				<div class="category-boxes double-slider relative clearfix" data-cols="<?php echo $cols_category; ?>">
				
					<?php
					
					if($cats == '') {
						
						// show all categories	
						
					}
					
					// To remove:
					
					$categories = get_terms('project-type');
					
					foreach ( $categories as $category ) {
					
						wp_reset_postdata();
						
						$post_in = '';
						$project_type = $category->slug;

						if( class_exists('crexisDemo') ) {
							$post_in = crexisDemo::crexisCategoryPosts( $category->term_id );
							$orderby = 'post__in';
							$project_type = '';
						}
						
						$args = array(
							'posts_per_page' => 3,
							'project-type'	=> $project_type,
							'post_type' => 'portfolio',
							'orderby'	=> $orderby,
							'order' => $order,
							'post__in' => $post_in
						);
						
						$the_query = new WP_Query($args); 
						
						?>
						
						<div class="box animated" data-animation="fadeIn" data-animation-delay="100">
							<!-- Category Inner Slider -->
							<div class="category-inner-slider inner-slider">
							
							<?php
							
							if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
							
							$img_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), $size);
							$thumb_url = $img_url[0];
							
							$post_link = get_permalink( get_the_ID() );
							
							$post_link_type = get_post_meta(get_the_ID(),'link_type',TRUE);
							
							if($post_link_type == 'external' && get_post_meta(get_the_ID(),'portfolio_external_url',TRUE)) {
								$post_link = esc_url(get_post_meta(get_the_ID(),'portfolio_external_url',TRUE));
							}
								
							?>
							
							<a href="<?php echo esc_url( $post_link ); ?>" class="category-box-link">
							
							<div class="image">
								<!-- Image SRC -->
								<img src="<?php echo $img_url[0]; ?>" alt="<?php the_title(); ?>">
							</div>
							
							</a>
								
							<?php
							
							endwhile; endif; wp_reset_postdata();
							
							?>
						
							</div>
							<!-- End Category Inner Slider -->
							<?php
							
							$font_family = 'georgia';
							
							if( $title_font == 'primary' ) {
								$font_family = 'font-primary';
							}
							?>
							<!-- Box Texts -->
							<div class="box-texts <?php echo esc_attr( $font_family ); ?> white">
								<!-- Header -->
								<h2 class="t-shadow <?php echo esc_attr( $font_family ); ?>"><?php echo $category->name; ?></h2>
								<!-- Description -->
								
								<?php if(isset($category->description)) { ?>
								<p class="t-shadow"><?php echo esc_textarea($category->description); ?></p>
								<?php } ?>
								
							</div>
							<!-- End Box Texts -->
						</div>
										
						<?php
						
					}
					
					
					
					?>
					
					
				</div>
				
			</div>
			
			
			<?php
		
		} else {
	
			echo '<div class="vntd-portfolio-carousel featured-slider-boxes box-carousel ' . crexis_column_items($cols) . $hover_class . ' no-pagination t-center" data-autoplay="' . esc_attr( $autoplay ) . '">';
		
			$size = 'crexis-sidebar-square';
						
			wp_reset_postdata();
			
			$post_in = '';
			
			if( class_exists('crexisDemo') ) {
				$post_in = crexisDemo::crexisCarouselPosts();
				$orderby = 'post__in';
			}
			
			$args = array(
				'posts_per_page' => $posts_nr,
				'project-type'		=> $cats,
				'post_type' => 'portfolio',
				'orderby'	=> $orderby,
				'order' => $order,
				'post__in' => $post_in
			);
			
			$the_query = new WP_Query($args); 
			
			if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
			
			$img_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), $size);
			$thumb_url = $img_url[0];
			
			$post_link = get_permalink();
			
			$post_link_type = get_post_meta(get_the_ID(),'link_type',TRUE);
			
			if($post_link_type == 'external' && get_post_meta(get_the_ID(),'portfolio_external_url',TRUE)) {
				$data_content = '';
				$ajax_class = '';
				$post_link = esc_url(get_post_meta(get_the_ID(),'portfolio_external_url',TRUE));
			}
			
			?>
			
			<!-- Box -->
			<a href="<?php echo $post_link; ?>" class="box ex-link">
				<!-- Box Image -->
				<div class="item_image">
					<!-- Image SRC -->
					<img src="<?php echo esc_url($thumb_url) ?>" alt="Crexis" />
				</div>
				<!-- Item Texts -->
				<div class="item_texts">
					<!-- Header -->
					<h2 class="light"><?php the_title(); ?></h2>
					<!-- Description -->
					<p class="light"><?php crexis_portfolio_overlay_categories(); ?></p>
				</div>
			</a>
			<!-- End Box -->
			
			<?php
			
			
			endwhile; endif; wp_reset_postdata();		
			
			echo '</div>';
		
		}
		
		echo '</div>';
		
		$content = ob_get_contents();
		ob_end_clean();
		
		return $content;
		
	}
	
}

remove_shortcode('portfolio_carousel');
add_shortcode('portfolio_carousel', 'crexis_portfolio_carousel');