<?php

// Blog Posts Carousel

function crexis_blog_carousel($atts, $content = null) {
	$posts_nr = $ids = $lightbox = $nav = $nav_position = $dots = $margin = $cols = $autoplay = $thumb_space = $style = $excerpt = $carousel_title = $cats = '';
	extract(shortcode_atts(array(
		"posts_nr" => '',
		"cats" => '',
		"ids" => '',
		"dots" => 'false',
		"margin" => 30,
		"cols" => 3,
		"autoplay" => 'true',
		"thumb_space" => 'yes',
		"excerpt" => 'yes',
		"autoplay" => 'true'
	), $atts));
	

	wp_enqueue_script('owl-carousel', '', '', '', true);
	wp_enqueue_style('owl-carousel');
	
	$margin = 30;
	
	if($thumb_space == 'no') {
		$margin = 0;
	}
	
	ob_start();	
	
	echo '<div class="vntd-carousel-holder news">';
	
	echo '<div class="vntd-carousel vntd-blog-carousel blog-slider t-left box-carousel ' . crexis_column_items($cols) . ' vntd-blog" data-autoplay="' . esc_attr( $autoplay ) . '">';	
	
		wp_reset_postdata();
		
		$args = array(
			'posts_per_page' 	=> $posts_nr,
			'cat'				=> $cats,
			'orderby'			=> 'slug'
		);		
		
		$the_query = new WP_Query($args);
		$i = 0;
		if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
		
		if(!has_post_thumbnail()) continue;
		$i++;
		
		$size = 'crexis-blog-thumb';
		
		?>

		<div class="item box">
			
			<?php
			
			$post_format = get_post_format(get_the_ID());
			
			if($post_format == 'gallery' && get_post_meta(get_the_ID(),'gallery_images',TRUE)) {
			
				$gallery_images = get_post_meta(get_the_ID(), 'gallery_images',TRUE);
				
				$ids = explode(",", $gallery_images);
				
				echo '<div class="inner-slider">';
				
				foreach($ids as $id) {
				
					$imgurl = wp_get_attachment_image_src($id, $size);
					
					echo '<div class="image"><a href="'.get_permalink(get_the_ID()).'"><img src="'.esc_url($imgurl[0]).'" alt="'.esc_attr(get_post_meta($id, '_wp_attachment_image_alt', true)).'"></a></div>';
				
				}
				
				echo '</div>';
			
			} else {
			
				$img_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), $size );
				$thumb_url = $img_url[0];
			
				echo '<div class="image"><a href="'.get_permalink(get_the_ID()).'"><img src="' . esc_url($thumb_url) . '" alt="blog_image" /></a></div>';
			
			}
			
			?>

			<!-- Post Details -->
			<div class="details extra-light">
				<!-- Header -->
				<h3 class="no-padding"><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo get_the_title(get_the_ID()); ?></a></h3>
				<!-- Post Details -->
				<p class="post-details">
					<i class="fa fa-clock-o"></i>
					<?php esc_html_e("on",'crexis'); ?> <?php the_time('d M, Y'); ?>
					<?php
					if( crexis_option('blog_meta_author') != false ) {
					?>
					<i class="fa fa-user"></i>
					<?php esc_html_e("Posted By",'crexis'); ?> <span class="colored"><?php the_author(); ?></span>
					<?php
					}
					?>
				</p>
				<!-- Post Message -->
				<p class="post_message"><?php echo crexis_excerpt(33, false); ?></p>
				<!-- Red More Button -->
				<a href="<?php echo get_permalink(get_the_ID()); ?>" class="post_read_more_button ex-link uppercase"><?php esc_html_e("Read More",'crexis'); ?></a>
			</div>
			<!-- End Post Details -->
		</div>	
							
		<?php		 
		endwhile; endif; wp_reset_postdata();
		
	echo '</div></div>';

	$content = ob_get_contents();
	ob_end_clean();
	
	return $content;
	
}
remove_shortcode('blog_carousel');
add_shortcode('blog_carousel', 'crexis_blog_carousel');