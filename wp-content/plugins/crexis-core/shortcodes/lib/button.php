<?php

// Shortcode Processing


function crexis_button($atts, $content = null) {

	$defaultFont = 'fontawesome';
	$defaultIconClass = 'fa fa-info-circle';
	
	extract(shortcode_atts(array(
		"label" => 'Text on the button',
		"label2" => '',
		"url" => '',
		"target" => '_self',
		"color" => 'accent',
		"hover" => 'accent2',
		"customcolor" => '',
		"size" => 'medium',
		"style" => 'solid-btn',
		"text_transform" => 'uppercase',
		"border_radius" => 'default',
		"scroll" => '',
		"align" => '',
		"text_align" => 'center',
		"icon_enabled" => '',
		"icon" => 'heart-o',
		"icon_type" => $defaultFont,
		"icon_fontawesome" => $defaultIconClass,
		"icon_typicons" => '',
		"icon_openiconic" => '',
		"icon_entypo" => '',
		"icon_linecons" => '',
		"el_class" => ''
	), $atts));		
	
	$icon = str_replace('fa-','',$icon);
	vc_icon_element_fonts_enqueue( $icon_type );
	
	$iconClass = isset( ${"icon_" . $icon_type} ) ? ${"icon_" . $icon_type} : $defaultIconClass;
	
	$custom_style = $scroll_class = $align_class = $icon_class = $icon_element = '';
	
	if($color == "custom") {
		$custom_style .= ' style="background-color:'.$customcolor.';border-color:'.$customcolor.';';
		if($style == "stroke") {
			$custom_style .= 'color:'.$customcolor.';';
		}
		$custom_style .= '" ';
		
	} elseif(strpos($color,'#') !== false) {
		$custom_style .= ' style="background-color:'.$color.';border-color:'.$color.';';
		if($style == "stroke") {
			$custom_style .= 'color:'.$customcolor.';';
		}
		$custom_style .= '" ';
	}

	if($align == 'center') {
		$align_class = ' btn-center';
	}
	
	// Button Size
	$size_class = $hover_class = $text_transform_class = $radius_class = $color_class = '';

	
	$size_class = ' ' . $size . '-btn';
	
	if($border_radius == 'circle') {
		$radius_class = ' circle-btn';
	}
	
	if($style == 'solid-btn') {
		$hover_class = ' btn-hover-' . esc_attr($hover);
	} elseif($style == 'border-btn') {
		$ccolor = $hover;
		if($color == "custom") $ccolor = "accent2";
		$hover_class = ' btn-hover-' . esc_attr($ccolor);
	}
	
	$color_class = ' btn-'. esc_attr($color);
	
	if($style == 'light-button' || $style == 'dark-button') {
		$color_class = '';
		$hover_class = '';
	}
	
	if($text_transform != 'none') $text_transform_class = ' uppercase';
	
	if($icon_enabled == "yes") {
		$icon_class = ' btn-icon';
		$icon_element = '<i class="'.$iconClass.'"></i>';
	}
	
	if(!$style) $style = 'default';
	
	$text_align_class = ' text-align-center';
	
	if($text_align == 'left') $text_align_class = ' text-align-left';
	
	$label2_holder = '';
	if($size == 'xlarge' && $label2 || $size == 'large' && $label2 || $size == 'medium' && $label2) $label2_holder = '<span>' . esc_textarea( $label2 ) . '</span>';

	$extra_class = '';
	
	if($el_class != '') $extra_class = ' ' . esc_attr( $el_class );
	
	return '<div class="btn-holder' . $extra_class . '"><a href="' . esc_url($url) . '" class="btn '.esc_attr($style) . $color_class . $size_class . $align_class . $text_align_class . $hover_class . $radius_class . $icon_class . $text_transform_class . '" target="' . $target . '"' . $custom_style . '>' . $icon_element . $label . $label2_holder . '</a></div>';

}
remove_shortcode('vntd_button');
add_shortcode('vntd_button', 'crexis_button');