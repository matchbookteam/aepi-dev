<?php

// Icon Box Shortcode

function crexis_icon_box($atts, $content = null) {

	$defaultFont = 'fontawesome';
	$defaultIconClass = 'fa fa-info-circle';
	
	extract(shortcode_atts(array(
		"icon" => 'heart-o',
		"icon_type" => $defaultFont,
		"icon_fontawesome" => $defaultIconClass,
		"icon_typicons" => '',
		"icon_openiconic" => '',
		"icon_entypo" => '',
		"icon_linecons" => '',
		"style" => 'big-centered-circle',
		"color" => 'accent',
		"title" => 'Icon Box Title',
		"text" => 'Icon Box text content, feel free to change it!',	
		"url" => '',
		"link_button" => 'no',
		"link_button_label" => 'Read more',
		"target" => '_self',
		"text_style" => 'fullwidth',		
		"animated" => '',
		"animation_delay" => 100			
	), $atts));
	
	$icon = str_replace('fa-','',$icon);
	vc_icon_element_fonts_enqueue( $icon_type );
	
	$iconClass = isset( ${"icon_" . $icon_type} ) ? ${"icon_" . $icon_type} : $defaultIconClass;
	
	$aligned_class = ' t-center';
	
	if (strpos($style,'left') !== false) {
		$aligned_class = ' icon-box-aligned icon-box-aligned-left';
	} elseif(strpos($style,'right') !== false) {
		$aligned_class = ' icon-box-aligned icon-box-aligned-right';
	}
	
	$color_class = '';
	
	if($style == 'medium-left-circle' || $style == 'medium-right-circle') {
		$aligned_class .= ' clearfix';
		$color_class = ' bg-' . $color;
	}
	
	$animated_class = $animated_data = '';
	
	if($animated != 'no') {
		$animated_class = Crexis_Core::get_animated_class();
		$animated_data = ' data-animation="fadeIn" data-animation-delay="' . $animation_delay . '"';
	}
	
	$url_extra_class = $anchor_attr = '';
	
	if($url) {
		$url_extra_class = ' icon-box-with-link';
		$anchor_attr = ' href="'.$url.'"';
	}
	
	$output = '<div class="vntd-icon-box box icon-box-' . $style . $animated_class . $aligned_class.$url_extra_class.'"'.$animated_data.'>';
	
	if($url) $output .= '<a href="' . esc_url($url) . '" target="' . esc_attr($target) . '">';
	
	$output .= '<div class="icon-box-icon box-icon fullwidth t-center normal">';
	$output .= '<div class="changeable-icon' . $color_class . '" target="' . esc_attr($target) . '">';
	$output .= '<i class="' . $iconClass . '"></i>';
	$output .= '</div>';
	$output .= '</div><div class="icon-box-content">';
	if($title) $output .= '<h4 class="icon-box-title box-header no-padding">' . $title . '</h4>';	
	$output .= '<p class="icon-description no-padding no-margin">'.esc_textarea($text).'</p>';
	
	$output .= '</div>';
	
	if($url) $output .= '</a>';
		
	
	
	if($url && $style == 'big-centered-square' || $url && $style == 'big-centered-circle' || $url && $style == 'big-centered-circle-outline') {
		$output .= '<a href="' . esc_url($url) . '" target="' . esc_attr($target) . '" class="box-button circle"><i class="fa fa-plus"></i></a>';
	}
	
	$output .= '</div>';
	
	return $output;
	
}
remove_shortcode('icon_box');
add_shortcode('icon_box', 'crexis_icon_box');