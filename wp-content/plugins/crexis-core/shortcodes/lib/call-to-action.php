<?php

function crexis_cta($atts, $content=null) {
	extract(shortcode_atts(array(
		"button1_title" => 'Click me!',
		"button1_subtitle" => 'Small text',
		"button1_url" => 'http://',
		"button1_style" => 'default',
		"button2_title" => '',
		"button2_subtitle" => '',
		"button2_url" => 'http://',		
		"text_color" => 'dark',
		"button1_color" => 'accent2',
		"button2_color" => 'accent2',
		"style" => 'classic',
		"subtitle" => "I'm the CTA subtitle, feel free to change me!",
		"heading" => 'This is the main heading.',
		"margin_bottom" => '',
		"extra_class" => '',
		"heading_font_family" => 'default'
	), $atts));
	
	$extra_style = $extra_style_color = $return = '';
	
	if($subtitle) $extra_class = ' cta-with-subtitle';
	
	$return .= '<div class="vntd-cta vntd-cta-style-'.$style.' vntd-cta-color-'.$text_color.' bottom-page-texts relative t-center ">';
	
	$heading_font_class = '';
	
	if($heading_font_family == 'georgia') $heading_font_class = ' georgia';
	
	$return .= '<h2 class="vntd-cta-heading' . $heading_font_class . '">' . esc_textarea($heading) . '</h2>';
	
	if($subtitle && $style == 'classic') {
		$return .= '<p class="normal raleway">' . esc_textarea($subtitle) . '</p>';
	}
	
	if($button1_title || $button2_title) {
	
		$return .= '<div class="bottom-page-buttons">';
		
		$button1_class = $button2_class = '';
		$button1_hover_class = $button2_hover_class = ' btn-hover-accent';
		
		if($style == "classic") {
		
			if($button1_color == 'accent') {
				$button1_hover_class = ' btn-hover-accent2';
			}
			
			$button1_class = 't-center scroll content-button btn-' . esc_attr($button1_color) . $button1_hover_class;
			
			if($button2_color == 'accent') {
				$button2_hover_class = ' btn-hover-accent2';
			}
			
			$button2_class = 't-center scroll content-button btn-' . esc_attr($button2_color) . $button2_hover_class;
			
		} else {
			$button1_hover_class = $button1_hover_class = '';
			$button1_class = $button2_class = 'home-button dark-button inline-block mt-20';
		}
		
		if($button1_title) {
			$return .= '<a href="' . esc_url($button1_url) . '" class="vntd-cta-btn1 ' . $button1_class . '"><p class="uppercase">' . esc_textarea($button1_title) . '</p>';
			
			if($button1_subtitle && $style == 'classic') {
				$return .= '<p class="normal">' . $button1_subtitle . '</p>';
			}
			
			$return .= '</a>';
		}
		
		if( $button2_title ) {
		
			$return .= '<a href="' . esc_url( $button2_url ) . '" class="vntd-cta-btn2 ' . $button2_class . '"><p class="uppercase">' . esc_textarea( $button2_title ) . '</p>';
			
			if( $button2_subtitle && $style == 'classic' ) {
				$return .= '<p class="normal">' . $button2_subtitle . '</p>';
			}
			
			$return .= '</a>';
		}
		
		$return .= '</div>';
	}
	

	$return .= '</div>';	

	return $return;
}
remove_shortcode('cta');
add_shortcode('cta', 'crexis_cta');  