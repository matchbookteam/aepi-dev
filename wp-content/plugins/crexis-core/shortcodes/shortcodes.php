<?php

define('CREXIS_SHORTCODES', plugin_dir_path( __FILE__ ));

// Add TinyMCE Button

add_action('init', 'veented_add_tinymce_button');

function veented_add_tinymce_button() {
	//if(strstr($_SERVER['REQUEST_URI'], 'wp-admin/post-new.php') || strstr($_SERVER['REQUEST_URI'], 'wp-admin/post.php')) {
			add_filter('mce_external_plugins', 'veented_add_tinymce_plugin');  
			add_filter('mce_buttons', 'veented_register_tinymce_button');  
	//}
}    

function veented_register_tinymce_button($buttons) {
   array_push($buttons, 'separator', "veented_shortcodes_button"); 
   //array_push($buttons, "waxom_visual_button"); 
   return $buttons;
}  

function veented_add_tinymce_plugin($plugin_array) {  
   $plugin_array['veented_shortcodes_button'] = plugins_url() . '/crexis-core/shortcodes/tinymce/tinymce-quick-shortcodes.js';
   //$plugin_array['waxom_visual_button'] = get_template_directory_uri() . '/framework/shortcodes/tinymce/tinymce-visual-shortcodes.js';
   return $plugin_array;  
}

// Load complex shortcodes

include_once( CREXIS_SHORTCODES . '/lib/google-map.php');
include_once( CREXIS_SHORTCODES . '/lib/button-slider.php');
include_once( CREXIS_SHORTCODES . '/lib/blog.php');
include_once( CREXIS_SHORTCODES . '/lib/carousel-portfolio.php');
include_once( CREXIS_SHORTCODES . '/lib/carousel-blog.php');
include_once( CREXIS_SHORTCODES . '/lib/carousel-boxes.php');
include_once( CREXIS_SHORTCODES . '/lib/carousel-testimonials.php');
include_once( CREXIS_SHORTCODES . '/lib/carousel-logos.php');
include_once( CREXIS_SHORTCODES . '/lib/button.php');
include_once( CREXIS_SHORTCODES . '/lib/counter.php');
include_once( CREXIS_SHORTCODES . '/lib/contact-block.php');
include_once( CREXIS_SHORTCODES . '/lib/contact-form.php');
include_once( CREXIS_SHORTCODES . '/lib/call-to-action.php');
include_once( CREXIS_SHORTCODES . '/lib/fullscreen-slider.php');
include_once( CREXIS_SHORTCODES . '/lib/fullscreen-features.php');
include_once( CREXIS_SHORTCODES . '/lib/icon.php');
include_once( CREXIS_SHORTCODES . '/lib/icon-box.php');
include_once( CREXIS_SHORTCODES . '/lib/list.php');
include_once( CREXIS_SHORTCODES . '/lib/grid-portfolio.php');
include_once( CREXIS_SHORTCODES . '/lib/grid-team.php');
include_once( CREXIS_SHORTCODES . '/lib/hero-section.php');
include_once( CREXIS_SHORTCODES . '/lib/portfolio-details.php');
include_once( CREXIS_SHORTCODES . '/lib/portfolio-timeline.php');
include_once( CREXIS_SHORTCODES . '/lib/pricing-box.php');
include_once( CREXIS_SHORTCODES . '/lib/product-features.php');
include_once( CREXIS_SHORTCODES . '/lib/section.php');
include_once( CREXIS_SHORTCODES . '/lib/social-icons.php');
include_once( CREXIS_SHORTCODES . '/lib/veented-slider.php');
include_once( CREXIS_SHORTCODES . '/lib/video-lightbox.php');


// - - -
// Function removing extra br and p tags
// - - -

function crexis_do_shortcode($content) {
    $array = array('<p>[' => '[','<br />[' => '[', '<br>[' => '[', ']</p>' => ']', ']<br />' => ']', ']<br>' => ']');
    $content = strtr($content, $array);
    return do_shortcode($content);
}

// - - - - - - - - - -
// Separator
// - - - - - - - - - -

function crexis_separator($atts, $content=null){
	extract(shortcode_atts(array(
		"type" => '',
		"style" => 'default',
		"label" => '',
		"align" => 'center',
		"space_height" => ''			
	), $atts));
	
	$separator_class = $separator_label = '';
	
	if($type != "space") {
		if($style != "default") {
			$separator_class .= ' separator-shadow';
		}
		if($type == "fullwidth") {
			$separator_class .= ' separator-fullwidth';
		}
		if($label) {
			$separator_label = '<div>'.$label.'</div>';
			$separator_class .= ' separator-text-align-'.$align;
		}
		$output = '<div class="separator'.esc_attr($separator_class).'">'.esc_html($separator_label).'</div>';
	} else {
		if($space_height != 40) {
			$space_style = 'style="height:'.esc_attr($space_height).'px;"';
		}
		$output = '<div class="white-space"'.esc_attr($space_style).'></div>';
	}
	
	
	return $output;
	
}
remove_shortcode('separator');
add_shortcode('separator', 'crexis_separator');

function crexis_spacer($atts, $content=null){
	extract(shortcode_atts(array(
		"height" => '40'			
	), $atts));
		
	if($height != 40) {
		$height_style = 'style="height:'.esc_attr($height).'px;"';
	}
	
	return '<div class="spacer"'.esc_attr($height_style).'></div>';	
	
}
remove_shortcode('spacer');
add_shortcode('spacer', 'crexis_spacer');


// - - - - - - - - - -
// Typography
// - - - - - - - - - -

function crexis_heading($atts, $content=null) {
	extract(shortcode_atts(array(
		"title" => 'Main Heading Text',
		"subtitle" => 'This is a subtitle, feel free to change it!',
		"uptitle" => '',
		"animated" => 'no',
		"heading_margin_bottom" => '35',
		"separator"		=> 'yes',
		"font"			=> 'primary', // Primary, secondary, Georgia
		"font_size"		=> '30',
		"font_weight"	=> 'default',
		"text_transform" => 'uppercase', // Uppercase
		"text_align"	=> 'center',
		"italic"		=> 'no',
		"el_class"		=> ''
	), $atts));
	
	$animation_class = $animation_data = $margin_style = $font_class = '';
	
	if($animated != 'no') {
		$animation_class = Crexis_Core::get_animated_class();
		$animation_data = ' data-animation="fadeIn" data-animation-delay="100"';
	}
	$margin_bottom = str_replace('px','',$heading_margin_bottom);
	if($margin_bottom != 30 && $margin_bottom != '') {		
		$margin_style = ' style="margin-bottom:'.esc_attr($margin_bottom).'px;"';
	}	
	
	$font_class = ' font-primary';
	
	if($font == 'secondary') {
		$font_class = ' font-secondary';
	} elseif($font == 'georgia') {
		$font_class = ' georgia';
	}
	
	$text_transform_class = ' uppercase';
	
	if($text_transform != 'uppercase') $text_transform_class = '';
	
	$font_weight_class = ' font-weight-default';
	
	if($font_weight != 'default') $font_weight_class = ' font-weight-' . esc_attr($font_weight);
	
	$separator_class = ' heading-separator';
	if($separator == 'bottom') {
		$separator_class = ' heading-separator-bottom';
	} elseif($separator == 'no') {
		$separator_class = ' heading-no-separator';
	}
	
	$font_size_class = '';
	if($font_size != 'default') $font_size_class = ' font-size-' . esc_attr($font_size);
	
	// Detect highlights
	
	$title = esc_textarea($title);
	$title = str_replace("(b)",'<span class="colored">', $title);
	$title = htmlspecialchars_decode(str_replace("(/b)",'</span>', $title));
	
	$extra_class = '';
	
	if($italic == 'yes') $extra_class = ' special-heading-italic';
	
	if($el_class != '') $extra_class .= ' ' . esc_attr( $el_class );
	
	$return = '<div class="vntd-special-heading special-heading-align-'.esc_attr($text_align) . $separator_class . $extra_class . '"'.$margin_style.'>';
	
	if($uptitle != '') {
		$return .= '<h4 class="header-first ' . esc_attr($font_class) . '">' . esc_textarea($uptitle) . '</h4>';
	}
	
	$return .= '<h1 class="header '.esc_attr($font_class).esc_attr($animation_class) . $text_transform_class . $font_size_class . $font_class . $font_weight_class . '" '.$animation_data.'>'.$title.'</h1>';
	
	
	if($subtitle) {
		$return .= '<p class="subtitle light '.esc_attr($animation_class).'" '.$animation_data.'>'.esc_textarea($subtitle).'</p>';
	}
	$return .= '</div>';
	return $return;

}

function crexis_callout_box($atts, $content=null) {
	extract(shortcode_atts(array(
		"title" => '',
		"subtitle" => ''
	), $atts));
	
	$return = '<div class="vntd-callout-box bs-callout bs-callout-north"><h2 class="colored uppercase font-primary">'.$title.'</h2><p>'.$subtitle.'</p></div>';

	return $return;

}


function crexis_highlight($atts, $content=null) {
	extract(shortcode_atts(array("color" => '',"bgcolor" => ''), $atts));
	
	if($color || $bgcolor) {
		$color_class = 'style="background-color:'.esc_attr($bgcolor).';color:'.esc_attr($color).'"';
	}	
	
	return '<span class="vntd-highlight vntd-accent-bgcolor"'.$color_class.'>'.$content.'</span>';
}

function crexis_alternative($atts, $content=null) {
	
	return '<div class="vntd-alternative-section">'.$content.'</div>';
}

function crexis_dropcap1($atts, $content=null) {
	extract(shortcode_atts(array("color" => '',"style" => 'style1'), $atts));
	
	$color_class = '';
	
	if($color && $color != 'accent') {
		if($style == 'style1') {
			$color_class = 'style="color:'.esc_attr($color).'"';
		} else {
			$color_class = 'style="background-color:'.esc_attr($color).'"';
		}
	}
	
	return '<span class="vntd-dropcap dropcap-'.esc_attr($style).'"'.$color_class.'>'.$content.'</span>';
}

function crexis_quote($atts, $content=null) {
	extract(shortcode_atts(array("style" => 'style1', 'author' => ''), $atts));
	
	return '<blockquote class="blockquote-'.esc_attr($style).' vntd-custom-blockquote"><div class="blockquote-content">'.$content.'</div><div class="blockquote-author">'.$author.'</div></blockquote>';
}

function crexis_tooltip($atts, $content=null) {
	extract(shortcode_atts(array("style" => 'style1', "label" => 'Your text'), $atts));
	
	$length = strlen($label)*8;
	$lengthHalf = $length/2;
	return '<span class="vntd-tooltip tooltip-'.esc_attr($style).'">'.$content.'<span class="vntd-tooltip-label" style="width:'.$length.'px;margin-left:-'.$lengthHalf.'px;">'.$label.'</span></span>';
}

function crexis_text($atts, $content=null) {
	extract(shortcode_atts(array("size" => '20'), $atts));
	
	return '<p class="vntd-text" style="font-size:'.str_replace('px','',esc_attr($size)).'px;">'.$content.'</p>';
}

remove_shortcode('tooltip');
remove_shortcode('highlight');
remove_shortcode('alternative');
remove_shortcode('dropcap1');
remove_shortcode('dropcap2');
remove_shortcode('special_heading');
remove_shortcode('quote');
remove_shortcode('callout_box');
remove_shortcode('text');

add_shortcode('tooltip', 'crexis_tooltip');
add_shortcode('special_heading', 'crexis_heading');
add_shortcode('dropcap', 'crexis_dropcap1');
add_shortcode('alternative', 'crexis_alternative');
add_shortcode('highlight', 'crexis_highlight');
add_shortcode('quote', 'crexis_quote');
add_shortcode('tooltip', 'crexis_tooltip');
add_shortcode('callout_box', 'crexis_callout_box');
add_shortcode('text', 'crexis_text');