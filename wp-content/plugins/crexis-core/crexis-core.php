<?php

	/*
	
	Plugin Name: 	Crexis Core
	Plugin URI: 	http://themeforest.net/user/Veented
	Description: 	Core functionalities for Crexis Theme.
	Version: 		1.8.0
	Author: 		Veented
	Author URI: 	http://themeforest.net/user/Veented
	License: 		GPL2
	
	*/
	
   	// Load Custom Post Types
	
   	require_once('custom-post-types/portfolio/portfolio-functions.php'); 		// Portfolio Post Type
   	require_once('custom-post-types/team/team-functions.php'); 					// Team Member Post Type
   	require_once('custom-post-types/testimonials/testimonials-functions.php'); 	// Testimonial Post Type
   	require_once('post-likes/post-like.php'); 
   
   	// Shortcodes
   
   	require_once('shortcodes/shortcodes.php');
   	
   	// Redux Importer
   	
   	if(!function_exists('redux_register_custom_extension_loader')) :
   	    function redux_register_custom_extension_loader($ReduxFramework) {
   	        $path    = dirname( __FILE__ ) . '/demo-importer/';
   	            $folders = scandir( $path, 1 );
   	            foreach ( $folders as $folder ) {
   	                if ( $folder === '.' or $folder === '..' or ! is_dir( $path . $folder ) ) {
   	                    continue;
   	                }
   	                $extension_class = 'ReduxFramework_Extension_' . $folder;
   	                if ( ! class_exists( $extension_class ) ) {
   	                    // In case you wanted override your override, hah.
   	                    $class_file = $path . $folder . '/extension_' . $folder . '.php';
   	                    $class_file = apply_filters( 'redux/extension/' . $ReduxFramework->args['opt_name'] . '/' . $folder, $class_file );
   	                    if ( $class_file ) {
   	                        require_once( $class_file );
   	                    }
   	                }
   	                if ( ! isset( $ReduxFramework->extensions[ $folder ] ) ) {
   	                    $ReduxFramework->extensions[ $folder ] = new $extension_class( $ReduxFramework );
   	                }
   	            }
   	    }
   	    // Modify {$redux_opt_name} to match your opt_name
   	    add_action("redux/extensions/crexis_options/before", 'redux_register_custom_extension_loader', 0);
   	
   	endif;
   	
   	if ( !function_exists( 'wbc_extended_example' ) ) {
   		function wbc_extended_example( $demo_active_import , $demo_directory_path ) {
   			reset( $demo_active_import );
   			$current_key = key( $demo_active_import );
   			/************************************************************************
   			* Import slider(s) for the current demo being imported
   			*************************************************************************/
   			
   			if ( class_exists( 'RevSlider' ) ) {
   				//If it's demo3 or demo5
   				
   				$wbc_sliders_array = array(
   					'home03__home-revolution-slider-1' => 'crexis_slider1.zip', //Set slider zip name
   					'home04__home-revolution-slider-2' => 'crexis-slider-2.zip',
   					'home05__home-revolution-slider-3' => 'crexis-slider-3.zip',
   					'home06__home-revolution-slider-4' => 'crexis-slider-4.zip', 
   					'home07__home-revolution-slider-5' => 'crexis-slider-5.zip', 
   					'onepager03__home-revolution-slider-1' => 'crexis_slider1.zip',
   					'onepager04__home-revolution-slider-2' => 'crexis-slider-2.zip',
   					'onepager05__home-revolution-slider-3' => 'crexis-slider-3.zip',
   					'onepager06__home-revolution-slider-4' => 'crexis-slider-4.zip', 
   					'onepager07__home-revolution-slider-5' => 'crexis-slider-5.zip', 
   					'home11__home-fullwidth-slider-1' => 'fullwidth-slider-1.zip',
   					'home12__home-fullwidth-slider-2' => 'fullwidth-slider-2.zip',
   					'home13__home-fullwidth-slider-3' => 'fullwidth-slider-3.zip',
   					'home14__home-fullwidth-slider-dark-4' => 'fullwidth-slider-4.zip',
   					'onepager11__home-fullwidth-slider-1' => 'fullwidth-slider-1.zip',
   					'onepager12__home-fullwidth-slider-2' => 'fullwidth-slider-2.zip',
   					'onepager13__home-fullwidth-slider-3' => 'fullwidth-slider-3.zip',
   					'onepager14__home-fullwidth-slider-4' => 'fullwidth-slider-4.zip'
   				);
   				
   				if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_sliders_array ) ) {
   					
   					$wbc_slider_import = $wbc_sliders_array[$demo_active_import[$current_key]['directory']];
   					
   					$sliders_directory = dirname( __FILE__ ) . '/demo-importer/wbc_importer/demo-sliders/';
   					
   					//if ( file_exists( $demo_directory_path.$wbc_slider_import ) ) {
   						
   					if ( file_exists( $sliders_directory.$wbc_slider_import ) ) {
   						$slider = new RevSlider();
   						//$slider->importSliderFromPost( true, true, $demo_directory_path.$wbc_slider_import );
   						$slider->importSliderFromPost( true, true, $sliders_directory.$wbc_slider_import );
   					}
   					
   				}
   			}
   			/************************************************************************
   			* Setting Menus
   			*************************************************************************/
   			// If it's demo1 - demo6
   			$wbc_menu_array = array( '0demo-main' );
   			
   			if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_array ) ) {
   			
   				$top_menu = get_term_by( 'name', 'Main Navigation', 'nav_menu' );
   				
   				if ( isset( $top_menu->term_id ) ) {
   					set_theme_mod( 'nav_menu_locations', array(
   							'primary' => $top_menu->term_id
   						)
   					);
   				}
   				
   			}
   			
   			// Set other menus
   			
   			if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) ) {
   			
   				$wbc_menu_onepager_array = array( 'onepager-rainyday' );
   				
				$top_menu = get_term_by( 'name', 'One Pager 2', 'nav_menu' );
				
				if( in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_onepager_array ) ) {
				
					if ( isset( $top_menu->term_id ) ) {
						set_theme_mod( 'nav_menu_locations', array(
								'primary' => $top_menu->term_id
							)
						);
					}
					
				}
				
			}
   			
   			
   			
   			// Custom Theme Options Settings
   			
   			if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) ) {
   				
   					$data = get_option( 'crexis_options' );
   					
   					// Dark Header
   					
   					$wbc_dark_header_array = array(
   						'home08__home-black-white-edition',
   						'home09__home-boxes-black-white',
   						'home14__home-fullwidth-slider-dark-4',
   						'home15__home-parallax-image-dark',
   						'home16__home-rainyday-effect',
   						'home19__home-video-background-dark',
   						'home20__home-video-background-night',
   						'onepager08__home-black-white-edition',
   						'onepager09__home-boxes-black-white',
   						'onepager14__home-fullwidth-slider-dark-4',
   						'onepager15__home-parallax-image-dark',
   						'onepager16__home-rainyday-effect',
   						'onepager19__home-video-background-dark',
   						'onepager20__home-video-background-night',
   						'onepagerf23__home-fullpage-version-1',
   					);
   				
   					if( in_array( $demo_active_import[$current_key]['directory'], $wbc_dark_header_array ) ) {
   					
   						$data['header_color'] = 'dark';
   						$data['header_dropdown_color'] = 'dark';
   						
   					} else {
   					
   						$data['header_color'] = 'white';
   						$data['header_dropdown_color'] = 'white';
   						
   					}
   					
   					// Dark Skin
   					
   					$wbc_dark_skin_array = array(
						'home14__home-fullwidth-slider-dark-4',
						'home15__home-parallax-image-dark',
						'home19__home-video-background-dark',
						'onepager14__home-fullwidth-slider-dark-4',
						'onepager15__home-parallax-image-dark',
						'onepager19__home-video-background-dark',
					);
					
					$wbc_night_skin_array = array(
						'home20__home-video-background-night',
						'onepager20__home-video-background-night',
					);
				
					if( in_array( $demo_active_import[$current_key]['directory'], $wbc_dark_skin_array ) ) {
					
						$data['theme_skin'] = 'dark';
						
						$data['bg_color'] = '#151515';
						$data['body_color'] = '#b6b6b6';
						$data['heading_color'] = '#ffffff';
						$data['heading_color'] = '#ffffff';
						
					} elseif( in_array( $demo_active_import[$current_key]['directory'], $wbc_night_skin_array ) ) {
					
						$data['theme_skin'] = 'night';
						
						$data['bg_color'] = '#1f2025';
						$data['body_color'] = '#b6b6b6';
						$data['heading_color'] = '#ffffff';
						$data['heading_color'] = '#ffffff';
						
					} else {
						$data['theme_skin'] = 'white';
						
						$data['bg_color'] = '#ffffff';
						$data['body_color'] = '#888888';
						$data['heading_color'] = '#666666';
						$data['special_heading_color'] = '#6e6e6e';
					}
					
					// Font Family
							
					$wbc_oswald_array = array(
						'home22__home-animated-background',
						'home08__home-black-white-edition',
						'home21__home-fullwidth-layer-slider',
						'home13__home-fullwidth-slider-3',
						'home17__home-neptun-type',
						'home15__home-parallax-image-dark',
						'home16__home-rainyday-effect',
						'home04__home-revolution-slider-2',
						'home07__home-revolution-slider-5',
						'home10__home-veented-slider',
						'home18__home-video-background',
						'home19__home-video-background-dark',
						'home20__home-video-background-night',
						'onepager22__home-animated-background',
						'onepager08__home-black-white-edition',
						'onepager21__home-fullwidth-layer-slider',
						'onepager13__home-fullwidth-slider-3',
						'onepager17__home-neptun-type',
						'onepager15__home-parallax-image-dark',
						'onepager16__home-rainyday-effect',
						'onepager04__home-revolution-slider-2',
						'onepager07__home-revolution-slider-5',
						'onepager10__home-veented-slider',
						'onepager18__home-video-background',
						'onepager19__home-video-background-dark',
						'onepager20__home-video-background-night',
						'onepagerf23__home-fullpage-version-1',
						'onepagerf24__home-fullpage-version-2',
						'onepagerf25__home-fullpage-version-3',
					);
					
					$wbc_georgia_array = array(
						'home12__home-fullwidth-slider-2',
						'home05__home-revolution-slider-3',
						'onepager12__home-fullwidth-slider-2',
						'onepager05__home-revolution-slider-3',
					);
				
					if( in_array( $demo_active_import[$current_key]['directory'], $wbc_oswald_array ) ) {
					
						$data['typography_primary']["font-family"] = 'Oswald';
						$data['typography_primary']["text-transform"] = 'uppercase';
						$data['typography_body']["font-family"] = 'Open Sans';
						
						$data['typography_special_heading']["font-weight"] = '400';
						$data['typography_special_heading']["text-transform"] = 'uppercase';
						
						$data['typography_navigation']["font-size"] = '13px';
						$data['typography_navigation_font'] = 'heading';
						
					} elseif( in_array( $demo_active_import[$current_key]['directory'], $wbc_georgia_array ) ) {
					
						$data['typography_primary']["font-family"] = 'Georgia, serif';
						$data['typography_primary']["text-transform"] = 'none';
						$data['typography_body']["font-family"] = 'Open Sans';
						
						$data['typography_special_heading']["font-weight"] = '400';
						$data['typography_special_heading']["text-transform"] = 'uppercase';
						
						$data['typography_navigation']["font-size"] = '12px';
						$data['typography_navigation_font'] = 'heading';
						
					} else {
					
						$data['typography_primary']["font-family"] = 'Raleway';
						$data['typography_primary']["text-transform"] = 'none';
						$data['typography_body']["font-family"] = 'Open Sans';
						
						$data['typography_special_heading']["font-weight"] = '';
						$data['typography_special_heading']["text-transform"] = 'uppercase';
						
						$data['typography_navigation']["font-size"] = '11px';
						$data['typography_navigation_font'] = 'body';
						
					}
					
					// Dark Header
							
					$wbc_footer_centered_array = array(
						'onepager01__home-default-homepage',
						'onepagerf23__home-fullpage-version-1',
						'onepagerf24__home-fullpage-version-2',
						'onepagerf25__home-fullpage-version-3',
						'onepager03__home-revolution-slider-1',
					);
				
					if( in_array( $demo_active_import[$current_key]['directory'], $wbc_footer_centered_array ) ) {
					
						$data['footer_style'] = 'centered';
						
					} else {
					
						$data['footer_style'] = 'classic';
						
					}
   					
   					
   					update_option( 'crexis_options', $data );
   					
   			}
   			
   			
   			/************************************************************************
   			* Set HomePage
   			*************************************************************************/
   			// array of demos/homepages to check/select from
   			$wbc_home_pages = array(
   				'0demo-main' => 'Homepage',
   				'home02__home-boxes-edition' => 'Home Boxes Edition',
   				'home03__home-revolution-slider-1' => 'Home Revolution Slider 1',
				'home04__home-revolution-slider-2' => 'Home Revolution Slider 2',
				'home05__home-revolution-slider-3' => 'Home Revolution Slider 3',
				'home06__home-revolution-slider-4' => 'Home Revolution Slider 4', 
				'home07__home-revolution-slider-5' => 'Home Revolution Slider 5', 
				'home08__home-black-white-edition' => 'Home Black White Edition', 
				'home09__home-boxes-black-white' => 'Home Boxes Black White', 
				'home10__home-veented-slider' => 'Home Veented Slider', 
				'home11__home-fullwidth-slider-1' => 'Home Fullwidth Slider',
				'home12__home-fullwidth-slider-2' => 'Home Fullwidth Slider 2',
				'home13__home-fullwidth-slider-3' => 'Home Fullwidth Slider 3',
				'home14__home-fullwidth-slider-dark-4' => 'Home Fullwidth Slider 4 Dark',
				'home15__home-parallax-image-dark' => 'Home Parallax Image Dark Edition',
				'home16__home-rainyday-effect' => 'Home RainyDay Effect',
				'home17__home-neptun-type' => 'Home Neptun Type',
				'home18__home-video-background' => 'Home Video Background',
				'home19__home-video-background-dark' => 'Home Video Background Dark',
				'home20__home-video-background-night' => 'Home Video Background Night',
				'home21__home-fullwidth-layer-slider' => 'Home Fullwidth Layer Slider',
				'home22__home-animated-background' => 'Home Animated Background',
				'onepager01__home-default-homepage' => 'OnePager Default Home Page',
				'onepager02__home-boxes-edition' => 'OnePager Home Boxes Edition',
				'onepager03__home-revolution-slider-1' => 'OnePager Revolution Slider 1',
				'onepager04__home-revolution-slider-2' => 'OnePager Revolution Slider 2',
				'onepager05__home-revolution-slider-3' => 'OnePager Revolution Slider 3',
				'onepager06__home-revolution-slider-4' => 'OnePager Revolution Slider 4', 
				'onepager07__home-revolution-slider-5' => 'OnePager Revolution Slider 5', 
				'onepager08__home-black-white-edition' => 'OnePager Black White Edition', 
				'onepager09__home-boxes-black-white' => 'OnePager Black White Boxes', 
				'onepager10__home-veented-slider' => 'OnePager Veented Slider', 
				'onepager11__home-fullwidth-slider-1' => 'OnePager Fullwidth Slider',
				'onepager12__home-fullwidth-slider-2' => 'OnePager Fullwidth Slider 2',
				'onepager13__home-fullwidth-slider-3' => 'OnePager Fullwidth Slider 3',
				'onepager14__home-fullwidth-slider-dark-4' => 'OnePager Fullwidth Slider 4 Dark',
				'onepager15__home-parallax-image-dark' => 'OnePager Parallax Image Dark',
				'onepager16__home-rainyday-effect' => 'OnePager RainyDay Effect',
				'onepager17__home-neptun-type' => 'OnePager Neptun Type',
				'onepager18__home-video-background' => 'OnePager Video Background',
				'onepager19__home-video-background-dark' => 'OnePager Video BG Dark',
				'onepager20__home-video-background-night' => 'OnePager Video BG Night',
				'onepager21__home-fullwidth-layer-slider' => 'OnePager Fullwidth Layer Slider',
				'onepager22__home-animated-background' => 'OnePager Animated Background',
				'onepagerf23__home-fullpage-version-1' => 'OnePager FullPage',
				'onepagerf24__home-fullpage-version-2' => 'OnePager FullPage 2',
				'onepagerf25__home-fullpage-version-3' => 'OnePager FullPage 3',
   			);
   			if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
   			
   				$page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
   				
   				if ( isset( $page->ID ) ) {
   					update_option( 'page_on_front', $page->ID );
   					update_option( 'show_on_front', 'page' );

   				}
   				
   				$blog = get_page_by_title( 'Blog' );
   				
   				if ( isset( $page->ID ) ) {
   					update_option( 'page_for_posts', $blog->ID );
   				}
   					
   				
   			}
   		}
   		
   		// Uncomment the below
   		add_action( 'wbc_importer_after_content_import', 'wbc_extended_example', 10, 2 );
   	}
   	
   	// Crexis Core Classe
   	
   	class Crexis_Core {
   	
   		public static function get_animated_class() {
   			return ' animated vntd-animated';
   		}
   		
   	}
   
?>