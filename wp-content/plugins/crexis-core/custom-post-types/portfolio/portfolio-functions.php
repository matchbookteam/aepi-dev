<?php

//
// New Post Type
//

add_action('init', 'crexis_portfolio_register');  

if( !function_exists("crexis_portfolio_register") ) {
	function crexis_portfolio_register() {
	    $args = array(
	        'label' => esc_html__('Portfolio', 'crexis'),
	        'public' => true,
	        'show_ui' => true,
	        'capability_type' => 'post',
	        'hierarchical' => true,
	        'has_archive' => false,
	        'rewrite'     => array(
	                         'slug'       => 'portfolio', // if you need slug
	                         'with_front' => false,
	                         ),
	        'menu_icon' => 'dashicons-art',
	        'supports' => array('title','editor','thumbnail')
	       );  
	
	    register_post_type( 'portfolio' , $args );
	    
	    register_taxonomy(
	    	"project-type", 
	    	array("portfolio"), 
	    	array(
	    		"hierarchical" => true, 
	    		"context" => "normal", 
	    		'show_ui' => true,
	    		"label" => "Portfolio Categories", 
	    		"singular_label" => "Portfolio Category", 
	    		"rewrite" => true
	    	)
	    );
	}
}

//
// Custom taxonomy field: category icon
//

function crexis_taxonomy_add_new_meta_field() {

	// this will add the custom meta field to the add new term page
	?>
	
	<div class="form-field">
		<label for="term_meta[category_icon]"><?php _e( 'Category Icon', 'crexis' ); ?></label>
		<input type="text" name="term_meta[category_icon]" id="term_meta[category_icon]" value="">
		<p class="description"><?php _e( 'Code of the FontAwesome icon that will represent this portfolio category. A full list of icon can be found <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">here</a>. Example: fa-camera-retro','crexis' ); ?></p>
	</div>
	
<?php
}

add_action( 'project-type_add_form_fields', 'crexis_taxonomy_add_new_meta_field', 10, 2 );

function crexis_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); ?>
	
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[category_icon]"><?php _e( 'Category Icon', 'crexis' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[category_icon]" id="term_meta[category_icon]" value="<?php echo esc_attr( $term_meta['category_icon'] ) ? esc_attr( $term_meta['category_icon'] ) : ''; ?>">
			<p class="description"><?php _e( 'Code of the FontAwesome icon that will represent this portfolio category. A full list of icon can be found <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">here</a>. Example: fa-camera-retro','crexis' ); ?></p>
		</td>
	</tr>
	
<?php
}

add_action( 'project-type_edit_form_fields', 'crexis_taxonomy_edit_meta_field', 10, 2 );

function crexis_save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  
add_action( 'edited_project-type', 'crexis_save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_project-type', 'crexis_save_taxonomy_custom_meta', 10, 2 );

// End custom taxonomy field


//
// New Columns
//

add_filter( 'manage_edit-portfolio_columns', 'crexis_portfolio_columns_settings' ) ;

function crexis_portfolio_columns_settings( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => esc_html__('Title', 'crexis'),
		'category' => esc_html__( 'Category', 'crexis'),
		'date' => esc_html__('Date', 'crexis'),
		'thumbnail' => ''	
	);

	return $columns;
}

add_action( 'manage_portfolio_posts_custom_column', 'crexis_portfolio_columns_content', 10, 2 );

function crexis_portfolio_columns_content( $column, $post_id ) {
	global $post;

	switch( $column ) {

		/* If displaying the 'duration' column. */
		case 'category' :

			$taxonomy = "project-type";
			$post_type = get_post_type($post_id);
			$terms = get_the_terms($post_id, $taxonomy);
		 
			if ( !empty($terms) ) {
				foreach ( $terms as $term )
					$post_terms[] = "<a href='edit.php?post_type={$post_type}&{$taxonomy}={$term->slug}'> " . esc_html(sanitize_term_field('name', $term->name, $term->term_id, $taxonomy, 'edit')) . "</a>";
				echo join( ', ', $post_terms );
			}
			else echo '<i>No categories.</i>';

			break;

		/* If displaying the 'genre' column. */
		case 'thumbnail' :

			the_post_thumbnail('thumbnail', array('class' => 'column-img'));

			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}
	

// Additional Settings

add_action("admin_init", "crexis_portfolio_extra_settings");   

function crexis_portfolio_extra_settings(){
    add_meta_box("portfolio_extra_settings", "Portfolio Post Settings", "crexis_portfolio_extra_settings_config", "portfolio", "normal", "low");
}   

function crexis_portfolio_extra_settings_config(){
        global $post;
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $link_type = $portfolio_external_url = $portfolio_post_excerpt = '';
		if(isset($custom["link_type"][0])) $link_type = $custom["link_type"][0];
		if(isset($custom["portfolio_external_url"][0])) $portfolio_external_url = $custom["portfolio_external_url"][0];
		if(isset($custom["portfolio_post_excerpt"][0])) $portfolio_post_excerpt = $custom["portfolio_post_excerpt"][0];
		
?>

	<div class="metabox-options form-table fullwidth-metabox image-upload-dep">
	
		<div class="metabox-option">
			<h6><?php esc_html_e('Optional post excerpt','crexis'); ?>:</h6>
			<?php if(is_null($portfolio_post_excerpt)) $portfolio_post_excerpt = ''; ?>
			<textarea name="portfolio_post_excerpt" class="portfolio_post_excerpt"><?php echo esc_textarea($portfolio_post_excerpt); ?></textarea>
			
		</div>
		
		<div class="metabox-option">
			<h6><?php esc_html_e('Thumbnail Link Type', 'crexis') ?>:</h6>
			
			<?php 
			
			$link_type_arr = array(
				'Default - according to Portfolio Grid settings' => 'default',
				'Always link - even if Portfolio Grid type is set to Lightbox, this post will open in a new page' => 'link',
				'External Link - thumbnail links to external URL' => 'external'
			); 
			
			crexis_create_dropdown('link_type',$link_type_arr,$link_type, true);
			
			?>
		</div>
		
		<div class="metabox-option fold fold-link_type fold-external" <?php if($link_type != "external") echo 'style="display:none;"'; ?>>
			<h6><?php esc_html_e('External URL','crexis'); ?>:</h6>
			
			<input type="text" name="portfolio_external_url" value="<?php echo esc_url($portfolio_external_url); ?>">
			
		</div>
		
		
		
		
	</div>

<?php

}
	
// Save Custom Fields
	
add_action('save_post', 'crexis_save_portfolio_post_settings'); 

function crexis_save_portfolio_post_settings(){
    global $post;  

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{
		if(isset($_POST["title_disable"])) update_post_meta($post->ID, "title_disable", $_POST["title_disable"]);	
		if(isset($_POST["tagline"])) update_post_meta($post->ID, "tagline", $_POST["tagline"]);	
		if(isset($_POST["thumb_lightbox"])) update_post_meta($post->ID, "thumb_lightbox", $_POST["thumb_lightbox"]);		
		if(isset($_POST["link_type"])) update_post_meta($post->ID, "link_type", $_POST["link_type"]);
		if(isset($_POST["home_button"])) update_post_meta($post->ID, "home_button", $_POST["home_button"]);
		if(isset($_POST["home_button_link"])) update_post_meta($post->ID, "home_button_link", $_POST["home_button_link"]);
		if(isset($_POST["related_work"])) update_post_meta($post->ID, "related_work", $_POST["related_work"]);
		if(isset($_POST["post_layout"])) update_post_meta($post->ID, "post_layout", $_POST["post_layout"]);
		if(isset($_POST["gallery_images"])) update_post_meta($post->ID, "gallery_images", $_POST["gallery_images"]);
		if(isset($_POST["gallery_type"])) update_post_meta($post->ID, "gallery_type", $_POST["gallery_type"]);
		if(isset($_POST["post_video_url"])) update_post_meta($post->ID, "post_video_url", $_POST["post_video_url"]);
		if(isset($_POST["portfolio_external_url"])) update_post_meta($post->ID, "portfolio_external_url", $_POST["portfolio_external_url"]);
		if(isset($_POST["portfolio_post_excerpt"])) update_post_meta($post->ID, "portfolio_post_excerpt", $_POST["portfolio_post_excerpt"]);
		
		
    }

}

//
// Filtering Menu
//


if(!function_exists('crexis_portfolio_overlay_categories')) {
	function crexis_portfolio_overlay_categories(){
		
		global $post;
	    $terms = wp_get_object_terms($post->ID, "project-type");
		foreach ( $terms as $term ) {
			echo esc_textarea($term->name);
			if(end($terms) !== $term){
				echo ", ";
			}
		}	
		
	}
}

if(!function_exists('crexis_portfolio_holder_class')) {
	function crexis_portfolio_holder_class(){
		
		global $post;
		
		if(get_post_meta($post->ID, 'portfolio_post_type', true) == "direct" || get_post_meta($post->ID, 'video_post_type', true) == "direct")
		
		echo "gallery clearfix";	
	}
}

if(!function_exists('crexis_portfolio_navigation')) {
	function crexis_portfolio_navigation(){
		
		global $post;
		
		// Check if Portfolio Navigation isn't disabled
		if(!get_post_meta($post->ID, 'nav_disabled', true)){
				
			echo '<div id="portfolio-navigation" class="page-title-side">';
			if(get_permalink(get_adjacent_post(false,'',false)) != get_permalink($post->ID)){
				echo '<a href="'.get_permalink(get_adjacent_post(false,'',false)).'" class="portfolio-prev fa fa-angle-left"></a>';
			}
			// Check if Parent Portfolio Page is set
			if(get_post_meta($post->ID, 'home_button', true) == 'enabled' && get_post_meta($post->ID, 'home_button_link', true)){
			
				$home_url = get_permalink(get_post_meta($post->ID, 'home_button_link', true));
				if(!$home_url) {
					$home_url = crexis_option('portfolio_url');
				}
				if($home_url) {
					echo '<a href="'.esc_url($home_url).'" class="portfolio-home fa fa-th"></a>';
				}			
			}
			
			if(get_permalink(get_adjacent_post(false,'',true)) != get_permalink($post->ID)){
				echo '<a href="'.get_permalink(get_adjacent_post(false,'',true)).'" class="portfolio-next fa fa-angle-right"></a>';
			}
			
			echo '</div>';
		}
	}
}


//
// Retrieve Custom Values
//

if(!function_exists('crexis_portfolio_overlay_icon')) {
	function crexis_portfolio_overlay_icon(){
	
		global $post;	
		$post_type = get_post_meta($post->ID, 'portfolio_post_type', true);
		
		if($post_type == "direct"){
			echo "resize-full";
		}else{
			echo "link";
		}
					
	}
}	

if(!function_exists('crexis_portfolio_zoom_icon')) {
	function crexis_portfolio_zoom_icon($page_id = NULL, $type = NULL) {
		
		global $post;
		
		$rel = 'prettyPhoto';
		$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
		
		if($page_id) {
			$rel = 'gallery[gallery'.$page_id.']'; // If Portfolio Page, create a global gallery
		}
	
		if(!get_post_meta($post->ID, 'lightbox_disabled', true)) {
			echo '<a href="'.esc_url($thumb_url[0]).'" rel="'.esc_attr($rel).'"><span class="hover-icon hover-icon-zoom"></span></a>';
		}	
	
	}
}

if(!function_exists('crexis_portfolio_hidden_images')) {
	function crexis_portfolio_hidden_images() {
	
		global $post;	
		
		$gallery_images = get_post_meta($post->ID, 'gallery_images', true);
	
		if(get_post_meta($post->ID, 'on_click', true) == "lightbox" && $gallery_images){
			echo '<div class="lightbox-hidden">';				
			$ids = explode(",", $gallery_images);		
			foreach(array_slice($ids,1) as $id){
				$imgurl = wp_get_attachment_image_src($id, "large");
				echo '<a href="'.esc_url($imgurl[0]).'" rel="gallery[gallery'. $post->ID .']"></a>';
			}
			echo '</div>';	
		}
	
	}
}

if(!function_exists('crexis_lightbox_gallery_images')) {
	function crexis_lightbox_gallery_images() {
		
		global $post;
		$gallery_images = get_post_meta($post->ID, 'gallery_images', true);
		
		if($gallery_images){
		
			echo '<div class="lightbox-hidden">';				
			$ids = explode(",", $gallery_images);				
			foreach($ids as $id){
				echo '<a href="'.wp_get_attachment_url($id).'" rel="gallery[gallery'. $post->ID .']"></a>';
			}
			echo '</div>';
						
		}
	
	}
}

if(!function_exists('crexis_portfolio_item_class')) {
	function crexis_portfolio_item_class(){
		
		global $post;
		$output = '';
	    $terms = wp_get_object_terms($post->ID, "project-type");
		foreach ( $terms as $term ) {
			$output .= $term->slug . " ";
		}		
		
		return $output;
		
	}
}

if(!function_exists('crexis_filters')) {
	function crexis_filters($taxonomy,$cats,$filter_style = null, $grid_id = null, $filter_counter = null) {
			
		if(!$cats) {
	
			$portfolios_cats = get_terms($taxonomy);
	
			$cats = '';		
			foreach($portfolios_cats as $portfolio_cat) {
				$cats .= $portfolio_cat->slug.',';
			}		
		}
		
		if(!$filter_style) $filter_style = 'simple';
		
		$filter_counter_div = '';
		
		if( $filter_counter == 'yes' ) {
			$filter_counter_div = '<div class="cbp-filter-counter"></div>';
		}
		
		?>
		
		<div class="vntd-filters portfolio-filters-wrap"> 

			<ul id="portfolio-filters-<?php echo esc_attr($grid_id); ?>" class="portfolio-filters cbp-l-filters-alignCenter portfolio-filters-<?php echo esc_attr($filter_style); ?>" data-gid="portfolio-<?php echo esc_attr($grid_id); ?>">
				<li data-filter="*" class="cbp-filter-item-active cbp-filter-item"><?php esc_html_e('All', 'crexis'); echo $filter_counter_div; ?></li>
				<?php	 
				
				$categories = explode(",", $cats);   
				
				foreach ($categories as $value){
					$term = get_term_by('slug', $value, $taxonomy);
					if(isset($term->name)) {
						echo '<li data-filter=".'. $value .'" class="cbp-filter-item">' . $term->name . $filter_counter_div . '</li>';	
					} 
				}
				?>
			</ul>
		</div>
		<?php
	}
}

if(!function_exists('crexis_related_work')) {
	function crexis_related_work() {
		global $post;
		
		$cols = 4;
		$title = $nav_style = $url = '';
		$thumb_size = 'vntd-portfolio-square';
		
		echo '<div id="related-work" class="vntd-carousel portfolio-carousel vntd-carousel-nav-side portfolio-style-default vntd-cols-'.esc_attr($cols).'" data-cols="'.esc_attr($cols).'">';
		echo '<h3>'.esc_html__('Related Work','crexis').'</h3>';
			
			crexis_carousel_heading($title,$nav_style,$url);
			echo '<div class="carousel-overflow"><div class="carousel-holder vntd-row"><ul>';
						
				wp_reset_postdata();
				
				$args = array(
					'posts_per_page' => 8,
					'project-type'		=> $cats,
					'post_type' => 'portfolio',
					'post__not_in' => array($post->ID)
				);
				$the_query = new WP_Query($args); 
				
				if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
				
				echo '<li class="carousel-item span'.esc_attr(crexis_carousel_get_cols($cols)).'">';			
				
				?>	
							
					<div class="portfolio-thumbnail-holder thumbnail-default hover-item">
						<a href="<?php echo get_permalink(); ?>" class="noSwipe portfolio-thumbnail">
							<img src="<?php echo crexis_thumb(460,345); ?>" alt>			    
							<span class="hover-icon hover-icon-link"></span>
						</a>
						
						<div class="portfolio-thumbnail-caption">
						    <h4 class="caption-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
						    <span class="caption-categories"><?php crexis_portfolio_overlay_categories(); ?></span>					    
						</div>			
					</div>
				
				<?php
	
				echo '</li>';
				
				endwhile; endif; wp_reset_postdata();		
			
			echo '</ul></div></div></div>';
	
	}
}

// Portfolio Navigation related functions

if(!function_exists('crexis_portfolio_nav_enabled')) {
	function crexis_portfolio_nav_enabled() {
		$return = 'enabled';
		
		return $return;
	}
}

if(!function_exists('crexis_portfolio_nav')) {
	function crexis_portfolio_nav() {
	
		global $post;
		
		?>
		<div class="vntd-portfolio-nav">
			<div class="inner">
			
				<div class="portfolio-nav-wrap">

					<?php 
					
					next_post_link('<div class="portfolio-nav-next">%link</div>','<div class="thin-arrow thin-arrow-left"></div>');
					
					if(crexis_option('portfolio_url')) { 
					
					$parent_url = 'test';
					
					if(get_post_meta($post->ID, 'portfolio_parent_url', true)) {
						$parent_url = get_permalink(get_post_meta($post->ID, 'portfolio_parent_url', true));
					} else {
						$parent_url = get_permalink(get_page_by_path(crexis_option('portfolio_url')));
					}
					
					?>
					<a href="<?php echo esc_url($parent_url); ?>" class="portfolio-nav-grid">
						<div class="nav-grid-1"></div>
						<div class="nav-grid-2"></div>
						<div class="nav-grid-3"></div>
						<div class="nav-grid-4"></div>
					</a>
					<?php 
					} 
					
					previous_post_link('<div class="portfolio-nav-prev">%link</div>','<div class="thin-arrow thin-arrow-right"></div>'); 
					
					?>
				</div>
				
			</div>		
		</div>		
		<?php
	}
}

if(!function_exists('crexis_love_button')) {
	function crexis_love_button() {
	
		if(function_exists('getPostLikeLink')) {
		
			echo '<div class="portfolio-love-button">'.getPostLikeLink( get_the_ID() ).'</div>';
			
		} else {
			return false;
		}
		
	}
}

if(!function_exists('crexis_get_category_icon')) {
	function crexis_get_category_icon() {
	
		$terms = wp_get_object_terms(get_the_ID(), "project-type");
		
		if( is_array( $terms ) ) {
		
			$term = $terms[0]; 
			
			$t_id = $term->term_id;
			
			$term_meta = get_option( "taxonomy_$t_id" );
			
			if( is_array( $term_meta ) ) {
				if( array_key_exists('category_icon', $term_meta) ) {
					return $term_meta["category_icon"];
				} else {
					return 'fa-camera-retro';
				}
			}
			
		}
		
		return 'fa-camera-retro';
		
	}
}