<?php

//
// New Post Type
//


add_action('init', 'crexis_team_register');  

function crexis_team_register() {
    $args = array(
        'label' => esc_html__('Team Members', 'crexis'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => true,
        'menu_icon' => 'dashicons-groups',
        'supports' => array('title','thumbnail')
       );  

    register_post_type( 'team' , $args );
    
    register_taxonomy(
    	"member-position", 
    	array("team"), 
    	array(
    		"hierarchical" => true, 
    		"context" => "normal", 
    		'show_ui' => true,
    		"label" => "Member Position", 
    		"singular_label" => "Member Position", 
    		"rewrite" => true
    	)
    );
}


//
// Thumbnail column
//

add_filter( 'manage_edit-team_columns', 'crexis_team_columns_settings' ) ;

function crexis_team_columns_settings( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => esc_html__('Title', 'crexis'),
		'date' => esc_html__('Date', 'crexis'),
		'slider-thumbnail' => ''	
	);

	return $columns;
}

add_action( 'manage_team_posts_custom_column', 'crexis_team_columns_content', 10, 2 );

function crexis_team_columns_content( $column, $post_id ) {
	global $post;
	the_post_thumbnail('thumbnail', array('class' => 'column-img'));
}

//
// Team Title and Caption
//

add_action("admin_init", "crexis_team_title_settings");   

if(!function_exists('crexis_team_title_settings')) {

	function crexis_team_title_settings(){
	    add_meta_box("team_title_settings", "Team Member", "crexis_team_member_config", "team", "normal", "high");
	    add_meta_box("team_member_social", "Team Member Social", "crexis_team_member_social", "team", "normal", "high");
	}   
	
	function crexis_team_member_config(){
	        global $post;
	        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;	        
	        $custom = get_post_custom($post->ID);
	        $member_name = $member_position = $member_bio = $member_bio_short = '';
	        if(isset($custom["member_name"][0])) $member_name = $custom["member_name"][0];
//	        if(isset($custom["member_position"][0])) $member_position = $custom["member_position"][0];
	        if(isset($custom["member_bio_short"][0])) $member_bio_short = $custom["member_bio_short"][0];
			if(isset($custom["member_bio"][0])) $member_bio = $custom["member_bio"][0];
			
	?>	    
		<div class="metabox-options form-table fullwidth-metabox image-upload-dep">
			
			<div class="metabox-option">
				<h6><?php esc_html_e('Name', 'crexis') ?>:</h6>
				<input type="text" name="member_name" value="<?php echo esc_textarea($member_name); ?>">
			</div>		
			
			<div class="metabox-option">
				<h6><?php esc_html_e('Short Bio', 'crexis') ?>:</h6>
				<textarea name="member_bio_short"><?php echo esc_textarea($member_bio_short); ?></textarea>
			</div>
			
			<div class="metabox-option">
				<h6><?php esc_html_e('Longer Bio', 'crexis') ?>:</h6>
				<textarea name="member_bio"><?php echo esc_textarea($member_bio); ?></textarea>
			</div>
			
		</div>
	<?php
	}
	
	function crexis_team_member_social(){
	        global $post;
	        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
	        $custom = get_post_custom($post->ID);	
	?>
	
		<div class="metabox-options form-table fullwidth-metabox image-upload-dep">
			
			<?php 
			
			$member_socials = array('facebook','twitter','googleplus','pinterest','linkedin','instagram','vimeo','email');
			
			foreach($member_socials as $member_social) {
				$current_val = '';
				if(isset($custom["member_".$member_social][0])) {
					$current_val = $custom["member_".$member_social][0];
				}
				
				echo '<div class="metabox-option">';
				echo '<h6>'.$member_social.'</h6>';
				echo '<input type="text" name="member_'.esc_attr($member_social).'" value="'.esc_attr($current_val).'">';
				echo '</div>';
			
			}
			
			?>					
			
		</div>
	
	<?php
	
	}
    
}  

	
	
// Save Slide
	
add_action('save_post', 'crexis_save_team_meta'); 

function crexis_save_team_meta(){
    global $post;  	
	
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{
	
		$post_metas = array('member_name','member_bio','member_bio_short');
		
		foreach($post_metas as $post_meta) {
			if(isset($_POST[$post_meta])) update_post_meta($post->ID, $post_meta, $_POST[$post_meta]);
		}
		
		$member_socials = array('twitter','facebook','instagram','pinterest','linkedin','email','vimeo','googleplus');
		
		foreach($member_socials as $member_social) {
			if(isset($_POST['member_'.$member_social])) update_post_meta($post->ID, 'member_'.$member_social, $_POST['member_'.$member_social]);
		}

    }

}

function crexis_team_member_categories() {
	global $post;
	
	$terms = wp_get_object_terms($post->ID, "member-position");
	
	if($terms) {
		foreach ( $terms as $term ) {
			echo $term->name;
			if(end($terms) !== $term){
				echo ", ";
			}
		}
	}
}

function crexis_team_item_class(){
	
	global $post;
	$output = '';
    $terms = wp_get_object_terms($post->ID, "member-position");
	foreach ( $terms as $term ) {
		$output .= $term->slug . " ";
	}		
	
	return $output;
	
}