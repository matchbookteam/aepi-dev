<?php

//
// New Post Type
//


add_action('init', 'crexis_testimonial_register');  

function crexis_testimonial_register() {
    $args = array(
        'label' => esc_html__('Testimonials', 'crexis'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => true,
        'menu_icon' => 'dashicons-format-quote',
        'supports' => array('title','thumbnail')
       );  

    register_post_type( 'testimonials' , $args );
}


//
// Thumbnail column
//



//
// Testimonial Title and Caption
//

add_action("admin_init", "crexis_testimonial_title_settings");   

function crexis_testimonial_title_settings(){
    add_meta_box("testimonial_title_settings", "Testimonial", "crexis_testimonial_title_config", "testimonials", "normal", "high");
}   

function crexis_testimonial_title_config(){
        global $post;
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
		
		$testimonial_content = $role = $name = $website_url = '';
		
		if(isset($custom["testimonial_content"][0])) $testimonial_content = $custom["testimonial_content"][0];
		if(isset($custom["role"][0])) $role = $custom["role"][0];
		if(isset($custom["name"][0])) $name = $custom["name"][0];
		if(isset($custom["website_url"][0])) $website_url = $custom["website_url"][0];
		
?>
	<div class="metabox-options form-table fullwidth-metabox image-upload-dep">
		
		<div class="metabox-option">
			<h6><?php esc_html_e('Name', 'crexis') ?>:</h6>
			<input type="text" name="name" value="<?php echo esc_textarea($name); ?>">
		</div>		
		
		<div class="metabox-option">
			<h6><?php esc_html_e('Subline', 'crexis') ?>:</h6>
			<input type="text" name="role" value="<?php echo esc_textarea($role); ?>">
		</div>
		
		<div class="metabox-option">
			<h6><?php esc_html_e('Website URL', 'crexis') ?>:</h6>
			<input type="text" name="website_url" value="<?php echo esc_html($website_url); ?>">
		</div>
		
		<div class="metabox-option">
			<h6><?php esc_html_e('Testimonial', 'crexis') ?>:</h6>
			<textarea name="testimonial_content"><?php echo esc_textarea($testimonial_content); ?></textarea>
		</div>
		
	</div>
<?php

}	
	
	
// Save Slide
	
add_action('save_post', 'crexis_save_testimonial_meta'); 

function crexis_save_testimonial_meta(){
    global $post;  	
	
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{
	
		$post_metas = array('name','role','testimonial_content', 'website_url');
		
		foreach($post_metas as $post_meta) {
			if(isset($_POST[$post_meta])) update_post_meta($post->ID, $post_meta, $_POST[$post_meta]);
		}
    }

}