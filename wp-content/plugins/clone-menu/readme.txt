=== WP Clone Menu ===
Plugin Name: WP Clone Menu
Plugin URI: http://afzalmultani.com/
Author: Afzal Multani
Author URI: http://afzalmultani.com/
Contributors: afzalmultani, bhaveshkhadodara
Tags: clone menu, menu duplicate
Requires at least: 4.6
Tested up to: 4.7
Stable tag: 4.7
License: GPLv2 or later

This plugin allows you to clone your menus from simply select the existing menu which you want to duplicate and enter new menu name, And just click on "Make Clone" button, and cheers.

== Description ==

Clone Menu plugin allows you to clone your menus from simply select the existing menu which you want to duplicate and enter new menu name, And just click on "Make Clone" button, and cheers.

== Installation ==

1. Upload the directory `/clone-menu/` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Click on the Settings link below the plugin name on the plugins page to clone your menu.

== Changelog ==

= 1.0.0
* This is the first version
