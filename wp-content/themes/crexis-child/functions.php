<?php
//
// Your code goes below!
//

add_action( 'wp_enqueue_scripts', 'crexis_child_styles' );
function crexis_child_styles() {
    wp_enqueue_style( 'veented-crexis-child-style', get_stylesheet_uri(), array( 'crexis-styles' ) );
}