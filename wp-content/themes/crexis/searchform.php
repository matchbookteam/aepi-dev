<form class="search-form relative" id="search-form" method="GET" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input name="s" id="s" type="text" placeholder="<?php esc_html_e('Search...','crexis') ?>" class="search">
	<button class="search-button"><i class="fa fa-search"></i></button>
</form>					