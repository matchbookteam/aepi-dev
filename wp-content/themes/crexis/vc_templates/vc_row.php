<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $full_width
 * @var $full_height
 * @var $content_placement
 * @var $parallax
 * @var $parallax_image
 * @var $css
 * @var $el_id
 * @var $video_bg
 * @var $video_bg_url
 * @var $video_bg_parallax
 * @var $content - shortcode content
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Row
 */
$el_class = $full_height = $full_width = $parallax_speed_bg = $equal_height = $content_placement = $parallax = $parallax_image = $css = $el_id = $video_bg = $video_bg_url = $video_bg_parallax = $bg_overlay = $color_scheme = '';
$output = $after_output = $disable_element = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

wp_enqueue_script( 'wpb_composer_front_js' );

$el_class = $this->getExtraClass( $el_class ) . $this->getCSSAnimation( $css_animation );

$css_classes = array(
	'vc_row',
	'wpb_row', //deprecated
	'vc_row-fluid',
	$el_class,
	vc_shortcode_custom_css_class( $css ),
);

if (vc_shortcode_custom_css_has_property( $css, array('border', 'background') ) || $video_bg || $parallax) {
	$css_classes[]='vc_row-has-fill';
}

$disabled_class = '';

if ( 'yes' === $disable_element ) {
	if ( vc_is_page_editable() ) {
		$css_classes[] = 'vc_hidden-lg vc_hidden-xs vc_hidden-sm vc_hidden-md';
		$disabled_class .= ' vc_hidden-lg vc_hidden-xs vc_hidden-sm vc_hidden-md ';
	} else {
		return '';
	}
}

$wrapper_attributes = array();
// build attributes for wrapper
if ( ! empty( $el_id ) ) {
	$wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
}
if ( ! empty( $full_width ) ) {

	if(!is_page_template('template-fullpage.php')) {
	
		$wrapper_attributes[] = 'data-vc-full-width="true"';
		$wrapper_attributes[] = 'data-vc-full-width-init="false"';
		if ( 'stretch_row_content' === $full_width ) {
			$wrapper_attributes[] = 'data-vc-stretch-content="true"';
			$css_classes[] = 'vc_row-fullwidth-padding';
		} elseif ( 'stretch_row_content_no_spaces' === $full_width ) {
			$wrapper_attributes[] = 'data-vc-stretch-content="true"';
			$css_classes[] = 'vc_row-no-padding';
		}
		
		$after_output .= '<div class="vc_row-full-width"></div>';
	} else {
		$css_classes[] = 'fullwidth-row';
	}
	
}





// ADDED BY VEENTED

// Row Inner
$inner_wrap_start = $inner_wrap_end = '';

if($full_width != 'stretch_row_content' && $full_width != 'stretch_row_content_no_spaces') {
	$inner_wrap_start = '<div class="inner">';
	$inner_wrap_end = '</div>';
}

// Row background overlay

$overlay_class = '';

if($bg_overlay != '') {
	$overlay_class = 'bg-overlay-'.$bg_overlay;
}

// Predefined BG Color

if ( ! empty( $bg_predefined_color ) ) {

	global $waxom_options;
	if(is_array($waxom_options)) {

		$custom_bg_color = waxom_string_between($css, 'background-color: ', ' !important');
		if($bg_predefined_color != '' && $bg_predefined_color != 'none' && !$custom_bg_color) {

			$css_classes[] = 'bg-'.$bg_predefined_color;
			
		}

	}
}

if(is_page_template('template-fullpage.php')) {
	$css_classes[] = 'section';
}

// Text Color

if($color_scheme == 'white') {
	$css_classes[] = 'vntd-section-white';
}

// END BY VEENTED


if ( ! empty( $full_height ) ) {
	$css_classes[] = 'vc_row-o-full-height';
	if ( ! empty( $columns_placement ) ) {
		$flex_row = true;
		$css_classes[] = 'vc_row-o-columns-' . $columns_placement;
		if ( 'stretch' === $columns_placement ) {
			$css_classes[] = 'vc_row-o-equal-height';
		}
	}
}

if ( ! empty( $equal_height ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-equal-height';
}

if ( ! empty( $content_placement ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-content-' . $content_placement;
}

if ( ! empty( $flex_row ) ) {
	$css_classes[] = 'vc_row-flex';
}

$has_video_bg = ( ! empty( $video_bg ) && ! empty( $video_bg_url ) && vc_extract_youtube_id( $video_bg_url ) );

$parallax_speed = $parallax_speed_bg;
if ( $has_video_bg ) {
	$parallax = $video_bg_parallax;
	$parallax_image = $video_bg_url;
	$css_classes[] = ' vc_video-bg-container';
	wp_enqueue_script( 'vc_youtube_iframe_api_js' );
}

if ( ! empty( $parallax ) ) {

	if( crexis_option('parallax_plugin') != 'dsadasd') {
	
		wp_enqueue_script( 'vc_jquery_skrollr_js' );
		
		$parallax_speed = 0;
		
		if( is_page_template('template-fullpage.php') ) {
			$parallax_speed = '1.5';
		} else {
			$parallax_speed = '2';
		}
		
		$wrapper_attributes[] = 'data-vc-parallax="' . $parallax_speed . '"'; // parallax speed
		$css_classes[] = 'vc_general vc_parallax vc_parallax-' . $parallax;
		if ( false !== strpos( $parallax, 'fade' ) ) {
			$css_classes[] = 'js-vc_parallax-o-fade';
			$wrapper_attributes[] = 'data-vc-parallax-o-fade="on"';
		} elseif ( false !== strpos( $parallax, 'fixed' ) ) {
			$css_classes[] = 'js-vc_parallax-o-fixed';
		}
		
	} else {
		
		wp_enqueue_script('vntd-parallax', '', '', '', true);
		$css_classes[] = 'parallax-bg';
		
		$rand_id = rand(1, 9999);
		
		$css_classes[] = 'parallax-row parallax-section-' . $rand_id;
		
	}
}

if ( ! empty( $parallax_image ) ) {
	if ( $has_video_bg ) {
		$parallax_image_src = $parallax_image;
	} else {
		$parallax_image_id = preg_replace( '/[^\d]/', '', $parallax_image );
		$parallax_image_src = wp_get_attachment_image_src( $parallax_image_id, 'full' );
		if ( ! empty( $parallax_image_src[0] ) ) {
			$parallax_image_src = $parallax_image_src[0];
		}
	}
	$wrapper_attributes[] = 'data-vc-parallax-image="' . esc_attr( $parallax_image_src ) . '"';
}

if ( ! $parallax && $has_video_bg ) {
	$wrapper_attributes[] = 'data-vc-video-bg="' . esc_attr( $video_bg_url ) . '"';
}


$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';
// Row Overlay


$output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
if($overlay_class != '') $output .= '<div class="bg-overlay '.$overlay_class.'"></div>';
$output .= $inner_wrap_start;
$output .= wpb_js_remove_wpautop( $content );
$output .= $inner_wrap_end . '</div>';
$output .= $after_output;

echo $output;