<?php 
$post = $wp_query->post;
get_header();
$layout = get_post_meta($post->ID, 'page_layout', true);
$page_width = get_post_meta($post->ID, 'page_width', true);
if(!$page_width) $page_width = 'content';
?>

<div class="single-post post blog page-holder page-layout-<?php echo esc_attr($layout); ?>">
		
	<?php 		
	
	if($page_width != 'fullwidth') {
		echo '<div class="inner clearfix">';
	}
	
	if($layout != "fullwidth") {
		echo '<div class="page_inner">';
	}
	
	if (have_posts()) : while (have_posts()) : the_post(); 
	        
		crexis_blog_post_content($layout);
		
		wp_link_pages();
		
		crexis_blog_post_tags();
		
		crexis_blog_post_author();
		
		if( crexis_option( 'blog_post_nav' ) != false ) {
			crexis_blog_post_nav();
		}
	          
	endwhile; endif; 
	
	if ( comments_open() && !post_password_required() ){ comments_template(); } // Load comments if enabled	     
	
	if($layout != "fullwidth") { 
		echo '</div>';
		get_sidebar();    		
	}
	
	if($page_width != 'fullwidth') {
		echo '</div>';
	}
	
	?>

</div>

<?php get_footer(); ?>