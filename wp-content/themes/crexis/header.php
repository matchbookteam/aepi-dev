<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">    

    <?php
	
	wp_head(); 

	?>        

</head>

<body <?php body_class( crexis_body_classes() ); ?>>

	<?php
	
	if( crexis_option('pageloader') != 'no' ) {
	
	?>
	
	<article id="pageloader" class="white-loader">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</article>
	
	<?php
	
	}
	
	$header_style = crexis_header_style();
	
	if(crexis_header_style('style') != 'disable') {
	
	$header_skin = 'light';
	
	if(crexis_option('header_skin')) {
		$header_skin = crexis_option('header_skin');
	}
	
	$dropdown_class = 'dropdown-dark';
	
	if(crexis_option('navbar_dropdown_color') == 'white' || get_post_meta(get_the_ID(), 'dropdown_color', true) == 'white') {
		$dropdown_class = 'dropdown-white';
	}
	
	$header_class = 'relative-nav';
	$nav_class = '';
	if(substr( $header_style, 0, 17 ) === "style-transparent") {
		$header_class = '';
		$nav_class = 'first-nav';
	}
	
	if(crexis_option('topbar') && $header_style != 'style-transparent' && $header_style != 'style-transparent-hamburger' || get_post_meta(get_the_ID(), 'force_topbar', true) == 'yes' || crexis_option('topbar') && crexis_option('topbar_force') ) {
	
		crexis_print_topbar();
	} 
	
	?>
	
	<nav id="navigation" class="<?php echo esc_attr( $header_class ) .  esc_attr( crexis_header_color() ) . ' navigation-' . esc_attr( crexis_header_style() ); ?>">
	
	<div class="navigation double-nav <?php echo esc_attr( $nav_class ) . esc_attr( crexis_header_color() ); ?>">
	
		<div class="nav-inner clearfix">
		
			<!-- Logo Area -->
			<div class="logo f-left">
				<!-- Logo Link -->
				<a href="<?php esc_url(crexis_logo_url()); ?>" class="logo-link scroll">

					<?php crexis_site_logo(); ?>
					
				</a>
			</div>
			<!-- End Logo Area -->
			
			<!-- Mobile Menu Button -->
			<!--<a class="mobile-nav-button"><i class="fa fa-bars"></i></a>-->
			
			<a id="mobile-nav-button" class="mobile-nav-button">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</a>
			
			<!-- Navigation Links -->
			<div class="desktop-nav nav-menu clearfix f-right">
			
				 <?php 
				 
				 if($header_style != 'style-transparent-hamburger') {
				 
				 	crexis_nav_menu();
				 
				 } else {
				 
				 	crexis_nav_menu('hamburger');
				 	echo '<span class="fa fa-bars"></span>';
				 
				 }
				 
				 ?>		

			</div>
			
		</div>
		
	</div>
	
	<nav id="mobile-nav" class="mobile-nav">
	
		<?php crexis_nav_menu('mobile-nav'); ?>
		
	</nav>
	
	</nav>

	<?php 
	
	} // End if header disabled
	
	?>
	
	<div id="page-content" <?php crexis_content_class(); ?>>
	
	<?php 
	
	if(!is_front_page() && ( crexis_option('header_title') != 0 || crexis_option('header_title') == '') && get_post_meta(get_the_ID(), 'page_header', true) != 'no-header' && !is_page_template('template-onepager.php') || is_search() && crexis_option('header_title') != 0) {		
		crexis_print_page_title();
	}
	
	?>