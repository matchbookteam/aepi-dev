<?php 
get_header(); 
$blog_style = $grid_style = '';


$layout = 'sidebar_right';
if(get_post_meta(get_option('page_for_posts'), 'page_layout', true)) {
	$layout = get_post_meta(get_option('page_for_posts'), 'page_layout', true);
}


$blog_style = 'classic';

if(crexis_option("blog_style")) $blog_style = crexis_option("blog_style");

$grid_classes = '';
$wide = false;

if($blog_style == "grid") {
	
	$grid_style = crexis_option('blog_grid_style');
	
	if(!crexis_option('blog_grid_style')) {
		$grid_style = 'simple';
	}
	
	$cols = 4;
	
	if(crexis_option("blog_grid_cols") && crexis_option("blog_grid_cols") != $cols) {
		$cols = crexis_option("blog_grid_cols");
	}
	
	$grid_classes = ' masonry-blog bl-'.$cols.'-col';
	
	if(crexis_option("blog_fullwidth") == true) $wide = true;

}




?>

<div class="page-holder blog blog-index page-layout-<?php echo esc_attr($layout); ?> blog-style-<?php echo esc_attr($blog_style); ?><?php echo esc_attr( $grid_classes ); ?>">

	

	<div id="blog" class="<?php if($wide == false) echo 'inner'; ?> clearfix">	
	
		<?php
		
		if($layout != "fullwidth") {
			echo '<div class="page_inner">';
		}
		
		$boxed = 'boxed';
		if($wide == true) $boxed = '';

		echo '<div class="blog-inner blog-style-'.esc_attr($blog_style).' blog-items '.$boxed.'">';
		
		if($blog_style == "grid") {
			$grid_cols = crexis_option('blog_grid_cols');
			if($blog_style == "timeline") {
				$grid_cols = 2;
			}
		}
		
		if (have_posts()) : while (have_posts()) : the_post();
		 	
		 	crexis_blog_post_content($layout, $blog_style); // Layout, Style, Grid Style, Masonry
		 	
		endwhile;
		
	    // Archive doesn't exist:
	    else :
	    
	        esc_html_e('Nothing found, sorry.','crexis');
	    
	    endif;	

    	echo '</div>';
    	
    	crexis_pagination();  
    	
    	if($blog_style == "gsrid") {
    	
    		?>
    		
    		<div id="loadMore-container" class="cbp-l-loadMore-text">
    			<a href="<?php echo get_next_posts_page_link(); ?>" class="cbp-l-loadMore-link">
    			<!--<a href="<?php echo get_template_directory_uri(); ?>/ajax/loadMoreBlog.html" class="cbp-l-loadMore-link">-->
    				<span class="cbp-l-loadMore-defaultText"><?php esc_html_e('Load more','crexis'); ?></span>
    				<span class="cbp-l-loadMore-loadingText"><img src="<?php echo get_template_directory_uri(); ?>/img/loader.gif" alt="loader" /></span>
    				<span class="cbp-l-loadMore-noMoreLoading"><?php esc_html_e('No more posts.','crexis'); ?></span>
    			</a>
    		</div>
    		
    		<?php
    	}
    	
    	if($layout != "fullwidth") { 
    		echo '</div>';
    		get_sidebar();    		
    	}
    	
    	?>
    	
    </div>

</div>

<?php get_footer(); ?>