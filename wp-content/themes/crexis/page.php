<?php 
$post = $wp_query->post;
get_header(); 

$layout = 'fullwidth';

if(get_post_meta(crexis_get_id(), 'page_layout', true)) {
	$layout = get_post_meta(crexis_get_id(), 'page_layout', true);
}

$page_width = get_post_meta(crexis_get_id(), 'page_width', true);
if(!$page_width) $page_width = 'content';
$page_links = '';

?>

<div class="page-holder page-layout-<?php echo esc_attr($layout); ?>">
	
	<?php 
	
	// If Visual Composer is not enabled for the page
	if(!crexis_vc_active() || $layout == 'sidebar_right' || $layout == 'sidebar_left') echo '<div class="inner">';		
	
	if($layout != "fullwidth" || $layout == 'fullwidth' && !crexis_vc_active()) {
		echo '<div class="page_inner">';
	}
	
	if (have_posts()) : while (have_posts()) : the_post(); 
	        
		the_content(); 
		
		wp_link_pages();
		
		if (comments_open()) { 
			echo '<div class="page-comments inner">'; 
			comments_template();
			echo '</div>';
		}
	          
	endwhile; endif; 	     
	
	if($layout != "fullwidth" || $layout == 'fullwidth' && !crexis_vc_active()) {
		echo '</div>';		
	}
	
	if($layout != "fullwidth") {
		get_sidebar();    
	}

	if(!crexis_vc_active() || $layout == 'sidebar_right' || $layout == 'sidebar_left') echo '</div>';
	
	if($page_links == 'yes') {
		wp_link_pages();
	}
	
	?>

</div>

<?php get_footer(); ?>