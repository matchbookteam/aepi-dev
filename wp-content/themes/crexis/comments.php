<?php

	if (have_comments()){	
	
?>
	<div class="post-comments-holder">
		<h2 class="comments-heading"><?php comments_number('0', '1', '%'); echo ' '.esc_html__('Comments','crexis').':'; ?></h2>
		<div id="comments" class="single-blog-post-comments">
			
			<ul class="comments">
			<?php 
			wp_list_comments('type=comment&callback=crexis_comment');				
			?>				
			</ul>
			<div class="pagination"><div class="pagination-inner">
			<?php 
			paginate_comments_links(array(
				'prev_text' => '',
				'next_text' => ''
			)); 
			?>
			</div></div>	
		
		</div>	
	</div>	
	
	<?php } // Comments list end ?>		
	
	<div id="respond">
		
		<h2 class="comments-heading"><?php echo esc_html__('Leave a comment','crexis').':'; ?></h2>
		
		<div class="reply-form post-respond-holder post-form clearfix">
			
		<?php 
		
		$args = array(
			 'title_reply'	=> '',
			 
			 
			 'fields' => apply_filters( 'comment_form_default_fields', array(
			 
			 	'author' 	=> '<div class="col-xs-6 pl-00 pr-10 mt-15 comment-form-author"><input id="author" name="author" placeholder="'.esc_html__('Your Name','crexis').'" type="text" required="required" class="form light-form"/></div>',
			 	
			 	'email' 	=> '<div class="col-xs-6 pl-10 pr-00 mt-15 comment-form-email"><input id="email" name="email" type="email" placeholder="'.esc_html__('Email','crexis').'" required="required" class="form light-form"/></div>',
			   )),
			   
			   'comment_field' =>  '<div class="comment-form-comment col-xs-12 pr-00 pl-00 mt-15"><textarea name="comment" id="comment" placeholder="'.esc_html__('Your comment','crexis').'..." class="form textarea light-form" aria-required="true"></textarea></div>',
		);
					
		comment_form($args); 		
		
		?>			   

		</div>
	</div>