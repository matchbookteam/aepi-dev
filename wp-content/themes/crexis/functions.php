<?php

//
// Crexis Theme Function
//
// Author: Veented
// URL: http://themeforest.net/user/Veented/
// Design: ThemeFire
//
//

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Load Framework
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


require_once( get_template_directory() . '/framework/plugins/plugins-config.php'); 			// Plugins Manager
require_once( get_template_directory() . '/framework/functions/general-functions.php'); 	// General functions
require_once( get_template_directory() . '/framework/functions/blog-functions.php'); 		// Blog related functions
require_once( get_template_directory() . '/framework/functions/page-functions.php'); 		// Page functions & metaboxes
require_once( get_template_directory() . '/framework/functions/header-functions.php'); 		// Header related functions
require_once( get_template_directory() . '/framework/widgets/widgets.php'); 				// Widgets

// Theme Options / Redux Framework

if ( !class_exists( 'ReduxFramework' ) && file_exists( get_template_directory() . '/framework/theme-panel/ReduxCore/framework.php' ) ) {
   require_once( get_template_directory() . '/framework/theme-panel/ReduxCore/framework.php' );
}

if ( !isset( $redux_demo ) && file_exists( get_template_directory() . '/framework/theme-panel/crexis/crexis-config.php' ) ) {
   require_once( get_template_directory() . '/framework/theme-panel/crexis/crexis-config.php' );
}

// Visual Composer:

if(class_exists('Vc_Manager')) {	

	function crexis_extend_composer() {
		require_once get_template_directory() . '/wpbakery/vc-extend.php';
	}
	
	$list = array(
	    'page',
	    'post',
	    'portfolio'
	);
	vc_set_default_editor_post_types( $list );

	add_action('init', 'crexis_extend_composer', 20);	
}


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Localization
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


function crexis_theme_setup() {

	load_theme_textdomain( 'crexis', get_template_directory() . '/lang' );
	add_editor_style( array( '/css/editor.css' ) );
	
	global $wp_version;
	
	if ( version_compare( $wp_version, '3.4', '>=' ) ) {
	    add_theme_support( "custom-header");
	    add_theme_support( "custom-background");
	}
	
}
add_action( 'after_setup_theme', 'crexis_theme_setup' );


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Theme Scripts & Styles
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


function crexis_custom() {
	if (!is_admin()) 
	{
	
		// Load jQuery scripts
			
		wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '', true);
		wp_enqueue_script('waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array( 'jquery' ), '', true);
		wp_enqueue_script('appear', get_template_directory_uri() . '/js/jquery.appear.js', array( 'jquery' ), '', true);
		wp_enqueue_script('easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array( 'jquery' ), '', true);
		wp_enqueue_script('crexis-custom', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), '', true);
							
		wp_register_script('vntd-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array('jquery'));	
		wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'));	
		wp_register_script('YTPlayer', get_template_directory_uri() . '/js/jquery.mb.YTPlayer.js', array('jquery'));		
		wp_register_script('google-map-label', get_template_directory_uri() . '/js/markerwithlabel.js', array('google-map-sensor'));	
		wp_register_script('magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array('jquery'));	
		wp_register_script('crexis-masonry-blog', get_template_directory_uri() . '/js/masonry-blog.js', array('jquery', 'cubePortfolio'));	
		wp_register_script('cubePortfolio', get_template_directory_uri() . '/js/jquery.cubeportfolio.min.js', array('jquery'));
		wp_register_script('cubeConfig', get_template_directory_uri() . '/js/jquery.cubeConfig.js', array('jquery'));
		wp_register_script('crexis-timeline', get_template_directory_uri() . '/js/jquery.timeline.js', array('jquery'));
		wp_register_script('rainyday', get_template_directory_uri() . '/js/rainyday.js', array('jquery'));
		wp_register_script('swiper', get_template_directory_uri() . '/js/swiper.min.js', array('jquery'));
		wp_register_script('vntd-particle', get_template_directory_uri() . '/js/particle.js', array('jquery', 'vntd-greensock'));	
		wp_register_script('vntd-greensock', get_template_directory_uri() . '/js/greensock.js', array('jquery'));
		wp_register_script('fullPage', get_template_directory_uri() . '/js/jquery.fullPage.js', array('jquery'));
		wp_register_script('slimscroll', get_template_directory_uri() . '/js/jquery.slimscroll.min.js', array('jquery'));
		wp_register_script('crexis-fullPageConf', get_template_directory_uri() . '/js/jquery.fullPageConf.js', array('jquery'));
		wp_register_script('tweecool', get_template_directory_uri() . '/js/tweecool.js', array('jquery'));
		
		wp_register_script('superslides', get_template_directory_uri() . '/js/jquery.superslides.js', array('jquery'));	
		wp_register_script('textrotator', get_template_directory_uri() . '/js/jquery.simple-text-rotator.js', array('jquery'));	
		wp_register_script('vntd-parallax', get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js', array('jquery'));	
		wp_register_script('vntd-parallax-conf', get_template_directory_uri() . '/js/parallax.js', array('jquery'));	
		wp_register_script('vntd-skrollr', get_template_directory_uri() . '/js/skrollr.min.js', array('jquery'));	
		
		wp_enqueue_script('vntd-parallax', '', '', '', true);
		
		// Load stylesheets	
		
		wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');	
		wp_enqueue_style('simple-line-icons', get_template_directory_uri() . '/css/simple-line-icons/simple-line-icons.css');
		wp_enqueue_style('animate', get_template_directory_uri() . '/css/scripts/animate.min.css');
		wp_deregister_style('font-awesome');
		wp_dequeue_style('font-awesome'); // Dequeue plugin version	
		wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome/css/font-awesome.min.css', false, '5.4.0');
		wp_enqueue_style('crexis-styles', get_template_directory_uri() . '/style.css', array('bootstrap', 'font-awesome')); // MAIN STYLESHEET
		wp_enqueue_style('socials', get_template_directory_uri() . '/css/socials.css');

		//wp_enqueue_style('crexis-dynamic-styles', get_template_directory_uri() . '/css/style-dynamic.php');
		wp_enqueue_style('crexis-responsive', get_template_directory_uri() . '/css/responsive.css');	// Load responsive stylesheet			
			
		wp_register_style('owl-carousel', get_template_directory_uri() . '/css/scripts/owl.carousel.css');
		wp_register_style('flexslider', get_template_directory_uri() . '/css/scripts/flexslider.css');
		wp_register_style('YTPlayer', get_template_directory_uri() . '/css/YTPlayer/jquery.mb.YTPlayer.min.css');
		wp_register_style('magnific-popup', get_template_directory_uri() . '/css/scripts/magnific-popup.css');
		wp_register_style('prettyPhoto', get_template_directory_uri() . '/css/scripts/prettyPhoto.css');		
		wp_register_style('vimeoBg', get_template_directory_uri() . '/css/scripts/fullscreen_background.css');
		wp_register_style('cubePortfolio', get_template_directory_uri() . '/css/scripts/cubeportfolio.min.css');
		wp_register_style('swiperCSS', get_template_directory_uri() . '/css/scripts/swiper.min.css');
		wp_register_style('crexis-timeline', get_template_directory_uri() . '/css/scripts/timeline.css');
		wp_register_style('crexis-fullPage', get_template_directory_uri() . '/css/scripts/fullpage.css');
		
		// Theme Tone
				
		if( crexis_option('theme_skin') ) {

			if(crexis_option("theme_skin") == "night") {
			
				wp_enqueue_style('crexis-night', get_template_directory_uri() . '/css/theme-skins/night.css');
				
			} elseif(crexis_option("theme_skin") == "dark") {
			
				wp_enqueue_style('crexis-dark', get_template_directory_uri() . '/css/theme-skins/dark.css');
				
			}
			
		}
		
		// Google Maps
		
		$api_key = '';
		
		if( crexis_option( 'google_maps_api' ) ) {
			$api_key = esc_attr( crexis_option( 'google_maps_api' ) );
		}
		
		wp_register_script('google-map-sensor', 'http://maps.google.com/maps/api/js?key=' . $api_key , array('jquery'));
		
		// Dynamic CSS
		
		wp_enqueue_style('crexis-dynamic-css', admin_url('admin-ajax.php').'?action=crexis_dynamic_css');
				
	}
}
add_action('wp_enqueue_scripts', 'crexis_custom');

// Dynamic Stylesheet

function crexis_dynamic_css_fn() {

	require( get_template_directory() . '/css/dynamic.css.php' );

	exit;
	
}

add_action('wp_ajax_crexis_dynamic_css', 'crexis_dynamic_css_fn');
add_action('wp_ajax_nopriv_crexis_dynamic_css', 'crexis_dynamic_css_fn');


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 		Custom Image Sizes & Post Formats
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


if (function_exists('add_theme_support')) { 
	
	// Post Formats	
	
	add_theme_support('post-formats', array('gallery', 'video', 'quote', 'link')); 	
	
	// Image Sizes		
	
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(100, 100, true);
		
	add_image_size('crexis-bg-image', 1920, 9999);
	add_image_size('crexis-fullwidth-landscape', 1170, 620, true);
	add_image_size('crexis-sidebar-landscape', 880, 470, true);
	add_image_size('crexis-sidebar-auto', 880, 9999);	
	add_image_size('crexis-sidebar-square', 660, 540, true);
	add_image_size('crexis-portfolio-square', 480, 350, true);	
	add_image_size('crexis-square-medium', 460, 460, true);
	add_image_size('crexis-blog-thumb', 460, 276, true);
	add_image_size('crexis-portfolio-auto', 480, 9999);
	add_image_size('crexis-portrait', 420, 616, true);
}

function crexis_image_sizes($sizes) {
	
    $sizes['vntd-fullwidth-landscape'] = esc_html__( 'Fullwidth Landscape', 'crexis');
    $sizes['vntd-sidebar-landscape'] = esc_html__( 'Content Landscape', 'crexis'); 
    $sizes['vntd-sidebar-auto'] = esc_html__( 'Content Portrait', 'crexis');
    $sizes['vntd-portfolio-auto'] = esc_html__( 'Portfolio Portrait', 'crexis');
    $sizes['vntd-portfolio-square'] = esc_html__( 'Portfolio Square', 'crexis');
    return $sizes;
}
add_filter('image_size_names_choose', 'crexis_image_sizes');


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 		Custom Menus
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


add_action('init', 'crexis_register_custom_menu');
 
function crexis_register_custom_menu() {
	register_nav_menu('primary', esc_html__('Primary Navigation','crexis'));
	register_nav_menu('secondary', esc_html__('Secondary Navigation','crexis'));
	register_nav_menu('topbar', esc_html__('Top Bar Navigation','crexis'));
}

class crexis_Custom_Menu_Class extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu\">\n";
	}
}

function crexis_walk_nav_menu_items($output, $item, $depth, $args) {

	global $post;
	$front_id = get_option('page_on_front');
	
	if(is_object($post)) {
		$output = str_replace( 'http://frontpage_url/', get_permalink($front_id), $output);	
		$output = str_replace( get_permalink($post->ID).'#', '#', $output );
	}
    
    
    return $output;
}
add_filter( 'walker_nav_menu_start_el', 'crexis_walk_nav_menu_items', 10, 4);

// Remove custom post type parent element

function crexis_remove_parent_classes($class)
{
	return ($class == 'current_page_item' || $class == 'current_page_parent' || $class == 'current_page_ancestor'  || $class == 'current-menu-item') ? FALSE : TRUE;
}

function crexis_add_class_to_wp_nav_menu($classes)
{

	$classes = array_filter($classes, "crexis_remove_parent_classes");

	return $classes;
}

// Theme Options

if(!function_exists('crexis_option')) {
	function crexis_option($option_name, $option_name_value = null) {

		global $crexis_options;
		
		if(is_array($crexis_options)) {
		
			if($option_name_value == null) {
			
				if(array_key_exists($option_name, $crexis_options)) {
					return $crexis_options[$option_name];
				} else {
					return null;
				}
				
			} else {
			
				
				if(array_key_exists($option_name, $crexis_options) && is_array( $crexis_options[$option_name] ) ) {
				
					if(array_key_exists( $option_name_value, $crexis_options[$option_name] )) {
						return $crexis_options[$option_name][$option_name_value];
					}
					
				} else {
					return null;
				}
				
			}
			
		}

		return '';

	}
}


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 		Sidebars
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


if (function_exists('register_sidebar') && !function_exists('crexis_register_sidebars')){
	function crexis_register_sidebars() {
	
		register_sidebar(array(
	        'name' => esc_html__('Default Sidebar','crexis'),
	        'id' => 'default_sidebar',
	        'description'   => esc_html__('Default theme sidebar.','crexis'),
	        'before_widget' => '<div id="%1$s" class="widget bar %2$s">',
	        'after_widget' => '</div>',
	        'before_title' => '<h5>',
	        'after_title' => '</h5>',
	    ));	
		register_sidebar(array(
	        'name' => esc_html__('Archives/Search Sidebar','crexis'),
	        'id' => 'archives',
	        'description'   => esc_html__('Sidebar for posts archive and search results.','crexis'),
	        'before_widget' => '<div id="%1$s" class="widget bar %2$s">',
	        'after_widget' => '</div>',
	        'before_title' => '<h5>',
	        'after_title' => '</h5>',
	    ));	
	    
	    register_sidebar(array(
	        'name' => esc_html__('Footer Column 1','crexis'),
	        'id' => 'footer1',
	        'description'   => esc_html__('Widgets for the first footer column.','crexis'),
	        'before_widget' => '<div class="bar footer-widget footer-widget-col-1 %2$s">',
	        'after_widget' => '</div>',
	        'before_title' => '<h4>',
	        'after_title' => '</h4>',
	    ));
	    register_sidebar(array(
	        'name' => esc_html__('Footer Column 2','crexis'),
	        'id' => 'footer2',
	        'description'   => esc_html__('Widgets for the second footer column.','crexis'),
	        'before_widget' => '<div class="bar footer-widget footer-widget-col-2 %2$s">',
	        'after_widget' => '</div>',
	        'before_title' => '<h4>',
	        'after_title' => '</h4>',
	    ));
	    register_sidebar(array(
	        'name' => esc_html__('Footer Column 3','crexis'),
	        'id' => 'footer3',
	        'description'   => esc_html__('Widgets for the third footer column.','crexis'),
	        'before_widget' => '<div class="bar footer-widget footer-widget-col-3 %2$s">',
	        'after_widget' => '</div>',
	        'before_title' => '<h4>',
	        'after_title' => '</h4>',
	    ));
	    register_sidebar(array(
	        'name' => esc_html__('Footer Column 4','crexis'),
	        'id' => 'footer4',
	        'description'   => esc_html__('Widgets for the fourth footer column.','crexis'),
	        'before_widget' => '<div class="bar footer-widget footer-widget-col-4 %2$s">',
	        'after_widget' => '</div>',
	        'before_title' => '<h4>',
	        'after_title' => '</h4>',
	    ));
	    
	    if (class_exists('Woocommerce')) { // If WooCommerce is enabled, activate related sidebars 
	    
	    	register_sidebar(array(
	    	    'name' => esc_html__('WooCommerce Shop Page', 'crexis'),
	    	    'id'	=> 'woocommerce_shop',
	    	    'description'   => esc_html__('WooCommerce shop page sidebar.','crexis'),
	    	    'before_widget' => '<div id="%1$s" class="widget sidebar-widget bar %2$s">',
	    	    'after_widget' => '</div>',
	    	    'before_title' => '<h5>',
	    	    'after_title' => '</h5>',
	    	));   	
	    	
	    }
		
		// Sidebar Generator
		
		if( crexis_option('sidebar_generator' ) ) {

			foreach(crexis_option('sidebar_generator') as $sidebar) {  
			
				register_sidebar( array(  
					'name' => esc_attr($sidebar),
					'id' => "sidebar_".esc_attr( strtolower( str_replace(' ', '-', $sidebar))),
					'description'   => esc_html__('Customly generated sidebar.','crexis'),
					'before_widget' => '<div id="%1$s" class="widget bar %2$s">',  
					'after_widget' => '</div>',
					'before_title' => '<h5>',
					'after_title' => '</h5>',
				));  
				
			}
		}
	}
	
	add_action( 'widgets_init', 'crexis_register_sidebars' );
	
}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Configure Tag Cloud
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_tag_cloud($args = array()) {
   $args['smallest'] = 14;
   $args['largest'] = 14;
   $args['unit'] = 'px';
   return $args;
   
}
add_filter('widget_tag_cloud_args', 'crexis_tag_cloud', 90);


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Print comment scripts
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


function crexis_comments() {
	if(is_singular() || is_page())
	wp_enqueue_script( 'comment-reply', '', '', '', true);
}
add_action('wp_enqueue_scripts', 'crexis_comments');


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Set content width
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


function crexis_custom_content_width_embed_size($embed_size){
	global $content_width;
	$content_width = 1170;
}
add_filter('template_redirect', 'crexis_custom_content_width_embed_size');


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		WooCommerce Support
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


add_theme_support('woocommerce');

if (class_exists('Woocommerce')) {
	require_once(get_template_directory() . '/woocommerce/config.php'); 	
}

add_theme_support("title-tag");


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Dashboard scripts & styles
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


function crexis_admin_scripts() {	
	wp_enqueue_media();
	wp_register_script('dashboard-jquery', get_template_directory_uri() . '/js/admin/jquery.dashboard.js');
	wp_register_script('media-uploader', get_template_directory_uri() . '/js/admin/media-uploader.js',array( 'jquery' ),true);
	
	wp_enqueue_script('dashboard-jquery', '', '', '', true);
	wp_enqueue_script('media-uploader', '', '', '', true);
	wp_enqueue_script('thickbox', '', '', '', true);	
	wp_localize_script('dashboard-jquery', 'WPURLS', array( 'themeurl' => get_template_directory_uri() ));	
}
add_action( 'admin_enqueue_scripts', 'crexis_admin_scripts' );

if ( !function_exists('crexis_sim_styles') ) {

	function crexis_sim_styles() {
		wp_enqueue_style('cubePortfolio');
		wp_enqueue_style('magnific-popup');
		wp_enqueue_style('owl-carousel');
	}
	
}
add_action('wp_enqueue_scripts', 'crexis_sim_styles');

function crexis_media_view_settings($settings, $post ) {
    if (!is_object($post)) return $settings;
    $shortcode = '[gallery ';
    $ids = get_post_meta($post->ID, 'gallery_images', TRUE);
    $ids = explode(",", $ids);
	
    if (is_array($ids))
        $shortcode .= 'ids = "' . implode(',',$ids) . '"]';
    else
        $shortcode .= "id = \"{$post->ID}\"]";
    $settings['shibaMlib'] = array('shortcode' => $shortcode);
    return $settings;

}

add_filter( 'media_view_settings','crexis_media_view_settings', 10, 2 );

function crexis_admin_styles() {
	wp_enqueue_style('vntd-admin', get_template_directory_uri() . '/css/admin/vntd-admin.css');		
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome/css/font-awesome.min.css');
}


add_action( 'admin_enqueue_scripts', 'crexis_admin_styles' );
add_theme_support( 'automatic-feed-links' );