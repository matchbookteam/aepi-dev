<?php 
/* Template Name: Full Page */
$post = $wp_query->post;
get_header();

	wp_enqueue_script('fullPage', '', '', '', true);
	wp_enqueue_script('slimscroll', '', '', '', true);
	wp_enqueue_script('crexis-fullPageConf', '', '', '', true);
	wp_enqueue_style('crexis-fullPage');
		
	if (have_posts()) : while (have_posts()) : the_post(); 
	        
		the_content(); 
	          
	endwhile; endif;   

get_footer(); 
 
?>