<?php

//
// Custom Visual Composer Scripts for a Theme Integration
//

vc_remove_element('vc_carousel');
vc_remove_element('vc_posts_grid');
vc_remove_element('vc_wp_pages');
vc_remove_element('vc_wp_recentcomments');
vc_remove_element('vc_wp_posts');
vc_remove_element('vc_flickr');
vc_remove_element('vc_pinterest');
vc_remove_element('vc_button2'); // To-do
vc_remove_element('vc_cta_button');
vc_remove_element('vc_cta_button2');


// Fade Animation for elements

function crexis_vc_animation($css_animation) {
	$animation_data = '';
	
	if($css_animation != '') {
		$animation_data = ' data-animation="';
		if($css_animation == 'left-to-right') {
			$animation_data .= 'fadeInLeft';
		} elseif($css_animation == 'right-to-left') {
			$animation_data .= 'fadeInRight';
		} elseif($css_animation == 'top-to-bottom') {
			$animation_data .= 'fadeInDown';
		} elseif($css_animation == 'bottom-to-top') {
			$animation_data .= 'fadeInUp';
		} else {
			$animation_data .= 'fadeIn';
		}
		$animation_data .= '" data-animation-delay="100"';
	}
	
	return $animation_data;
}

// VC Row

//vc_remove_param("vc_row","el_class");

vc_add_param("vc_row", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => esc_html__("Font Color Scheme", 'crexis'),
	"param_name" => "color_scheme",
	"value" => array(
		"Default" => "",
		"White Scheme" => "white"	
	),
	"description" => esc_html__("White Scheme - all text styled to white color, recommended for dark backgrounds. Custom - choose your own heading and text color.", 'crexis'),
));

vc_add_param("vc_row", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => "Background Image Overlay",
	"param_name" => "bg_overlay",
	"value" => array(
		"None" => "",
		"Dark 20%" => "dark",
		"Dark 75%" => "dark75",
		"Gray 80%" => "darker",
		"Gray 89%" => "dark80",
		"Dark 90%" => "black",
		"Dark Dots" => "dark_dots",
		"Accent" => "accent",
		"Light" => "light",
		"Dark Blue" => 'dark_blue',
		"Dark Red" => 'dark_red'
	),
	"description" => esc_html__("Enable the row's background overlay to darken or lighten the background image.", 'crexis'),
));



// VC Pie

$colors_pie_arr = array(
	esc_html__( 'Accent', 'crexis' ) => 'accent',
	esc_html__( 'Grey', 'crexis' ) => 'vntd-color-grey',
	esc_html__( 'Blue', 'crexis' ) => 'vntd-color-blue',
	esc_html__( 'Turquoise', 'crexis' ) => 'vntd-color-turquoise',
	esc_html__( 'Green', 'crexis' ) => 'vntd-color-green',
	esc_html__( 'Orange', 'crexis' ) => 'vntd-color-orange',
	esc_html__( 'Red', 'crexis' ) => 'vntd-color-red',
	esc_html__( 'Black', 'crexis' ) => "vntd-color-black"
);

// VC Accordion

vc_add_param("vc_accordion", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => "Accordion Style",
	"param_name" => "style",
	"value" => array(
		"Style 1" => "style1",
		"Style 2" => "style2",
		"Style 3" => "style3",
		"Style 4" => "style4",
	),
	"description" => "Choose a style for your accordion section."
));

// VC Video

vc_add_param("vc_video", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => "Frame Style",
	"param_name" => "frame",
	"value" => array(
		"None" => "",
		"iPad" => "ipad"		
	),
	"description" => "Enable the iPad frame around the video embed. If enabled, the video will be displayed with preset height and width.",
));
 
// VC Tabs

vc_remove_param("vc_tabs","el_class");

vc_add_param("vc_tabs", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => "Tabs Style",
	"param_name" => "style",
	"value" => array(
		"Style 1" => 'style1',
		"Style 2" => 'style2',
		"Style 3" => 'style3',
		"Style 4" => 'style4',
		"Style 5 (Minimalistic)" => 'style5'
	),
	"description" => "Tab's style.",
));


// VC Separator

vc_add_param("vc_separator", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => "Fullscreen width",
	"param_name" => "fullwidth",
	"value" => array(
		"No" => "",
		"Yes" => "yes"
	),
	"description" => "Make the divider stretch to full size of the browser's viewport.",
));

// VC Text

vc_add_param("vc_column_text", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => "Paragraph font size",
	"param_name" => "font_size",
	"value" => array(
		"Default" => "",
		"13px" => "p13px",
		"16px" => "p16px",
		"17px" => "p17px",
		"18px" => "p18px"
	),
	"description" => "Choose a font size for the paragraph text.",
));

// VC Progress Bar

vc_remove_param("vc_progress_bar","options");
vc_remove_param("vc_progress_bar","el_class");

//vc_add_param("vc_progress_bar", array(
//	'type' => 'textfield',
//	'heading' => __( 'Label2', ' ),
//	'param_name' => 'label2',
//	'description' => __( 'Enter text used as title of bar.', 'crexis' ),
//	'admin_label' => true,
//));

//vc_add_param("vc_progress_bar", array(
//	"type" => "dropdown",
//	"class" => "",
//	"heading" => "Progress Bar Style",
//	"param_name" => "style",
//	"value" => array(
//		"Style 1" => "style1",
//		"Style 2 (text inside the bar)" => "style3",
//	),
//	"description" => "Choose a style for your accordion section."
//));

function crexis_vc_update_defaults() {

  	// Change the default single image size to 'full'
  	$param = WPBMap::getParam( 'vc_single_image', 'img_size' );
  	$param['value'] = 'full';
  	vc_update_shortcode_param( 'vc_single_image', $param );
  
  	// Add Accent color to Progress Bars
  	
  	$param = WPBMap::getParam( 'vc_progress_bar', 'bgcolor' );
  	$param['value'][esc_html__( 'Accent', 'crexis' )] = 'accent';
  	vc_update_shortcode_param( 'vc_progress_bar', $param );
  	
  	
  	// Add Crexis Theme tab style
  	
  	$param = WPBMap::getParam( 'vc_tta_tabs', 'style' );
  	$param['value'][esc_html__( 'Crexis Theme Style', 'crexis' )] = 'crexis';
  	vc_update_shortcode_param( 'vc_tta_tabs', $param );
}

add_action('init', 'crexis_vc_update_defaults', 100); // Visual Composer Defaults

//
// Register new params
//

// Dropdown menu of blog categories

function crexis_vc_blog_cats() {
	$blog_cats = array();
	$blog_categories = get_categories();
	
	foreach($blog_categories as $blog_cat) {
		$blog_cats[$blog_cat->name] = $blog_cat->term_id;
	}
	
	return $blog_cats;
}

// Dropdown menu of portfolio categories

function crexis_vc_portfolio_cats() {

	$portfolio_categories = get_categories('taxonomy=project-type');
	
	if(is_array($portfolio_categories)) {
	
		$portfolio_cats = array();
		
		foreach($portfolio_categories as $portfolio_cat) {
			if(is_object($portfolio_cat)) {
				$portfolio_cats[$portfolio_cat->name] = $portfolio_cat->slug;
			}
		}
		
		return $portfolio_cats;
	
	}
		
	return null;
	
}

function crexis_vc_slider_cats() {

	$portfolio_categories = get_categories('taxonomy=slide-locations');
	
	$portfolio_cats = array();
	
	foreach($portfolio_categories as $portfolio_cat) {
		if(is_object($portfolio_cat)) {
			$portfolio_cats[$portfolio_cat->name] = $portfolio_cat->slug;
		}
	}
	
	return $portfolio_cats;
	
}

function crexis_vc_team_cats() {

	$team_categories = get_categories('taxonomy=member-position');
	
	$team_cats = array();
	
	foreach($team_categories as $team_cat) {
		if(is_object($team_cat)) {
			$team_cats[$team_cat->name] = $team_cat->slug;
		}
	}
	
	return $team_cats;
	
}

//
// Register new shortcodes:
//


// Carousel Portfolio

add_action("admin_init", "crexis_vc_shortcodes"); 

function crexis_vc_shortcodes() {

	// Link Target array
	$target_arr = array(esc_html__("Same window", 'crexis') => "_self", esc_html__("New window", 'crexis') => "_blank");
	$colors_arr = array(esc_html__("Accent Color", 'crexis') => "accent",esc_html__("Accent Color 2", 'crexis') => "accent2",esc_html__("Accent Color 3", 'crexis') => "accent3", esc_html__("Dark", 'crexis') => "dark",esc_html__("Blue", 'crexis') => "blue", esc_html__("Turquoise", 'crexis') => "turquoise", esc_html__("Green", 'crexis') => "green", esc_html__("Orange", 'crexis') => "orange", esc_html__("Red", 'crexis') => "red", esc_html__("Dark", 'crexis') => "dark",esc_html__("Grey", 'crexis') => "grey", esc_html__("Light", 'crexis') => "grey", esc_html__("Custom Color", 'crexis') => "custom");
	$pixel_icons = array(
		array( 'vc_pixel_icon vc_pixel_icon-alert' => esc_html__( 'Alert', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-info' => esc_html__( 'Info', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-tick' => esc_html__( 'Tick', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-explanation' => esc_html__( 'Explanation', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-address_book' => esc_html__( 'Address book', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-alarm_clock' => esc_html__( 'Alarm clock', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-anchor' => esc_html__( 'Anchor', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-application_image' => esc_html__( 'Application Image', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-arrow' => esc_html__( 'Arrow', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-asterisk' => esc_html__( 'Asterisk', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-hammer' => esc_html__( 'Hammer', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-balloon' => esc_html__( 'Balloon', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-balloon_buzz' => esc_html__( 'Balloon Buzz', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-balloon_facebook' => esc_html__( 'Balloon Facebook', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-balloon_twitter' => esc_html__( 'Balloon Twitter', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-battery' => esc_html__( 'Battery', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-binocular' => esc_html__( 'Binocular', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-document_excel' => esc_html__( 'Document Excel', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-document_image' => esc_html__( 'Document Image', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-document_music' => esc_html__( 'Document Music', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-document_office' => esc_html__( 'Document Office', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-document_pdf' => esc_html__( 'Document PDF', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-document_powerpoint' => esc_html__( 'Document Powerpoint', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-document_word' => esc_html__( 'Document Word', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-bookmark' => esc_html__( 'Bookmark', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-camcorder' => esc_html__( 'Camcorder', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-camera' => esc_html__( 'Camera', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-chart' => esc_html__( 'Chart', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-chart_pie' => esc_html__( 'Chart pie', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-clock' => esc_html__( 'Clock', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-fire' => esc_html__( 'Fire', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-heart' => esc_html__( 'Heart', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-mail' => esc_html__( 'Mail', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-play' => esc_html__( 'Play', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-shield' => esc_html__( 'Shield', 'crexis' ) ),
		array( 'vc_pixel_icon vc_pixel_icon-video' => esc_html__( 'Video', 'crexis' ) ),
	);
	
	$bg_overlay_arr = array(
		"None" => "",
		"Dark 20%" => "dark",
		"Dark 75%" => "dark75",
		"Gray 80%" => "darker",
		"Gray 89%" => "dark80",
		"Dark 90%" => "black",
		"Dark Dots" => "dark_dots",
		"Accent" => "accent",
		"Light" => "light",
		"Dark Blue" => 'dark_blue'
	);
	
	vc_map( array(
	   "name" => esc_html__("Portfolio Carousel", 'crexis'),
	   "base" => "portfolio_carousel",
	   "class" => "font-awesome",
	   "icon"      => "fa-briefcase",
	   "controls" => "full",
	   "description" => "Carousel of portfolio posts",
	   "category" => array("Carousels", "Posts"),
	   "params" => array(
		   	array(
		   	   "type" => "dropdown",
		   	   "description" => "Choose a type of your portfolio carousel. Category Carousel: carousel of 3 latest projects for each category.",
		   	   "class" => "hidden-label",
		   	   "heading" => esc_html__("Carousel Type", 'crexis'),
		   	   "param_name" => "type",
		   	   "value" => array(
		   	   		"Classic Portfolio Carousel" => "classic",
		   			"Portfolio Category Carousel" => "category"
		   	   )
		   	),
		   		array(
		   			   "type" => "dropdown",
		   			   "description" => "Hover style for thumbnails.",
		   			   "class" => "hidden-label",
		   			   "heading" => esc_html__("Hover Style", 'crexis'),
		   			   "param_name" => "hover_style",
		   			   "dependency" => Array('element' => "type", 'value' => array("classic")),
		   			   "value" => array(
		   			   		"Dark" => "dark",
		   					"Light" => "light"
		   			   )
		   			),
	      array(
	         "type" => "checkbox",
	         "class" => "hidden-label",
	         "value" => crexis_vc_portfolio_cats(),
	         "heading" => esc_html__("Portfolio Categories", 'crexis'),
	         "param_name" => "cats",
	         "admin_label" => true,
	         "description" => esc_html__("Select categories to be displayed in your carousel. Leave blank for all.", 'crexis')
	      ),	      
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Number of posts to show", 'crexis'),
	         "param_name" => "posts_nr",
	         "value" => "8",
	         "dependency" => Array('element' => "type", 'value' => array("classic")),
	         "description" => esc_html__("This is a total number of posts in the carousel.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "heading" => esc_html__("Columns", 'crexis'),
	         "param_name" => "cols",
	         "value" => array("6","5","4","3","2"),
	         "std" => "3",
	         "dependency" => Array('element' => "type", 'value' => array("classic")),
	         "description" => esc_html__("Number of columns", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Carousel Autoplay", 'crexis'),
	         "param_name" => "autoplay",
	         "value" => array("False" => "false","True" => "true"),
	         "description" => esc_html__("Enable or disable the carousel autoplay.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "heading" => esc_html__("Columns", 'crexis'),
	         "param_name" => "cols_category",
	         "value" => array("5","4","3"),
	         "std" => "5",
	         "dependency" => Array('element' => "type", 'value' => array("category")),
	         "description" => esc_html__("Number of columns", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "heading" => esc_html__("Thumbnail Size", 'crexis'),
	         "param_name" => "thumb_size",
	         "value" => array(
	         	"Portrait" => "portrait",
	         	"Landscape" => "landscape"
	         ),
	         "dependency" => Array('element' => "type", 'value' => array("category")),
	         "description" => esc_html__("Thumbnail size.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "heading" => esc_html__("Category Title Font", 'crexis'),
	         "param_name" => "title_font",
	         "value" => array(
	         	"Georgia" => "georgia",
	         	"Primary Heading Font" => "primary"
	         ),
	         "std" => "5",
	         "dependency" => Array('element' => "type", 'value' => array("category")),
	         "description" => esc_html__("Font Family for the category title.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "description" => "Sort/order your posts by a certain value.",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Order posts by", 'crexis'),
	         "param_name" => "orderby",
	         "value" => array(
	         	"Date" => "date",
	      	    "None - no order" => "none",
	      	    "Post ID" => "ID",
	      	    "Author" => "author",
	      	    "Title" => "title",
	      	    "Name (slug)" => "name",
	      	    "Menu Order" => "menu_order",
	         	),
	         "group" => esc_html__("Order Settings", 'crexis')
	      ),
	     array(
	        "type" => "dropdown",
	        "description" => "Posts order.",
	        "class" => "hidden-label",
	        "heading" => esc_html__("Posts order", 'crexis'),
	        "param_name" => "order",
	        "value" => array(
	        	"ASC" => "ASC",
	        	"DESC" => "DESC",
	        	),
	        "group" => esc_html__("Order Settings", 'crexis')
	     ),
	      
//	     array(
//	        "type" => "dropdown",
//	        "description" => "Choose a hover style",
//	        "class" => "hidden-label",
//	        "heading" => esc_html__("Hover Style", 'crexis'),
//	        "param_name" => "hover_style",
//	        "dependency" => Array('element' => "type", 'value' => array("minimal")),
//	        "value" => array(
//	        	"Dark" => "dark",
//	     	    "Light" => "light"
//	        )
//	     ),
	      
	   )
	));
	
	
	// Blog Carousel
	
	vc_map( array(
	   "name" => esc_html__("Blog Carousel", 'crexis'),
	   "base" => "blog_carousel",
	   "class" => "font-awesome",
	   "icon"      => "fa-file-text-o",
	   "controls" => "full",
	   "category" => array("Carousels", "Posts"),
	   "description" => "Carousel of your blog posts",
	   "params" => array(
	   	array(
	   	   "type" => "checkbox",
	   	   "class" => "hidden-label",
	   	   "value" => crexis_vc_blog_cats(),
	   	   "heading" => esc_html__("Blog Categories", 'crexis'),
	   	   "param_name" => "cats",
	   	   "admin_label" => true,
	   	   "description" => esc_html__("Select categories to be displayed in your carousel. Leave blank for all.", 'crexis')
	   	),
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Number of posts to show", 'crexis'),
	         "param_name" => "posts_nr",
	         "value" => "8",
	         "admin_label" => true,
	         "description" => esc_html__("Number of team members to be displayed in the carousel.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "heading" => esc_html__("Columns", 'crexis'),
	         "param_name" => "cols",
	         "value" => array("5","4","3","2"),
	         "std" => "3",
	         "description" => esc_html__("Number of columns", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Bullet Navigation", 'crexis'),
	         "param_name" => "dots",
	         "value" => array("True" => "true","False" => "false"),
	         "description" => esc_html__("Enable or disable the carousel bullet navigation", 'crexis')
	      ),
	      
	   )
	));
		
	// Testimonials Carousel
	
	vc_map( array(
	   "name" => esc_html__("Testimonials Carousel", 'crexis'),
	   "base" => "testimonials",
	   "icon" => "fa-comments",
	   "class" => "font-awesome",
	   "category" => array("Carousels"),
	   "description" => "Fancy testimonials",
	   "params" => array(  
			array(
			   "type" => "dropdown",
			   "description" => "Choose a style for your testimonials carousel",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Testimonials Style", 'crexis'),
			   "param_name" => "style",
			   "value" => array(
			   		"Basic Carousel" => "simple",
				    "Fullwidth, centered" => "fullwidth",
				    "Fullwidth, centered, small" => "fullwidth2"
			   	),
			   "admin_label" => true
			),
			array(	      
			 "type" => "textfield",
			 "class" => "hidden-label",
			 "heading" => esc_html__("Number of posts to show", 'crexis'),
			 "param_name" => "posts_nr",
			 "value" => "8"
			),
			array(
			   "type" => "dropdown",
			   "heading" => esc_html__("Columns", 'crexis'),
			   "param_name" => "cols",
			   "std" => "3",
			   "dependency" => Array('element' => "style", 'value' => array("simple")),
			   "value" => array("4","3","2"),
			   "description" => esc_html__("Number of columns", 'crexis')
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Carousel Autoplay", 'crexis'),
			   "param_name" => "autoplay",
			   "value" => array("True" => "true","False" => "false"),
			   "description" => esc_html__("Enable or disable the carousel autoplay.", 'crexis')
			),
	   )
	));
	
	// Logos Carousel
	
	vc_map( array(
	   "name" => esc_html__("Logos Carousel", 'crexis'),
	   "base" => "logos_carousel",
	   "icon" => "fa-css3",
	   "class" => "font-awesome",
	   "category" => array("Carousels"),
	   "description" => "Carousel of logo images",
	   "params" => array(  
			array(
				'type' => 'attach_images',
				'heading' => esc_html__( 'Images', 'crexis' ),
				'param_name' => 'images',
				'value' => '',
				'description' => esc_html__( 'Select images from media library.', 'crexis' )
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'On click', 'crexis' ),
				'param_name' => 'onclick',
				'value' => array(
					esc_html__( 'Do nothing', 'crexis' ) => 'link_no',
					esc_html__( 'Open custom link', 'crexis' ) => 'custom_link'
				),
				'description' => esc_html__( 'Define action for onclick event if needed.', 'crexis' )
			),
			array(
				'type' => 'exploded_textarea',
				'heading' => esc_html__( 'Custom links', 'crexis' ),
				'param_name' => 'custom_links',
				'description' => esc_html__( 'Enter links for each logo here. Divide links with linebreaks (Enter) . ', 'crexis' ),
				'dependency' => array(
					'element' => 'onclick',
					'value' => array( 'custom_link' )
				)
			),
			array(
			   "type" => "dropdown",
			   "heading" => esc_html__("Columns", 'crexis'),
			   "param_name" => "cols",
			   "value" => array("7","6","5","4"),
				"std" => "4",
			   "description" => esc_html__("Number of columns", 'crexis')
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Logos Height', 'crexis' ),
				'param_name' => 'logos_height',
				'value' => array(
					esc_html__( 'Regular', 'crexis' ) => 'regular',
					esc_html__( 'High', 'crexis' ) => 'high'
				),
				'description' => esc_html__( 'Use the "high" option for high, vertical logo images.', 'crexis' )
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Logos Style", 'crexis'),
			   "param_name" => "style",
			   'value' => array(
			   	esc_html__( 'Default', 'crexis' ) => 'default',
			   	esc_html__( 'Transparent Background', 'crexis' ) => 'background'
			   ),
			   "description" => esc_html__("Choose a style of your logo carousel.", 'crexis')
			),
			array(
			   "type" => "textfield",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Carousel Title", 'crexis'),
			   "param_name" => "carousel_title",
			   "value" => "",
			   "description" => esc_html__("Optional carousel title.", 'crexis')
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Bullet Navigation", 'crexis'),
			   "param_name" => "dots",
			   "value" => array("False" => "false","True" => "true"),
			   "description" => esc_html__("Enable or disable the carousel bullet navigation", 'crexis')
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Carousel Autoplay", 'crexis'),
			   "param_name" => "autoplay",
			   "value" => array("True" => "true","False" => "false"),
			   "description" => esc_html__("Enable or disable the carousel autoplay.", 'crexis')
			),
	   )
	));
	
	// Fullscreen Features 
	
	vc_map( array(
		   "name" => esc_html__("Boxes Carousel", 'crexis'),
		   "base" => "vntd_boxes_carousel",
		   "class" => "font-awesome",
		   "icon" => "fa-columns",
		   "description" => "Fullheight content carousel",
		   "category" => 'Content',
		   "params" => array(   
		   
		   		array(
		   		   "type" => "dropdown",
		   		   "class" => "hidden-label",
		   		   "heading" => esc_html__("Boxes Background Image", 'crexis'),
		   		   "param_name" => "bg_image",
		   		   "value" => array("Yes" => "yes","No" => "no"),
		   		   "description" => esc_html__("Enable or disable the individual background images for each box.", 'crexis')
		   		),
	   			
	   			array(
	   				'type' => 'param_group',
	   				'heading' => __( 'Boxes', 'crexis' ),
	   				'param_name' => 'boxes',
	   				'description' => __( 'Boxes for the carousel.', 'crexis' ),
	   				'value' => urlencode( json_encode( array(
	   					array(
	   						'bg_image' => '',
	   						'box_heading_top' => '#01',
	   						'box_heading' => __( 'People on Photography', 'crexis' ),
	   						'box_content' => "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock.",
	   						'box_url'	=> '#',
	   						'box_url_label'	=> 'Learn More',
	   					),
	   					array(
   							'bg_image' => '',
   							'box_heading_top' => '#02',
   							'box_heading' => __( 'Love of Nature', 'crexis' ),
   							'box_content' => "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock.",
   							'box_url'	=> '#',
   							'box_url_label'	=> 'Learn More',
   						),
   						array(
							'bg_image' => '',
							'box_heading_top' => '#03',
							'box_heading' => __( 'Animals', 'crexis' ),
							'box_content' => "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock.",
							'box_url'	=> '#',
							'box_url_label'	=> 'Learn More',
						),
						array(
							'bg_image' => '',
							'box_heading_top' => '#04',
							'box_heading' => __( 'Vintage', 'crexis' ),
							'box_content' => "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock.",
							'box_url'	=> '#',
							'box_url_label'	=> 'Learn More',
						),
						array(
							'bg_image' => '',
							'box_heading_top' => '#05',
							'box_heading' => __( 'Foods and Drinks', 'crexis' ),
							'box_content' => "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock.",
							'box_url'	=> '#',
							'box_url_label'	=> 'Learn More',
						),
						
	   				) ) ),
	   				'params' => array(
	   					array(
   							'type' => 'attach_image',
   							'heading' => esc_html__( 'Box Background Image', 'crexis' ),
   							'param_name' => 'bg_image',
   							'value' => '',
   							'description' => esc_html__( 'Select image from media library to be used as the box background.', 'crexis' ),
   						),
	   					array(
   							'type' => 'textfield',
   							'heading' => __( 'Box Top Heading', 'crexis' ),
   							'param_name' => 'box_heading_top',
   							'value' => '#01',
   							'description' => __( 'Enter the big, top heading of the box.', 'crexis' ),
   							'admin_label' => true,
   						),
	   					array(
	   						'type' => 'textfield',
	   						'heading' => __( 'Box Heading', 'crexis' ),
	   						'param_name' => 'box_heading',
	   						'description' => __( 'Enter the box heading.', 'crexis' ),
	   						'admin_label' => true,
	   					),
	   					array(
	   						'type' => 'textfield',
	   						'heading' => __( 'Box Content', 'crexis' ),
	   						'param_name' => 'box_content',
	   						'description' => __( 'Box content, supports HTML.', 'crexis' ),
	   					),
	   					array(
   							'type' => 'textfield',
   							'heading' => __( 'Box URL', 'crexis' ),
   							'param_name' => 'box_url',
   							'description' => __( 'Box content URL. Optional.', 'crexis' ),
   						),
   						array(
							'type' => 'textfield',
							'heading' => __( 'Box Link Label', 'crexis' ),
							'param_name' => 'box_url_label',
							'value' => 'Read more',
							'dependency' => array(
								'element' => 'box_url',
								'not_empty' => true
							),
							'description' => __( 'Box content link label.', 'crexis' ),
						),
	   				),
	   			),

	   			
		   	)
		   		
	));
	
	// Video Lightbox
	
	vc_map( array(
	   "name" => esc_html__("Video Lightbox", 'crexis'),
	   "base" => "video_lightbox",
	   "icon" => "fa-play-circle-o",
	   "category" => array("Media"),
	   "class" => "font-awesome",
	   "description" => "Video in lightbox window",
	   "params" => array(  
	   		array(
	   			'type' => 'textfield',
	   			'heading' => esc_html__( 'Video link', 'crexis' ),
	   			'param_name' => 'link',
	   			'admin_label' => true,
	   			'description' => sprintf( esc_html__( 'Link to the video. More about supported formats at %s.', 'crexis' ), '<a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">WordPress codex page</a>' )
	   		),
		   array(
			   "type" => "textfield",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Title", 'crexis'),
			   "param_name" => "title",
			   "value" => "Our video",
			   "desc" => esc_html__('Title of the element.', 'crexis')
		   ),
			array(	      
			 "type" => "textfield",
			 "class" => "hidden-label",
			 "heading" => esc_html__("Description", 'crexis'),
			 "param_name" => "description",
			 "value" => "This is our video!",
			 "desc" => esc_html__('Description of the element', 'crexis')
			),
			array(	      
			 "type" => "textfield",
			 "class" => "hidden-label",
			 "heading" => esc_html__("Video length", 'crexis'),
			 "param_name" => "length",
			 "value" => "03:46",
			 "desc" => esc_html__('Video length will be displayed under the description.', 'crexis')
			)
	   )
	));
	
	// Team Carousel
	
//	vc_map( array(
//	   "name" => esc_html__("Team Carousel", 'crexis'),
//	   "base" => "team_carousel",
//	   "class" => "font-awesome",
//	   "icon"      => "fa-users",
//	   "controls" => "full",
//	   "category" => array("Carousels", "Posts"),
//	   "description" => "Carousel of your team members",
//	   "params" => array(
//	   	array(
//	   	   "type" => "textfield",
//	   	   "class" => "hidden-label",
//	   	   "value" => "",
//	   	   "heading" => esc_html__("Team Members ID", 'crexis'),
//	   	   "param_name" => "ids",
//	   	   "description" => esc_html__("Insert IDs of specific team members to be displayed in this carousel. Leave blank to display all", 'crexis')
//	   	),
//	      array(
//	         "type" => "textfield",
//	         "class" => "hidden-label",
//	         "heading" => esc_html__("Number of posts to show", 'crexis'),
//	         "param_name" => "posts_nr",
//	         "value" => "8",
//	         "admin_label" => true,
//	         "description" => esc_html__("Number of team members to be displayed in the carousel.", 'crexis')
//	      ),
//	      array(
//	         "type" => "dropdown",
//	         "heading" => esc_html__("Columns", 'crexis'),
//	         "param_name" => "cols",
//	         "value" => array("5","4","3","2"),
//	         "description" => esc_html__("Number of columns", 'crexis')
//	      ),
//	      array(
//	         "type" => "dropdown",
//	         "class" => "hidden-label",
//	         "heading" => esc_html__("Style", 'crexis'),
//	         "param_name" => "style",
//	         "value" => array(
//	         	"Default - title, role and social icons below the thumbnail" => "simple",
//	         	"Default Transparent - title, role and social icons below the thumbnail" => "transparent",
//	         	"Circle - title, role and social icons below the rounded thumbnail" => "circle",
//	         	"Hover - title, role and social icons displayed on thumbnail hover" => "hover",
//	      		"Slide from bottom - member data slide up on thumbnail hover" => "slide_bottom"
//	         ),
//	         "description" => esc_html__("Choose a style of your portfolio.", 'crexis')
//	      ),
//	      array(
//	         "type" => "dropdown",
//	         "class" => "hidden-label",
//	         "heading" => esc_html__("Display member description", 'crexis'),
//	         "param_name" => "excerpt",
//	         "value" => array("Yes" => "yes","No" => "no"),
//	         "description" => esc_html__("Display team member's description.", 'crexis')
//	      ),
//	      array(
//	         "type" => "dropdown",
//	         "class" => "hidden-label",
//	         "heading" => esc_html__("Thumbnail Spacing", 'crexis'),
//	         "param_name" => "thumb_space",
//	         "value" => array("Yes" => "yes","No" => "no"),
//	         "description" => esc_html__("Enable or disable the white space between post thumbnails.", 'crexis')
//	      ),
//	      array(
//	         "type" => "textfield",
//	         "class" => "hidden-label",
//	         "heading" => esc_html__("Carousel Title", 'crexis'),
//	         "param_name" => "carousel_title",
//	         "value" => "",
//	         "description" => esc_html__("Optional carousel title.", 'crexis')
//	      ),
//	      array(
//	         "type" => "dropdown",
//	         "class" => "hidden-label",
//	         "heading" => esc_html__("Bullet Navigation", 'crexis'),
//	         "param_name" => "dots",
//	         "value" => array("True" => "true","False" => "false"),
//	         "description" => esc_html__("Enable or disable the carousel bullet navigation", 'crexis')
//	      ),
//	      array(
//	         "type" => "dropdown",
//	         "class" => "hidden-label",
//	         "heading" => esc_html__("Arrow Navigation", 'crexis'),
//	         "param_name" => "nav",
//	         "value" => array("False" => "false","True" => "true"),
//	         "description" => esc_html__("Enable or disable the carousel arrow navigation", 'crexis')
//	      ),
//	      array(
//	         "type" => "dropdown",
//	         "class" => "hidden-label",
//	         "heading" => esc_html__("Arrow Navigation Position", 'crexis'),
//	         "param_name" => "nav_position",
//	         "value" => array("Bottom" => "bottom","Top Right" => "top_right"),
//	         "dependency" => Array('element' => "nav", 'value' => array("true")),
//	         "description" => esc_html__("Select position for the carousel arrow navigation.", 'crexis')
//	      ),
//	      
//	   )
//	));
	
	// Team Grid
	
	vc_map( array(
	   "name" => esc_html__("Team Members", 'crexis'),
	   "base" => "team_grid",
	   "class" => "font-awesome",
	   "icon"      => "fa-users",
	   "controls" => "full",
	   "category" => array('Posts'),
	   "description" => "Portfolio posts grid",
	   "params" => array(
	      array(
	         "type" => "dropdown",
	         "heading" => esc_html__("Columns", 'crexis'),
	         "param_name" => "cols",
	         "std" => "3",
	         "value" => array("4","3","2"),
	         "description" => esc_html__("Number of columns", 'crexis')
	      ),
	      array(
	         "type" => "checkbox",
	         "class" => "hidden-label",
	         "value" => crexis_vc_team_cats(),
	         "heading" => esc_html__("Team member positions", 'crexis'),
	         "param_name" => "cats",
	         "description" => esc_html__("Select member positions to be displayed in the grid. Leave blank to display all.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Thumbnail Style", 'crexis'),
	         "param_name" => "style",
	         "value" => array(
	         	"Minimal - team member details visible on thumbnail hover" => "type-1",
	         	"Default - title, role and social icons below the thumbnail" => "type-2"
	         ),

	         "description" => esc_html__("Choose a style of your team member thumbnail.", 'crexis')
	      ),      
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Posts Number", 'crexis'),
	         "param_name" => "posts_nr",
	         "value" => '',
	         "description" => esc_html__("Number of portfolio posts to be displayed. Leave blank for no limit.", 'crexis')
	      ),
	      array(
	          "type" => "dropdown",
	          "description" => "Sort/order your posts by a certain value.",
	          "class" => "hidden-label",
	          "heading" => esc_html__("Order posts by", 'crexis'),
	          "param_name" => "orderby",
	          "value" => array(
	          	"Date" => "date",
	       	    "None - no order" => "none",
	       	    "Post ID" => "ID",
	       	    "Author" => "author",
	       	    "Title" => "title",
	       	    "Name (slug)" => "name",
	       	    "Menu Order" => "menu_order",
	          	),
	          "group" => esc_html__("Order Settings", 'crexis')
	       ),
	      array(
	         "type" => "dropdown",
	         "description" => "Posts order.",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Posts order", 'crexis'),
	         "param_name" => "order",
	         "value" => array(
	         	"DESC" => "DESC",
	      	    "ASC" => "ASC",
	         	),
	         "group" => esc_html__("Order Settings", 'crexis')
	      ),
	   )
	));
	
	// Portfolio Grid
	
	vc_map( array(
	   "name" => esc_html__("Portfolio Grid", 'crexis'),
	   "base" => "portfolio_grid",
	   "class" => "font-awesome",
	   "icon"      => "fa-th",
	   "controls" => "full",
	   "category" => array('Posts'),
	   "description" => "Portfolio posts grid",
	   "params" => array(
	      
	      array(
	         "type" => "checkbox",
	         "class" => "hidden-label",
	         "value" => crexis_vc_portfolio_cats(),
	         "heading" => esc_html__("Portfolio Categories", 'crexis'),
	         "param_name" => "cats",
	         "description" => esc_html__("Select categories to be displayed in the grid. Leave blank to display all.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "heading" => esc_html__("Columns", 'crexis'),
	         "param_name" => "cols",
	         "value" => array("6","5","4","3","2"),
	         "std" => "4",
	         "description" => esc_html__("Number of columns", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Thumbnail Style", 'crexis'),
	         "param_name" => "thumb_style",
	         "value" => array(
	         	"Classic (title displayed below)" => "classic",
	         	"Minimal (title on hover)" => "minimal"
	         ),
	         "description" => esc_html__("Choose a style of your portfolio.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Thumbnail Spacing", 'crexis'),
	         "param_name" => "thumb_space",
	         "value" => array("Yes" => "yes","No" => "no"),
	         "description" => esc_html__("Enable or disable the white space between post thumbnails.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Masonry", 'crexis'),
	         "std"	=> 'square',
	         "param_name" => "masonry",
	         "value" => array("No" => "no","Yes" => "yes"),
	         "description" => esc_html__("Enable the masonry effect.", 'crexis')
	      ),	      
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Posts Number", 'crexis'),
	         "param_name" => "posts_nr",
	         "value" => '',
	         "description" => esc_html__("Number of portfolio posts to be displayed. Leave blank for no limit.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__( "Pagination", 'crexis' ),
	         "param_name" => "pagination",
	         "value" => array(
	         	esc_html__( "No", 'crexis' ) => "no",
	         	esc_html__( "Yes", 'crexis' ) => "yes"
	         ),
	         "description" => esc_html__( "Enable a classic pagination for your portfolio grid.", 'crexis')
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__( "Ajax Load More", 'crexis' ),
	         "param_name" => "ajax",
	         "value" => array(
	         	esc_html__( "No", 'crexis' ) => "no",
	         	esc_html__( "Yes", 'crexis' ) => "yes"
	         ),
	         "description" => esc_html__( "Enable the dynamic loading of posts. Classic pagination will be disabled if Ajax is turned on.", 'crexis' )
	      ),
	      array(
  	         "type" => "dropdown",
  	         "class" => "hidden-label",
  	         "heading" => esc_html__("Filtering menu", 'crexis'),
  	         "param_name" => "filter",
  	         "value" => array("Yes" => "yes","No" => "no"),
  	         "description" => esc_html__("Enable or disable the filterable effect.", 'crexis'),
  	         "group" => esc_html__("Filtering", 'crexis')
  	      ),
  	      array(
  	         "type" => "dropdown",
  	         "class" => "hidden-label",
  	         "heading" => esc_html__("Filtering menu style", 'crexis'),
  	         "param_name" => "filter_style",
  	         "value" => array(
  	         	"Simple" => "simple",
  	         	"Boxed" => "boxed"
  	         ),
  	         "dependency" => Array('element' => "filter", 'value' => array("yes")),
  	         "description" => esc_html__("Choose a style of the filtering menu.", 'crexis'),
  	         "group" => esc_html__("Filtering", 'crexis')
  	      ),
  	      array(
  	             "type" => "dropdown",
  	             "class" => "hidden-label",
  	             "heading" => esc_html__("Filtering menu counter", 'crexis'),
  	             "param_name" => "filter_counter",
  	             "value" => array("Yes" => "yes","No" => "no"),
  	             "dependency" => Array('element' => "filter", 'value' => array("yes")),
  	             "description" => esc_html__("Enable or disable the item counter for each category.", 'crexis'),
  	             "group" => esc_html__("Filtering", 'crexis')
  	          ),
  	      array(
  	         "type" => "dropdown",
  	         "class" => "hidden-label",
  	         "heading" => esc_html__("Filtering animation", 'crexis'),
  	         "param_name" => "animation",
  	         "std" => "quicksand",
  	         "value" => array(
  	         	'rotateSides',
  	         	'fadeOut',
  	         	'quicksand',
  	         	'boxShadow',
  	         	'bounceLeft',
  	         	'bounceTop',
  	         	'bounceBottom',
  	         	'moveLeft',
  	         	'slideLeft',
  	         	'fadeOutTop',
  	         	'sequentially',
  	         	'skew',
  	         	'slideDelay',
  	         	'flipOutDelay',
  	         	'flipOut',
  	         	'unfold',
  	         	'foldLeft',
  	         	'scaleDown',
  	         	'scaleSides',
  	         	'frontRow',
  	         	'flipBottom',
  	         	'rotateRoom'
  	         ),
  	         "dependency" => Array('element' => "filter", 'value' => array("yes")),
  	         "description" => esc_html__("Choose a style of the filtering menu.", 'crexis'),
  	         "group" => esc_html__("Filtering", 'crexis')
  	      ),
  	      array(
  	          "type" => "dropdown",
  	          "description" => "Sort/order your posts by a certain value.",
  	          "class" => "hidden-label",
  	          "heading" => esc_html__("Order posts by", 'crexis'),
  	          "param_name" => "orderby",
  	          "value" => array(
  	          	"Date" => "date",
  	       	    "None - no order" => "none",
  	       	    "Post ID" => "ID",
  	       	    "Author" => "author",
  	       	    "Title" => "title",
  	       	    "Name (slug)" => "name",
  	       	    "Menu Order" => "menu_order",
  	          	),
  	          "group" => esc_html__("Order Settings", 'crexis')
  	       ),
  	      array(
  	         "type" => "dropdown",
  	         "description" => "Posts order.",
  	         "class" => "hidden-label",
  	         "heading" => esc_html__("Posts order", 'crexis'),
  	         "param_name" => "order",
  	         "value" => array(
  	         	"DESC" => "DESC",
  	      	    "ASC" => "ASC",
  	         	),
  	         "group" => esc_html__("Order Settings", 'crexis')
  	      ),
	   )
	));
	
	vc_map( array(
	   "name" => esc_html__("Portfolio Timeline", 'crexis'),
	   "base" => "vntd_portfolio_timeline",
	   "class" => "font-awesome",
	   "icon"      => "fa-briefcase",
	   "controls" => "full",
	   "description" => "Carousel of portfolio posts",
	   "category" => array("Carousels", "Posts"),
	   "params" => array(
	      array(
	         "type" => "checkbox",
	         "class" => "hidden-label",
	         "value" => crexis_vc_portfolio_cats(),
	         "heading" => esc_html__("Portfolio Categories", 'crexis'),
	         "param_name" => "cats",
	         "admin_label" => true,
	         "description" => esc_html__("Select categories to be displayed in your carousel. Leave blank for all.", 'crexis')
	      ),	      
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Number of posts to show", 'crexis'),
	         "param_name" => "posts_nr",
	         "value" => "8",
	         "dependency" => Array('element' => "type", 'value' => array("classic")),
	         "description" => esc_html__("This is a total number of posts in the carousel.", 'crexis')
	      ),
//	      array(
//	         "type" => "dropdown",
//	         "heading" => esc_html__("Columns", 'crexis'),
//	         "param_name" => "cols",
//	         "value" => array("6","5","4","3","2"),
//	         "std" => "4",
//	         "dependency" => Array('element' => "type", 'value' => array("classic")),
//	         "description" => esc_html__("Number of columns", 'crexis')
//	      ),
	      //),
	      
	   )
	));
	
	// Blog
	
	vc_map( array(
	   "name" => esc_html__("Blog", 'crexis'),
	   "base" => "vntd_blog",
	   "class" => "font-awesome",
	   "icon" => "fa-file-text-o",
	   "controls" => "full",
	   "category" => "Posts",
	   "params" => array(     
	      array(
	         "type" => "checkbox",
	         "class" => "hidden-label",
	         "value" => crexis_vc_blog_cats(),
	         "heading" => esc_html__("Blog Categories", 'crexis'),
	         "param_name" => "cats"
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Blog Style", 'crexis'),
	         "param_name" => "blog_style",
	         "value" => array(
	         	"Classic" 	=> "classic",
	         	"Grid" 		=> "grid",
	         	"Minimal"	=> "minimal"
	         )
	      ), 
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Grid Columns", 'crexis'),
	         "param_name" => "grid_cols",
	         "value" => array("6","5","4","3","2"),
	         "std" => "3",
	         "dependency" => Array('element' => "blog_style", 'value' => array("grid"))
	      ),
	      array(
	         "type" => "dropdown",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Ajax Pagination (Load More button)", 'crexis'),
	         "param_name" => "ajax",
	         "value" => array(
	         	"No" 	=> "no",
	         	"Yes" 	=> "yes",
	         )
	      ),
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Posts per page", 'crexis'),
	         "param_name" => "posts_nr",
	         "value" => '',
	      )      
	   )
	));
	
	
	// Icon Box
	
	vc_map( array(
	   "name" => esc_html__("Icon Box", 'crexis'),
	   "base" => "icon_box",
	   "class" => "font-awesome",
	   "icon" => "fa-check-circle-o",
	   "controls" => "full",
	   "category" => 'Content',
	   "description" => "Text with an icon",
	   "params" => array(
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Title", 'crexis'),
	         "param_name" => "title",
	         "holder" => "h4",
	         "value" => 'Icon Box Title'
	      ), 	      
	      array(
	         "type" => "textarea",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Text Content", 'crexis'),
	         "param_name" => "text",
	         "holder" => "span",
	         "value" => 'Icon Box text content, feel free to change it!',
	      ), 
	      array(
	        "type" => "textfield",
	        "heading" => esc_html__("URL (Link)", 'crexis'),
	        "param_name" => "url",
	        "description" => esc_html__("Optional icon link.", 'crexis')
	      ),
//	      array(
//	        "type" => "dropdown",
//	        "heading" => esc_html__("Link Button", 'crexis'),
//	        "param_name" => "link_button",
//	        "value" => array(
//	        	'No' => 'no',
//	        	'Yes' => 'yes'
//	        	),
//	        "dependency" => Array('element' => "url", 'not_empty' => true)
//	      ),
//	      array(
//	        "type" => "textfield",
//	        "heading" => esc_html__("Link Button Label", 'crexis'),
//	        "param_name" => "link_button_label",
//	        "value" => 'Read more',
//	        "dependency" => Array('element' => "link_button", 'value' => 'yes')
//	      ),
	      array(
	        "type" => "dropdown",
	        "heading" => esc_html__("Target", 'crexis'),
	        "param_name" => "target",
	        "value" => $target_arr,
	        "dependency" => Array('element' => "url", 'not_empty' => true)
	      ),
	      array(
	         "type" 		=> "dropdown",
	         "class" 		=> "hidden-label",
	         "heading" 		=> esc_html__("Style", 'crexis'),
	         "param_name" 	=> "style",
	         "value" => array(
	         	'Big Centered Circle' 			=> 'big-centered-circle',
	         	'Big Centered Circle Outline' 	=> 'big-centered-circle-outline',
	         	'Big Centered Square' 			=> 'big-centered-square',
	         	'Small Aligned Left'			=> 'small-left',
	         	'Medium Aligned Left'			=> 'medium-left',
	         	'Medium Aligned Left Square'	=> 'medium-left-square',
	         	'Medium Aligned Left Circle'	=> 'medium-left-circle',
	         	'Medium Aligned Right'			=> 'medium-right',
	         	'Medium Aligned Right Square'	=> 'medium-right-square',
	         	'Medium Aligned Right Circle'	=> 'medium-right-circle',
	         	)
	      ),
	      
	      	array(
	      	  "type" => "dropdown",
	      	  "heading" => esc_html__("Icon Box Color", 'crexis'),
	      	  "param_name" => "color",
	      	  "value" => array(
	      	  	"Accent 1" => "accent",
	      	  	"Accent 2" => "accent2",
	      	  	"Accent 3" => "accent3"
	      	  ),
	      	  "description" => "Choose a color for your icon box.",
	      	  "dependency" => Array('element' => "style", 'value' => array('medium-right-circle','medium-left-circle'))
	      	),
	      	
	      array(
	        "type" => "dropdown",
	        "heading" => esc_html__("Animated", 'crexis'),
	        "param_name" => "animated",
	        "value" => array("Yes" => "yes","No" => "no"),
	        "description" => "Enable the element fade in animation on scroll"
	      ),
	      	array(
	      	  "type" => "textfield",
	      	  "heading" => esc_html__("Animation Delay", 'crexis'),
	      	  "param_name" => "animation_delay",
	      	  "value" => '100',
	      	  "description" => "Fade in animation delay. Can be used to create a nice delay effect if multiple elements of same type.",
	      	  "dependency" => Array('element' => "animated", 'value' => 'yes')
	      	),
	      	
	      array(
	      	'type' => 'dropdown',
	      	'heading' => esc_html__( 'Icon library', 'crexis' ),
	      	'value' => array(
	      		esc_html__( 'Font Awesome', 'crexis' ) => 'fontawesome',
	      		esc_html__( 'Open Iconic', 'crexis' ) => 'openiconic',
	      		esc_html__( 'Typicons', 'crexis' ) => 'typicons',
	      		esc_html__( 'Entypo', 'crexis' ) => 'entypo',
	      		esc_html__( 'Linecons', 'crexis' ) => 'linecons',
	      		esc_html__( 'Pixel', 'crexis' ) => 'pixelicons',
	      	),
	      	'param_name' => 'icon_type',
	      	'description' => esc_html__( 'Select icon library.', 'crexis' ),
	      ),
	      array(
	      	'type' => 'iconpicker',
	      	'heading' => esc_html__( 'Icon', 'crexis' ),
	      	'param_name' => 'icon_fontawesome',
	          'value' => 'fa fa-info-circle',
	      	'settings' => array(
	      		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	      		'iconsPerPage' => 200, // default 100, how many icons per/page to display
	      	),
	      	'dependency' => array(
	      		'element' => 'icon_type',
	      		'value' => 'fontawesome',
	      	),
	      	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	      ),
	      array(
	      	'type' => 'iconpicker',
	      	'heading' => esc_html__( 'Icon', 'crexis' ),
	      	'param_name' => 'icon_openiconic',
	      	'settings' => array(
	      		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	      		'type' => 'openiconic',
	      		'iconsPerPage' => 200, // default 100, how many icons per/page to display
	      	),
	      	'dependency' => array(
	      		'element' => 'icon_type',
	      		'value' => 'openiconic',
	      	),
	      	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	      ),
	      array(
	      	'type' => 'iconpicker',
	      	'heading' => esc_html__( 'Icon', 'crexis' ),
	      	'param_name' => 'icon_typicons',
	      	'settings' => array(
	      		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	      		'type' => 'typicons',
	      		'iconsPerPage' => 200, // default 100, how many icons per/page to display
	      	),
	      	'dependency' => array(
	      	'element' => 'icon_type',
	      	'value' => 'typicons',
	      ),
	      	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	      ),
	      array(
	      	'type' => 'iconpicker',
	      	'heading' => esc_html__( 'Icon', 'crexis' ),
	      	'param_name' => 'icon_entypo',
	      	'settings' => array(
	      		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	      		'type' => 'entypo',
	      		'iconsPerPage' => 300, // default 100, how many icons per/page to display
	      	),
	      	'dependency' => array(
	      		'element' => 'icon_type',
	      		'value' => 'entypo',
	      	),
	      ),
	      array(
	      	'type' => 'iconpicker',
	      	'heading' => esc_html__( 'Icon', 'crexis' ),
	      	'param_name' => 'icon_linecons',
	      	'settings' => array(
	      		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	      		'type' => 'linecons',
	      		'iconsPerPage' => 200, // default 100, how many icons per/page to display
	      	),
	      	'dependency' => array(
	      		'element' => 'icon_type',
	      		'value' => 'linecons',
	      	),
	      	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	      ),
	      array(
	      	'type' => 'iconpicker',
	      	'heading' => esc_html__( 'Icon', 'crexis' ),
	      	'param_name' => 'icon_pixelicons',
	      	'settings' => array(
	      		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	      		'type' => 'pixelicons',
	      		'source' => $pixel_icons,
	      	),
	      	'dependency' => array(
	      		'element' => 'icon_type',
	      		'value' => 'pixelicons',
	      	),
	      	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	      ),     
	   )
	));	
	
	
	// Google Map
	
	vc_map( array(
	   "name" => esc_html__("Google Map", 'crexis'),
	   "base" => "gmap",
	   "icon"      => "icon-wpb-map-pin",
	   "category" => 'Content',
	   "description" => "Map block",
	   "params" => array(
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Location Latitude", 'crexis'),
	         "param_name" => "lat",
	         "value" => '41.862274',
	         "description" => esc_html__("Please insert the map address latitude if you have problems displaying it. Helpful site: <a target='_blank' href='http://www.latlong.net/'>http://www.latlong.net/</a>", 'crexis')
	      ),
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Location Longitude", 'crexis'),
	         "param_name" => "long",
	         "value" => '-87.701328',
	         "description" => esc_html__("Please insert the map address longitude value.", 'crexis')
	      ),
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Map Height", 'crexis'),
	         "param_name" => "height",
	         "value" => '400',
	         "description" => esc_html__("Height of the map element in pixels.", 'crexis')
	      ),
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Map Zoom", 'crexis'),
	         "param_name" => "zoom",
	         "value" => '15',
	         "description" => esc_html__("Choose the map zoom. Default value: 15", 'crexis')
	      ),
	      array(
	      	'type' => 'dropdown',
	      	'heading' => esc_html__( 'Map Style', 'crexis' ),
	      	'value' => array(
	      		esc_html__( 'Grayscale', 'crexis' ) => 'grayscale',
	      		esc_html__( 'Regular', 'crexis' ) => 'regular',
	      	),
	      	'param_name' => 'map_style',
	      	'description' => esc_html__( 'Choose a style for your map.', 'crexis' ),
	      ),
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Marker 1 Title", 'crexis'),
	         "param_name" => "marker1_title",
	         "value" => 'Office 1',
	         "description" => esc_html__("Marker 1 Title.", 'crexis')
	      ),
	      
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Marker 1 Text", 'crexis'),
	         "param_name" => "marker1_text",
	         "value" => 'Your description goes here.',
	         "description" => esc_html__("Marker 1 description text.", 'crexis')
	      ),	      
	      array(
	        "type" => "dropdown",
	        "heading" => esc_html__("Marker 1 Location", 'crexis'),
	        "param_name" => "marker1_location",
	        "value" => array(esc_html__("Map Center", 'crexis') => "center", esc_html__("Custom", 'crexis') => "custom"),
	        "description" => esc_html__("The first marker location", 'crexis')
	      ),
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Marker 1 Custom Location", 'crexis'),
	         "param_name" => "marker1_location_custom",
	         "value" => '41.863774,-87.721328',
	         "description" => esc_html__("Marker 1 custom location in latitude,longitude format.", 'crexis')
	      ),	
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Marker 2 Title", 'crexis'),
	         "param_name" => "marker2_title",
	         "value" => '',
	         "description" => esc_html__("Marker 2 Title.", 'crexis')
	      ), 
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Marker 2 Text", 'crexis'),
	         "param_name" => "marker2_text",
	         "value" => 'My secondary marker description.',
	         "dependency" => Array('element' => "marker2_title", 'not_empty' => true),
	         "description" => esc_html__("Marker 2 description text.", 'crexis')
	      ),
	      array(
	         "type" => "textfield",
	         "class" => "hidden-label",
	         "heading" => esc_html__("Marker 2 Custom Location", 'crexis'),
	         "param_name" => "marker2_location",
	         "value" => '41.858774,-87.685328',
	         "dependency" => Array('element' => "marker2_title", 'not_empty' => true),
	         "description" => esc_html__("Marker 2 location in latitude,longitude format.", 'crexis')
	      ),
	      array(
	      	'type' => 'dropdown',
	      	'heading' => esc_html__( 'Mouse scroll for zoom', 'crexis' ),
	      	'value' => array(
	      		esc_html__( 'No', 'crexis' ) => 'false',
	      		esc_html__( 'Yes', 'crexis' ) => 'true',
	      	),
	      	'param_name' => 'map_scroll',
	      	'description' => esc_html__( 'Choose a style for your map.', 'crexis' ),
	      )         
	      
	   )
	));
	
	// Special Heading
	
	vc_map( array(
	   "name" => esc_html__("Special Heading", 'crexis'),
	   "base" => "special_heading",
	   "class" => "no-icon",
	   "icon" => "fa-header",
	   "description" => "Centered heading text",
	   "category" => 'Content',
	   "params" => array(     
			array(
			    "type" => "textfield",	         
			    "heading" => esc_html__("Title", 'crexis'),
			    "param_name" => "title",
			    "holder" => "h5",
			    "description" => esc_html__("Main heading text. Use (b)word(/b) tags for highlighted text.", 'crexis'),
			    "value" => "This is a Special Heading"
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Subtitle", 'crexis'),
			    "param_name" => "subtitle",
			    "holder" => "span",
			    "description" => esc_html__("Smaller text visible below the Main one.", 'crexis'),
			    "value" => "This is a subtitle, feel free to change it!"
			),    	
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Extra top title", 'crexis'),
			    "param_name" => "uptitle",
			    "description" => esc_html__("Additional title displayed above the main one.", 'crexis'),
			    "value" => ""
			),  		    
			array(
			  "type" => "dropdown",
			  "heading" => esc_html__("Animation", 'crexis'),
			  "param_name" => "animated",
			  "class" => "hidden-label",
			  "value" => array( esc_html__("No", 'crexis') => "no", esc_html__("Yes", 'crexis') => "yes"),
			  "description" => esc_html__("Enable the fade-in animation of the heading elements on site scroll.", 'crexis')
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Separator", 'crexis'),
			   "description" => esc_html__("Display a short line separator.", 'crexis'),
			   "param_name" => "separator",
			   "std"	=> 'secondary',
			   "value" => array(
			   		'Yes - between title and subtitle' => 'yes',
			   		'Under subtitle' => 'bottom',
			   		'Disabled'   => 'no'
			   	)
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Heading Font", 'crexis'),
			   "param_name" => "font",
			   "std"	=> 'primary',
			   "value" => array(
			   		'Primary' => 'primary',
			   		'Secondary' => 'secondary',
			   		'Georgia'	=> 'georgia'
			   	)
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Heading Font Size", 'crexis'),
			   "param_name" => "font_size",
			   //"std"	=> "default",
			   "value" => array(
			   		'Default set in Theme Options' => 'default',
			   		'16px' => '16px',
			   		'30px' => '30px',
			   		'34px' => '34px',
			   		'40px' => '40px'
			   	)
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Heading Font Weight", 'crexis'),
			   "param_name" => "font_weight",
			   //"std"	=> "600",
			   "value" => array(
			   		'Default set in Theme Options' => 'default',
			   		'300 (light)' => '300',
			   		'400 (normal)' => '400',
			   		'600 (semibold)' => '600',
			   		'700 (bold)' => '700',
			   	)
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Heading Font Transform", 'crexis'),
			   "param_name" => "text_transform",
			   "value" => array(
			   		'Default set in Theme Options' => 'default',
			   		'Uppercase' => 'uppercase',
			   		'None' 		=> 'none'
			   	)
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Italic", 'crexis'),
			   "param_name" => "italic",
			   "value" => array(
			   		'No' => 'no',
			   		'Yes' 		=> 'yes'
			   	)
			),
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Text Align", 'crexis'),
			   "param_name" => "text_align",
			   "value" => array(
			   	'Center' => '',
			   	'Left' => 'left'
			   	)
			),
			array(
			  "type" => "textfield",
			  "heading" => esc_html__("Margin Bottom", 'crexis'),
			  "param_name" => "heading_margin_bottom",
			  "class" => "hidden-label",
			  "value" => '35',
			  "description" => esc_html__("Bottom margin of the heading section, given in pixels. Default: 30", 'crexis')
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'crexis' ),
				'param_name' => 'el_class',
				'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'crexis' ),
			),
			
	   )
	));	
	
	vc_map( array(
	  "name" => esc_html__("Button", 'crexis'),
	  "base" => "vntd_button",
	  "icon" => "icon-wpb-ui-button",
	  "category" => 'Content',
	  "description" => "Theme Button",
	  "params" => array(
	    array(
	      "type" => "textfield",
	      "heading" => esc_html__("Text on the button", 'crexis'),
	      "holder" => "button",
	      "class" => "button",
	      "param_name" => "label",
	      "value" => esc_html__("Text on the button", 'crexis'),
	      "description" => esc_html__("Text on the button.", 'crexis')
	    ),
	    array(
	      "type" => "textfield",
	      "heading" => esc_html__("URL (Link)", 'crexis'),
	      "param_name" => "url",
	      "description" => esc_html__("Button link.", 'crexis')
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => esc_html__("Target", 'crexis'),
	      "param_name" => "target",
	      "value" => $target_arr,
	      "dependency" => Array('element' => "url", 'not_empty' => true)
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => esc_html__("Button Size", 'crexis'),
	      "param_name" => "size",
	      "class" => "hidden-label",
	      "std" => "medium",
	      "value" => array(
	      		esc_html__("Mini", 'crexis') => "mini", 
	      		esc_html__("Small", 'crexis') => "small", 
	      		esc_html__("Medium", 'crexis') => "medium",
	      		esc_html__("Large", 'crexis') => "large",
	      		esc_html__("XLarge", 'crexis') => "xlarge"
	      ),
	      "description" => esc_html__("Button size.", 'crexis')
	    ),
	    array(
	      "type" => "textfield",
	      "heading" => esc_html__("Secondary text on the button", 'crexis'),
	      "class" => "button",
	      "param_name" => "label2",
	      "value" => "",
	      "dependency" => Array('element' => "size", 'value' => array("xlarge","large","medium")),
	      "description" => esc_html__("Optional secondary text displayed under the main one.", 'crexis')
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => esc_html__("Button Style", 'crexis'),
	      "param_name" => "style",
	      "class" => "hidden-label",
	      "std" => "solid-btn",
	      "value" => array(
	      		esc_html__("Default (solid color)", 'crexis') => "solid-btn", 
	      		esc_html__("Outline", 'crexis') => "border-btn", 
	      		esc_html__("Transparent Solid", 'crexis') => "light-button",
	      		esc_html__("Transparent Outline", 'crexis') => "dark-button"
	      ),
	      "description" => esc_html__("Button size.", 'crexis')
	    ),
	    array(
	      "type" => "dropdown",
	      "heading" => esc_html__("Button color", 'crexis'),
	      "param_name" => "color",
	      "class" => "hidden-label",
	      "value" => $colors_arr,
	      "dependency" => Array('element' => "style", 'value' => array("solid-btn", "border-btn")),
	      "description" => esc_html__("Select button color.", 'crexis'),
	    ),
		    array(
		      "type" => "colorpicker",
		      "heading" => esc_html__("Button custom color", 'crexis'),
		      "param_name" => "customcolor",
		      "class" => "hidden-label",
		      "description" => esc_html__("Select custom color for your button.", 'crexis'),
		      "dependency" => Array('element' => "color", 'value' => array('custom'))
		    ),
		    
		array(
		  "type" => "dropdown",
		  "heading" => esc_html__("Button hover color", 'crexis'),
		  "param_name" => "hover",
		  "class" => "hidden-label",
		  "dependency" => Array('element' => "style", 'value' => array("solid-btn","border-btn")),
		  "std" => "accent2",
		  "value" => array(
		  	esc_html__("Accent", 'crexis') => "accent",
		  	esc_html__("Accent 2 (dark)", 'crexis') => "accent2",
		  	esc_html__("Accent 3 (gray)", 'crexis') => "accent3",
		  	esc_html__("Lower opacity", 'crexis') => "opacity",
		  	esc_html__("White", 'crexis') => "white",
		  ),
		  "description" => esc_html__("Select button hover color.", 'crexis'),
		),
		
		array(
		  "type" => "dropdown",
		  "heading" => esc_html__("Border Radius", 'crexis'),
		  "param_name" => "border_radius",
		  "value" =>  array(
		 	esc_html__("Default", 'crexis') => "default",
		 	esc_html__("Circle", 'crexis') => "circle",
		  ),
		  "description" => esc_html__("Button align.", 'crexis')
		), 
		
		array(
		  "type" => "dropdown",
		  "heading" => esc_html__("Text Transform", 'crexis'),
		  "param_name" => "text_transform",
		  "value" =>  array(
		  	esc_html__("Uppercase", 'crexis') => "uppercase",
		 	esc_html__("None", 'crexis') => "none",
		  ),
		  "description" => esc_html__("Button align.", 'crexis')
		), 
		
		array(
		  "type" => "dropdown",
		  "heading" => esc_html__("Text Align", 'crexis'),
		  "param_name" => "text_align",
		  "value" =>  array(esc_html__("Center", 'crexis') => "center", esc_html__("Left", 'crexis') => "left"),
		  "description" => esc_html__("Button text align.", 'crexis')
		),
		
//		array(
//		  "type" => "dropdown",
//		  "heading" => esc_html__("Button Align", 'crexis'),
//		  "param_name" => "align",
//		  "value" =>  array(esc_html__("Left", 'crexis') => "", esc_html__("Centered", 'crexis') => "center"),
//		  "description" => esc_html__("Button align.", 'crexis')
//		),
		
	    array(
	      "type" => "dropdown",
	      "heading" => esc_html__("Icon", 'crexis'),
	      "param_name" => "icon_enabled",
	      "class" => "hidden-label",
	      "value" => array(esc_html__("No", 'crexis') => "no", esc_html__("Yes", 'crexis') => "yes",),
	      "description" => esc_html__("Button size.", 'crexis')
	    ),	
	    array(
	    	'type' => 'dropdown',
	    	'heading' => esc_html__( 'Icon library', 'crexis' ),
	    	'value' => array(
	    		esc_html__( 'Font Awesome', 'crexis' ) => 'fontawesome',
	    		esc_html__( 'Open Iconic', 'crexis' ) => 'openiconic',
	    		esc_html__( 'Typicons', 'crexis' ) => 'typicons',
	    		esc_html__( 'Entypo', 'crexis' ) => 'entypo',
	    		esc_html__( 'Linecons', 'crexis' ) => 'linecons',
	    		esc_html__( 'Pixel', 'crexis' ) => 'pixelicons',
	    	),
	    	'param_name' => 'icon_type',
	    	'description' => esc_html__( 'Select icon library.', 'crexis' ),
	    ),
	    array(
	    	'type' => 'iconpicker',
	    	'heading' => esc_html__( 'Icon', 'crexis' ),
	    	'param_name' => 'icon_fontawesome',
	        'value' => 'fa fa-info-circle',
	    	'settings' => array(
	    		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	    		'iconsPerPage' => 200, // default 100, how many icons per/page to display
	    	),
	    	'dependency' => array(
	    		'element' => 'icon_type',
	    		'value' => 'fontawesome',
	    	),
	    	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	    ),
	    array(
	    	'type' => 'iconpicker',
	    	'heading' => esc_html__( 'Icon', 'crexis' ),
	    	'param_name' => 'icon_openiconic',
	    	'settings' => array(
	    		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	    		'type' => 'openiconic',
	    		'iconsPerPage' => 200, // default 100, how many icons per/page to display
	    	),
	    	'dependency' => array(
	    		'element' => 'icon_type',
	    		'value' => 'openiconic',
	    	),
	    	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	    ),
	    array(
	    	'type' => 'iconpicker',
	    	'heading' => esc_html__( 'Icon', 'crexis' ),
	    	'param_name' => 'icon_typicons',
	    	'settings' => array(
	    		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	    		'type' => 'typicons',
	    		'iconsPerPage' => 200, // default 100, how many icons per/page to display
	    	),
	    	'dependency' => array(
	    	'element' => 'icon_type',
	    	'value' => 'typicons',
	    ),
	    	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	    ),
	    array(
	    	'type' => 'iconpicker',
	    	'heading' => esc_html__( 'Icon', 'crexis' ),
	    	'param_name' => 'icon_entypo',
	    	'settings' => array(
	    		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	    		'type' => 'entypo',
	    		'iconsPerPage' => 300, // default 100, how many icons per/page to display
	    	),
	    	'dependency' => array(
	    		'element' => 'icon_type',
	    		'value' => 'entypo',
	    	),
	    ),
	    array(
	    	'type' => 'iconpicker',
	    	'heading' => esc_html__( 'Icon', 'crexis' ),
	    	'param_name' => 'icon_linecons',
	    	'settings' => array(
	    		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	    		'type' => 'linecons',
	    		'iconsPerPage' => 200, // default 100, how many icons per/page to display
	    	),
	    	'dependency' => array(
	    		'element' => 'icon_type',
	    		'value' => 'linecons',
	    	),
	    	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	    ),
	    array(
	    	'type' => 'iconpicker',
	    	'heading' => esc_html__( 'Icon', 'crexis' ),
	    	'param_name' => 'icon_pixelicons',
	    	'settings' => array(
	    		'emptyIcon' => false, // default true, display an "EMPTY" icon?
	    		'type' => 'pixelicons',
	    		'source' => $pixel_icons,
	    	),
	    	'dependency' => array(
	    		'element' => 'icon_type',
	    		'value' => 'pixelicons',
	    	),
	    	'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	    ),    
	    array(
	      "type" => "textfield",
	      "heading" => esc_html__("Margin Top", 'crexis'),
	      "param_name" => "margin_top",
	      "description" => esc_html__("Change button's top margin value. Default value: 0px", 'crexis')
	    ),
	    array(
	      "type" => "textfield",
	      "heading" => esc_html__("Margin Bottom", 'crexis'),
	      "param_name" => "margin_bottom",
	      "description" => esc_html__("Change button's bottom margin value. Default value: 20px", 'crexis')
	    ),
	    array(
	    	'type' => 'textfield',
	    	'heading' => __( 'Extra class name', 'crexis' ),
	    	'param_name' => 'el_class',
	    	'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'crexis' ),
	    ),
	  )
	));
	
	// Button Slider
	
	vc_map( array(
		   "name" => esc_html__("Button Image Slider", 'crexis'),
		   "base" => "vntd_button_slider",
		   "class" => "font-awesome",
		   "icon" => "fa-picture-o",
		   "description" => "Simple Button Image Slider",
		   "category" => 'Content',
		   "params" => array(   
		   		array(
		   			'type' => 'param_group',
		   			'heading' => __( 'Slides', 'crexis' ),
		   			'param_name' => 'slides',
		   			'description' => __( 'Add slides to the Button Slider.', 'crexis' ),
		   			'value' => urlencode( json_encode( array(
		   				array(
		   					'image' => '',
		   					'button_label' => 'See Our Portfolio',
		   					'button_url' => '#',
		   					'button_url_target'	=> 'self'
		   				),
		   				array(
	   						'image' => '',
	   						'button_label' => 'Keep in Touch',
	   						'button_url' => '#',
	   						'button_url_target'	=> 'self'
	   					),
		   			) ) ),
		   			'params' => array(
		   				array(
	   						'type' => 'attach_image',
	   						'heading' => esc_html__( 'Slide Image', 'crexis' ),
	   						'param_name' => 'image',
	   						'value' => '',
	   						'description' => esc_html__( 'Select image from media library for the slide background.', 'crexis' ),
	   					),
		   				array(
		   					'type' => 'textfield',
		   					'heading' => __( 'Button Label', 'crexis' ),
		   					'param_name' => 'button_label',
		   					'description' => __( 'Slide button label.', 'crexis' ),
		   					'admin_label' => true,
		   				),
		   				array(
		   					'type' => 'textfield',
		   					'heading' => __( 'Button URL', 'crexis' ),
		   					'param_name' => 'button_url',
		   					'description' => __( 'Button link URL.', 'crexis' ),
		   				),
		   				array(
	   					   "type" => "dropdown",
	   					   "class" => "hidden-label",
	   					   "heading" => esc_html__("Open link in a new tab?", 'crexis'),
	   					   "param_name" => "button_url_target",
	   					   "value" => array(
	   					   		'No' => 'self',
	   					   		'Yes' => '_blank',
	   					   		
	   					   	)
	   					),
		   			),
		   		), 
		   		array(
	   				'type' => 'textfield',
	   				'heading' => __( 'Slider Height', 'crexis' ),
	   				'param_name' => 'height',
	   				'value'	=> '600',
	   				'description' => __( 'Slider height in pixels. Default: 600', 'crexis' ),
	   			), 
		   		array(
		   			"type" => "dropdown",
		   			"class" => "",
		   			"heading" => "Background Image Overlay",
		   			"param_name" => "bg_overlay",
		   			"value" => $bg_overlay_arr,
		   			"description" => esc_html__("Enable the background overlay to darken or lighten the background image.", 'crexis'),
		   		)
		   	)
	));
	
	// Fullscreen Features 
	
	vc_map( array(
		   "name" => esc_html__("Fullscreen Features", 'crexis'),
		   "base" => "vntd_fullscreen_features",
		   "class" => "font-awesome",
		   "icon" => "fa-flag",
		   "description" => "Fullscreen Features",
		   "category" => 'Content',
		   "params" => array(   
		   
		   		array(
	   				'type' => 'attach_image',
	   				'heading' => esc_html__( 'Background Image', 'crexis' ),
	   				'param_name' => 'bg_image',
	   				'value' => '',
	   				'description' => esc_html__( 'Select background image from the media library.', 'crexis' ),
	   			),
		   			
		   		array(
	   				'type' => 'textarea',
	   				'heading' => __( 'Main Heading', 'crexis' ),
	   				'param_name' => 'heading',
	   				"value"	=> 'We create (rotate)awesome,stunning(/rotate) themes!',
	   				'description' => __( 'Section heading. Use (rotate)word1,word2(/rotate) for a simple text rotator. Example: We create (rotate)awesome,stunning(/rotate) themes!', 'crexis' ),
	   			),
	   			
	   			array(
	   				'type' => 'param_group',
	   				'heading' => __( 'Feature Boxes', 'crexis' ),
	   				'param_name' => 'boxes',
	   				'description' => __( 'Feature Boxes for the carousel.', 'crexis' ),
	   				'value' => urlencode( json_encode( array(
	   					array(
	   						'box_heading' => __( 'Unique Design', 'crexis' ),
	   						'box_content' => "This is Photoshop's version Lorem Ipsum. Proin gravida nibh vel  is avelit auctor aliquet. a sit amet mauris. Morbi accumsan ipsum velit. Sed non  mauris vitae erat.",
	   						'icon_fontawesome'	=> 'fa fa-rocket'
	   					),
	   					array(
	   						'box_heading' => __( 'Powerful Code', 'crexis' ),
	   						'box_content' => "This is Photoshop's version Lorem Ipsum. Proin gravida nibh vel  is avelit auctor aliquet. a sit amet mauris. Morbi accumsan ipsum velit. Sed non  mauris vitae erat.",
	   						'icon_fontawesome'	=> 'fa fa-rocket'
	   					),
	   					array(
	   						'box_heading' => __( 'Friendly Support', 'crexis' ),
	   						'box_content' => "This is Photoshop's version Lorem Ipsum. Proin gravida nibh vel  is avelit auctor aliquet. a sit amet mauris. Morbi accumsan ipsum velit. Sed non  mauris vitae erat.",
	   						'icon_fontawesome'	=> 'fa fa-rocket'
	   					),
	   					array(
   							'box_heading' => __( 'Friendly Support', 'crexis' ),
   							'box_content' => "This is Photoshop's version Lorem Ipsum. Proin gravida nibh vel  is avelit auctor aliquet. a sit amet mauris. Morbi accumsan ipsum velit. Sed non  mauris vitae erat.",
   							'icon_fontawesome'	=> 'fa fa-rocket'
   						),
	   				) ) ),
	   				'params' => array(
	   					array(
	   						'type' => 'textfield',
	   						'heading' => __( 'Feature Title', 'crexis' ),
	   						'param_name' => 'box_heading',
	   						'description' => __( 'Enter the box heading.', 'crexis' ),
	   						'admin_label' => true,
	   					),
	   					array(
	   						'type' => 'textfield',
	   						'heading' => __( 'Feature Text', 'crexis' ),
	   						'param_name' => 'box_content',
	   						'description' => __( 'Box content, supports HTML.', 'crexis' ),
	   					),
	   					array(
	   						'type' => 'iconpicker',
	   						'heading' => esc_html__( 'Feature Icon', 'crexis' ),
	   						'param_name' => 'icon_fontawesome',
	   					    'value' => 'fa fa-rocket',
	   						'settings' => array(
	   							'emptyIcon' => false, // default true, display an "EMPTY" icon?
	   							'iconsPerPage' => 200, // default 100, how many icons per/page to display
	   						),
	   						'description' => esc_html__( 'Select icon from library.', 'crexis' ),
	   					),
	   				),
	   			),
	   			
	   			array(
	   			    "type" => "textfield",	         
	   			    "heading" => esc_html__("Secondary Heading", 'crexis'),
	   			    "param_name" => "heading_secondary",
	   			    "description" => esc_html__("Secondary heading text displayed below the feature boxes. Use (b)word(/b) tags for highlighted text.", 'crexis'),
	   			    "value" => "Super Responsive, clean and creative theme - (b)Crexis!(/b)"
	   			),
	   			
	   			array(
	   				   "type" => "dropdown",
	   				   "class" => "hidden-label",
	   				   "value" => array(
	   					   "Yes" => 'yes',
	   					   "No" => 'no'
	   				   ),
	   				   "heading" => esc_html__("Scroll down button?", 'crexis'),
	   				   "param_name" => "scroll_button",
	   				   "description" => esc_html__("Enable or disable a button that scrolls to a #second row of your page (row with an id 'second')", 'crexis'),
	   			),
	   			
	   				array(
	   				    "type" => "textfield",	       
	   				    "heading" => esc_html__("Scroll Button URL", 'crexis'),
	   				    "param_name" => "scroll_url",
	   				    "description" => esc_html__("URL for the scroll button: #second by default.", 'crexis'),
	   				    "value" => "#second",
	   				    'dependency' => array(
	   				    	'element' => 'scroll_button',
	   				    	'value' => 'yes'
	   				    ),
	   				), 
	   			
	   				array(
	   				    "type" => "textfield",	       
	   				    "heading" => esc_html__("Scroll down label", 'crexis'),
	   				    "param_name" => "scroll_label",
	   				    "description" => esc_html__("Scroll Button Label", 'crexis'),
	   				    "value" => "Learn More",
	   				    'dependency' => array(
	   				    		'element' => 'scroll_button',
	   				    		'value' => 'yes'
	   				    ),
	   				),
	   			
		   	)
		   		
	));
	
	// Special Heading
	
	vc_map( array(
		   "name" => esc_html__("Hero Section", 'crexis'),
		   "base" => "vntd_hero_section",
		   "class" => "font-awesome",
		   "icon" => "fa-eye",
		   "description" => "Simple Hero Section",
		   "category" => 'Content',
		   "params" => array(   
		   		array(
		   		   "type" => "dropdown",
		   		   "class" => "hidden-label",
		   		   "heading" => esc_html__("Background Type", 'crexis'),
		   		   "param_name" => "media_type",
		   		   "value" => array(
		   		   		'Images' => 'images',
		   		   		'YouTube video' => 'youtube'
		   		   	)
		   		),  
		   		array(
	   			   "type" => "dropdown",
	   			   "class" => "hidden-label",
	   			   "heading" => esc_html__("Extra Effect", 'crexis'),
	   			   "param_name" => "effect",
	   			   'description' => esc_html__( 'Please note that slider will be automatically disabled if any effect is applied. If multiple images are being added, the first one will be used as the background image.', 'crexis' ),
	   			   "value" => array(
	   			   		'None' => 'none',
	   			   		'Rainy Day' => 'rainyday',
	   			   		'Particles' => 'particles',
	   			   		'Animated' => 'animated'
	   			   	)
	   			),
		   		array(
		   			'type' => 'attach_images',
		   			'heading' => esc_html__( 'Images', 'crexis' ),
		   			'param_name' => 'images',
		   			'value' => '',
		   			'description' => esc_html__( 'Select images from media library. If more than is selected, images will be displayed as a slider.', 'crexis' ),
		   			'dependency' => array(
		   				'element' => 'media_type',
		   				'value' => 'images'
		   			),
		   		),
		   		array(
		   			'type' => 'textfield',
		   			'heading' => esc_html__( 'YouTube video ID', 'crexis' ),
		   			'param_name' => 'youtube_id',
		   			'value' => 'nKAxHHTxXIU', // default video url
		   			'description' => esc_html__( 'Insert your desired YouTube video ID. Example: nKAxHHTxXIU', 'crexis' ),
		   			'dependency' => array(
		   				'element' => 'media_type',
		   				'value' => array('youtube'),
		   			),
		   		),
		   		array(
		   				'type' => 'attach_image',
		   				'heading' => esc_html__( 'Video Placeholder', 'crexis' ),
		   				'param_name' => 'video_img_placeholder',
		   				'value' => '',
		   				'description' => esc_html__( 'Select an image that will be used as a placeholder until the video loads.', 'crexis' ),
		   				'dependency' => array(
		   					'element' => 'media_type',
		   					'value' => 'youtube'
		   				),
		   			),
		   		
		   		array(
	   			   "type" => "dropdown",
	   			   "class" => "hidden-label",
	   			   "heading" => esc_html__("Hero Style", 'crexis'),
	   			   "param_name" => "style",
	   			   "value" => array(
	   			   		'Style 1 - default' => 'default',
	   			   		'Style 2 - with boxes' => 'style2'
	   			   	)
	   			), 
	   			
	   			array(
	   				   "type" => "dropdown",
	   				   "class" => "hidden-label",
	   				   "heading" => esc_html__("Boxes Style", 'crexis'),
	   				   "param_name" => "boxes_style",
	   				   "value" => array(
	   				   		'Default' => 'default',
	   				   		'Minimal - Only Box Heading' => 'minimal'
	   				   	),
	   				   	'dependency' => array(
   				   			'element' => 'style',
   				   			'value' => 'style2'
   				   		),
	   				), 
	   			
	   			array(
	   				'type' => 'exploded_textarea',
	   				'heading' => __( 'Main Heading', 'crexis' ),
	   				'param_name' => 'heading_dynamic',
	   				"value"	=> 'We Create Awesome Themes!,Crexis Creative Design Studio',
	   				'description' => __( 'Insert one or multiple lines of the Main Heading text for the Hero section.', 'crexis' ),
	   				'dependency' => array(
	   					'element' => 'style',
	   					'value' => array('default'),
	   				),
	   			), 
		   		 
		   		array(
		   		    "type" => "textfield",	         
		   		    "heading" => esc_html__("Main Heading", 'crexis'),
		   		    "param_name" => "heading",
		   		    "description" => esc_html__("Main Heading.", 'crexis'),
		   		    "value" => "Design Your Life",
		   		    'dependency' => array(
		   		    	'element' => 'style',
		   		    	'value' => array('style2'),
		   		    ),
		   		),
		   		
		   		array(
	   			    "type" => "textfield",	         
	   			    "heading" => esc_html__("Main Heading Small Text", 'crexis'),
	   			    "param_name" => "heading_small",
	   			    "description" => esc_html__("This is an optional smaller text displayed on the right side of the Main Heading.", 'crexis'),
	   			    "value" => "never give up!",
	   			    'dependency' => array(
	   			    	'element' => 'style',
	   			    	'value' => array('style2'),
	   			    ),
	   			),
				
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Top Heading", 'crexis'),
				    "param_name" => "heading_top",
				    "description" => esc_html__("Small text displayed above the Main Heading.", 'crexis'),
				    "value" => "",
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Subtitle", 'crexis'),
				    "param_name" => "subtitle",
				    "description" => esc_html__("Smaller text visible below the Main one.", 'crexis'),
				    "value" => "Contrary to popular belief, Lorem Ipsum is not simply random text. Piece of classical Latin literature."
				),  
				array(
					'type' => 'param_group',
					'heading' => __( 'Boxes', 'crexis' ),
					'param_name' => 'boxes',
					'dependency' => array(
					   	'element' => 'style',
					   	'value' => array('style2'),
					),
					'description' => __( 'Add boxes to your Hero Section style 2.', 'crexis' ),
					'value' => urlencode( json_encode( array(
						array(
							'box_heading' => __( 'Who We Are?', 'crexis' ),
							'box_content' => 'Contrary to popular belief, Lorem Ipsum is not simp random text. It has roots in a piece of classical Latin literature from 45 BC.',
							'box_url'	=> '#',
							'box_dark' => 'no'
						),
						array(
							'box_heading' => __( 'Welcome to Crexis Agency!', 'crexis' ),
							'box_content' => 'Crexis a Stunning OnePage&MultiPage Theme.',
							'box_url'	=> '#',
							'box_dark' => 'no'
						),
						array(
							'box_heading' => __( 'See Our Great Works!', 'crexis' ),
							'box_content' => 'Click for our awesome works!',
							'box_url'	=> '#',
							'box_dark' => 'no'
						),
						array(
							'box_heading' => __( 'Follow Us On Social Networks!', 'crexis' ),
							'box_content' => 'Click for our awesome works!',
							'box_url'	=> '',
							'box_dark' => 'yes'
						),
					) ) ),
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'Box Heading', 'crexis' ),
							'param_name' => 'box_heading',
							'description' => __( 'Enter the box heading.', 'crexis' ),
							'admin_label' => true,
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Box Content', 'crexis' ),
							'param_name' => 'box_content',
							'description' => __( 'Box text content. You can use [social-icons] shortcode.', 'crexis' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Box URL', 'crexis' ),
							'param_name' => 'box_url',
							'description' => __( 'Enter the box link URL. Leave blank to disable link.', 'crexis' ),
						),
						array(
							'type' => 'dropdown',
							'heading' => __( 'Dark Box?', 'crexis' ),
							'param_name' => 'box_dark',
							'description' => __( 'Check to make the box stand out by having a darker background.', 'crexis' ),
							"value" => array(
							   	'No' => "no",
							   	'Yes' => "yes",
							),
						),
					),
				),
					
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Button Label", 'crexis'),
				    "param_name" => "button1_label",
	
				    "description" => esc_html__("First button label.", 'crexis'),
				    "value" => "Button Text"
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Button URL", 'crexis'),
				    "param_name" => "button1_url",
				    "description" => esc_html__("First button URL (external link or #section_id)", 'crexis'),
				    "value" => "#second",
				    'dependency' => array(
				    	'element' => 'button1_label',
				    	'not_empty' => true,
				    ),
				),  
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Content Align", 'crexis'),
				   "param_name" => "content_align",
				   "value" => array(
					   	'Center' => "center",
					   	'Left' => "left",
				   	),
				   	'dependency' => array(
				   		'element' => 'style',
				   		'value' => array('default'),
				   	),
				   	"description" => esc_html__("Choose the alignment of the hero section content.", 'crexis'),
				), 	   
					
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Separator Line", 'crexis'),
				   "param_name" => "separator_line",
				   "value" => array(
					   	'No' => "no",
					   	'Yes - between Main Heading and Subtitle ' => "yes below-mainheading",
					   	'Yes - below the Subtitle' => "yes below-subtitle",
				   	),
				   	'dependency' => array(
				   		'element' => 'style',
				   		'value' => array('default'),
				   	),
				   	"description" => esc_html__("Simple separator line between slider texts.", 'crexis'),
				),
				
				array(
					   "type" => "dropdown",
					   "class" => "hidden-label",
					   "value" => array(
						   "No" => 'no',
						   "Yes" => 'yes'
					   ),
					   "heading" => esc_html__("Scroll down button?", 'crexis'),
					   "param_name" => "scroll_button",
					   "description" => esc_html__("Enable or disable a button that scrolls to a #second row of your page (row with an id 'second')", 'crexis'),
				),
				
					array(
						   "type" => "dropdown",
						   "class" => "hidden-label",
						   "value" => array(
							   "Circle" => 'circle',
							   "Mouse Icon + Text" => 'mouse'
						   ),
						   "heading" => esc_html__("Scroll down button style", 'crexis'),
						   "param_name" => "scroll_button_style",
						   "description" => esc_html__("Style of the scroll down button.", 'crexis'),
						   'dependency' => array(
						   	'element' => 'scroll_button',
						   	'value' => 'yes'
						   ),
					),
				
					array(
					    "type" => "textfield",	       
					    "heading" => esc_html__("Scroll Button URL", 'crexis'),
					    "param_name" => "scroll_url",
					    "description" => esc_html__("URL for the scroll button: '#second' by default. ", 'crexis'),
					    "value" => "#second",
					    'dependency' => array(
					    	'element' => 'scroll_button',
					    	'value' => 'yes'
					    ),
					), 
				
					array(
					    "type" => "textfield",	       
					    "heading" => esc_html__("Scroll down label", 'crexis'),
					    "param_name" => "scroll_label",
					    "description" => esc_html__("Optional, medium sized text displayed at the very bottom of the slider, right above the scroll button. Links to the #second row.", 'crexis'),
					    "value" => "",
					    'dependency' => array(
					    		'element' => 'scroll_button',
					    		'value' => 'yes'
					    ),
					),
					
				//
				// Settings Group
				//
				
				array(
					"type" => "dropdown",
					"class" => "",
					"heading" => "Background Overlay",
					"param_name" => "bg_overlay",
					"value" => $bg_overlay_arr,
					"group" => esc_html__("Settings", 'crexis'),
					"description" => esc_html__("Set a background overlay to darken or lighten the background image/video and make the text better visible.", 'crexis'),
				),
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Height", 'crexis'),
				   "param_name" => "height",
				   "group" => esc_html__("Settings", 'crexis'),
				   "value" => array(
					   	'Fullscreen' => 'fullscreen',
					   	'Custom' => 'custom'
				   	)
				), 	
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Custom Hero Height', 'crexis' ),
					'param_name' => 'height_custom',
					'dependency' => array(
						'element' => 'height',
						'value' => 'custom',
					),
					'value' => '700px',
					"group" => esc_html__("Settings", 'crexis'),
					"description" => esc_html__("Set a custom height for your hero section in pixels i.e: 400px, 600px, 800px", 'crexis'),
				),
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "value" => array(
					   'Yes' => 'yes',
					   'No' => 'no'
				   ),
				   "heading" => esc_html__("Parallax?", 'crexis'),
				   "param_name" => "parallax",
				   "description" => esc_html__("Enable the parallax effect for images.", 'crexis'),
				   'dependency' => array(
				   		'element' => 'media_type',
				   		'value' => 'images'
				   ),
				   "group" => esc_html__("Settings", 'crexis')
				),
					
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "value" => array(
					   "Yes" => 'yes',
					   "No" => 'no'
				   ),
				   "heading" => esc_html__("Arrow Navigation?", 'crexis'),
				   "param_name" => "arrow_navigation",
				   'dependency' => array(
				   		'element' => 'media_type',
				   		'value' => 'images'
				   ),
				   "group" => esc_html__("Settings", 'crexis'),
				   "description" => esc_html__("Enable or disable the arrow navigation.", 'crexis'),
				),
				
					array(
					   "type" => "dropdown",
					   "class" => "hidden-label",
					   "value" => array(
						   'Classic Side' => 'side',
						   'Circle Bottom' => 'bottom',
						   'Square Bottom Left Side' => 'bottom-left'
					   ),
					   "heading" => esc_html__("Arrow Navigation Style", 'crexis'),
					   "param_name" => "arrow_navigation_style",
					   "description" => esc_html__("Choose a style for the arrow navigation.", 'crexis'),
					   'dependency' => array(
					   		'element' => 'arrow_navigation',
					   		'value' => 'yes'
					   ),
					   "group" => esc_html__("Settings", 'crexis')
					),
					
				array(
						   "type" => "dropdown",
						   "class" => "hidden-label",
						   "value" => array(
						   		"Yes" => 'true',
							  	"No" => 'false',
						   ),
						   "heading" => esc_html__("Video Controls?", 'crexis'),
						   "param_name" => "video_controls",
						   'dependency' => array(
						   	'element' => 'media_type',
						   	'value' => 'youtube'
						   ),
						   "group" => esc_html__("Settings", 'crexis'),
						   "description" => esc_html__("Enable or disable the video controls (play/pause/mute).", 'crexis'),
					),
						array(
								   "type" => "dropdown",
								   "class" => "hidden-label",
								   "value" => array(
								   		'Default Bottom' => 'bottom',
								   		'Animated' => 'animated'
								   ),
								   "heading" => esc_html__("Video Controls Style", 'crexis'),
								   "param_name" => "video_controls_style",
								   'dependency' => array(
								   		'element' => 'media_type',
								   		'value' => 'youtube'
								   ),
								   "group" => esc_html__("Settings", 'crexis'),
								   "description" => esc_html__("Choose a style for the video controls.", 'crexis'),
							),
					array(
						   "type" => "dropdown",
						   "class" => "hidden-label",
						   "value" => array(
							   "Yes" => 'true',
							   "No" => 'false'
						   ),
						   "heading" => esc_html__("Mute Video?", 'crexis'),
						   "param_name" => "video_mute",
						   'dependency' => array(
						   	'element' => 'media_type',
						   	'value' => 'youtube'
						   ),
						   "group" => esc_html__("Settings", 'crexis'),
						   "description" => esc_html__("Mute the video.", 'crexis'),
					),
					array(
						   "type" => "dropdown",
						   "class" => "hidden-label",
						   "value" => array(
							   "Yes" => 'true',
							   "No" => 'false'
						   ),
						   "heading" => esc_html__("Video Autoplay?", 'crexis'),
						   "param_name" => "video_autoplay",
						   'dependency' => array(
						   	'element' => 'media_type',
						   	'value' => 'youtube'
						   ),
						   "group" => esc_html__("Settings", 'crexis'),
						   "description" => esc_html__("Should the video start playing automatically?", 'crexis'),
					),
					
				array(
					   "type" => "textfield",
					   "class" => "hidden-label",
					   "heading" => esc_html__("Slider Speed", 'crexis'),
					   "param_name" => "slider_speed",
					   'dependency' => array(
					   	'element' => 'media_type',
					   	'value' => 'images'
					   ),
					   'std' => '12000',
					   "group" => esc_html__("Settings", 'crexis'),
					   "description" => esc_html__("Image slider speed in miliseconds i.e. 12000 = 12 seconds.", 'crexis'),
				),
					
				// Design Tab
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Main Heading Font Size", 'crexis'),
				   "param_name" => "heading_font_size",
				   //"std"	=> "default",
				   "value" => array(
				   		'Default value' => 'default',
				   		'50px' => '50px',
				   		'55px' => '55px',
				   		'62px' => '62px',
				   		'95px' => '95px'
				   	),
				   	'group' => esc_html__("Design", 'crexis')
				),
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Subtitle container width", 'crexis'),
				   "param_name" => "subtitle_width",
				   "value" => array(
				   		'Default' => 'default',
				   		'Narrow (500px)' => 'narrow'
				   	),
				   	'group' => esc_html__("Design", 'crexis')
				),
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Button Color", 'crexis'),
				   "param_name" => "button1_color",
				   "value" => array(
					   	'Light' => "light",
					   	'Dark' => "dark",
				   	),
				   	'dependency' => array(
				   		'element' => 'button1_label',
				   		'not_empty' => true,
				   	),
				   	'group' => esc_html__("Design", 'crexis')
				),
					
				//
				// Tooltips Group
				//
				
				array(
					   "type" => "dropdown",
					   "class" => "hidden-label",
					   "value" => array(
					   		"No" => 'no',
						   	"Yes" => 'yes'
					   ),
					   "heading" => esc_html__("Enable Tooltips?", 'crexis'),
					   "param_name" => "tooltips_enable",
					   "group" => esc_html__("Tooltips", 'crexis'),
					   "description" => esc_html__("Tooltips are small, parallax plus icons moving around your Hero Section. They display a small, custom label on hover.", 'crexis'),
				),
				
				array(
					'type' => 'param_group',
					'heading' => __( 'Tooltips', 'crexis' ),
					'param_name' => 'tooltips',
					"group" => esc_html__("Tooltips", 'crexis'),
					'description' => __( 'Add tooltips.', 'crexis' ),
					'value' => urlencode( json_encode( array(
						array(
							'tooltip_title' => __( 'Welcome To Crexis!', 'crexis' ),
							'tooltip_url' => '#',
							'tooltip_placement'	=> 'bottom',
							'tooltip_depth'	=> '0.12',
						),
						array(
							'tooltip_title' => __( 'Click for our great works!', 'crexis' ),
							'tooltip_url' => '#',
							'tooltip_placement'	=> 'top',
							'tooltip_depth'	=> '0.72',
						),
					) ) ),
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'Box Heading', 'crexis' ),
							'param_name' => 'tooltip_title',
							'value' => 'Hello there!',
							'description' => __( 'Enter the box heading.', 'crexis' ),
							'admin_label' => true,
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Tooltip URL', 'crexis' ),
							'param_name' => 'tooltip_url',
							'value' => '#',
							'description' => __( 'Optional tooltip URL.', 'crexis' ),
						),
						array(
							'type' => 'dropdown',
							"value" => array(
									"Top" => 'top',
								   	"Bottom" => 'bottom',
								   	"Left" => 'left',
								   	"Right" => 'right'
							),
							'heading' => __( 'Tooltip Placement', 'crexis' ),
							'param_name' => 'tooltip_placement',
							'description' => __( 'Placement of the tooltip.', 'crexis' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Tooltip Parallax Depth', 'crexis' ),
							'param_name' => 'tooltip_depth',
							'value' => '0.42',
							'description' => __( 'Depth of the parallax effect. Bigger the value is, more parallax effects will be added.', 'crexis' ),
						),
					),
				),
				
				
		   )
		));
	
	// Callout Box
	
	vc_map( array(
	   "name" => esc_html__("Callout Box", 'crexis'),
	   "base" => "callout_box",
	   "class" => "font-awesome",
	   "icon" => "fa-align-left",
	   "category" => 'Content',
	   "description" => "Nice blockquote",
	   "params" => array(     
			array(
			    "type" => "textfield",	         
			    "heading" => esc_html__("Title", 'crexis'),
			    "param_name" => "title",
			    "holder" => "h3",
			    "description" => esc_html__("Main heading text.", 'crexis'),
			    "value" => "Callout"
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Subtitle", 'crexis'),
			    "param_name" => "subtitle",
			    "holder" => "span",
			    "description" => esc_html__("Smaller text visible below the Main one.", 'crexis'),
			    "value" => "Callout Box subtitle"
			)
	   )
	));


		vc_map( array(
		   "name" => esc_html__("Portfolio Post Details", 'crexis'),
		   "base" => "portfolio_details",
		   "class" => "font-awesome",
		   "icon" => "fa-suitcase",
		   "description" => "Single portfolio post details",
		   "category" => 'Content',
		   "params" => array(				
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Style", 'crexis'),
				   "param_name" => "style",
				   "value" => array(
				   	'Default Style - 2 columns' => 'default2',
				   	'Default Style - 1 column' => 'default1',
				   	'Minimal Style' => 'minimal'
				   	)
				),
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Title", 'crexis'),
				   "param_name" => "title_type",
				   "value" => array(
				   		'Post Title' => 'post_title',
				   		'Custom Title' => 'custom'
				   	)
				),
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Custom Title", 'crexis'),
				    "param_name" => "title",
				    "dependency" => array('element' => 'title_type', 'value' => array('custom')),
				    "description" => esc_html__("Custom main heading text.", 'crexis'),
				    "value" => "Project Details"
				),
				array(
				    "type" => "textarea_html",	       
				    "heading" => esc_html__("Text Content", 'crexis'),
				    "param_name" => "content",
				    "holder" => "div",
				    "description" => esc_html__("Smaller text visible below the Main one.", 'crexis'),
				    "value" => "Contrary to popular belief, Lorem Ipsum is not simply random text. It has rootsin piece of classical Latin literature from old. Richard McClintock, a Latin profes sor at Hampden-Sydney College in Virginia, looked up."
				),			
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Client Name", 'crexis'),
				    "param_name" => "client",
				    "holder"	=> "p",
				    "description" => esc_html__("Your project client name.", 'crexis'),
				    "dependency" => array('element' => 'style', 'value' => array('default2','default1')),
				    "value" => "Client Name"
				),	
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Client Website", 'crexis'),
				    "param_name" => "client_website",
				    "description" => esc_html__("Your project client's website. Leave blank to hide.", 'crexis'),
				    "dependency" => array('element' => 'style', 'value' => array('default2','default1')),
				    "value" => "www.client-site.com"
				),
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Budget", 'crexis'),
				    "param_name" => "budget",
				    "holder"	=> "p",
				    "description" => esc_html__("Project budget.", 'crexis'),
				    "value" => "$800 - $1200"
				),
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Display Likes?", 'crexis'),
				   "param_name" => "likes",
				   "value" => array(
				   		'Yes' => 'yes',
				   		'No' => 'no'
				   	)
				),	
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Display Date?", 'crexis'),
				   "param_name" => "date",
				   "value" => array(
				   		'No' => 'no',
				   		'Yes' => 'yes'
				   	),
				   	"dependency" => array('element' => 'style', 'value' => array('default2','default1')),
				),
				array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Display Categories?", 'crexis'),
				   "param_name" => "categories",
				   "value" => array(
				   		'Yes' => 'yes',
				   		'No' => 'no'
				   	)
				),	
				array(
				  "type" => "exploded_textarea",
				  "heading" => esc_html__("Skills", 'crexis'),
				  "param_name" => "skills",
				  "holder" => "p",
				  "value"	=> 'Design,Photography,HTML,jQuery',
				  "dependency" => array('element' => 'style', 'value' => array('default2','default1')),
				  "description" => esc_html__('Enter the projects skills here. Divide each feature with linebreaks (Enter).', 'crexis')
				),
					
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Button1 Label", 'crexis'),
				    "param_name" => "button1_label",
				    "description" => esc_html__("Primary button label.", 'crexis'),
				    "value" => "Live Preview"
				),
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Button1 URL", 'crexis'),
				    "param_name" => "button1_url",
				    "description" => esc_html__("Primary button URL.", 'crexis'),
				    "value" => "http://",
				    'dependency' => Array('element' => "button1_label", 'not_empty' => true)
				),
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Button2 Label", 'crexis'),
				    "param_name" => "button2_label",
				    "description" => esc_html__("Primary button label.", 'crexis'),
				    "value" => ""
				),
				array(
				    "type" => "textfield",	         
				    "heading" => esc_html__("Button2 URL", 'crexis'),
				    "param_name" => "button2_url",
				    "description" => esc_html__("Secondary button URL.", 'crexis'),
				    "value" => "http://",
				    'dependency' => Array('element' => "button1_label", 'not_empty' => true)
				)				 		   
				
			)
		));

	
	// Product Features
	
	vc_map( array(
	   "name" => esc_html__("Product Features", 'crexis'),
	   "base" => "vntd_product_features",
	   "class" => "font-awesome",
	   "icon" => "fa-list-alt",
	   "category" => 'Content',
	   "description" => "Simple way to show features of your product.",
	   "params" => array(   
	   		array(
   				'type' => 'attach_image',
   				'heading' => esc_html__( 'Product Image', 'crexis' ),
   				'param_name' => 'img',
   				'value' => '',
   				'admin_label' => true,
   				'description' => esc_html__( 'Upload the product image.', 'crexis' ),
   			),
			array(
			    "type" => "textfield",	         
			    "heading" => esc_html__("Left Side Box 1 Title", 'crexis'),
			    "param_name" => "left_box1_title",
			    "description" => esc_html__("Main heading text.", 'crexis'),
			    "value" => "Feature 1 Title"
			),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 1 Text", 'crexis'),
				    "param_name" => "left_box1_text",
				    "description" => esc_html__("Smaller text visible below the Main one.", 'crexis'),
				    "value" => "This is a short description of the feature. Please feel free to change me!"
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Left Side Box 1 Icon', 'crexis' ),
					'param_name' => 'left_box1_icon',
				    'value' => 'fa fa-info-circle',
					'settings' => array(
						'emptyIcon' => false, // default true, display an "EMPTY" icon?
						'iconsPerPage' => 200, // default 100, how many icons per/page to display
					),
					'description' => esc_html__( 'Select icon from library.', 'crexis' ),
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 1 Link URL", 'crexis'),
				    "param_name" => "left_box1_url",
				    "description" => esc_html__("URL for the feature box link. Leave blank to disable.", 'crexis'),
				    "value" => ""
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 1 Tooltip Label", 'crexis'),
				    "param_name" => "left_box1_tooltip_label",
				    "description" => esc_html__("Text that appears when hovering over the tooltip icon. Leave blank to disable.", 'crexis'),
				    "value" => "This is a simple tooltip label."
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 1 Tooltip Left Offset", 'crexis'),
				    "param_name" => "left_box1_offset_left",
				    "description" => esc_html__("Box tooltip left offset in pixels. Default: 160px", 'crexis'),
				    'dependency' => array(
				    	'element' => 'left_box1_tooltip_label',
				    	'not_empty' => true,
				    ),
				    "value" => "160px"
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 1 Tooltip Top Offset", 'crexis'),
				    "param_name" => "left_box1_offset_top",
				    "description" => esc_html__("Box tooltip top offset in pixels. Default: 50px", 'crexis'),
				    'dependency' => array(
				    	'element' => 'left_box1_tooltip_label',
				    	'not_empty' => true,
				    ),
				    "value" => "50px"
				),
			array(
			    "type" => "textfield",	         
			    "heading" => esc_html__("Left Side Box 2 Title", 'crexis'),
			    "param_name" => "left_box2_title",
			    "description" => esc_html__("Main heading text.", 'crexis'),
			    "value" => "Feature 2 Title"
			),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 2 Text", 'crexis'),
				    "param_name" => "left_box2_text",
				    "description" => esc_html__("Smaller text visible below the Main one.", 'crexis'),
				    "value" => "This is a short description of the feature. Please feel free to change me!"
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Left Side Box 2 Icon', 'crexis' ),
					'param_name' => 'left_box2_icon',
				    'value' => 'fa fa-info-circle',
					'settings' => array(
						'emptyIcon' => false, // default true, display an "EMPTY" icon?
						'iconsPerPage' => 200, // default 100, how many icons per/page to display
					),
					'description' => esc_html__( 'Select icon from library.', 'crexis' ),
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 2 Link URL", 'crexis'),
				    "param_name" => "left_box2_url",
				    "description" => esc_html__("URL for the feature box link. Leave blank to disable.", 'crexis'),
				    "value" => ""
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 2 Tooltip Label", 'crexis'),
				    "param_name" => "left_box2_tooltip_label",
				    "description" => esc_html__("Text that appears when hovering over the tooltip icon. Leave blank to disable.", 'crexis'),
				    "value" => "This is a simple tooltip label."
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 2 Tooltip Left Offset", 'crexis'),
				    "param_name" => "left_box2_offset_left",
				    "description" => esc_html__("Box tooltip left offset in pixels. Default: 160px", 'crexis'),
				    'dependency' => array(
				    	'element' => 'left_box2_tooltip_label',
				    	'not_empty' => true,
				    ),
				    "value" => "180px"
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Left Side Box 2 Tooltip Top Offset", 'crexis'),
				    "param_name" => "left_box2_offset_top",
				    "description" => esc_html__("Box tooltip top offset in pixels. Default: 50px", 'crexis'),
				    'dependency' => array(
				    	'element' => 'left_box2_tooltip_label',
				    	'not_empty' => true,
				    ),
				    "value" => "20px"
				),
				
			array(
			    "type" => "textfield",	         
			    "heading" => esc_html__("Right Side Box 1 Title", 'crexis'),
			    "param_name" => "right_box1_title",
			    "description" => esc_html__("Main heading text.", 'crexis'),
			    "value" => "Feature 3 Title"
			),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 1 Text", 'crexis'),
				    "param_name" => "right_box1_text",
				    "description" => esc_html__("Smaller text visible below the Main one.", 'crexis'),
				    "value" => "This is a short description of the feature. Please feel free to change me!"
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Right Side Box 1 Icon', 'crexis' ),
					'param_name' => 'right_box1_icon',
				    'value' => 'fa fa-info-circle',
					'settings' => array(
						'emptyIcon' => false, // default true, display an "EMPTY" icon?
						'iconsPerPage' => 200, // default 100, how many icons per/page to display
					),
					'description' => esc_html__( 'Select icon from library.', 'crexis' ),
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 1 Link URL", 'crexis'),
				    "param_name" => "right_box1_url",
				    "description" => esc_html__("URL for the feature box link. Leave blank to disable.", 'crexis'),
				    "value" => ""
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 1 Tooltip Label", 'crexis'),
				    "param_name" => "right_box1_tooltip_label",
				    "description" => esc_html__("Text that appears when hovering over the tooltip icon. Leave blank to disable.", 'crexis'),
				    "value" => "This is a simple tooltip label."
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 1 Tooltip Right Offset", 'crexis'),
				    "param_name" => "right_box1_offset_left",
				    "description" => esc_html__("Box tooltip Right offset in pixels. Default: 160px", 'crexis'),
				    'dependency' => array(
				    	'element' => 'right_box1_tooltip_label',
				    	'not_empty' => true,
				    ),
				    "value" => "160px"
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 1 Tooltip Top Offset", 'crexis'),
				    "param_name" => "right_box1_offset_top",
				    "description" => esc_html__("Box tooltip top offset in pixels. Default: 50px", 'crexis'),
				    'dependency' => array(
				    	'element' => 'right_box1_tooltip_label',
				    	'not_empty' => true,
				    ),
				    "value" => "50px"
				),
			array(
			    "type" => "textfield",	         
			    "heading" => esc_html__("Right Side Box 2 Title", 'crexis'),
			    "param_name" => "right_box2_title",
			    "description" => esc_html__("Main heading text.", 'crexis'),
			    "value" => "Feature 4 Title"
			),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 2 Text", 'crexis'),
				    "param_name" => "right_box2_text",
				    "description" => esc_html__("Smaller text visible below the Main one.", 'crexis'),
				    "value" => "This is a short description of the feature. Please feel free to change me!"
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Right Side Box 2 Icon', 'crexis' ),
					'param_name' => 'right_box2_icon',
				    'value' => 'fa fa-info-circle',
					'settings' => array(
						'emptyIcon' => false, // default true, display an "EMPTY" icon?
						'iconsPerPage' => 200, // default 100, how many icons per/page to display
					),
					'description' => esc_html__( 'Select icon from library.', 'crexis' ),
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 2 Link URL", 'crexis'),
				    "param_name" => "right_box2_url",
				    "description" => esc_html__("URL for the feature box link. Leave blank to disable.", 'crexis'),
				    "value" => ""
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 2 Tooltip Label", 'crexis'),
				    "param_name" => "right_box2_tooltip_label",
				    "description" => esc_html__("Text that appears when hovering over the tooltip icon. Leave blank to disable.", 'crexis'),
				    "value" => "This is a simple tooltip label."
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 2 Tooltip Right Offset", 'crexis'),
				    "param_name" => "right_box2_offset_left",
				    "description" => esc_html__("Box tooltip Right offset in pixels. Default: 160px", 'crexis'),
				    'dependency' => array(
				    	'element' => 'right_box2_tooltip_label',
				    	'not_empty' => true,
				    ),
				    "value" => "180px"
				),
				array(
				    "type" => "textfield",	       
				    "heading" => esc_html__("Right Side Box 2 Tooltip Top Offset", 'crexis'),
				    "param_name" => "right_box2_offset_top",
				    "description" => esc_html__("Box tooltip top offset in pixels. Default: 50px", 'crexis'),
				    'dependency' => array(
				    	'element' => 'right_box2_tooltip_label',
				    	'not_empty' => true,
				    ),
				    "value" => "20px"
				),
			
			array(
			   "type" => "dropdown",
			   "class" => "hidden-label",
			   "heading" => esc_html__("Plus Icon with toggle content?", 'crexis'),
			   "param_name" => "plus_icon",
			   "description" => "Enable the extra content displayed after clicking the plus icon.",
			   "value" => array(
			   		'Yes' => 'yes',
			   		'No' => 'no',
			   	),
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Toggle Content Heading", 'crexis'),
			    "param_name" => "toggle_heading",
			    "description" => esc_html__("Main heading of the toggle content.", 'crexis'),
			    'dependency' => array(
			    	'element' => 'plus_icon',
			    	'value' => array('yes'),
			    ),
			    "value" => "Clean, Responsive and Professional design with powerful code!"
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Toggle Content Link Title", 'crexis'),
			    "param_name" => "toggle_link_title",
			    "description" => esc_html__("Title of the link.", 'crexis'),
			    'dependency' => array(
			    	'element' => 'plus_icon',
			    	'value' => array('yes'),
			    ),
			    "value" => "View all our features"
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Toggle Content Link URL", 'crexis'),
			    "param_name" => "toggle_link_url",
			    "description" => esc_html__("URL of the link.", 'crexis'),
			    'dependency' => array(
			    	'element' => 'plus_icon',
			    	'value' => array('yes'),
			    ),
			    "value" => "http://yoursite.com"
			),
	   )
	));
		
	// Social Icons
	
	$social_icons_param = array(
	  "type" => "dropdown",
	  "heading" => esc_html__("Style", 'crexis'),
	  "param_name" => "style",
	  "class" => "hidden-label",
	  "value" => array("Round" => "round"),
	  "description" => esc_html__("Social icons style.", 'crexis')
	);
	
	//$social_icons_param = array();

	$social_icons_params_arr[] = $social_icons_param;
	
	$social_icons = array('mail' => "E-Mail",'facebook','twitter','instagram','mail' => "E-Mail",'tumblr','linkedin','youtube' => 'YouTube','vimeo','skype','google_plus' => 'Google Plus','flickr','dropbox','pinterest','dribbble','soundcloud','rss');
	
	$icon_key = '';
	
	foreach($social_icons as $social_icon_key => $social_icon_name) {
	
		$icon_key = $social_icon_key;
		
		if(is_numeric($social_icon_key)) {
			$icon_key = $social_icon_name;
		}
		
		$social_icons_params_arr[] = array(
		    "type" => "textfield",	         
		    "heading" => ucfirst($social_icon_name),
		    "param_name" => $icon_key,
		    "holder" => "h5",
		    "description" => ucfirst($social_icon_name).' social site URL.'
		);
	}
	
	$social_icons_params_arr[] = array(
		'type' => 'css_editor',
		'heading' => __( 'CSS box', 'crexis' ),
		'param_name' => 'css',
		'group' => __( 'Design Options', 'crexis' ),
	);
	
	vc_map( array(
	   "name" => esc_html__("Social Icons", 'crexis'),
	   "base" => "social_icons",
	   "class" => "font-awesome",
	   "icon" => "fa-twitter",
	   "category" => 'Content',
	   "description" => "List of social icons",
	   "params" => $social_icons_params_arr
	));
	
	// Simple Contact Form
	
	
	include_once(ABSPATH . 'wp-admin/includes/plugin.php'); // Require plugin.php to use is_plugin_active() below

	if (is_plugin_active('contact-form-7/wp-contact-form-7.php')) {
	
		function crexis_add_vc_cf7() {
			vc_remove_element('contact-form-7');
			global $wpdb;
			$cf7 = $wpdb->get_results(
			"
			SELECT ID, post_title
			FROM $wpdb->posts
			WHERE post_type = 'wpcf7_contact_form'
			"
			);
			$contact_forms = array();
			if ($cf7) {
				$contact_forms['Select Contact Form'] = 'none';
			foreach ( $cf7 as $cform ) {
			  $contact_forms[$cform->post_title] = $cform->ID;
			}
			} else {
			$contact_forms["No contact forms found"] = 0;
			}	  
			vc_map( array(
			"base" => "crexis_contact_form",
			"name" => esc_html__("Contact Form 7", 'crexis'),
			"icon" => "icon-wpb-contactform7",
			"category" => esc_html__('Content', 'crexis'),
			"description" => esc_html__('Place Contact Form7', 'crexis'),
			"params" => array(
			  array(
			    "type" => "dropdown",
			    "heading" => esc_html__("Select contact form", 'crexis'),
			    "param_name" => "id",
			    "admin_label" => true,
			    "value" => $contact_forms,
			    "description" => esc_html__("Choose previously created Contact Form 7 form from the drop down list.", 'crexis')
			  ),
			//	      array(
			//	        "type" => "dropdown",
			//	        "heading" => esc_html__("Color Scheme", 'crexis'),
			//	        "param_name" => "color_scheme",
			//	        "value" => array("White" => "white", "Dark" => "dark"),
			//	        "description" => esc_html__("Color scheme of the contact form.", 'crexis')
			//	      ),
			  array(
			    "type" => "dropdown",
			    "heading" => esc_html__("Animated", 'crexis'),
			    "param_name" => "animated",
			    "value" => array("No" => "no", "Yes" => "no"),
			    "description" => esc_html__("Enable fade in animation on scroll", 'crexis')
			  )
			)
			) );
		}
		
		crexis_add_vc_cf7();
	
	} // if contact form7 plugin active
	

	
	// Contact Block
	
	vc_map( array(
	   "name" => esc_html__("Contact Block", 'crexis'),
	   "base" => "contact_block",
	   "class" => "font-awesome",
	   "icon" => "fa-envelope-square",
	   "description" => "Show off your contact data",
	   "category" => 'Content',
	   "params" => array( 
		   	array(
		   	  "type" => "dropdown",
		   	  "heading" => esc_html__("Style", 'crexis'),
		   	  "param_name" => "style",
		   	  "value" => array(
		   	  	"Horizontal" => "horizontal", 
		   	  	"Vertical" => "vertical"
		   	  ),
		   	  "description" => esc_html__("Contact Block style.", 'crexis')
		   	),
		   	array(
		   	    "type" => "textfield",	         
		   	    "heading" => esc_html__("Title", 'crexis'),
		   	    "param_name" => "title",
		   	    "holder" => "div",
		   	    "description" => esc_html__("Title of the contact block.", 'crexis'),
		   	    "dependency" => Array('element' => "style", 'value' => array('vertical')),
		   	    "value" => "Keep in Touch"
		   	),  
//		   	array(
//		   	    "type" => "textarea",
//		   	    "heading" => esc_html__("Description", 'crexis'),
//		   	    "param_name" => "description",
//		   	    "value" => "",
//		   	    "description" => esc_html__("Optional description displayed under the title.", 'crexis')
//		   	),  			
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 1 Title", 'crexis'),
			    "param_name" => "field1_title",
			    "description" => esc_html__("First field title.", 'crexis'),
			    "value" => "Phone"
			),   
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 1 Text", 'crexis'),
			    "param_name" => "field1_text",
			    "description" => esc_html__("First field text content.", 'crexis'),
			    "value" => "0123 456 789 - 0123 456 788"
			),   
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 1 URL", 'crexis'),
			    "param_name" => "field1_url",
			    "description" => esc_html__("URL for the first field. Leave blank for no action.", 'crexis'),
			    "value" => "tel:123456789"
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Field 1 Icon', 'crexis' ),
				'param_name' => 'field1_icon',
			    'value' => 'fa fa-mobile',
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'iconsPerPage' => 200, // default 100, how many icons per/page to display
				),
				'description' => esc_html__( 'Select icon for the first field.', 'crexis' ),
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 2 Title", 'crexis'),
			    "param_name" => "field2_title",
			    "description" => esc_html__("Second field title.", 'crexis'),
			    "value" => "E-Mail"
			),   
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 2 Text", 'crexis'),
			    "param_name" => "field2_text",
			    "description" => esc_html__("Second field text content.", 'crexis'),
			    "value" => "hello@mysite.com"
			),   
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 2 URL", 'crexis'),
			    "param_name" => "field2_url",
			    "description" => esc_html__("URL for the second field. Leave blank for no action.", 'crexis'),
			    "value" => "mailto:hello@mysite.com"
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Field 2 Icon', 'crexis' ),
				'param_name' => 'field2_icon',
			    'value' => 'fa fa-envelope',
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'iconsPerPage' => 200, // default 100, how many icons per/page to display
				),
				'description' => esc_html__( 'Select icon for the second field.', 'crexis' ),
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 3 Title", 'crexis'),
			    "param_name" => "field3_title",
			    "description" => esc_html__("Third field title.", 'crexis'),
			    "value" => "Address"
			),   
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 3 Text", 'crexis'),
			    "param_name" => "field3_text",
			    "description" => esc_html__("Third field text content.", 'crexis'),
			    "value" => "Street 1352, Melbourne Australia"
			),   
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Field 3 URL", 'crexis'),
			    "param_name" => "field3_url",
			    "description" => esc_html__("URL for the third field. Leave blank for no action.", 'crexis'),
			    "value" => ""
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Field 3 Icon', 'crexis' ),
				'param_name' => 'field3_icon',
			    'value' => 'fa fa-map-marker',
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'iconsPerPage' => 200, // default 100, how many icons per/page to display
				),
				'description' => esc_html__( 'Select icon for the third field.', 'crexis' ),
			),
			
	   )
	));
	
	// Call to Action
	
	vc_map( array(
	   "name" => esc_html__("Call to Action", 'crexis'),
	   "base" => "cta",
	   "class" => "",
	   "icon" => "icon-wpb-call-to-action",
	   "controls" => "edit_popup_delete",
	   "category" => 'Content',
	   "description" => "Heading text with buttons",
	   "params" => array(   	  
	      array(
	          "type" => "textarea",
	          "heading" => esc_html__("Heading", 'crexis'),
	          "param_name" => "heading",
	          "value" => "This is the main heading.",
	          "description" => esc_html__("Enter your Call to Action Heading", 'crexis'),
	          "admin_label" => true
	      ),
	      array(
	          "type" => "textarea",
	          "heading" => esc_html__("Subtitle", 'crexis'),
	          "param_name" => "subtitle",
	          "value" => "I'm the CTA subtitle, feel free to change me!",
	          "dependency" => Array('element' => "style", 'value' => array('classic')),
	          "description" => esc_html__("Subtitle displayed under the main Heading.", 'crexis'),
	          "admin_label" => true
	      ),
	      array(
	      	"type" => "dropdown",
	      	"class" => "hidden-label",
	      	"value" => array(
	      		"Classic" => 'classic',
	      		"Minimal" => 'minimal'
	      	),
	      	"heading" => esc_html__("Style", 'crexis'),
	      	"description" => esc_html__('Style of your Call to Action area.', 'crexis'),
	      	"param_name" => "style"
	      	),	
	      array(
	          "type" => "textfield",	          
	          "heading" => esc_html__("Button 1 Title", 'crexis'),
	          "param_name" => "button1_title",
	          "description" => esc_html__("Enter label for the first button", 'crexis'),
	          "value" => "Click me!",
	          "admin_label" => true
	      ),
	      array(
	          "type" => "textfield",	          
	          "heading" => esc_html__("Button 1 Subtitle", 'crexis'),
	          "param_name" => "button1_subtitle",
	          "description" => esc_html__("Enter secondary, smaller text for the first button. Displayed under the main label.", 'crexis'),
	          "dependency" => Array('element' => "style", 'value' => array('classic')),
	          "value" => "Small text",
	          "admin_label" => true
	      ),
	      array(
	          "type" => "textfield",
	          "heading" => esc_html__("Button 1 Link", 'crexis'),
	          "param_name" => "button1_url",
	          "description" => esc_html__("Enter the URL the first button will link to", 'crexis'),
	           "dependency" => Array('element' => "button1_title", 'not_empty' => true),
	          "value" => "http://"
	      ), 
	      array(
	          "type" => "textfield",	          
	          "heading" => esc_html__("Button 2 Title", 'crexis'),
	          "param_name" => "button2_title",
	          "description" => esc_html__("Enter the title for the second button", 'crexis'),
	          "value" => ""
	      ),
	      array(
	          "type" => "textfield",	          
	          "heading" => esc_html__("Button 2 Subtitle", 'crexis'),
	          "param_name" => "button2_subtitle",
	          "description" => esc_html__("Enter secondary, smaller text for the second button. Displayed under the main label.", 'crexis'),
	          "dependency" => Array('element' => "style", 'value' => array('classic')),
	          "value" => "",
	          "admin_label" => true
	      ),
	      array(
	          "type" => "textfield",
	          "heading" => esc_html__("Button 2 Link", 'crexis'),
	          "param_name" => "button2_url",
	          "description" => esc_html__("Enter the URL the second button will link to", 'crexis'),
	          "dependency" => Array('element' => "button2_title", 'not_empty' => true),
	          "value" => "http://"
	      ), 
	      array(
	          "type" => "textfield",
	          "heading" => esc_html__("Margin bottom", 'crexis'),
	          "param_name" => "margin_bottom",
	          "value" => "0",
	          "dependency" => Array('element' => "fullscreen", 'value' => array("yes"))
	      ),
	      
	      
	    array(
	       "type" => "dropdown",
	    	"class" => "hidden-label",
	    	"value" => array(
	    		"Dark" => 'dark',
	    		"White" => 'white'
	    	),
	       "heading" => esc_html__("Text color", 'crexis'),
	       "param_name" => "text_color",
	       "description" => esc_html__("Select text color. Leave blank for default", 'crexis'),
	       "group" => esc_html__("Design", 'crexis')
	    ), 
	      
	    array(
	       "type" => "dropdown",
	    	"class" => "hidden-label",
	    	"value" => array(
	    		"Default" => 'default',
	    		"Georgia" => 'georgia',
	    	),
	       "heading" => esc_html__("Heading font family", 'crexis'),
	       "param_name" => "heading_font_family",
	       "description" => esc_html__("Heading font family.", 'crexis'),
	       "group" => esc_html__("Design", 'crexis')
	    ),
	    
	    array(
	      	"type" => "dropdown",
	      	"class" => "hidden-label",
	      	"value" => array(
	      		"Accent 1" => 'accent',
	      		"Accent 2" => 'accent2',
	      		"Accent 3" => 'accent3',
	      		"Dark" => 'dark'
	      	),
	      	"std" => 'accent2',
	      	"heading" => esc_html__("Button 1 Color", 'crexis'),
	      	"description" => esc_html__('Color of your Call to Action area button.', 'crexis'),
	      	"dependency" => Array('element' => "style", 'value' => array('classic')),
	      	"param_name" => "button1_color",
	      	"group" => esc_html__("Design", 'crexis')
	      	),
	       array(
	    	"type" => "dropdown",
	    	"class" => "hidden-label",
	    	"value" => array(
	    		"Accent 1" => 'accent',
	    		"Accent 2" => 'accent2',
	    		"Accent 3" => 'accent3',
	    		"Dark" => 'dark'	
	    	),
	    	"std" => 'accent2',
	    	"heading" => esc_html__("Button 2 Color", 'crexis'),
	    	"dependency" => Array('element' => "button2_title", 'not_empty' => true),
	    	"description" => esc_html__('Color of your Call to Action area buttons.', 'crexis'),
	    	"dependency" => Array('element' => "style", 'value' => array('classic')),
	    	"param_name" => "button2_color",
	    	"group" => esc_html__("Design", 'crexis')
	    ), 
	    
	      
	   )
	));
	
	// Centered Heading
	
	vc_map( array(
	   "name" => esc_html__("Pricing Box", 'crexis'),
	   "base" => "pricing_box",
	   "class" => "font-awesome",
	   "icon" => "fa-usd",
	   "category" => 'Content',
	   "description" => "Product box with prices",
	   "params" => array(     
			array(
			    "type" => "textfield",	         
			    "heading" => esc_html__("Box Title", 'crexis'),
			    "param_name" => "title",
			    "description" => esc_html__("Your Pricing Box title", 'crexis'),
			    "value" => "Standard",
			    "admin_label" => true
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Price", 'crexis'),
			    "param_name" => "price",
			    "description" => esc_html__("Pricing Box price", 'crexis'),
			    "value" => "$99",
			    "admin_label" => true,
			),  
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Period", 'crexis'),
			    "param_name" => "period",
			    "description" => esc_html__("Pricing Box period", 'crexis'),
			    "value" => "Month",
			), 
			array(
			  "type" => "exploded_textarea",
			  "heading" => esc_html__("Features", 'crexis'),
			  "param_name" => "features",
			  "value" => "Feature 1,Feature 2",
			  "description" => esc_html__('Enter features here. Divide each feature with linebreaks (Enter). You can also use FontAwesome icons like (icon)fa-close(/icon) for cross icon or (icon)fa-check(/icon) for a check icon!', 'crexis')
			),  
			array(
				"type" => "dropdown",
				"class" => "hidden-label",
				"value" => array(
					"Not Featured" => 'no',
					"Featured" => 'yes',
				),
				"heading" => esc_html__("Featured?", 'crexis'),
				"description" => esc_html__('Make the box stand out from the crew.', 'crexis'),
				"param_name" => "featured",
				),
			
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Button Label", 'crexis'),
			    "param_name" => "button_label",
			    "description" => esc_html__("Text visible on the box button", 'crexis'),
			    "value" => "Buy Now",
			),  
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Button URL", 'crexis'),
			    "param_name" => "button_url",
			    "description" => esc_html__("Button URL, start with http://", 'crexis'),
			    "value" => "#",
			    'dependency' => Array('element' => "button_label", 'not_empty' => true),
			),
			array(
			  "type" => "dropdown",
			  "heading" => esc_html__("Animated", 'crexis'),
			  "param_name" => "animated",
			  "value" => array("Yes" => "yes","No" => "no"),
			  "description" => "Enable the element fade in animation on scroll"
			),
				array(
				  "type" => "textfield",
				  "heading" => esc_html__("Animation Delay", 'crexis'),
				  "param_name" => "animation_delay",
				  "value" => '100',
				  "description" => "Fade in animation delay. Can be used to create a nice delay effect if multiple elements of same type.",
				  "dependency" => Array('element' => "animated", 'value' => 'yes')
				),
				
			
				
			
	   )
	));
	
	// Centered Heading
	
	vc_map( array(
	   "name" => esc_html__("Counter", 'crexis'),
	   "base" => "counter",
	   "class" => "font-awesome",
	   "icon" => "fa-clock-o",
	   "category" => 'Content',
	   "description" => "Countdown numbers",
	   "params" => array(     
			array(
			    "type" => "textfield",	         
			    "heading" => esc_html__("Counter Title", 'crexis'),
			    "param_name" => "title",
			    "description" => esc_html__("Your Counter title.", 'crexis'),
			    "value" => "Days",
			    "admin_label" => true
			),
			array(
			    "type" => "textfield",	       
			    "heading" => esc_html__("Number value", 'crexis'),
			    "param_name" => "number",
			    "description" => esc_html__("Value of the counter number.", 'crexis'),
			    "value" => "100",
			    "admin_label" => true
			),
			array(
			   "type" => "dropdown",
				"class" => "hidden-label",
				"value" => array(
					"Light" => 'white',
					"Dark" => 'dark',
					"Accent" => 'accent',
//					"Custom" => 'custom'
				),
			   "heading" => esc_html__("Counter Color", 'crexis'),
			   "param_name" => "color",
			   "description" => esc_html__("Select counter color.", 'crexis'),
			),
//			array(
//			   "type" => "colorpicker",
//			   "heading" => esc_html__("Custom Counter Color", 'crexis'),
//			   "param_name" => "color_custom",
//			   "value" => '',
//			   "dependency" => Array('element' => "color", 'value' => array("custom")),
//			   "description" => esc_html__("Select a custom color for the counter's icon and text.", 'crexis'),
//			), 		
			
	   )
	));
	
	
	
	// Veented Slider
	
	vc_map( array(
	   "name" => esc_html__("Veented Slider", 'crexis'),
	   "base" => "veented_slider",
	   "class" => "font-awesome",
	   "icon" => "fa-picture-o",
	   "category" => 'Media',
	   "params" => array(  
	   		array(
   				'type' => 'param_group',
   				'heading' => __( 'Slides', 'crexis' ),
   				'param_name' => 'slides',
   				'description' => __( 'Add slides to the Button Slider.', 'crexis' ),
   				'value' => urlencode( json_encode( array(
   					array(
   						'image' => '',
   						'heading_top' => 'Hello there',
   						'heading' => 'Welcome to our site.',
   						'text' => 'This is a truly simple slide description text. Feel free to change it!',
   						'button_label' => 'Get Started',
   						'button_action' => 'scroll',
   					),
   					array(
   						'image' => '',
   						'heading_top' => 'Secondary Slide',
   						'heading' => 'We are amazing.',
   						'text' => 'This is a truly simple slide description text. Feel free to change it!',
   						'button_label' => 'Get Started',
   						'button_action' => 'link',
   						'button_url' => '#',
   						'button_url_target'	=> 'self'
   					),
   				) ) ),
   				'params' => array(
   					array(
   						'type' => 'attach_image',
   						'heading' => esc_html__( 'Slide Background Image', 'crexis' ),
   						'param_name' => 'image',
   						'value' => '',
   						'description' => esc_html__( 'Select image from media library for the slide background.', 'crexis' ),
   					),
   					array(
						'type' => 'textfield',
						'heading' => __( 'Top Heading', 'crexis' ),
						'param_name' => 'heading_top',
						'description' => __( 'Smaller heading displayed above the Main Heading.', 'crexis' ),
						'value' => 'Simple top heading'
					),
					array(
						'type' => 'textfield',
						'heading' => __( 'Main Heading', 'crexis' ),
						'param_name' => 'heading',
						'description' => __( 'Main slide heading.', 'crexis' ),
						'value' => 'I am a slide heading',
						'admin_label' => true
					),
					array(
					   "type" => "textarea",
					   "class" => "hidden-label",
					   "heading" => esc_html__("Description Text", 'crexis'),
					   "param_name" => "text",
					   "value" => 'This is a slide description text.',
					), 
   					array(
   						'type' => 'textfield',
   						'heading' => __( 'Button Label', 'crexis' ),
   						'param_name' => 'button_label',
   						'value' => 'Get Started',
   						'description' => __( 'Slide button label.', 'crexis' ),
   					),
   					array(
					   "type" => "dropdown",
					   "class" => "hidden-label",
					   "heading" => esc_html__("Button Action", 'crexis'),
					   "param_name" => "button_action",
					   "value" => array(
					   		'Scroll after slider' => 'scroll',
					   		'Simple link' => 'link',
					   		
					   	)
					),
   					array(
   						'type' => 'textfield',
   						'heading' => __( 'Button URL', 'crexis' ),
   						'param_name' => 'button_url',
   						'dependency' => array(
   							'element' => 'button_action',
   							'value' => 'link',
   						),
   						'description' => __( 'Button link URL.', 'crexis' ),
   					),
   					array(
   					   "type" => "dropdown",
   					   "class" => "hidden-label",
   					   "heading" => esc_html__("Open button link in a new tab?", 'crexis'),
   					   "param_name" => "button_url_target",
   					   'dependency' => array(
   					   		'element' => 'button_action',
   					   		'value' => 'link',
   					   	),
   					   "value" => array(
   					   		'No' => 'self',
   					   		'Yes' => '_blank',
   					   		
   					   	)
   					),
   				),
   			),   
			array(
			   "type" => "dropdown",
				"class" => "hidden-label",
				"value" => array(
					"Centered" => 'centered',
					"Aligned Left" => 'aligned'
				),
			   "heading" => esc_html__("Slider Style", 'crexis'),
			   "param_name" => "style",
			   "description" => esc_html__("Choose the slider's style.", 'crexis'),
			),
			array(
			   "type" => "dropdown",
				"class" => "hidden-label",
				"value" => array(
					"Fullscreen" => 'fullscreen',
					"Custom" => 'custom'
				),
			   "heading" => esc_html__("Slider Height", 'crexis'),
			   "param_name" => "height",
			   "description" => esc_html__("Set the height of your slider.", 'crexis'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Custom Slider Height', 'crexis' ),
				'param_name' => 'height_custom',
				'dependency' => array(
					'element' => 'height',
					'value' => 'custom',
				),
				'value' => '700px',
				"description" => esc_html__("Set a custom height for your slider in pixels i.e: 400px, 600px, 800px", 'crexis'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Slider Top Offset', 'crexis' ),
				'param_name' => 'offset',
				'value' => '0px',
				"description" => esc_html__("Set a top offset for your slider: it's an additional space added above the slider's text. It's useful for vertically centering the slider's content when using a transparent header style. Provide a value in pixels like: 80px, 50px etc.", 'crexis'),
			),
			
			array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "value" => array(
					   "No" => 'no',
					   "Yes" => 'yes'
				   ),
				   "heading" => esc_html__("Scroll down button?", 'crexis'),
				   "param_name" => "scroll_button",
				   "description" => esc_html__("Enable or disable a button that scrolls to a #second row of your page (row with an id 'second')", 'crexis'),
			),
			array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "value" => array(
					   "Fade" => 'fade',
					   "Slide" => 'slide'
				   ),
				   "heading" => esc_html__("Slide Animation Effect", 'crexis'),
				   "param_name" => "slider_effect",
				   "description" => esc_html__("Choose the slider animation effect.", 'crexis'),
			),
			array(
				   "type" => "textfield",
				   "class" => "hidden-label",
				   "heading" => esc_html__("Autoplay", 'crexis'),
				   "param_name" => "autoplay",
				   'value' => '7000',
				   "description" => esc_html__("Delay between slide transitions in miliseconds (7000ms = 7s). Leave blank to disable autoplay.", 'crexis'),
			),
			array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "value" => array(
				   		"Yes" => 'yes',
					  	"No" => 'no',
				   ),
				   "heading" => esc_html__("Arrow Navigation", 'crexis'),
				   "param_name" => "arrow_nav",
				   "description" => esc_html__("Enable or disable the arrow navigation.", 'crexis'),
			),
			array(
				   "type" => "dropdown",
				   "class" => "hidden-label",
				   "value" => array(
				   		"Yes" => 'yes',
					   	"No" => 'no',
				   ),
				   "heading" => esc_html__("Bullet Navigation", 'crexis'),
				   "param_name" => "bullet_nav",
				   "description" => esc_html__("Enable or disable the bullet navigation.", 'crexis'),
			),
			 // Design Tab
			array(
			   "type" => "dropdown",
				"class" => "hidden-label",
				"value" => array(
					"Default" => 'default',
					"Custom" => 'custom',
				),
			   "heading" => esc_html__("Top Heading Font Size", 'crexis'),
			   "param_name" => "heading_top_size",
			   "description" => esc_html__("Top heading font size.", 'crexis'),
			   "group" => esc_html__("Design", 'crexis')
			),
				array(
				  "type" => "textfield",
				  "heading" => esc_html__("Top Heading Custom Font Size", 'crexis'),
				  "param_name" => "heading_top_size_custom",
				  "value" => "18px",
				  "description" => esc_html__("Insert top heading font size in pixels. Example: 18px", 'crexis'),
				  "dependency" => Array('element' => "heading_top_size", 'value' => array('custom')),
				  "group" => esc_html__("Design", 'crexis')
				),
			array(
			   "type" => "dropdown",
				"class" => "hidden-label",
				"value" => array(
					"Default" => 'default',
					"Custom" => 'custom',
				),
			   "heading" => esc_html__("Main Heading Font Size", 'crexis'),
			   "param_name" => "heading_size",
			   "description" => esc_html__("Main Heading font size.", 'crexis'),
			   "group" => esc_html__("Design", 'crexis')
			),
				array(
				  "type" => "textfield",
				  "heading" => esc_html__("Main Heading Custom Font Size", 'crexis'),
				  "param_name" => "heading_size_custom",
				  "value" => "72px",
				  "description" => esc_html__("Insert mainheading font size in pixels. Example: 72px", 'crexis'),
				  "dependency" => Array('element' => "heading_size", 'value' => array('custom')),
				  "group" => esc_html__("Design", 'crexis')
				),
				
			array(
			   "type" => "dropdown",
				"class" => "hidden-label",
				"value" => array(
					"Default" => 'default',
					"Custom" => 'custom',
				),
			   "heading" => esc_html__("Description Text Font Size", 'crexis'),
			   "param_name" => "text_size",
			   "description" => esc_html__("Slide description text font size.", 'crexis'),
			   "group" => esc_html__("Design", 'crexis')
			),
				array(
				  "type" => "textfield",
				  "heading" => esc_html__("Description Text Custom Font Size", 'crexis'),
				  "param_name" => "text_size_custom",
				  "value" => "15px",
				  "description" => esc_html__("Description text font size in pixels. Example: 15px", 'crexis'),
				  "dependency" => Array('element' => "text_size", 'value' => array('custom')),
				  "group" => esc_html__("Design", 'crexis')
				),
			array(
			   "type" => "dropdown",
				"class" => "hidden-label",
				"value" => array(
					"Dark" => 'dark',
					"Light" => 'white'
				),
			   "heading" => esc_html__("Button Color", 'crexis'),
			   "param_name" => "button_color",
			   "description" => esc_html__("Slider button color.", 'crexis'),
			   "group" => esc_html__("Design", 'crexis')
			),
	   )
	));


}

?>