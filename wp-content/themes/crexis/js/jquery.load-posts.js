jQuery(document).ready(function ($) {

    // The number of the next page to load (/page/x/).
    //var pageNum = parseInt(pbd_alp.startPage) + 1;

    // The maximum number of pages the current query can return.
    //var max = parseInt(pbd_alp.maxPages);

    // The link of the next page of posts.
    //var nextLink = pbd_alp.nextLink;
    

    /**
     * Replace the traditional navigation with our own,
     * but only if there is at least one page of new posts to load.
     */
    
    if( $('.blog-inner').length > 0 ) vntd_ajax_blog();
	
	function vntd_ajax_blog() {
	
		$('.blog-pagination').remove();
	
		// The number of the next page to load (/page/x/).
	    var pageNum = parseInt(pbd_alp_blog.startPage) + 1;
	
	    // The maximum number of pages the current query can return.
	    var max = parseInt(pbd_alp_blog.maxPages);
	
	    // The link of the next page of posts.
	    var nextLink = pbd_alp_blog.nextLink;
	
	    /**
	         * Load new posts when the link is clicked.
	         */
	    $('#ajax-load-posts a').click(function () {
	
	        // Are there more posts to load?
	        if (pageNum <= max) {
	
	            // Show that we're working.
	            $(this).html('Loadings posts.. <div class="spinner-ajax"></div>');
	            $(this).find('.spinner-ajax').css('opacity',1);
	
	            $.get(nextLink, function (data) {
					//alert('test');
	                pageNum++;
	                nextLink = nextLink.replace(/\/page\/[0-9]?/, '/page/' + pageNum);
	
	                if (pageNum <= max) {
	                    $('#ajax-load-posts a').text('Load More Posts');
	                } else {
	                    $('#ajax-load-posts a').text('No more posts to load.').addClass('ajax-no-posts');
	                }
	                                
	
	            }).done(function (data) {
					
	                var $newItems = $(data).find('.blog .post');				
	
	                $newItems.find('img').bind("load", function () { 
	                	
	                	var $holder = $('.blog-items');	                     
	                    
	                    if($holder.length !== 0) {   
	                                     	
	                    	$('.blog-items').cubeportfolio('appendItems', $newItems, function() {
	
	                    	});
	                    	
	                    }           
						
	                });
	
		                            
	            });
	                        
	
	        } else {
	            //$('#ajax-load-posts a').append('.');
	        }
	
	        return false;
	    });
	    
	}
    
    if($('#portfolio-load-posts a').length > 0) {
    	vntd_ajax_portfolio();
    }
    
    
    function vntd_ajax_portfolio() {
    
    	// The number of the next page to load (/page/x/).
	    var pageNum = parseInt(pbd_alp_portfolio.startPage) + 1;
	
	    // The maximum number of pages the current query can return.
	    var max = parseInt(pbd_alp_portfolio.maxPages);
	
	    // The link of the next page of posts.
	    var nextLink = pbd_alp_portfolio.nextLink;
    
	    /**
	         * Load new posts when the link is clicked.
	         */
	    $( '#portfolio-load-posts a' ).click(function () {
	
	        // Are there more posts to load?
	        if (pageNum <= max) {
	
	            // Show that we're working.
	            
	            if( $(this).hasClass('circle-button') ) {
	            	$(this).html('...');
	            } else {
	            	$(this).html('Loadings posts.. <div class="spinner-ajax"></div>');
	            	$(this).find('.spinner-ajax').css('opacity',1);
	            } 
	
	            $.get(nextLink, function (data) {
					//alert('test');
	                pageNum++;
	                nextLink = nextLink.replace(/\/page\/[0-9]?/, '/page/' + pageNum);
	
	                if (pageNum <= max) {
	                    $('#portfolio-load-posts a.minimal-button').text('Load More Posts');
	                    $('#portfolio-load-posts a.circle-button').html('<i class="fa fa-plus"></i>');
	                } else {
	                    $('#portfolio-load-posts a.minimal-button').text('No more posts to load.').addClass('ajax-no-posts');
	                    $('#portfolio-load-posts a.circle-button').html('!');
	                }
	                                
	
	            }).done(function (data) {
					
	                var $newItems = $(data).find('.portfolio-items .item');				
	
	                $newItems.find('img').bind("load", function () { 
	                	
	                	var $holder = $('.portfolio-items');	                     
	                    
	                    if($holder.length !== 0) {   
	                                     	
	                    	$('.portfolio-items').cubeportfolio('appendItems', $newItems, function() {
	
	                    	});
	                    	
	                    }           
						
	                });
	
		                            
	            });
	                        
	
	        } else {
	            //$('#ajax-load-posts a').append('.');
	        }
	
	        return false;
	    });
	    
	}
        
});