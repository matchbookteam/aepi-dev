//
// fullPage script configuration file
//

// WordPress navigation preparation

jQuery('.page-template-template-fullpage #navigation .nav-menu a').each(function() {

	var dataValue = jQuery(this).data('getanchor');
	jQuery(this).closest('li').attr('data-menuanchor', dataValue);
	
});

// Initialize arrays

var pageAnchors = [];
var pageTooltips = [];
var darkNavSections = [];

// Fill arrays (loop through all sections)

jQuery('#page-content .section').each(function() {

	var id = jQuery(this).attr('id');
	
	// Add tooltips based on a section ID attribute
	
	//var tooltipName = id.replace(/\-/g, ' ');
	var tooltipName = id;
	pageTooltips.push(tooltipName);
	
	// Add Page Anchors based on a section ID attribute
	
	pageAnchors.push(id);
	
	// Add sections with dark navigation
	
	if(jQuery(this).hasClass('dark-nav')) {
		darkNavSections.push(id);
	}
	
	// Remove section ID to avoid conflict with fullPage 
	
	jQuery(this).attr('id','');
	
});

jQuery('.scroll').removeClass('scroll');

// Relocate footer to the last section

if(jQuery( '#page-content > .vc_row:last-child > .vc_column_container').length > 0) {

	jQuery( 'footer.footer' ).insertAfter( "#page-content > .vc_row:last-child > .vc_column_container" );
	
	if( jQuery( '#footer-widgets-area' ).length > 0 ) {
		jQuery( '#footer-widgets-area' ).insertAfter( "#page-content > .vc_row:last-child > .vc_column_container" );
	}
	
} else {

	jQuery( 'footer.footer' ).insertAfter( "#page-content > .vc_row:last-child > .inner" );
	
	if( jQuery( '#footer-widgets-area' ).length > 0 ) {
		jQuery( '#footer-widgets-area' ).insertAfter( "#page-content > .vc_row:last-child > .inner" );
	}
	
}



// Initialize fullPage

jQuery('#page-content').fullpage({
	navigation: true,
	scrollingSpeed: 900,
	navigationPosition: 'right',
	continuousVertical: false,
	scrollBar:true,
	css3: true,
	menu: '#navigation .nav-menu > .nav',
	navigationTooltips: pageTooltips,
	anchors: pageAnchors,
	scrollOverflow: true,
	afterLoad: function(anchorLink, index) {
	
		// If section has a dark nav
		
		if( jQuery.inArray(anchorLink, darkNavSections) !== -1 ) {
			jQuery('#navigation').addClass('active');
		} else {
			jQuery('#navigation').removeClass('active');
		}
	
	}
});