<?php
//
// Blog Post Settings
//


add_action("admin_init", "crexis_blog_post_settings");   

// Add Blog Metaboxes
function crexis_blog_post_settings(){   
    add_meta_box("blog_gallery_post_format", esc_html__("Gallery Settings",'crexis'), "crexis_blog_gallery_settings_config", "post", "normal", "high");
    add_meta_box("blog_video_post_format", esc_html__("Video Settings",'crexis'), "crexis_blog_video_settings_config", "post", "normal", "high");
    add_meta_box("blog_quote_post_format", esc_html__("Quote Settings",'crexis'), "crexis_blog_quote_settings_config", "post", "normal", "high");
    add_meta_box("blog_link_post_format", esc_html__("Link Settings",'crexis'), "crexis_blog_link_settings_config", "post", "normal", "high");
}

function crexis_blog_post_settings_config(){
        global $post;
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
		$thumb_setting = $thumb_height = $thumb_lightbox = '';
		if(isset($custom["thumb_setting"][0])) $thumb_setting = $custom["thumb_setting"][0];	
		if(isset($custom["thumb_height"][0])) $thumb_height = $custom["thumb_height"][0];
		if(isset($custom["thumb_lightbox"][0])) $thumb_lightbox = $custom["thumb_lightbox"][0];
?>
    <div class="form-table custom-table fullwidth-metabox">
    	<div class="metabox-option">
    	    		
    	    <h6><?php esc_html_e('Thumbnail Display', 'crexis') ?>:</h6>
    	    <div class="metabox-option-side">    	    
    	    <?php 
    	    
    	    $thumb_setting_arr = array("Display thumbnail on single post page" => "on", "Do NOT display the thumbnail on post page" => "off");
    	    
    	    crexis_create_dropdown('thumb_setting',$thumb_setting_arr,$thumb_setting);
    	    
    	    ?>   	    
    	    </div>
    	</div>
    	<div class="metabox-option">    		
    	    <h6><?php esc_html_e('Thumbnail Lightbox', 'crexis') ?>: <span class="form-caption">(<?php esc_html_e('standard post format', 'crexis') ?>)</span></h6>
    	    <div class="metabox-option-side">   	    
    	    <?php 
    	    
    	    $thumb_lightbox_arr = array("Disable lightbox" => "off","Enable lightbox zoom of thumbnail image" => "on");
    	    
    	    crexis_create_dropdown('thumb_lightbox',$thumb_lightbox_arr,$thumb_lightbox);
    	    
    	    ?>    

    	    </div>
    	</div>
    	<div class="metabox-option">  
            <h6><?php esc_html_e('Thumbnail Height', 'crexis') ?>:</h6>
            <div class="metabox-option-side">            
        	<?php 
        	
        	$thumb_heights = array("Landscape" => "landscape", "Original Aspect Ratio" => "auto");
        	
        	crexis_create_dropdown('thumb_height',$thumb_heights,$thumb_height);
        	
        	?>        
            </div> 
       </div> 
        
    </div> 
<?php
}	

// Gallery Metabox

function crexis_blog_gallery_settings_config(){	
        global $post;
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $gallery_type = $gallery_images = '';
		if(isset($custom["gallery_type"][0])) $gallery_type = $custom["gallery_type"][0];
		if(isset($custom["gallery_images"][0])) $gallery_images = $custom["gallery_images"][0];
?>
    <div class="form-table custom-table fullwidth-metabox">
    	<div class="metabox-option">
    		<h6><?php esc_html_e('Gallery Images', 'crexis') ?>:</h6> 
    		
    		<div class="metabox-option-side">
    		<?php crexis_gallery_metabox($gallery_images); ?>	 
    		</div>  
    	</div>   
    </div>
<?php
}


// Video Metabox


function crexis_blog_video_settings_config(){	
        global $post;
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $video_site_url = '';
		if(isset($custom["video_site_url"][0])) $video_site_url = $custom["video_site_url"][0];
?>
    <div class="form-table custom-table fullwidth-metabox">
    	<div class="metabox-option">
    		<h6><?php esc_html_e('Video URL', 'crexis') ?>:<span class="form-caption">(<a target="_blank" href="https://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F"> <?php esc_html_e('List of supported sites', 'crexis') ?></a>)</span></h6>
    		
    		<div class="metabox-option-side">
    	    <td class="description-textarea">
    	    	<input type="text" name="video_site_url" value="<?php echo esc_url($video_site_url); ?>">
    	    </td>
    	    </div>
    	</div>      
    </div>
<?php
}

// Quote Metabox


function crexis_blog_quote_settings_config(){	
        global $post;
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $quote_content = $quote_author = '';
		if(isset($custom["quote_content"][0])) $quote_content = $custom["quote_content"][0];
		if(isset($custom["quote_author"][0])) $quote_author = $custom["quote_author"][0];
?>
    <div class="form-table custom-table fullwidth-metabox">
    	<div class="metabox-option">
    		<h6><?php esc_html_e('Quote Content', 'crexis') ?>:</h6>
    		
    		<div class="metabox-option-side">
    			<textarea class="textarea-full" name="quote_content"><?php echo esc_attr($quote_content); ?></textarea>
    		</div>
    	</div>
    	<div class="metabox-option">
    		<h6><?php esc_html_e('Quote Author', 'crexis') ?>:</h6>
    		
    	    <div class="metabox-option-side">
    	    	<input type="text" name="quote_author" value="<?php echo esc_attr($quote_author); ?>">
    	    </div>
    	</div>
    </div>
<?php
}

// Link Metabox


function crexis_blog_link_settings_config(){	
        global $post;
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $post_link = '';
		if(isset($custom["post_link"][0])) $post_link = $custom["post_link"][0];
?>
    <div class="form-table custom-table fullwidth-metabox">
    	<div class="metabox-option">
    		<h6><?php esc_html_e('Link URL', 'crexis') ?>:</h6>
    	    <div class="metabox-option-side">
    	    	<input type="text" name="post_link" value="<?php echo esc_attr($post_link); ?>">
    	    </div>
    	</div>
    </div>
<?php
}

	
// Save Custom Fields
	
add_action('save_post', 'crexis_save_post_settings'); 

function crexis_save_post_settings(){
    global $post;  

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{
		if(isset($_POST["page_title"])) update_post_meta($post->ID, "page_title", $_POST["page_title"]);	
		if(isset($_POST["tagline"])) update_post_meta($post->ID, "tagline", $_POST["tagline"]);	
		if(isset($_POST["page_layout"])) update_post_meta($post->ID, "page_layout", $_POST["page_layout"]);
		if(isset($_POST["page_sidebar"])) update_post_meta($post->ID, "page_sidebar", $_POST["page_sidebar"]);
		if(isset($_POST["thumb_setting"])) update_post_meta($post->ID, "thumb_setting", $_POST["thumb_setting"]);
		if(isset($_POST["thumb_height"])) update_post_meta($post->ID, "thumb_height", $_POST["thumb_height"]);
		if(isset($_POST["thumb_lightbox"])) update_post_meta($post->ID, "thumb_lightbox", $_POST["thumb_lightbox"]);
		if(isset($_POST["gallery_type"])) update_post_meta($post->ID, "gallery_type", $_POST["gallery_type"]);	
		if(isset($_POST["gallery_images"])) update_post_meta($post->ID, "gallery_images", $_POST["gallery_images"]);
		if(isset($_POST["video_site_url"])) update_post_meta($post->ID, "video_site_url", $_POST["video_site_url"]);
		if(isset($_POST["video_file_url"])) update_post_meta($post->ID, "video_file_url", $_POST["video_file_url"]);
		if(isset($_POST["quote_content"])) update_post_meta($post->ID, "quote_content", $_POST["quote_content"]);
		if(isset($_POST["quote_author"])) update_post_meta($post->ID, "quote_author", $_POST["quote_author"]);
		if(isset($_POST["post_link"])) update_post_meta($post->ID, "post_link", $_POST["post_link"]);	
    }

}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Comments Layout
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; 
   global $post;
   ?>
   
	<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
	
		<!-- Comment -->
		<div class="comment media">
			<!-- Image -->
			<div class="comment-author-avatar media-left">
				<?php echo get_avatar($comment,$size='100'); ?>
			</div>
			<!-- Description -->
			<div class="comment-text media-body">
			
				<div class="details">
					<!-- Reply Button -->
					
					<h4 class="comment-heading media-heading">
								
						<!-- Name -->
						<span class="comment-author"><?php echo get_comment_author(); ?></span>
						<!-- Date -->
						<span class="comment-date light mini-text ml-15"><?php echo get_comment_date('F d, Y'); ?></span>
						
					</h4>
					<!-- Description -->
					<?php comment_text(); ?>
					
					<p class="votes t-right mt-20 comment-reply" style="margin-top: 20px;">
						<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'],'reply_text' => esc_html__('Reply','crexis')))); ?>
					</p>
					
				</div>
				
			</div>
			<!-- End Description -->
		</div>
		<!-- End Comment -->
		
	</li>
	
<?php
}

// Blog Comments Script

function crexis_comments_script() {
	if(is_singular())
	wp_enqueue_script('comment-reply');
}
add_action('wp_enqueue_scripts', 'crexis_comments_script');


function crexis_blog_post_tags(){
	if(has_tag()){
	?>
	<div class="post-tags"><i class="fa fa-tags"></i> <?php the_tags('', ', ', '<br />'); ?></div>
	<?php
	}
}

function crexis_blog_post_author($blog_style = NULL){
	global $post;
	
	if(get_the_author_meta('description')) {
	?>
	<div class="post-author">
	
		<div class="post-author-avatar">
			<div class="post-author-circle"><?php echo get_avatar( get_the_author_meta('ID'), 100 ); ?></div>
		</div>		
		
		<div class="post-author-info">
			<h4 class="post-section-heading"><?php the_author(); ?></h4>
			<p><?php echo get_the_author_meta('description'); ?></p>
		</div>	
	
	</div>
	<?php	
	}
}

function crexis_post_meta() {
	global $post;
	
	?>
	<div class="vntd-meta-section classic-meta-section">
		<span class="vntd-meta-author">
			<?php echo esc_html__('By','crexis'); ?>: 
			<a href="<?php echo get_the_author_meta( 'user_url'); ?>">
				<?php the_author(); ?>
			</a>
		</span>
		<span class="vntd-meta-date">
			<?php echo esc_html__('On','crexis'); ?>: 
			<span class="meta-value"><?php the_time( get_option( 'date_format' ) ); ?></span>
		</span>
		<span class="vntd-meta-categories">
			<?php echo esc_html__('In','crexis'); ?>:
			<?php the_category(', '); ?>
		</span>	
		<span class="vntd-meta-comments">
			<?php echo esc_html__('Comments','crexis'); ?>:
			<a href="<?php echo get_permalink($post->ID).'#comments'?>" title="<?php esc_html_e('View comments','crexis');?>"><?php comments_number('0', '1', '%'); ?></a>
		</span>
		
	</div> 
	<?php

}

function crexis_post_meta_extra() {
	?>
	
	<div class="blog-extra-meta">
		<div class="extra-meta-item extra-meta-date">
			<?php

			echo '<span class="vntd-day">';
			$date_format = 'd';
			the_time( $date_format );
			echo '</span><span class="vntd-month">';
			$date_format = 'M';
			the_time( $date_format );
			echo '</span>';

			?>
		</div>
	</div>
	
	<?php
}

function crexis_post_tags(){

	$posttags = get_the_tags();
	
	if($posttags == NULL) return false;
	
	if ($posttags) {
		echo '<span class="post-meta-tags">';
		$i = 0;
		$len = count($posttags);
		foreach($posttags as $tag) {	
		  echo '<a href="'. get_tag_link($tag->term_id) .'">'; 
		  echo esc_textarea($tag->name);	 
		  echo "</a>";
		   $i++;
		  if($i != $len) echo ', ';		 
		}
		echo '</span>';
	}	
}

function crexis_meta_enabled() {

	if( !( !is_single() && crexis_option('blog_meta') == false ) && !( is_single() && crexis_option('blog_single_meta') == false ) ) {
		return true;
	}
	
	return false;
	
}

function crexis_blog_post_content( $page_layout = NULL, $blog_style = NULL, $grid_style = NULL, $masonry = NULL ) {

	global $post;
	
	$post_format = get_post_format($post->ID);
	
	if(!$post_format) {
		$post_format = 'standard';
	}
	
	$extra_classes = array();
	
	$excerpt_size = 50;
	$grid_style = 'simple';
	if ( is_null( $grid_style ) ) {
		$grid_style = crexis_option('blog_grid_style');
	}
	
	if ( is_null( $blog_style ) ) {
		$blog_style = 'classic';
	}
	
	
	if ( $blog_style == "grid" ) {
	
		wp_enqueue_script('cubePortfolio', '', '', '', true);
		wp_enqueue_style('cubePortfolio');
		wp_enqueue_script('crexis-masonry-blog', '', '', '', true);
		

		$extra_classes = array('item', 'cbp-item', 'vntd-grid-item');
		$excerpt_size = 20;
		
		if ( crexis_option( 'blog_grid_style' ) == 'thumb_bg' ) {
			if( !has_post_thumbnail() ) return null;
			$excerpt_size = 18;
		}
		
	} elseif ( $blog_style == "minimal" ) {
		$excerpt_size = 30;
	}
	
	$post_media_class = 'post-no-media';
	
	if(has_post_thumbnail()) {
		$post_media_class = 'post-has-media';
	}
	array_push($extra_classes, $post_media_class);
	array_push($extra_classes, 'clearfix');
	
	if(!$masonry) {
		$masonry = crexis_option('blog_masonry');
		if($masonry == true) $masonry = 'yes';
	}
	
	?>
	
	<div <?php post_class($extra_classes); ?>>
	
	<?php
	
	if($blog_style == "grid") { 
	
	// Masonry Blog
	
	?>
	
		<!-- Item Image -->
	    <div class="item-top">
	    	<!-- Post Link -->

	        <?php
	        
	        if(!$post_format || $post_format == 'standard' || $post_format == 'quote' || $post_format == 'link' || $post_format == 'gallery' || $post_format == 'video') {
	        
	        $img_size = 'crexis-portfolio-auto';
	        
	        $imgurl = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $img_size);
	        
	        ?>
	        
	        <a href="<?php echo get_permalink( $post->ID ); ?>" class="ex-link item_image">
	        	<!-- Image Src -->
	            <img src="<?php echo esc_url( $imgurl[0] ); ?>" alt="<?php echo esc_attr( get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true)); ?>">
	        </a>
	        
	        <?php
	        
	        } elseif($post_format == 'video') {
	        
	        }
	        
	        ?>
			<!-- Icon -->
			<a href="#" class="item_button first">
				<i class="fa fa-heart"></i>
			</a>

	    </div>
	    <!-- End Item Image -->
	
		<!-- Details -->
	    <div class="details">
	    	<!-- Item Name -->
	    	<a href="<?php echo get_permalink($post->ID); ?>" class="ex-link">
	            <h2 class="head">
	            	<a href="<?php echo get_permalink($post->ID); ?>">
	            	<?php echo get_the_title($post->ID); ?>
	            	</a>
	            </h2>
	        </a>
	        
	        <!-- Description -->
	        <p class="note mt-13 thin italic">
	        	<?php the_time( get_option( 'date_format' ) ); ?>
	        </p>
	        <!-- Description -->
	        <div class="description">
	        	<?php echo crexis_excerpt($excerpt_size, true); ?>
	        </div>
	    </div>
	    <!-- End Center Details Div -->
	
	    <!-- Posted By -->
	    <div class="posted_button">
	    	<!-- Image SRC -->
	    	<?php echo get_avatar($size='100'); ?>
	    	<p>
	    		<?php esc_html_e("Posted By",'crexis'); ?> <a href="<?php echo get_the_author_meta( 'user_url'); ?>"><?php the_author(); ?></a>
	    		<?php
	    		
	    		if( crexis_option('blog_meta_categories') != false ) { 
	    		?>
	    		<span><?php the_category(', '); ?></span>
	    		<?php } ?>
	    	</p>
	    </div>
	    
	<?php
	
	} else {
	
	
	// Default Blog Style
	
	$post_class = '';
	
	// Meta Style
	
	if( crexis_option('blog_meta_style') != 'classic' && crexis_meta_enabled() ) {
	?>
	
		<div class="dates f-left">
			<!-- Post Time -->
			<h6 class="date">
				<span class="day colored helvetica"><?php $date_format = 'd'; the_time( $date_format ); ?></span>
				<?php $date_format = 'M, Y'; the_time( $date_format ); ?>
			</h6>
			<!-- Details -->
			<div class="details">
				<ul class="t-right fullwidth">
					<?php if( crexis_option('blog_meta_author') != false ) { ?>
					<!-- Posted By -->
					<li class="meta-author">
						<?php esc_html_e("Posted By",'crexis'); ?> <a href="<?php echo get_the_author_meta( 'user_url'); ?>">
							<?php the_author(); ?>
						</a>
						<i class="fa fa-user"></i>
					</li>
					<?php }
					
					if( crexis_option('blog_meta_comments') != false ) { 
					?>
					<!-- Comments -->
					<li class="meta-comments-count">
						<a href="<?php echo get_permalink($post->ID).'#comments'?>" title="<?php esc_html_e('View comments','crexis');?>"><?php comments_number('0', '1', '%'); ?> <?php  esc_html_e('Comments','crexis'); ?></a>
						<i class="fa fa-comments"></i>
					</li>
					<?php }
					
					if( crexis_option('blog_meta_categories') != false ) { 
					?>
					<!-- Tags -->
					<li class="meta-categories">
						<?php the_category(', '); ?>
						<i class="fa fa-user"></i>
					</li>
					<?php } ?>
					<!-- Liked -->
				</ul>
			</div>
			<!-- End Details -->
		</div>
		
		<?php
		
			$post_class = 'f-right';
		
		}
		
		?>
						
		<div class="post-inner <?php echo $post_class; if( crexis_option('blog_meta_style') == 'classic' || !crexis_meta_enabled() ) echo ' post-classic-meta-position'; ?>">
		
			<h2 class="post-header semibold">
				<a href="<?php echo get_permalink($post->ID); ?>">
				<?php echo get_the_title($post->ID); ?>
				</a>
			</h2>
	
			<?php 
			
			if(has_post_thumbnail()) {
				crexis_post_media($blog_style, $page_layout, $grid_style, $masonry); 
			}
			
			?>
			
			<div class="dates f-left for-mobile<?php if( crexis_option('blog_meta_style') == 'classic' ) echo ' classic-meta-position'; ?>">
				<!-- Post Time -->
				<h6 class="date">
					<span class="day colored helvetica"><?php $date_format = 'd'; the_time( $date_format ); ?></span>
					<?php $date_format = 'M, Y'; the_time( $date_format ); ?>
				</h6>
				<!-- Details -->
				<div class="details">
					<ul class="t-right fullwidth">
						<?php if( crexis_option('blog_meta_author') != false ) { ?>
						<!-- Posted By -->
						<li class="meta-author">
							<i class="fa fa-user"></i>
							<?php esc_html_e("Posted By",'crexis'); ?> <a href="<?php echo get_the_author_meta( 'user_url'); ?>">
								<?php the_author(); ?>
							</a>
							
						</li>
						<?php }
						
						if( crexis_option('blog_meta_comments') != false ) { 
						?>
						<!-- Comments -->
						<li class="meta-comments-count">
							<i class="fa fa-comments"></i>
							<a href="<?php echo get_permalink($post->ID).'#comments'?>" title="<?php esc_html_e('View comments','crexis');?>"><?php comments_number('0', '1', '%'); ?> <?php  esc_html_e('Comments','crexis'); ?></a>
							
						</li>
						<?php }
						
						if( crexis_option('blog_meta_categories') != false ) { 
						?>
						<!-- Tags -->
						<li class="meta-categories">
							<i class="fa fa-tags"></i>
							<?php the_category(', '); ?>
							
						</li>
						<?php } ?>
						<!-- Liked -->
					</ul>
				</div>
				<!-- End Details -->
			</div>
			
			<?php
			
			if(!is_single()) { 
			
			?>	
		
			<div class="post-text">
			
				<?php echo crexis_excerpt($excerpt_size, true); ?>		
			
			</div>
		
			<?php 
			
			} elseif(is_single()) { 
			
				echo '<div class="post-content-holder">';
			
				the_content();
				
				echo '</div>';
			
			}
			
			?>
		
		</div>
		
	<?php
	
	} // End Blog Style
	
	?>
	
		
	</div>
		
	<?php
	
	if( 'open' == $post->ping_status && $blog_style != "grid" && is_single() && crexis_option('blog_trackback') != false ) {
		echo '<p class="post-trackback"><i class="fa fa-chain"></i> ' . esc_html__('Trackback URL', 'crexis') . ': <a href="'.get_trackback_url().'">'.get_trackback_url().'</a></p>';
	}

}

function crexis_post_media( $blog_style = null, $page_layout = null, $grid_style = null, $masonry = null ) {
	
	global $post;
	
	$post_format = get_post_format($post->ID);
	
	$img_size = 'crexis-sidebar-landscape';

	if($blog_style == 'timeline') {
		$img_size = 'crexis-sidebar-auto';
	}
	
	if($grid_style == "thumb_bg" && $blog_style == "grid" || $blog_style == 'aligned' && $page_layout != 'fullwidth') {
		$img_size = 'crexis-sidebar-square';
	} elseif($blog_style == 'aligned' && $page_layout == 'fullwidth') {
		$img_size = 'crexis-sidebar-landscape';	
	} elseif($page_layout == 'fullwidth' || $blog_style == 'classic' && $page_layout == 'fullwidth' || $blog_style == 'minimal') {
		$img_size = 'crexis-fullwidth-landscape';
	} elseif($blog_style == 'grid') {
		if($masonry == 'yes') {
			$img_size = 'crexis-sidebar-auto';
		} else {
			$img_size = 'crexis-sidebar-square';
		}
	}
	
	echo '<div class="post-media-container">';
	
	
	if(!$post_format || $post_format == 'standard' || $post_format == 'quote' || $post_format == 'link' || $post_format == 'gallery' && !get_post_meta($post->ID,'gallery_images',TRUE) || $blog_style == "grid" && $grid_style == "thumb_bg") {
	

	wp_enqueue_script('magnific-popup', '', '', '', true);
	
	// Extra meta item
	
	$imgurl = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $img_size);
	
	$post_link = $imgurl[0];
	$extra_class = " mp-gallery";
	
	if( crexis_option( 'blog_media_link_type' ) == 'link' ) {
		$post_link = get_permalink( $post->ID );
		$extra_class = "";
	}
	
	if($post_format == "link") {
		$post_link = esc_url(get_post_meta($post->ID, 'post_link', true));
		$extra_class = '';
	} elseif($post_format == "quote") {
		$post_link = get_permalink($post->ID);
		$extra_class = '';
	}
	
	?>
	<div class="post-media <?php echo $extra_class; ?>">
		<a href="<?php echo esc_url($post_link); ?>" title="<?php echo esc_attr(get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true)); ?>">
			<img src="<?php echo esc_url($imgurl[0]); ?>" alt="<?php echo esc_attr(get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true)); ?>">
		</a>
	</div>
	<?php
	
	} elseif($post_format == 'gallery') {
	
	wp_enqueue_script('magnific-popup', '', '', '', true);
	wp_enqueue_script('vntd-flexslider', '', '', '', true);
	wp_enqueue_style('flexslider');	
	
	if($masonry == 'yes' && $page_layout == 'fullwidth' && $blog_style != 'grid') {
		$img_size = 'crexis-fullwidth-landscape';
	} elseif($masonry == 'yes' && $blog_style == 'grid') {
		$img_size = 'crexis-sidebar-square';
	}
	
	?>

	<div class="post-media basic_slider t-right">
	
		<ul class="image_slider clearfix<?php if( crexis_option( 'blog_media_link_type' ) != 'link' ) echo ' mp-gallery'; ?>">	
			<?php
			
			$gallery_images = get_post_meta($post->ID,'gallery_images',TRUE);
			
			if ( $gallery_images ) {
			
				$ids = explode(",", $gallery_images);
				
				foreach ( $ids as $id ) {
				
					$imgurl = wp_get_attachment_image_src($id, $img_size);
					$slide_link = $imgurl[0];
					
					if( crexis_option( 'blog_media_link_type' ) == 'link' ) $slide_link = get_permalink( $post->ID );
					
					echo '<li class="slide"><a href="' . $slide_link . '" title="'.esc_attr( get_post_meta( $id, '_wp_attachment_image_alt', true ) ).'"><img src="' . esc_url( $imgurl[0] ) . '" alt="' . esc_attr( get_post_meta( $id, '_wp_attachment_image_alt', true ) ) . '"></a></li>';
				}
			
			}	
			
			?>
		</ul>

	</div>
	
	<?php
	
	} elseif($post_format == 'video') {
	
		if(!get_post_meta($post->ID, 'video_site_url', true)) echo 'No video URL inserted!';
		 
		echo '<div class="video-containers single_item">'.wp_oembed_get(esc_url(get_post_meta($post->ID, 'video_site_url', true))).'</div>';
	}
	
	echo '</div>';

}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 		Post Views Count
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function crexis_setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

if( !function_exists( 'crexis_blog_post_nav' ) ) {
	function crexis_blog_post_nav() {
		echo '<div class="vntd-post-nav-holder"><div class="vntd-post-nav"><div class="post-nav-prev">' . get_previous_post_link() . '</div><div class="post-nav-next">' . get_next_post_link() . '</div></div></div>';
	}
}