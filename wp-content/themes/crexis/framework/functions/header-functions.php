<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//
// 		Header related functions
//
//		Q: Why place theme here instead of the functions.php file?
//		A: WordPress totally breaks if you make any accident changes
//		   to that file.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

//
// Site Logo
//

if(!function_exists('crexis_site_logo')) {
	function crexis_site_logo() {
	
		$logo_img = $logo_img_secondary = '';
		
		if(crexis_option("site_logo")) {
		
			$logo_img = $logo_img_secondary = crexis_option('site_logo', 'url');
		
			if(substr( crexis_header_style(), 0, 17 ) === "style-transparent" && crexis_option('site_logo_white') || crexis_option('header_skin') == 'dark' && crexis_option('site_logo_white') ) {
			
				$logo_img = crexis_option('site_logo_white', 'url');
				
			}
			
		}
		
		if(!$logo_img && !$logo_img_secondary) {
			$logo_img = $logo_img_secondary = get_template_directory_uri() . '/img/logo-dark.png';
		}
		
		$extra_class = '';
		
		if( crexis_option('header_color') != 'dark') {
		
			$extra_class = ' logo-primary';
			echo '<img src="' . esc_url( $logo_img_secondary ) . '" class="logo-secondary" alt="' . get_bloginfo() .'" />';
			
		} elseif( crexis_option('site_logo_white') ) {
			$logo_img_secondary = crexis_option('site_logo_white', 'url');
		}
		
		echo '<img src="' . esc_url( $logo_img ) . '" class="site-logo' . esc_attr( $extra_class ) . '" data-second-logo="' . esc_url( $logo_img_secondary ) . '" alt="' . get_bloginfo() .'" />';
		
		
		
	}
}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 		Mobile Navigation
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if(!function_exists('crexis_mobile_nav')) {
	function crexis_mobile_nav($button = NULL) {
		
		if($button) {
			echo '<div id="vntd-mobile-nav-toggle"><i class="fa fa-bars"></i></div>';
		} else { ?>
			<div id="mobile-navigation" class="vntd-container">
				<?php wp_nav_menu( array('theme_location' => 'primary' )); ?>
			</div>	
		<?php }
	
	}
}

if(!function_exists('veented_crexis_nav_wrap')) {

	function veented_crexis_nav_wrap( $style = null ) {
		// default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'
		
		if( $style == null ) $style = 'default';
		
		// open the <ul>, set 'menu_class' and 'menu_id' values
		$wrap  = '<ul id="%1$s" class="%2$s">';
		
		// get nav items as configured in /wp-admin/
		$wrap .= '%3$s';
		
		// Search
		
		if(crexis_option('header_search') && crexis_header_style() != 'style-transparent-hamburger' && $style != 'mobile-nav' || crexis_option('header_search') == '' && $style != 'mobile-nav') {
		
			$wrap .= '<li class="dropdown-toggle search-toggle"><a href="#" id="search-toggle" class="search"><i class="fa fa-search"></i></a>
				<!-- DropDown Menu -->
				<ul id="search-dropdown" class="dropdown-menu dropdown-search pull-right clearfix">
					<li class="raleway mini-text gray">
						<form method="GET" class="search-form" id="search-form" action="' . esc_url( home_url('/') ) . '">
							<input type="text" name="s" id="s" class="transparent uppercase" placeholder="' . esc_html__('Search...','crexis') .'">
							<button type="submit"><i class="fa fa-search"></i></button>
						</form>
					</li>
				</ul>
			</li>';
			
		}
				
		if(class_exists('Woocommerce') && crexis_option('header_woocommerce') != false && crexis_header_style() != 'style-transparent-hamburger' && !is_page_template('template-fullpage.php') && $style != 'mobile-nav') {
			$wrap .= crexis_woo_nav_cart();	
		}
	  
		// close the <ul>
		$wrap .= '</ul>';
		
		// return the result
		return $wrap;
		
	}
	
}

if(!function_exists('crexis_nav_menu')) {
	function crexis_nav_menu($style = null) {
		global $post;
		
		if( $style == null ) $style == 'default';
		
		if(is_page_template('template-fullpage.php')) {
		
			add_filter( 'nav_menu_link_attributes', 'crexis_custom_nav_attributes', 10, 3 );
			
		}
		
		if (has_nav_menu('primary')) {
		
			if(get_post_meta(get_the_ID(), 'page_nav_menu', true) && get_post_meta(get_the_ID(), 'page_nav_menu', true) != 'default') {
			
				wp_nav_menu( array(
					'menu' 			=> get_post_meta(get_the_ID(), 'page_nav_menu', true),
					'items_wrap' 	=> veented_crexis_nav_wrap( $style ),
					'container' 	=> false,
					'menu_class' 	=> 'nav uppercase normal',
					'walker' 		=> new crexis_Custom_Menu_Class()
				)); 
				
			} else {
			
				wp_nav_menu( array(
					'theme_location'	=> 'primary',
					'items_wrap' 		=> veented_crexis_nav_wrap( $style ),
					'container' 		=> false,
					'menu_class' 		=> 'nav uppercase normal',
					'walker' 			=> new crexis_Custom_Menu_Class()
				)); 
				
			}
			
		} else {
			echo '<span class="vntd-no-nav">No custom menu created!</span>';
		}
	}
}

if(!function_exists('crexis_custom_nav_attributes')) {

	function crexis_custom_nav_attributes ( $atts, $item, $args ) {
		
		$temp = $item->url;
		
		if(substr( $item->url, 0, 1 ) === "#") {
			$atts['data-getanchor'] = str_replace("#", "", $item->url);
		}
		
	    return $atts;
	}
	
}


if(!function_exists('crexis_header_extra_content')) {

	function crexis_header_extra_content() {
	
		echo '<div class="nav-extra-item nav-extra-item-text">';
		
		if(crexis_option('navbar_extra_type') == 'text') {
		
			echo do_shortcode(crexis_option('navbar_extra'));
			
		} elseif(crexis_option('navbar_extra_type') == 'search') {
		
			echo '<div class="nav-extra-search">';
			get_template_part('searchform');
			echo '</div>';
			
		} elseif(crexis_option('navbar_extra_type') == 'search-product') {
		
		}				
		
		
		
		echo '</div>';
	}
}

if(!function_exists('crexis_header_color')) {

	function crexis_header_color() {
		
		$header_color = ' white-nav';
		
		if(crexis_option('header_color') == 'dark') {
			$header_color = ' dark-nav';
		}
		
		return $header_color;
		
	}
	
}


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 		Breadcrumbs
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if(!function_exists('crexis_breadcrumbs')) {
function crexis_breadcrumbs() {

	global $post;

	if (!is_front_page()) {
	
        echo '<ul id="breadcrumbs" class="breadcrumbs page-title-side right f-right light">';
        echo '<li><a href="';
        echo esc_url(home_url('/'));
        echo '">';
        //bloginfo('name');
        echo 'Home</a></li>';
        
		$product = false;
		if(class_exists('Woocommerce')) {
			if(is_product()) $product = true;
		}
        if (is_category()){

		} elseif (is_single() && get_post_type($post->ID) == 'portfolio') { // Portfolio post type
		
			$portfolio_parent_id = get_post_meta($post->ID, 'home_button_link', true);
			
			if(!$portfolio_parent_id) {
				$portfolio_parent_id = crexis_option('portfolio_url');
			}

			if($portfolio_parent_id != '') {
				echo '<li><a href="'.get_permalink( $portfolio_parent_id ).'">'.get_the_title( $portfolio_parent_id ).'</a></li>';
			}
			
		} elseif (is_single() && !$product){
		
        	echo '<li>';
        	$frontpage_id = get_option('page_for_posts');
        	echo '<a href="'.get_permalink($frontpage_id).'">'.get_the_title($frontpage_id).'</a>';
            echo '</li>';
            
            
        }
        if (class_exists('Woocommerce')) {
	        if(is_woocommerce() || is_product() || is_shop() || is_cart() || is_checkout() || is_account_page()) {        
	        	echo '<li><a href="'.get_permalink(get_option('woocommerce_shop_page_id')).'" title="'.get_the_title(get_option('woocommerce_shop_page_id')).'">'.get_the_title(get_option('woocommerce_shop_page_id')).'</a></li>';        
	        }
	    }
	    
	    if(is_404()) {
	    	echo '<li>'.esc_html__('Page not found','crexis').'</li>';
	    }

        if (is_single()) {
            echo '<li>';
            
            if(strlen(get_the_title()) > 30) {
            	echo substr(get_the_title(),0,30).'...';
            } else {
            	echo get_the_title();
            }
            
            echo '</li>';
        }

        if (is_page()) {
        	$parent_id = $post->post_parent;
        	if($parent_id) {
        	
        		$parent_page = get_page($post->post_parent);
        		
        		if($parent_page->post_parent) {
        			echo '<li><a href="'.get_permalink($parent_page->post_parent).'" title="'.get_the_title($parent_page->post_parent).'">'.get_the_title($parent_page->post_parent).'</a></li>';
        	     }
        	     echo '<li><a href="'.get_permalink($parent_id).'" title="'.get_the_title($parent_id).'">'.get_the_title($parent_id).'</a></li>';
        	}
        	echo '<li>';
        	
            if(strlen(get_the_title()) > 30) {
            	echo substr(get_the_title(),0,30).'...';
            } else {
            	echo get_the_title();
            }
            
            echo '</li>';
        }
        
        if (is_tag()) {
        	echo '<li>'.esc_html__('Archives','crexis').'</li>';
        	echo '<li>'.esc_html__('Posts tagged by','crexis').' "';
            echo single_tag_title('', false);
            echo '"</li>';
        } elseif (is_category()) {
        	echo '<li>'.esc_html__('Archives','crexis').'</li>';
        	echo '<li>'.esc_html__('Posts by category','crexis').' "';
            echo single_cat_title('', false);
            echo '"</li>';
        } elseif (is_month() || is_day()) {
        	echo '<li>'.esc_html__('Archives','crexis').'</li>';
        	echo '<li>';
        	$date = 'F Y';
            the_time( $date );
            echo '</li>';
        } elseif (is_year()) {
        	echo '<li>'.esc_html__('Archives','crexis').'</li>';
        	echo '<li>';
           $date = 'Y';
           the_time( $date );
            echo '"</li>';
        } elseif (is_search()) {
            echo '<li>'.esc_html__('Search results for','crexis').' "'.get_search_query().'"</li>';
        }

        if (is_home()){
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ( $page_for_posts_id ) { 
                $post = get_page($page_for_posts_id);
                setup_postdata($post);
                echo '<li>';
                the_title();
                echo '</li>';
                rewind_posts();
            }
        }

        echo '</ul>';
    }
}
}

if(!function_exists('crexis_logo_url')) {
function crexis_logo_url() {
	if(is_front_page()) {
		echo '#home';
	} else {
		echo home_url();
	}
}
}

//
// Page Title Function
//

if(!function_exists('crexis_print_page_title')) {
function crexis_print_page_title() {

	global $post;
	
	$page_id = 1;
	
	if(get_post_type() == 'services' || get_post_type() == 'testimonials') {
		return false;
	}	
	
	if(is_object($post)) {
		$page_id = $post->ID;
	} else {
		$page_id = get_the_ID();
	}
	
	$page_title = get_the_title($page_id);
	
	if(is_search()) {
		$page_title = esc_html__('Search','crexis');
	} elseif(is_404()) {
		$page_title = esc_html__('Page not found','crexis');
	} elseif(is_archive()) {
		$page_title = esc_html__('Archives','crexis');
	} elseif(is_single() && get_post_type() == 'portfolio') {
	
		$page_title = esc_html__('Portfolio','crexis');
		
		$portfolio_parent_id = get_post_meta($post->ID, 'home_button_link', true);
					
		if(!$portfolio_parent_id) {
			$portfolio_parent_id = crexis_option('portfolio_url');
		}

		if($portfolio_parent_id != '') {
			
			$page_title = get_the_title( $portfolio_parent_id );
			
		}
		
		
	} elseif(is_single() && get_post_type() == 'post') {
		$page_title = esc_html__('Blog','crexis');
	} elseif( is_home() ) {
		$page_title = esc_html__('Blog','crexis');
	}
	
	if (class_exists('Woocommerce')) {
	
		global $wp_query;
		
		if(is_shop()) {
			$page_title = esc_html__('Shop','crexis');
		}elseif(is_product_category()) {
			$cat = $wp_query->get_queried_object();
			$page_title = $cat->name;
		}elseif(is_product_tag()) {
			$cat = $wp_query->get_queried_object();
			$page_title = $cat->name;
		}
	}
	
	$extra_class = $overlay_class = '';
	
	$height_class = 'little-header';
	
	if(get_post_meta(crexis_get_id(), 'customize_enable', TRUE) == 'yes') {
		
		echo '<style type="text/css"> #page-content #page-header {';
		
		if(get_post_meta(crexis_get_id(), 'customize_bgcolor', TRUE)) {
			$attr = '-color';
			if(!get_post_meta(crexis_get_id(), 'customize_bgimage', TRUE)) $attr = '';
			echo 'background' . $attr . ': '.esc_attr(get_post_meta(crexis_get_id(), 'customize_bgcolor', TRUE)) . ';';
		} 
		
		if(get_post_meta(crexis_get_id(), 'customize_bgimage', TRUE)) {
			$imgurl = wp_get_attachment_image_src( get_post_meta(crexis_get_id(), 'customize_bgimage', TRUE), 'crexis-bg-image');			
			echo 'background-image: url(' . esc_url($imgurl[0]) . ') !important;';
			if(get_post_meta(crexis_get_id(), 'customize_bg_align_y', TRUE) == 'top' || get_post_meta(crexis_get_id(), 'customize_bg_align_y', TRUE) == 'bottom') {
				echo 'background-position-y: ' . esc_attr( get_post_meta(crexis_get_id(), 'customize_bg_align_y', TRUE) ) . ';';
			}
		}
		 
		
		echo '}';
		
		// Font Weight
		
		if(get_post_meta(crexis_get_id(), 'customize_fontweight', TRUE) == 700 || get_post_meta(crexis_get_id(), 'customize_fontweight', TRUE) == 800 || get_post_meta(crexis_get_id(), 'customize_fontweight', TRUE) == 'bold') {
			echo '#page-header h1 { font-weight:'.esc_attr(get_post_meta(crexis_get_id(), 'customize_fontweight', TRUE)).'; }';			
		}
		
		// Font Size
		
		if(get_post_meta(crexis_get_id(), 'customize_fontsize', TRUE) != 30) {
			echo '#page-header h1 { font-size:'.esc_attr(get_post_meta(crexis_get_id(), 'customize_fontsize', TRUE)).'px; }';			
		}
		
		// Font Color
		
		if(get_post_meta(crexis_get_id(), 'customize_textcolor', TRUE) == 'white') {
			echo '#page-header h2, #breadcrumbs li { color: #fff; }';	
			echo '#page-header h5, #page-header #breadcrumbs a { color: rgba(255,255,255,0.75); }';
		}
		
		// Text Transform
		
//		if(get_post_meta(crexis_get_id(), 'customize_texttransform', TRUE) == "uppercase") {
//			echo '#page-header h1, #breadcrumbs { text-transform:uppercase; }';			
//		}
		
		// Height
		
//		if(get_post_meta(crexis_get_id(), 'customize_height', TRUE) != '80') {
//			$half = get_post_meta(crexis_get_id(), 'customize_height', TRUE)/2-28;
//			echo '#page-header h1 { line-height:'.esc_attr(get_post_meta(crexis_get_id(), 'customize_height', TRUE)).'px; } #breadcrumbs { margin-bottom: '.$half.'px; }';			
//		}
		
		// Breadcrumbs
		
		if(get_post_meta(crexis_get_id(), 'customize_breadcrumbs', TRUE) == "disabled") {
			echo '#breadcrumbs { display:none; }';			
		}		
		
		echo '</style>';
		
		$extra_class = ' page-title-animated';
		
		$overlay_class = '';
		if(get_post_meta(crexis_get_id(), 'customize_bgoverlay', TRUE) != 'none' && get_post_meta(crexis_get_id(), 'customize_bgoverlay', TRUE)) {
			$overlay_class = 'bg-overlay-' . esc_attr(get_post_meta(crexis_get_id(), 'customize_bgoverlay', TRUE));
		}
		
		if(get_post_meta(crexis_get_id(), 'customize_height', TRUE) == 'big') {
			$height_class = 'big-header';
		}
		
	}
	
	$parallax = $parallax_class = 'yes';
	
	if( !get_post_meta(crexis_get_id(), 'customize_parallax', TRUE) || get_post_meta(crexis_get_id(), 'customize_parallax', TRUE) == 'default' ) {
		$parallax = crexis_option( 'pagetitle_parallax' );
		
	} else {
		$parallax = get_post_meta(crexis_get_id(), 'customize_parallax', TRUE);
	}
	
	
	if( $parallax == 'yes') {
		wp_enqueue_script('vntd-parallax', '', '', '', true);
		$parallax_class = 'parallax3';
	}
	
	?>
	
	<!-- Page Header - litle-header or bigger-header - soft-header, dark-header or background -->
	<section id="page-header" class="soft-header <?php echo esc_attr( $height_class ) . ' ' . esc_attr( $parallax_class ) . ' page-title-align-' . get_post_meta(crexis_get_id(), 'customize_textalign', TRUE); ?>">
	
		<?php 
		
		if($overlay_class != '') echo '<div class="bg-overlay ' . $overlay_class . '"></div>';
		
		if( get_post_meta( $page_id, 'page_title', TRUE ) && get_post_meta($page_id,'page_title',TRUE) != '' && !is_home() ) {
			$page_title = get_post_meta( $page_id, 'page_title', TRUE );
		}
		
		?>
		<!-- Page Header Inner -->
		<div class="page_header_inner clearfix dark">
			<!-- Left -->
			<div class="left f-left">
				<!-- Header -->
				<h2 class="page_header light"><?php echo esc_textarea($page_title); ?></h2>
				
				<?php
				
				if(get_post_meta($page_id,'page_subtitle',TRUE)) {
					$page_tagline_wrap = '<h5 class="page_note light">' . esc_html( get_post_meta($page_id,'page_subtitle',TRUE) ) . '</h5>';
				}
				
				?>
			</div>
			
			<?php
			if(crexis_option('breadcrumbs') || crexis_option('breadcrumbs') == '') {
			
				crexis_breadcrumbs();
				
			}
			?>
		</div>
		<!-- End Inner -->
	</section>
	<!-- End #page-header -->
	
	<?php
	
}
}

//
// Top Bar
//

if(!function_exists('crexis_print_topbar')) {
function crexis_print_topbar() {

$topbar_skin = 'light';

if(crexis_option('topbar_skin')) {
	$topbar_skin = crexis_option('topbar_skin');
}

$topbar_class = 'white-pagetop';

if( crexis_header_style() == 'style-transparent' ) {
	$topbar_class = 'transparent-pagetop';
}

?>
<div id="pagetop" class="<?php echo esc_attr( $topbar_class ); ?>">
	<!-- Inner -->
	<div class="pagetop_inner clearfix">
		<!-- Left Text -->
		<div class="f-left texts">
			<!-- Text -->
			<?php crexis_topbar_content('left'); ?>
		</div>
		<!-- Socials -->
		<div class="f-right socials">
		
			<?php crexis_topbar_content('right'); ?>

		</div>
		<!-- End Socials -->
	</div>
	<!-- End Inner -->
</div>
<?php
}
}

if(!function_exists('crexis_topbar_content')) {
function crexis_topbar_content($side) {
	
	$type = crexis_option('topbar_'.$side);
	
	$top_bar_text = '';
	
	$icon_style = 'font_awesome';
	if(crexis_option('social_icons_style') != 'font_awesome') $icon_style = crexis_option('social_icons_style');
	
	if($icon_style != 'font_awesome') {
		$top_bar_text = str_replace("[icon icon", '[icon icon_style="simple-line" icon',crexis_option('topbar_text_'.$side));
	} else {
		$top_bar_text = crexis_option('topbar_text_'.$side);
	}	
	
	$bar_text = '<span class="topbar-text">'.do_shortcode($top_bar_text).'</span>';
	
	// If more than 1 WPML language, display switcher
	
	if(function_exists('icl_get_languages') && sizeof(icl_get_languages('skip_missing=0')) > 1 && $side == 'right' && crexis_option('topbar_wpml')) {
		crexis_topbar_langs();
	}
	
	// Switch content type
		
	if($type == 'social') {
	
		echo '<div class="topbar-section topbar-social">';
		
		crexis_print_social_icons();
		
		echo '</div>';
	
	} elseif($type == 'Menu') {
	
		echo '<div class="topbar-section topbar-menu">';
	
		wp_nav_menu(array('theme_location' => 'topbar'));
		
		echo '</div>';
	
	} elseif($type == 'textsocial') {
	
		echo '<p class="topbar-section topbar-text topbar-text-socials icons-'.$icon_style.'">'.$bar_text.'</p>';
		echo '<div class="topbar-section topbar-social">';
		
		crexis_print_social_icons();
		
		echo '</div>';
		
	} else {
		echo '<p>'.$bar_text.'</p>';	
	}
	

}
}

if(!function_exists('crexis_print_big_search')) {
function crexis_print_big_search() {
	?>
	<div class="header-big-search">
		<form class="search-form relative" id="search-form" action="<?php echo esc_url(home_url('/')); ?>/">
			<input name="s" id="s" type="text" value="" placeholder="<?php esc_html_e('Type and hit Enter..','crexis') ?>" class="search">
			<div class="header-search-close accent-hover-color"><i class="fa fa-close"></i></div>
		</form>	
	</div>
	<?php
}
}

if(!function_exists('crexis_header_style')) {
	function crexis_header_style() {
		global $post;
		
		$style = 'style-default';
		
		$style = crexis_option('header_style');
		
		if(!is_search() && !is_archive() && !is_tag()) {
			if(get_post_meta(crexis_get_id(),'navbar_style',TRUE) && get_post_meta(crexis_get_id(),'navbar_style',TRUE) != $style && get_post_meta(crexis_get_id(),'navbar_style',TRUE) != 'default') {
				$style = get_post_meta(crexis_get_id(),'navbar_style',TRUE);
			}	
		} elseif(is_search() || is_archive() || is_tag()) {
			$style = 'style-default';
		}
		
		return $style;
	}
}