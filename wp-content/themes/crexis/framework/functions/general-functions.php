<?php

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//
// 		Theme Functions
//
//		Q: Why place theme here instead of the functions.php file?
//		A: WordPress totally breaks if you make any accident changes
//		   to that file.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Image cropping functions
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if(!function_exists("crexis_thumb")) {
	function crexis_thumb($w,$h = null){
	
		get_template_part( 'includes/aq_resizer' );
		
		global $post;
		$imgurl = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');
		return $imgurl[0];
		return aq_resize($imgurl[0],$w,$h,true);
	}
}

if(!function_exists("crexis_crop")) {
	function crexis_crop($id,$w,$h = null){
	
		get_template_part( 'includes/aq_resizer' );
		
		$imgurl = wp_get_attachment_image_src($id, 'full');
		
		$return = aq_resize($imgurl[0],$w,$h,true);
		
		if( $return == null || $return == '') {
			$return = $imgurl;
		} 
		
		return $return;
		
	}
}

if(!function_exists("crexis_content_class")) {
	function crexis_content_class() {
	
		global $post;
	
		$return = '';
		
		$header_style = 'style-default';
		if( crexis_header_style() ) $header_style = crexis_header_style();
		
		if(crexis_option('topbar') && $header_style != 'style-boxed' && $header_style != 'style-transparent') {
			$return .= ' page-with-topbar';
		}
		
		if(crexis_vc_active()) {
			$return .= ' page-with-vc';
		} else {
			$return .= ' page-without-vc';
		}
		
		echo 'class="header-' . $header_style . $return . '"';
		
	}
}




// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 		Pagination
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


function crexis_pagination($the_query = NULL)
{  

	global $wp_query,$paged;
	
	$query = '';
	
	if(!$the_query) {
		$query = $wp_query;
	} else {
		$query = $the_query;
	}
	
	$big = 999999999; // need an unlikely integer
    $pages = paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
             'format' => ( ( get_option( 'permalink_structure' ) && ! $query->is_search ) || ( is_home() && get_option( 'show_on_front' ) !== 'page' && ! get_option( 'page_on_front' ) ) ) ? '?paged=%#%' : '&paged=%#%', // %#% will be replaced with page number	
            'current' => max( 1, get_query_var('paged') ),
            'total' => $query->max_num_pages,
            'prev_next' => false,
            'type'  => 'array',
            'prev_next'   => TRUE,
			'prev_text' => '<i class="fa fa-angle-left"></i>'.esc_html__('Prev','crexis'),
			'next_text' => esc_html__('Next','crexis').'<i class="fa fa-angle-right"></i>'
        ) );
    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<div class="col-md-12 blog-pagination pagination block t-center"><ul class="vntd-pagination pagination">';
        foreach ( $pages as $page ) {
                echo "<li>".$page."</li>";
        }
       echo '</ul></div>';
    }
        
        
	
}

// Pretty Permalinks Fix for Custom Post Types

add_action('init', 'crexis_custom_rewrite_basic');
function crexis_custom_rewrite_basic() {
    global $wp_post_types;
    foreach ($wp_post_types as $wp_post_type) {
        if ($wp_post_type->_builtin) continue;
        if (!$wp_post_type->has_archive && isset($wp_post_type->rewrite) && isset($wp_post_type->rewrite['with_front']) && !$wp_post_type->rewrite['with_front']) {
            $slug = (isset($wp_post_type->rewrite['slug']) ? $wp_post_type->rewrite['slug'] : $wp_post_type->name);
            $page = crexis_get_page_by_slug($slug);
            if ($page) add_rewrite_rule('^' .$slug .'/page/([0-9]+)/?', 'index.php?page_id=' .$page->ID .'&paged=$matches[1]', 'top');
        }
    }
}

function crexis_get_page_by_slug($page_slug, $output = OBJECT, $post_type = 'page' ) {
    global $wpdb;

    $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $page_slug, $post_type ) );

    return ($page ? get_post($page, $output) : NULL);
}

// Fix End

function crexis_ajax_pagination($query = null, $name = null) {
	global $wp_query;

	// Add code to index pages.
	
	wp_enqueue_script('ajax-load-posts', get_template_directory_uri() . '/js/jquery.load-posts.js', array('jquery'));
	
	if(!$query) $query = $wp_query;
	
	
	// What page are we on? And what is the pages limit?
	$max = $query->max_num_pages;
	$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
	
	if($name == null) $name = 'portfolio';
	
	// Add some parameters for the JS.
	wp_localize_script(
		'ajax-load-posts',
		'pbd_alp_' . $name,
		array(
			'startPage' => $paged,
			'maxPages' => $max,
			'nextLink' => next_posts($max, false)
		)
	);

}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Custom Excerpt Size
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_custom_excerpt_length( $length ) {
	return 50; // Increase maximum excerpt size
}
add_filter( 'excerpt_length', 'crexis_custom_excerpt_length', 999 );


if(!function_exists('crexis_excerpt')) {
	function crexis_excerpt($limit, $more = NULL) {
	
		global $post;
		
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if (count($excerpt)>=$limit) {
			array_pop($excerpt);
			$excerpt = implode(" ",$excerpt).'...';
		} else {
			$excerpt = implode(" ",$excerpt);
		}
		
		$excerpt = '<p>'.preg_replace('`[[^]]*]`','',$excerpt).'</p>';
		
		if(get_post_format($post->ID) == "link") {
			$excerpt .= '<a href="'.esc_url(get_post_meta($post->ID,"post_link",TRUE)).'" class="ex-link post-more uppercase light st">'.esc_html__('Visit Site','crexis').'</a>';
		}elseif($more) {
			$excerpt .= '<a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="ex-link post-more uppercase light st">' . esc_html__('Read more','crexis') . '</a>';
		}
		
		return $excerpt;
	}
}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		Post Gallery
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

function crexis_post_gallery($type,$thumb_size) {
		
	global $post;

	$gallery_images = get_post_meta($post->ID, 'gallery_images', true);
	
	if(!$gallery_images && has_post_thumbnail()) { // No Gallery Images	
		$url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $thumb_size);
		return '<img src="'.$url[0].'" alt="'.get_the_title($post->ID).'">';
	}
	
	echo '<div class="vntd-post-gallery vntd-post-gallery-'.$type.'">';	
				
	if($type == "slider") { // Slider Gallery
	
		wp_enqueue_script('vntd-flexslider', '', '', '', true);
		
		echo '<div class="flexslider vntd-flexslider"><ul class="slides">';
					
		$ids = explode(",", $gallery_images);				
		foreach($ids as $id){
			$image_url = wp_get_attachment_image_src($id, $thumb_size);
			echo '<li><img src="'.esc_url($image_url[0]).'" alt></li>';
		}
							
		echo '</ul></div>';				
		
	} elseif($type == "list" || $type == "list_lightbox") {
		
		$ids = explode(",", $gallery_images);				
		foreach($ids as $id){
			//global $post = $post=>$id;
			$image_url = wp_get_attachment_image_src($id, $thumb_size);
			$big_url = wp_get_attachment_image_src($id, 'fullwidth-auto');
			echo '<div class="vntd-gallery-item">';
			if($type == "list_lightbox") echo '<a href="'.esc_url($big_url[0]).'" class="hover-item" rel="gallery[gallery'.$post->ID.']" title="'.get_post($id)->post_excerpt.'"><span class="hover-overlay"></span><span class="hover-icon hover-icon-zoom"></span>';			
			echo '<img src="'.esc_url($image_url[0]).'" alt>';
			if($type == "list_lightbox") echo '</a>';
			echo '</div>';
		}	
	
	} else {	
		// If Lightbox Gallery
		echo '<div class="featured-image-holder"><div class="gallery clearfix">';
		
		$ids = explode(",", $gallery_images);
		if($gallery_images) $id = array_shift(array_values($ids));
		$image_url = wp_get_attachment_image_src($id, $thumb_size);
		$large_url = wp_get_attachment_image_src($id, 'large');
		echo '<a class="hover-item" href="'.esc_url($large_url[0]).'" rel="gallery[gallery'.$post->ID.']"><img src="'.esc_url($image_url[0]).'"><span class="hover-overlay"></span><span class="hover-icon hover-icon-zoom"></span></a>';
			
			if($gallery_images){
			
				echo '<div class="lightbox-hidden">';								
				foreach(array_slice($ids,1) as $id){
					echo '<a href="'.wp_get_attachment_url($id).'" rel="gallery[gallery'. $post->ID .']"></a>';
				}
				echo '</div>';
							
			}
								
		echo '</div></div>';
		
	}
	
	echo '</div>';
}


if(!function_exists('crexis_gallery_metabox')) {
	function crexis_gallery_metabox($gallery_images) {
	
		$modal_update_href = esc_url( add_query_arg( array(
		     'page' => 'shiba_gallery',
		     '_wpnonce' => wp_create_nonce('shiba_gallery_options'),
		 ), admin_url('upload.php') ) );
		 ?>	          
		
		 <div class="vntd-gallery-thumbs">
		 	<?php
		 	
	 		if($gallery_images){
	 		
	 			$ids = explode(",", $gallery_images);
	 			
	 			foreach($ids as $id){
	 				echo '<img src="'.wp_get_attachment_thumb_url($id).'" alt>';
	 			}
	 		
	 		}
		 	
		 	?>
		 </div>
		 
		 <input type="text" class="hidden" id="gallery_images" name="gallery_images" value="<?php echo esc_textarea($gallery_images); ?>">
		 <?php if($gallery_images) { $button_text = esc_html__("Modify Gallery", 'crexis'); } else { $button_text = esc_html__("Create Gallery", 'crexis'); } ?>
		 <a id="vntd-gallery-add" class="button" href="#"
		     data-update-link="<?php echo esc_attr( $modal_update_href ); ?>"
		     data-choose="<?php esc_html_e('Choose a Default Image', 'crexis'); ?>"
		     data-update="<?php esc_html_e('Set as default image', 'crexis'); ?>"><?php echo esc_textarea($button_text); ?>
		 </a>
		 <?php if($gallery_images){ ?><span class="vntd-gallery-or">
		 <?php esc_html_e('or', 'crexis') ?> </span><input type="button" id="vntd-gallery-remove" class="button" value="Remove Gallery">
		 
		 
		 <?php
		 }
		 // Add to the top of our data-update-link page
		 if (isset($_REQUEST['file'])) { 
		     check_admin_referer("shiba_gallery_options");
		  
		         // Process and save the image id
		     $options = get_option('shiba_gallery_options', TRUE);
		     $options['default_image'] = absint($_REQUEST['file']);
		     update_option('shiba_gallery_options', $options);
		 
		}
	
	}
}

function crexis_get_id() {

	global $post;
	
	$post_id = '';
	
	if(is_object($post)) {
		$post_id = $post->ID;
	}
	if(is_home()) {
		$post_id = get_option('page_for_posts');
	}
	
	return $post_id;
}

if(!function_exists('crexis_fonts')) {
	function crexis_fonts() {
		
		$font_body = 'Open Sans';	
		$font_primary = 'Raleway';	
		$font_secondary = 'Raleway';	
		$font_weight = $nav_font_weight = '';
		
		// Read Font Families from Options Panel
		
		if(crexis_option("typography_body", "font-family") && crexis_option("typography_body", "font-family") != $font_body) {
			$font_body = crexis_option("typography_body", "font-family");
		}
	
		if(crexis_option("typography_primary", "font-family") && crexis_option("typography_primary", "font-family") != $font_primary) {
			$font_primary = crexis_option("typography_primary", "font-family");
		}
		
		if(crexis_option("typography_secondary", "font-family") && crexis_option("typography_secondary", "font-family") != $font_primary) {
			$font_secondary = crexis_option("typography_secondary", "font-family");
		}
	
		// Heading font weight
		
		$font_primary_weight = ':100,300,400,500,600,700'; // Each weight is required at some point
		
		if( $font_primary == 'Georgia, serif' ) {
			$font_primary = 'Georgia';
		}
		
		if( $font_body == 'Georgia, serif' ) {
			$font_body = 'Georgia';
		}
		
		// Load Fonts
		
		if( $font_primary != 'Georgia' && strpos($font_primary, ',') === false ) {
			wp_enqueue_style('vntd-google-font-primary', '//fonts.googleapis.com/css?family='.str_replace(' ','+',$font_primary).$font_primary_weight);	
		}
		
		if($font_body != $font_primary && strpos($font_body, ',') === false ) { // If same font is used, there is no point to load it twice
		
			// Body font weight
			
			$font_body_weight = ':300,400';
			
			if(crexis_option("typography_body", "font-weight") && crexis_option("typography_body", "font-weight") != '400') {
				$font_body_weight = ':'.crexis_option("typography_body", "font-weight");
			}
			
			// Load body font
		
			wp_enqueue_style('vntd-google-font-body', '//fonts.googleapis.com/css?family='.str_replace(' ','+',$font_body).$font_body_weight);
		}	
		
		if($font_secondary != $font_primary && strpos($font_secondary, ',') === false ) { // If same font is used, there is no point to load it twice
				
			// Body font weight
			
			$font_secondary_weight = ':100,300,400';
			
			// Load body font
		
			wp_enqueue_style('vntd-google-font-secondary', '//fonts.googleapis.com/css?family='.str_replace(' ','+',$font_secondary).$font_secondary_weight);
		}	
	
	}
	add_action('wp_enqueue_scripts', 'crexis_fonts');
	add_action( 'admin_enqueue_scripts', 'crexis_fonts');
}

if(!function_exists('crexis_get_primary_font')) {
	function crexis_get_primary_font() {
		
		$font_primary = 'Raleway';	
		
		if(crexis_option("typography_primary", "font-family") && crexis_option("typography_primary", "font-family") != $font_primary) {
			$font_primary = crexis_option("typography_primary", "font-family");
		}
		
		return $font_primary;
	}
}

if(!function_exists('crexis_print_social_icons')) {
	function crexis_print_social_icons($style = NULL) {
	
		global $smof_data;
		$target = '';
		
		if(!$style) $style = 'classic';
		
		$icon_style = 'fa fa-';
		
		$social_icons = crexis_option("social_icons");
		if(!$social_icons) {
			$social_icons = array(
				"facebook" 	=> "You have no icons",
				"twitter" 	=> "You have no icons",
				"dropbox" 	=> "You have no icons",
				"vimeo" 	=> "You have no icons",
				"dribbble" 	=> "You have no icons"
			);
		}
		if($social_icons) {
		
			echo '<div class="vntd-social-icons social-icons-'.esc_attr($style).' social-icons-'.esc_attr(crexis_option('social_icons_style')).'">';
			
			$target = ' target="_blank"';
			
			foreach($social_icons as $social_icon => $value) {

				if($value != "") {
					echo '<a class="social social-'.strtolower($social_icon).' '.strtolower($social_icon).'" href="'.esc_url($value).'"'.$target.'><i class="'.$icon_style.strtolower($social_icon).'"></i></a>';
				}
				
			}
			
			echo '</div>';
		}
	}
}

// Footer Widgets related functions

function crexis_get_footer_cols() {
	
	if(is_active_sidebar('footer1') && is_active_sidebar('footer2') && is_active_sidebar('footer3') && is_active_sidebar('footer4')) {
		return 4;
	} elseif(is_active_sidebar('footer1') && is_active_sidebar('footer2') && is_active_sidebar('footer3')) {
		return 3;
	} elseif(is_active_sidebar('footer1') && is_active_sidebar('footer2')) {
		return 2;
	} else {
		return 1;
	}
	
	return 0;
}

function crexis_get_footer_cols_class() {

	$widget_col_class = 'col-md-3';
	
	if(crexis_get_footer_cols() == 1) {
		$widget_col_class = 'col-md-12';
	} elseif(crexis_get_footer_cols() == 2) {
		$widget_col_class = 'col-md-6';
	} elseif(crexis_get_footer_cols() == 3) {
		$widget_col_class = 'col-md-4';
	}
	
	return $widget_col_class;
}

function crexis_get_footer_widgets_class() {
	
	global $smof_data;
	
	if($smof_data['crexis_footer_widgets_skin'] == 'dark') {
		return 'footer-widgets-dark';
	} elseif($smof_data['crexis_footer_widgets_skin'] == 'night') { 
		return 'footer-widgets-night';
	} elseif($smof_data['crexis_footer_widgets_skin'] == 'dark') {
		return 'footer-widgets-white';
	} else {
		
	}
	
	return 'footer-widgets-white';
	
}

function crexis_vc_active() { // Function to check if Visual Composer is enabled on a specific page.

	global $post;
	
	$found = false;
	
	if(is_object($post)) {
		$post_to_check = get_post($post->ID);
	} else {
		return $found;
	}
	
	     
	// check the post content for the short code
	if ( stripos($post_to_check->post_content, '[vc_row') !== false ) {
	    // we have found the short code
	    $found = true;
	}
	
	if(is_home()) {
		$found = false;
	}
	 
	// return our final results
	return $found;

}

// Importer


if ( !function_exists( 'crexis_create_dropdown' ) ) {
	function crexis_create_dropdown($name,$elements,$current_value,$folds = NULL) {
		
		$folds_class = $selected = '';
		if($folds) $folds_class = ' folds';
		echo '<select name="'.$name.'" class="select'.$folds_class.'">';
		
		if(crexis_isAssoc($elements)) {
		
			foreach($elements as $title => $key) {		
				
				if($key == $current_value) $selected = 'selected';
				
				echo '<option value="'.$key.'"'.$selected.'>'.$title.'</option>';
				
				$selected = '';
			}
			
		} else {
			
			foreach($elements as $key) {			
				
				if($key == $current_value) $selected = 'selected';
				
				echo '<option value="'.$key.'"'.$selected.'>'.$key.'</option>';
				
				$selected = '';
			}
			
		}
		
		echo '</select>';
		
	}
}

if ( !function_exists( 'crexis_pages_dropdown' ) ) {
	function crexis_pages_dropdown($name,$current_value) {
		echo '<select name="'.$name.'" class="select">';
			echo '<option>Select page:</option>';
			$pages = get_pages(); 
			$selected = '';
			foreach ( $pages as $page ) {						
				if($page->ID == $current_value) { $selected = 'selected="selected"'; }	
				echo '<option value="'.$page->ID.'" '.$selected.'>'.esc_textarea($page->post_title).'</option>';
				$selected = '';
			}
		
		echo '</select>';
	}
}

if ( !function_exists( 'crexis_isAssoc' ) ) {
	function crexis_isAssoc($arr)
	{
	    return array_keys($arr) !== range(0, count($arr) - 1);
	}
}

if ( !function_exists('crexis_string_between') ) {
	function crexis_string_between($string, $start, $end){
	    $string = ' ' . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return '';
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;
	    return substr($string, $ini, $len);
	}
}

if ( !function_exists('crexis_query_pagination') ) {
	function crexis_query_pagination() {
	
		$paged = '';
		
		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
		elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
		else { $paged = 1; }
		
		return $paged;
		
	}
}

if( !function_exists('crexis_column_items') ) {
	
	function crexis_column_items($cols) {
	
		$return = 'three-items';
		
		if($cols == 1) {
			$return = 'one-item';
		} elseif($cols == 2) {
			$return = 'two-items';
		} elseif($cols == 4) {
			$return = 'four-items';
		} elseif($cols == 5) {
			$return = 'five-items';
		} elseif($cols == 6) {
			$return = 'six-items';
		}
		
		return $return;
		
	}
	
}

if ( function_exists('crexis_custom_js_code') ) {
	add_action('wp_footer', 'crexis_custom_js_code');
}

function crexis_custom_js_code() {
	
	if( crexis_option('custom_js') ) {
		echo '<script type="text/javascript">' . crexis_option('custom_js') . '</script>';
	}
	
}

// Google Fonts Update

function getVariants( $var ) {
    $result = array();
    $italic = array();

    foreach ( $var as $v ) {
        $name = "";
        if ( $v[0] == 1 ) {
            $name = 'Ultra-Light 100';
        } else if ( $v[0] == 2 ) {
            $name = 'Light 200';
        } else if ( $v[0] == 3 ) {
            $name = 'Book 300';
        } else if ( $v[0] == 4 || $v[0] == "r" || $v[0] == "i" ) {
            $name = 'Normal 400';
        } else if ( $v[0] == 5 ) {
            $name = 'Medium 500';
        } else if ( $v[0] == 6 ) {
            $name = 'Semi-Bold 600';
        } else if ( $v[0] == 7 ) {
            $name = 'Bold 700';
        } else if ( $v[0] == 8 ) {
            $name = 'Extra-Bold 800';
        } else if ( $v[0] == 9 ) {
            $name = 'Ultra-Bold 900';
        }

        if ( $v == "regular" ) {
            $v = "400";
        }

        if ( strpos( $v, "italic" ) || $v == "italic" ) {
            $name .= " Italic";
            $name = trim( $name );
            if ( $v == "italic" ) {
                $v = "400italic";
            }
            $italic[] = array(
                'id'   => $v,
                'name' => $name
            );
        } else {
            $result[] = array(
                'id'   => $v,
                'name' => $name
            );
        }
    }

    foreach ( $italic as $item ) {
        $result[] = $item;
    }

    return array_filter( $result );
}   //function

function getSubsets( $var ) {
    $result = array();

    foreach ( $var as $v ) {
        if ( strpos( $v, "-ext" ) ) {
            $name = ucfirst( str_replace( "-ext", " Extended", $v ) );
        } else {
            $name = ucfirst( $v );
        }

        array_push( $result, array(
            'id'   => $v,
            'name' => $name
        ) );
    }

    return array_filter( $result );
}

if( !function_exists('crexis_body_classes') ) {
	function crexis_body_classes() {
		
		return crexis_header_color();
		
	}
}