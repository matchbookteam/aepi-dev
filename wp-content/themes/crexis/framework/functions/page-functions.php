<?php
//
// Blog Post Settings
//


add_action("admin_init", "crexis_page_metaboxes");   

function crexis_page_metaboxes(){    
    add_meta_box("crexis_page_settings", "Page Settings", "crexis_page_settings_config", "page", "side", "low");
    add_meta_box("crexis_page_settings", "Page Settings", "crexis_page_settings_config", "post", "side", "low");
    add_meta_box("crexis_page_settings", "Page Settings", "crexis_page_settings_config", "portfolio", "side", "low");
    
    add_meta_box("crexis_page_settings_advanced", "Advanced Page Settings", "crexis_page_settings_advanced_config", "page", "side", "low", true, true);
    add_meta_box("crexis_page_settings_advanced", "Advanced Page Settings", "crexis_page_settings_advanced_config", "post", "side", "low");
    add_meta_box("crexis_page_settings_advanced", "Advanced Page Settings", "crexis_page_settings_advanced_config", "portfolio", "side", "low");
   
    add_meta_box("crexis_page_custom_pagetitle", "Customize Page Title", "crexis_pagetitle_config", "page", "normal", "low");
    add_meta_box("crexis_page_custom_pagetitle", "Customize Page Title", "crexis_pagetitle_config", "post", "normal", "low");
    add_meta_box("crexis_page_custom_pagetitle", "Customize Page Title", "crexis_pagetitle_config", "portfolio", "normal", "low");
}   

function crexis_page_settings_config() {
        global $post;	
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $footer_widgets = $page_header = $page_subtitle = $navbar_style = $navbar_color = $page_layout = $page_sidebar = $page_width = $footer_color = $footer_widgets = $page_title = '';
        if(array_key_exists("page_header", $custom)) {
			$page_header = $custom["page_header"][0];
		}
		if(array_key_exists("page_title", $custom)) {
			$page_title = $custom["page_title"][0];
		}
		if(array_key_exists("page_subtitle", $custom)) {
			$page_subtitle = $custom["page_subtitle"][0];
		}
		if(array_key_exists("navbar_style", $custom)) {
			$navbar_style = $custom["navbar_style"][0];
		}
		if(array_key_exists("navbar_color", $custom)) {
			$navbar_color = $custom["navbar_color"][0];
		}
		if(array_key_exists("page_layout", $custom)) {
			$page_layout = $custom["page_layout"][0];	
		}
		if(array_key_exists("page_sidebar", $custom)) {
			$page_sidebar = $custom["page_sidebar"][0];
		}
		if(array_key_exists("page_width", $custom)) {
			$page_width = $custom["page_width"][0];
		}
		if(array_key_exists("footer_color", $custom)) {
			$footer_color = $custom["footer_color"][0];
		}
		if(array_key_exists("footer_widgets", $custom)) {
			$footer_widgets = $custom["footer_widgets"][0];
		}
?>
    <div class="metabox-options form-table side-options">
  		
		<div id="page-header" class="label-radios">  		
			<h5><?php esc_html_e('Page Title','crexis'); ?>:</h5>
    	    <?php
    	    $headers = array(
    	    	'Enabled' => "default",
    	    	'No Page Title' => 'no-header'
    	    );
    	    
    	    crexis_create_dropdown('page_header',$headers,$page_header);
    	    
    	    ?>
    	    
    	</div>
    	
    	<div id="vntd_page_header_default" <?php if($page_header == "no-header") { echo 'class="hidden"'; } ?>>
    		<h5><?php _e('Custom Page Title','crexis'); ?>:</h5>
    		<input type="text" name="page_title" class="fullwidth-input" value="<?php echo esc_textarea( $page_title ); ?>">
    	</div>
    	
    	<div id="vntd_page_header_default" <?php if($page_header == "no-header") { echo 'class="hidden"'; } ?>>
    		<h5><?php _e('Page Tagline','crexis'); ?>:</h5>
    		<input type="text" name="page_subtitle" class="fullwidth-input" value="<?php echo esc_textarea($page_subtitle); ?>">
    	</div>
    	
    	<div id="navbar-style">  		
    		<h5><?php esc_html_e('Header Style','crexis'); ?>:</h5>
    	    <?php
    	    $navbar_styles = array(
    	    	'Default set in Theme Options' => "default",    	    	
    	    	"Default Style" => "style-default",
    	    	"Transparent" => "style-transparent",
    	    	"Transparent Hamburger Menu" => "style-transparent-hamburger",
    	    	"Disable" => "disable" 
    	    );
    	    
    	    crexis_create_dropdown('navbar_style',$navbar_styles,$navbar_style);
    	    
    	    ?>
    	    
    	</div>
    	
    	<?php if(get_post_type(get_the_id()) == 'portfolio') { } else { ?>
    	
    	<div class="metabox-option">
			<h5><?php esc_html_e('Page Layout','crexis'); ?>:</h5>
			
			<?php 
			if(!$page_layout) $page_layout = crexis_option('default_layout');
			$page_layout_arr = array('Right Sidebar' => 'sidebar_right', 'Left Sidebar' => 'sidebar_left', "Fullwidth" => 'fullwidth');  
			
			crexis_create_dropdown('page_layout',$page_layout_arr,$page_layout,true);
			
			?>
		</div>
		<div class="metabox-option fold fold-page_layout fold-sidebar_right fold-sidebar_left" <?php if($page_layout == "fullwidth" || !$page_layout) echo 'style="display:none;"'; ?>>
			<h5><?php esc_html_e('Page Sidebar','crexis'); ?>:</h5>
			<select name="page_sidebar" class="select"> 
                <option value="Default Sidebar"<?php if($page_sidebar == "Default Sidebar" || !$page_sidebar) echo "selected"; ?>>Default Sidebar</option>
            	<?php
            								
				// Retrieve custom sidebars
  				
  				if( crexis_option("sidebar_generator") ) {
  					foreach(crexis_option("sidebar_generator") as $sidebar)  
  					{  
  						
  						if($sidebar != "") {
  						
  							$sidebar = "sidebar_" . esc_attr( strtolower( str_replace(' ', '-', $sidebar) ) );
  						?>
  						
  						<option value="<?php echo $sidebar; ?>"<?php if($page_sidebar == $sidebar) echo "selected"; ?>><?php echo esc_textarea($sidebar); ?></option>
  						
  						<?php 
  						
  						}
  					}
  				}
				
				if( class_exists('Woocommerce') ) {
				
					if($page_sidebar == "WooCommerce Shop Page") $selected_shop = "selected";
					if($page_sidebar == "WooCommerce Product Page") $selected_product = "selected";
					
					echo '<option value="WooCommerce Shop Page" '.$selected_shop.'>WooCommerce Shop Page</option>';
					echo '<option value="WooCommerce Product Page" '.$selected_product.'>WooCommerce Product Page</option>';
				}
							
				?>            	

            </select>
		</div>
		
		<?php } ?>
        
    </div>
<?php

}	

function crexis_page_settings_advanced_config() {
        global $post;	
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $footer_widgets = $footer_style = $footer_color = $page_nav_menu = '';
        if(array_key_exists("page_nav_menu", $custom)) {
        	$page_nav_menu = $custom["page_nav_menu"][0];
        }
		if(array_key_exists("footer_style", $custom)) {
			$footer_style = $custom["footer_style"][0];
		}
		if(array_key_exists("footer_color", $custom)) {
			$footer_color = $custom["footer_color"][0];
		}
		if(array_key_exists("footer_widgets", $custom)) {
			$footer_widgets = $custom["footer_widgets"][0];
		}
?>
    <div class="metabox-options form-table side-options">
    	
    	<div class="metabox-option fold fold-page_layout fold-sidebar_right fold-sidebar_left">
			<h5><?php esc_html_e('Page Nav Menu','crexis'); ?>:</h5>
			
			<select name="page_nav_menu" class="select"> 
			    <option value="default" <?php if($page_nav_menu == "default" || !$page_nav_menu) echo "selected"; ?>>Default Nav Menu</option>
			<?php
			
			$nav_menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );	
			$selected = '';
			foreach($nav_menus as $nav_menu)  
			{  	
				$selected = '';
				if($page_nav_menu == $nav_menu->slug) $selected = ' selected';
				echo '<option value="'.$nav_menu->slug.'"'.esc_attr($selected).'>'.$nav_menu->name.'</option>';
			}
			
			?>
			</select>
		</div>
		
		<div id="footer-color">  		
			<h5><?php esc_html_e('Footer Widgets Area','crexis'); ?>:</h5>
		    <?php
		    $footer_widgets_arr = array(
		    	'Default set in Theme Options' => "default",
		    	'Enabled' => 'enabled',
		    	'Disabled' => 'disabled'
		    );
		    
		    crexis_create_dropdown('footer_widgets',$footer_widgets_arr,$footer_widgets);
		    
		    ?>
		    
		</div>
        
    </div>
<?php

}
 

function crexis_pagetitle_config() {
        global $post;	
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $customize_enable = $customize_textcolor = $customize_bgcolor = $customize_bg_image = $customize_textalign = $customize_bgimage = $customize_breadcrumbs = $customize_fontsize = $customize_fontweight = $customize_texttransform = $customize_height = $customize_animated = $customize_bgoverlay = $customize_bg_align_y = $parallax = '';
        if(array_key_exists("customize_enable", $custom)) {
			$customize_enable = $custom["customize_enable"][0];
		}
		if(array_key_exists("customize_textcolor", $custom)) {
			$customize_textcolor = $custom["customize_textcolor"][0];
		}
		if(array_key_exists("customize_bgcolor", $custom)) {
			$customize_bgcolor = $custom["customize_bgcolor"][0];
		}
		if(array_key_exists("customize_bgimage", $custom)) {
			$customize_bgimage = $custom["customize_bgimage"][0];
		}
		if(array_key_exists("customize_textalign", $custom)) {
			$customize_textalign = $custom["customize_textalign"][0];	
		}
		if(array_key_exists("customize_fontweight", $custom)) {
			$customize_fontweight = $custom["customize_fontweight"][0];
		}
		if(array_key_exists("customize_fontsize", $custom)) {
			$customize_fontsize = $custom["customize_fontsize"][0];
		}
		if(array_key_exists("customize_texttransform", $custom)) {
			$customize_texttransform = $custom["customize_texttransform"][0];
		}
		if(array_key_exists("customize_breadcrumbs", $custom)) {
			$customize_breadcrumbs = $custom["customize_breadcrumbs"][0];
		}
		if(array_key_exists("customize_height", $custom)) {
			$customize_height = $custom["customize_height"][0];
		}
		if(array_key_exists("customize_animated", $custom)) {
			$customize_animated = $custom["customize_animated"][0];
		}
		if(array_key_exists("customize_bgoverlay", $custom)) {
			$customize_bgoverlay = $custom["customize_bgoverlay"][0];
		}
		
		if(array_key_exists("customize_bg_align_y", $custom)) {
			$customize_bg_align_y = $custom["customize_bg_align_y"][0];
		}
		
		if(array_key_exists("customize_parallax", $custom)) {
			$customize_parallax = $custom["customize_parallax"][0];
		}
		
		wp_enqueue_style('wp-color-picker');
		wp_enqueue_script('wp-color-picker', '', '', '', true);

?>
    <div class="metabox-options form-table side-options pagetitle-customize">
  		
  		<div id="customize_textcolor">  		
  		    <?php
  		    
  		    $arr_enable = array(
  		    	"Don't use a custom page title" => 'no',
  		    	'Use a custom page title' => "yes",
  		    	
  		    );
  		    
  		    crexis_create_dropdown('customize_enable',$arr_enable,$customize_enable);
  		    
  		    ?>
  		    
  		</div>	
    	
    	<?php 
    	$extra_class = 'hidden';
    	if($customize_enable == 'yes') $extra_class = ' not-hidden';
    	
    	?>
    	<div id="customize_textalign" class="hidden <?php echo esc_attr($extra_class); ?>">  		
    		<h5><?php esc_html_e('Text Align','crexis'); ?>:</h5>
    		
    	    <?php
    	    
    		$arr_textalign = array(
    			'Left' => "left",
    			'Center' => "center"
    		);
    		
    		crexis_create_dropdown('customize_textalign',$arr_textalign,$customize_textalign);
    	    
    	    ?>
    	    
    	</div> 	
    	
    	<div id="customize_breadcrumbs" class="hidden <?php echo esc_attr($extra_class); ?>">  		
    		<h5><?php esc_html_e('Breadcrumbs','crexis'); ?>:</h5>
    		
    	    <?php
    	    
			$arr_breadcrumbs = array(
				'Enabled' => "enabled",
				'Disabled' => 'disabled'	
			);
			
			crexis_create_dropdown('customize_breadcrumbs',$arr_breadcrumbs,$customize_breadcrumbs);
    	    
    	    ?>
    	    
    	</div>
    	
    	<div id="customize_height" class="hidden <?php echo esc_attr($extra_class); ?>">  		
    		<h5><?php esc_html_e('Page Title Height','crexis'); ?>:</h5>
    	    
    	    <?php
    	    
    	    $arr_height = array(
    	    	'Regular' 	=> "regular",
    	    	'Big' 		=> "big"
    	    );
    	    
    	    crexis_create_dropdown('customize_height',$arr_height,$customize_height);
    	    
    	    ?>
    	    
    	</div>
    	
		<div id="customize_textcolor" class="hidden <?php echo esc_attr($extra_class); ?>">  		
			<h5><?php esc_html_e('Text Color','crexis'); ?>:</h5>

		    <?php
		    
		    $arr_textcolor = array(
		    	'White' => "white",
		    	'Dark' => "dark"
		    );
		    
		    crexis_create_dropdown('customize_textcolor',$arr_textcolor,$customize_textcolor);
		    
		    ?>
		</div>
    	
		<div id="customize_bgcolor" class="hidden <?php echo esc_attr($extra_class); ?>">  		
			<h5><?php esc_html_e('Background Color','crexis'); ?>:</h5>

		    <input name="customize_bgcolor" type="text" value="<?php echo esc_attr($customize_bgcolor); ?>" class="wp-color-picker">
		</div>
		
		<div id="customize_bgimage" class="hidden image-upload image-upload-dep <?php echo esc_attr($extra_class); ?>">  		
			<h5><?php esc_html_e('Background Image','crexis'); ?>:</h5>

		    <input name="customize_bgimage" type="hidden" value="<?php echo esc_textarea($customize_bgimage); ?>" class="image-upload-data">
		    <div class="image-upload-preview show-on-upload" <?php if($customize_bgimage) echo 'style="display:block;"'; ?>>
		    	<?php
		    		$imgurl = '';
		    		if($customize_bgimage) {
			    		$imgurl = wp_get_attachment_image_src( $customize_bgimage, 'thumbnail');			
			    		$imgurl = $imgurl[0];
			    	}
		    	?>
		    	<img src="<?php echo esc_url($imgurl);?>">
		    </div>
		    <div class="button add-single-image"><?php if($customize_bgimage) { esc_html_e('Change image','crexis'); } else { esc_html_e('Upload image','crexis'); } ?></div>
		    <div class="button remove-single-image show-on-upload" <?php if($customize_bgimage) echo 'style="display:inline-block;"'; ?>><?php esc_html_e('Remove image','crexis'); ?></div>
		</div>
		
		<div id="customize_overlay" class="hidden <?php echo esc_attr($extra_class); ?>">  		
			<h5><?php esc_html_e('Background Image Overlay','crexis'); ?>:</h5>
			
		    <?php
		    
			$arr_bgoverlays = array(
				"None" => "",
				"Dark 20%" => "dark",
				"Dark 75%" => "dark75",
				"Gray 80%" => "darker",
				"Gray 89%" => "dark80",
				"Dark 90%" => "black",
				"Dark Dots" => "dark_dots",
				"Accent" => "accent",
				"Light" => "light",
				"Dark Blue" => 'dark_blue',
				"Dark Red" => 'dark_red'
			);
			
			crexis_create_dropdown('customize_bgoverlay',$arr_bgoverlays,$customize_bgoverlay);
		    
		    ?>
		    
		</div>
		
		<div id="customize_overlay" class="hidden <?php echo esc_attr($extra_class); ?>">  		
			<h5><?php esc_html_e('Background Image Vertical Alignment','crexis'); ?>:</h5>
			
		    <?php
		    
			$arr_bg_align_y = array(
				'Center' => 'center',
				'Top' => 'top',
				'Bottom' => 'bottom',
			);
			
			crexis_create_dropdown('customize_bg_align_y',$arr_bg_align_y,$customize_bg_align_y);
		    
		    ?>
		    
		</div>
		
		<div id="customize_parallax" class="hidden <?php echo esc_attr($extra_class); ?>">  		
			<h5><?php esc_html_e('Background Image Parallax Effect','crexis'); ?>:</h5>
			
		    <?php
		    
			$arr_parallax = array(
				'Theme Defaults' => 'default',
				'Yes' => 'yes',
				'No' => 'no'
			);
			
			crexis_create_dropdown('customize_parallax',$arr_parallax,$customize_parallax);
		    
		    ?>
		    
		</div>
        
    </div>
<?php

}	


	
// Save Custom Fields
	
add_action('save_post', 'crexis_save_page_settings'); 

function crexis_save_page_settings(){
    global $post;  

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{		
	
		$post_metas = array('page_layout','page_sidebar','page_width','navbar_style','navbar_color','footer_color','footer_style','page_header','page_title','page_subtitle','footer_widgets', 'customize_fontsize', 'customize_texttransform', 'customize_textalign', 'customize_fontweight', 'customize_textcolor', 'customize_bgcolor', 'customize_bgimage', 'customize_breadcrumbs', 'customize_enable', 'customize_height', 'customize_animated', 'page_nav_menu', 'customize_bgoverlay', 'customize_bg_align_y', 'customize_parallax', 'page_title');
		
		foreach($post_metas as $post_meta) {
			if(isset($_POST[$post_meta])) update_post_meta($post->ID, $post_meta, $_POST[$post_meta]);
		}

    }

}