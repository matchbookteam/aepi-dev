<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "crexis_options";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( get_template_directory() . '/framework/theme-panel/waxom/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( get_template_directory() . '/framework/theme-panel/waxom/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Crexis', 'crexis' ),
        'page_title'           => esc_html__( 'Crexis Theme Options', 'crexis' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => 'AIzaSyBC3L1znn2yrtdsXm9hEryNjbBlWbjrewrwe5nEk',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
		'templates_path'		=> get_template_directory() . '/framework/theme-panel/crexis/templates/panel/',
        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => esc_html__( 'Documentation', 'crexis' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => esc_html__( 'Support', 'crexis' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => esc_html__( 'Extensions', 'crexis' ),
    );

    // Add content after the form.
    $args['footer_text'] = '<p>Need help? Visit our dedicated <a href="http://veented.com/support" target="_blank">Support Forums</a>.</p>';

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'crexis' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'crexis' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'crexis' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'crexis' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'crexis' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */
     
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'General', 'crexis' ),
        'id'     => 'general',
        'desc'   => esc_html__( 'General Theme Settings.', 'crexis' ),
        'icon'   => 'fa fa-home',
        'fields' => array(
            array(
                'id'       => 'site_logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'Website Logo Image', 'crexis' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( "Upload your website's logo image.", 'crexis' ),
                'default'  => array( 'url' => get_template_directory_uri() . '/img/logo-dark.png' ),
            ),
            array(
                'id'       => 'site_logo_white',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'White Logo Image Version', 'crexis' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( "Used for Transparent Header style.", 'crexis' ),
                'default'  => array( 'url' => get_template_directory_uri() . '/img/logo-white.png' ),
            ),
            array(
                'id'       => 'stt',
                'type'     => 'switch',
                'title'    => esc_html__( 'Scroll to Top Button', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Scroll to Top button on your website.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'default_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Default Page Layout', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a default page layout for your pages: Fullwidth, Sidebar Right or Sidebar Left', 'crexis' ),
                'options'  => array(
                    'fullwidth' => array(
                        'alt' => '1 Column',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'sidebar_left' => array(
                        'alt' => '2 Column Left',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'sidebar_right' => array(
                        'alt' => '2 Column Right',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default'  => 'fullwidth'
            ),
            array(
                'id'       => 'parallax_plugin',
                'type'     => 'select',
                'title'    => esc_html__( 'Parallax Plugin', 'crexis' ),
                'subtitle' => esc_html__( "Choose a plugin to be used for Visual Composer row's image backgrounds.", 'crexis' ),
                'options'  => array(
                    "crexis" => "Default Crexis Plugin",
                    "vc" 	=> "Native Visual Composer Plugin"
                ),
                'default'  => 'yes'
            ),
            array(
                'id'       => 'pageloader',
                'type'     => 'select',
                'title'    => esc_html__( 'Page Loader', 'crexis' ),
                'subtitle' => esc_html__( 'Enable or disable the page loading icon.', 'crexis' ),
                'options'  => array(
                    "yes" => "Yes",
                    "no" 	=> "No"
                ),
                'default'  => 'yes'
            ),
        )
    ) );
    
    // Header Tab
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Header', 'crexis' ),
        'id'     => 'header',
        'desc'   => esc_html__( 'Header Settings.', 'crexis' ),
        'icon'   => 'fa fa-columns',
        'fields' => array(
            array(
                'id'       => 'header_style',
                'type'     => 'select',
                'title'    => esc_html__( 'Header Style', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a style of your Site Header.', 'crexis' ),
                'options'  => array(
                    'style-default' => 'Classic',
                    'style-transparent' => 'Transparent',
                    'style-transparent-hamburger' => 'Transparent Hamburger Menu',
                    'disable' => 'Disable',
                ),
                'default'  => 'style-default'
            ),
            array(
                'id'       => 'header_color',
                'type'     => 'select',
                'title'    => esc_html__( 'Header Color', 'crexis' ),
                'subtitle' => esc_html__( 'Choose color scheme for your Header.', 'crexis' ),
                'options'  => array(
                    "white" => "White",
                    "dark" 	=> "Dark"
                ),
                'default'  => 'white'
            ),
            array(
                'id'       => 'header_dropdown_color',
                'type'     => 'select',
                'title'    => esc_html__( 'Dropdown Menu Color', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a color scheme for the dropdown menu.', 'crexis' ),
                'options'  => array(
                    "white" => "White",
                    "dark" 	=> "Dark"
                ),
                'default'  => 'white'
            ),
            array(
                'id'       => 'header_search',
                'type'     => 'switch',
                'title'    => esc_html__( 'Header Search', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Search field in Header.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'header_title',
                'type'     => 'switch',
                'title'    => esc_html__( 'Page Title', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Page Title area globally.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'breadcrumbs',
                'type'     => 'switch',
                'title'    => esc_html__( 'Breadcrumbs', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Breadcrumbs navigation.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'pagetitle_parallax',
                'type'     => 'select',
                'title'    => esc_html__( 'Page Title Background Parallax', 'crexis' ),
                'subtitle' => esc_html__( 'Enable or disable the parallax effect for a custom page title background.', 'crexis' ),
                'options'  => array(
                    "yes" => "Yes",
                    "no" 	=> "No"
                ),
                'default'  => 'yes'
            ),
            
        )
    ) );
    
    // Top Bar
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Top Bar', 'crexis' ),
        'id'     => 'topbar',
        'desc'   => esc_html__( 'Top Bar Settings.', 'crexis' ),
        'icon'   => 'fa fa-columns',
        'fields' => array(
            array(
                'id'       => 'topbar',
                'type'     => 'switch',
                'title'    => esc_html__( 'Top Bar', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Top Bar section.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'topbar_force',
                'type'     => 'switch',
                'title'    => esc_html__( 'Top Bar in Transparent Header', 'crexis' ),
                'subtitle' => esc_html__( 'Show the Top Bar section with Transparent Header style.', 'crexis' ),
                'default'  => false,
            ),
            array(
                'id'       => 'topbar_left',
                'type'     => 'select',
                'title'    => esc_html__( 'Left side content type', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a content type for the left side of the Top Bar section.', 'crexis' ),
                'options'  => array(
                    "social" => "Social Icons",
//                    "menu" => "Menu",
                    "text" => "Text",
                    "textsocial" => "Text + Social Icons"
                ),
                'default'  => 'text'
            ),
            array(
                'id'       => 'topbar_right',
                'type'     => 'select',
                'title'    => esc_html__( 'Right side content type', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a content type for the right side of the Top Bar section.', 'crexis' ),
                'options'  => array(
                    "social" => "Social Icons",
//                    "menu" => "Menu",
                    "text" => "Text",
                    "textsocial" => "Text + Social Icons"
                ),
                'default'  => 'social'
            ),
            array(
                'id'       => 'topbar_text_left',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Left Top Bar Text', 'crexis' ),
                'subtitle' => esc_html__( 'The text content that is being selectable as one of the "content types" for the Top Bar. Supports HTML.', 'crexis' ),
                'default'  => 'E-Mail: hello@crexis.com Phone: 591 341 344',
            ),
            array(
                'id'       => 'topbar_text_right',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Right Top Bar Text', 'crexis' ),
                'subtitle' => esc_html__( 'The text content that is being selectable as one of the "content types" for the Top Bar. Supports HTML.', 'crexis' ),
                'default'  => '[icon icon="envelope"] hello@crexis.com [icon icon="phone"] 591 341 344',
            )
            
    	)
    ) );
    
    // Footer
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Footer', 'crexis' ),
        'id'     => 'footer',
        'desc'   => esc_html__( 'Footer Settings.', 'crexis' ),
        'icon'   => 'fa fa-download',
        'fields' => array(
            array(
                'id'       => 'copyright',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Copyright Text', 'crexis' ),
                'subtitle' => esc_html__( 'Copyright text displayed in the footer.', 'crexis' ),
                'default'  => 'Copyright 2017 All rights reserved. Designed by (link)My Website(/link).',
            ),
            array(
                'id'       => 'copyright_url',
                'type'     => 'text',
                'title'    => esc_html__( 'Copyright Link URL', 'crexis' ),
                'subtitle' => esc_html__( 'Link for the (link) shortcode in the copyright text.', 'crexis' ),
                'default'  => 'http://yoursite.url',
            ),
            array(
                'id'       => 'footer_style',
                'type'     => 'select',
                'title'    => esc_html__( 'Footer Style', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a style of your footer.', 'crexis' ),
                'options'  => array(
                    "classic" => "Classic (copyright + social icons)",
                    "centered" => "Centered with Image"
                ),
                'default'  => 'classic'
            ),
            array(
                'id'       => 'footer_logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'Footer Logo Image', 'crexis' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( "Upload an image that will be displayed above the copyright text.", 'crexis' ),
                'default'  => array( 'url' => get_template_directory_uri() . '/img/logo-white.png' ),
                'required' => array('footer_style','=',"centered")
            ),
            array(
                'id'       => 'footer_widgets',
                'type'     => 'switch',
                'title'    => esc_html__( 'Footer Widgets Area', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Footer Widgets area globally. Please visit Appearance / Widgets menu to add new widgets!', 'crexis' ),
                'default'  => true,
            ),
//            array(
//                'id'       => 'footer_column_margin',
//                'type'     => 'text',
//                'title'    => esc_html__( 'First column top margin', 'crexis' ),
//                'subtitle' => esc_html__( 'Set a top margin for the first column of the Widgets Area. Handy if you want to vertically center the first column content (for example with a logo image) with the rest of the columns. Example: -20px.', 'crexis'),
//                'default'  => '-27px',
//            ),
            
    	)
    ) );    
    
    // Blog
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Blog', 'crexis' ),
        'id'     => 'blog',
        'desc'   => esc_html__( 'Blog Settings.', 'crexis' ),
        'icon'   => 'fa fa-file-text-o',
        'fields' => array(
            array(
                'id'       => 'blog_style',
                'type'     => 'select',
                'title'    => esc_html__( 'Blog Style', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a style for your Blog.', 'crexis' ),
                'options'  => array(
                    "classic" => "Classic",
                    "grid" => "Grid",
                    "minimal" => "Minimal",
                ),
                'default'  => 'classic'
            ),
            array(
                'id'       => 'blog_meta_style',
                'type'     => 'select',
                'title'    => esc_html__( 'Blog Meta Style', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a style for your meta data (date, author, categories etc).', 'crexis' ),
                'options'  => array(
                    "default" => "Default - aside",
                    "classic" => "Classic - below Featured Image",
                ),
                'default'  => 'default'
            ),
            array(
                'id'       => 'blog_masonry',
                'type'     => 'switch',
                'title'    => esc_html__( 'Masonry', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Masonry effect for your grid.', 'crexis' ),
                'default'  => true,
                'required' => array('blog_style','=',"grid")
            ),
            array(
                'id'       => 'blog_grid_cols',
                'type'     => 'select',
                'title'    => esc_html__( 'Blog Grid Columns', 'crexis' ),
                'subtitle' => esc_html__( 'Select number of columns for your grid.', 'crexis' ),
                'options'  => array(
                	"6" => "6",
                	"5" => "5",
                    "4" => "4",
                    "3" => "3",
                    "2" => "2",
                ),
                'default'  => '3',
                'required' => array('blog_style','=',"grid")
            ),
            array(
                'id'       => 'blog_fullwidth',
                'type'     => 'switch',
                'title'    => esc_html__( 'Fullwidth', 'crexis' ),
                'subtitle' => esc_html__( 'Check this option to enable a 100% width blog page.', 'crexis' ),
                'default'  => false,
                'required' => array( 'blog_style', '=', "grid" )
            ),
            array(
                'id'       => 'blog_ajax',
                'type'     => 'switch',
                'title'    => esc_html__( 'Ajax Pagination', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Ajax Pagination.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_media_link_type',
                'type'     => 'select',
                'title'    => esc_html__( 'Blog Index Image Behaviour', 'crexis' ),
                'subtitle' => esc_html__( 'Choose if image should open a lightbox or link to the post page.', 'crexis' ),
                'options'  => array(
                    "lightbox" => esc_html__( "Open lightbox", 'crexis' ),
                    "link" => esc_html__( "Open post page", 'crexis' ),
                ),
            ),
            array(
                'id'       => 'blog_meta_author',
                'type'     => 'switch',
                'title'    => esc_html__( 'Display Post Author', 'crexis' ),
                'subtitle' => esc_html__( 'Display author in the blog post meta.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_meta_comments',
                'type'     => 'switch',
                'title'    => esc_html__( 'Display Comments Count', 'crexis' ),
                'subtitle' => esc_html__( 'Display the blog post comments count.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_meta_categories',
                'type'     => 'switch',
                'title'    => esc_html__( 'Display Post Categories', 'crexis' ),
                'subtitle' => esc_html__( 'Display the post categories.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_trackback',
                'type'     => 'switch',
                'title'    => esc_html__( 'Post Trackback', 'crexis' ),
                'subtitle' => esc_html__( 'Display the post trackback URL.', 'crexis' ),
                'default'  => false
            ),
            array(
                'id'       => 'blog_post_nav',
                'type'     => 'switch',
                'title'    => esc_html__( 'Post Navigation', 'crexis' ),
                'subtitle' => esc_html__( 'Display links for next/previous posts on single blog post pages.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_meta',
                'type'     => 'switch',
                'title'    => esc_html__( 'Blog Index Meta Section', 'crexis' ),
                'subtitle' => esc_html__( 'Enable the meta section on blog index page.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_single_meta',
                'type'     => 'switch',
                'title'    => esc_html__( 'Single Post Meta Section', 'crexis' ),
                'subtitle' => esc_html__( 'Enable the  blog post meta section on individual post page.', 'crexis' ),
                'default'  => true
            ),
            
    	)
    ) );    
    
    // Portfolio
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Portfolio', 'crexis' ),
        'id'     => 'portfolio',
        'desc'   => esc_html__( 'Portfolio Settings.', 'crexis' ),
        'icon'   => 'fa fa-briefcase',
        'fields' => array(
            array(
                'id'       => 'portfolio_url',
                'type'     => 'select',
                'data'     => 'pages',
                'title'    => esc_html__( 'Main Portfolio Page', 'crexis' ),
                'subtitle' => esc_html__( 'Select a default portfolio page for the "Back to portfolio" link on single portfolio posts.', 'crexis' ),
            ),
            
    	)
    ) );
    
    // Sidebars
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Sidebars', 'crexis' ),
        'id'     => 'sidebars',
        'icon'   => 'fa fa-indent',
        'fields' => array(
            array(
                'id'       => 'sidebar_generator',
                'type'     => 'multi_text',
                'title'    => esc_html__( 'Sidebar Manager', 'crexis' ),
                'subtitle' => esc_html__( 'Create new sidebars.', 'crexis' ),
            ),
            
    	)
    ) );
    
    // Social Icons
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Social Icons', 'crexis' ),
        'id'     => 'socialicons',
        'icon'   => 'fa fa-twitter',
        'fields' => array(
            array(
                'id'       => 'social_icons',
                'type'     => 'sortable',
                'title'    => esc_html__( 'Social Icons Generator', 'crexis' ),
                'subtitle' => esc_html__( 'Easily add and arrange social icons for use in Footer and Top Bar.<br>Leave blank field to disable a specific icon.', 'crexis' ),
                'label'    => true,
                'options'  => array(
                    'Facebook URL' => 'http://your_facebook_page_url',
                    'Twitter' => '#',
                    'Google-plus' => '',
                    'Linkedin' => '',
                    'Dribbble' => '#',
                    'Vimeo' => '#',
                    'Youtube' => '#',
                    'Pinterest' => '',
                    'Skype' => '',
                    'Tumblr' => '',
                    'Dropbox' => '',
                    'RSS' => '',
                    'Weibo' => '',
                    'Flickr' => '',
                    'Instagram' => '',
                    'Stack-exchange' => '',
                    'Stack-overflow' => '',
                    'Github' => '',
                    'Maxcdn' => '',
                    'Behance' => '',
                    'Soundcloud' => '',
                    'E-mail' => ''
                )
            ),
            
    	)
    ) );
    
    // Appearance
    
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Appearance', 'crexis' ),
        'id'    => 'appearance',
        'icon'  => 'fa fa-paint-brush'
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General', 'crexis' ),
        'id'         => 'appearance_general',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'accent_color',
                'type'     => 'color',
                'title'    => esc_html__( 'Accent Color', 'crexis' ),
                'subtitle' => esc_html__( 'Main Accent color.', 'crexis' ),
                'default'  => '#e54343',
                'transparent' => false
            ),
            array(
                'id'       => 'accent_color2',
                'type'     => 'color',
                'title'    => esc_html__( 'Accent Color 2', 'crexis' ),
                'subtitle' => esc_html__( 'Main Accent color.', 'crexis' ),
                'default'  => '#f35050',
                'transparent' => false
            ),
			array(
			    'id'       => 'accent_color3',
			    'type'     => 'color',
			    'title'    => esc_html__( 'Accent Color 3', 'crexis' ),
			    'subtitle' => esc_html__( 'Alternative Accent color.', 'crexis' ),
			    'default'  => '#222222',
			    'transparent' => false
			),
			array(
			    'id'       => 'bg_color',
			    'type'     => 'color',
			    'title'    => esc_html__( 'Background Color', 'crexis' ),
			    'default'  => '#ffffff',
			    'transparent' => false
			),
			array(
			    'id'       => 'theme_skin',
			    'type'     => 'select',
			    'title'    => esc_html__( 'Theme Skin', 'crexis' ),
			    'subtitle' => esc_html__( 'Choose a skin.', 'crexis' ),
			    'options'  => array(
			        "white" => "White",
			        "dark" => "Dark",
			        "night" => "Night",
			    ),
			    'default'  => 'white'
			),
			array(
			    'id'       => 'predefined_colors',
			    'type'     => 'palette',
			    'title'    => esc_html__( 'Predefined Color Palettes', 'crexis' ),
			    'subtitle' => esc_html__( 'Select a predefined scheme for the accent color.', 'crexis' ),
			    'default'  => 'red',
			    'palettes' => array(
			        'red'  => array( // Original red
			            '#e54343',
			            '#f35050',
			            '#222222',
			        ),
			        'orange'  => array(
			            '#ffa800',
			            '#FFB423',
			            '#222222'
			        ),
			        'green'  => array( // Original emerald
			            '#7bcba8',
			            '#00a652',
			        ),
			        'green-bright'  => array( // Brighter Green
			            '#8dc63f',
			            '#7ab22f',
			        ),
			        'blue'  => array(
			            '#00bff3',
			            '#00abe4',
			        ),  
			        
			    )
			),
        ),
        
    ) );
    
    Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Header', 'crexis' ),
            'id'         => 'appearance_header',
            'subsection' => true,
            'fields'     => array(
				array(
				    'id'       => 'color_navigation',
				    'type'     => 'color',
				    'title'    => esc_html__( 'Site Navigation Text Color', 'crexis' ),
				    'subtitle' => esc_html__( 'Choose a color for your main site navigation links.', 'crexis' ),
				    'default'  => '#3a3a3a',
				    'transparent' => false
				),
    			array(
    			    'id'       => 'topbar_bg_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Top Bar Background Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Background color of the Top Bar section.', 'crexis' ),
    			    'default'  => '#ffffff',
    			    'transparent' => false
    			),
            ),
            
        ) );
        
    Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Page Title', 'crexis' ),
            'id'         => 'appearance_pagetitle',
            'subsection' => true,
            'fields'     => array(
    			array(
    			    'id'       => 'breadcrumbs_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Breadcrumbs Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Text color of the Breadcrumbs navigation.', 'crexis' ),
    			    'default'  => '#959595',
    			    'transparent' => false
    			),
            ),
            
        ) );
        
        
    Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Page Content', 'crexis' ),
            'id'         => 'appearance_pagecontent',
            'subsection' => true,
            'fields'     => array(
                array(
                    'id'       => 'body_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Text Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Website paragraph text color.', 'crexis' ),
                    'default'  => '#888888',
                    'transparent' => false
                ),
                array(
                    'id'       => 'link_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Link Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Leave blank to use accent color.', 'crexis' ),
                    'default'  => '',
                    'transparent' => false
                ),
                array(
                    'id'       => 'link_hover_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Link Hover Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Basic link hover color.', 'crexis' ),
                    'default'  => '#333333',
                    'transparent' => false
                ),
    			array(
    			    'id'       => 'heading_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Heading Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Color of text Headings.', 'crexis' ),
    			    'default'  => '#666666',
    			    'transparent' => false
    			),
    			array(
    			    'id'       => 'special_heading_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Special Heading Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Color of text Headings.', 'crexis' ),
    			    'default'  => '#666666',
    			    'transparent' => false
    			),
            ),
            
        ) );
        
    Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Footer', 'crexis' ),
            'id'         => 'appearance_footer',
            'subsection' => true,
            'fields'     => array(
                array(
                    'id'       => 'footer_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Text Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Footer text color.', 'crexis' ),
                    'default'  => '#666666',
                    'transparent' => false
                ),
                array(
                    'id'       => 'footer_bg_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Footer Background Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Background color of the Footer.', 'crexis' ),
                    'default'  => '#111111',
                    'transparent' => false
                ),
    			array(
    			    'id'       => 'footer_widgets_bg_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Footer Widgets Background Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Background color of the Footer Widgets.', 'crexis' ),
    			    'default'  => '#191919',
    			    'transparent' => false
    			),
            ),
            
        ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Typography', 'crexis' ),
        'id'         => 'appearance_typography',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'typography_body',
                'type'     => 'typography',
                'title'    => esc_html__( 'Body Font', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the body font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
                "font-style" => false,
                "color" => false,
                'default'  => array(
                    'font-size'   => '14px',
                    'font-family' => 'Open Sans',
                    'font-weight' => '400',
                ),
            ),
            array(
                'id'       => 'typography_primary',
                'type'     => 'typography',
                'title'    => esc_html__( 'Primary Heading Font', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the primary heading font.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
                "font-size" => false,
                "font-style" => false,
                "color" => false,
                "text-transform" => true,
                'default'  => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '500',
                    "font-size" => "36px",
                    "text-transform" => "none"
                ),
            ),
            array(
                'id'       => 'typography_secondary',
                'type'     => 'typography',
                'title'    => esc_html__( 'Secondary Heading Font', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the secondary heading font.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
                "font-size" => false,
                "color" => false,
                "font-weight" => false,
                "subsets" => false,
                "font-style" => false,
                "text-transform" => false,
                'default'  => array(
                    'color'       => '#333333',
                    'font-family' => 'Oswald',
                    'font-weight' => '400',
                    "font-size" => "36px",
                    "text-transform" => "none"
                ),
            ),
            array(
                'id'       => 'typography_navigation_font',
                'type'     => 'select',
                'title'    => esc_html__( 'Site Navigation Font Family', 'crexis' ),
                'subtitle' => esc_html__( 'Select the font family for the site navigation.', 'crexis' ),
                'options'  => array(
                	"body" => "Body Font Family",
                	"heading" => "Primary Heading Font Family",
                ),
                'default'  => 'body'
            ),
            array(
                'id'       => 'typography_navigation',
                'type'     => 'typography',
                'title'    => esc_html__( 'Site Navigation Font Properties', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the site navigation font properties.', 'crexis' ),
                "font-family" => false,
                'google'   => false,
                "text-align" => false,
                "letter-spacing" => true,
                "font-weight" => true,
                "font-style" => false,
                "line-height" => false,
                "subsets" => false,
                "text-transform" => true,
                "color" => false,
                'default'  => array(
                    'font-weight' => '500',
                    "font-size" => "11px",
                    "text-transform" => "none",
                    "letter-spacing" => "0px"
                ),
            ),
            array(
                'id'       => 'typography_special_heading',
                'type'     => 'typography',
                'title'    => esc_html__( 'Special Heading', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the special heading font properties.', 'crexis' ),
                "font-family" => false,
                'google'   => false,
                "text-align" => false,
                "letter-spacing" => true,
                "font-weight" => true,
                "font-style" => false,
                "line-height" => false,
                "subsets" => false,
                "text-transform" => true,
                "color" => false,
                'default'  => array(
                    'font-weight' => '600',
                    "font-size" => "30px",
                    "text-transform" => "uppercase"
                ),
            ),
            array(
                'id'       => 'typography_h1',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 1 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
				"color" => false,
				"font-family" => false,
				"font-style" => false,
				"font-weight" => false,
                'default'  => array(
                    "font-size" => "36px"
                ),
            ),
            array(
                'id'       => 'typography_h2',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 2 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "30px"
                ),
            ),
            array(
                'id'       => 'typography_h3',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 3 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "26px"
                ),
            ),
            array(
                'id'       => 'typography_h4',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 4 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "24px"
                ),
            ),
            array(
                'id'       => 'typography_h5',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 5 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "21px"
                ),
            ),
            array(
                'id'       => 'typography_h6',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 6 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "18px"
                ),
            ),
            array(
                'id'       => 'typography_page_title',
                'type'     => 'typography',
                'title'    => esc_html__( 'Page Title', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the page title font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
            	'color' => false,
                'default'  => array(
                    "font-size" => "30px",
                    "font-weight" => '300'
                ),
            ),
            array(
                'id'       => 'typography_copyright',
                'type'     => 'typography',
                'title'    => esc_html__( 'Copyright Text Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the copyright section font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "13px"
                ),
            ),
        )
    ) );
    
    // Archives/Search
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Archives/Search', 'crexis' ),
        'id'     => 'archives',
        'icon'   => 'fa fa-search',
        'fields' => array(
            array(
                'id'       => 'archives_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Archives Page Layout', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a default page layout for your pages: Fullwidth, Sidebar Right or Sidebar Left', 'crexis'),
                'options'  => array(
                    'fullwidth' => array(
                        'alt' => '1 Column',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'sidebar_left' => array(
                        'alt' => '2 Column Left',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'sidebar_right' => array(
                        'alt' => '2 Column Right',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default'  => 'sidebar_right'
            ),
            array(
                'id'       => 'search_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Search Page Layout', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a default page layout for your pages: Fullwidth, Sidebar Right or Sidebar Left', 'crexis' ),
                'options'  => array(
                    'fullwidth' => array(
                        'alt' => '1 Column',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'sidebar_left' => array(
                        'alt' => '2 Column Left',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'sidebar_right' => array(
                        'alt' => '2 Column Right',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default'  => 'sidebar_right'
            ),
            
    	)
    ) );
    
    //$true = true;
    
    //if( class_exists('Woocommerce') ) { // Enable this section only if the WooCommerce plugin is activated
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'WooCommerce', 'crexis' ),
        'id'     => 'woocommerce',
        'icon'   => 'fa fa-shopping-cart',
        'fields' => array(
            array(
                'id'       => 'header_woocommerce',
                'type'     => 'switch',
                'title'    => esc_html__( 'Shopping Cart Icon', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the WooCommerce icon in the Header section.', 'crexis' ),
                'default'  => true
            ),
            
    	)
    ) );
    
    //}
    
    // Advanced 
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Advanced', 'crexis' ),
        'id'     => 'advanced',
        'icon'   => 'fa fa-wrench',
        'fields' => array(
            array(
                'id'       => 'custom_css',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'Custom CSS Code', 'crexis' ),
                'subtitle' => esc_html__( 'Paste your CSS code here.', 'crexis' ),
                'mode'     => 'css',
                'theme'    => 'monokai',
                'default'  => "#header{\n   margin: 0 auto;\n}"
            ),
            array(
                'id'       => 'custom_js',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'Custom JS Code', 'crexis' ),
                'subtitle' => esc_html__( 'Paste your JavaScript code here.', 'crexis' ),
                'mode'     => 'javascript',
                'theme'    => 'chrome',
                'default'  => "jQuery(document).ready(function(){\n\n});"
            ),
            
    	)
    ) );
    
    // Google Maps API 
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Google Maps API', 'crexis' ),
        'id'     => 'apis',
        'icon'   => 'fa fa-map',
        'fields' => array(
            array(
                'id'       => 'google_maps_api',
                'type'     => 'text',
                'placeholder' => esc_html__( 'Your API key goes here..' , 'crexis' ),
                'title'    => esc_html__( 'Google Maps API Key', 'crexis' ),
                'subtitle' => esc_html__( 'Paste your Google Maps Api Key. For more information, check ', 'crexis' ) . '<a href="https://veented.ticksy.com/article/7856/" target="_blank">' . esc_html__('this article', 'crexis'). '</a>',
                'default'  => ""
            ),
            
    	)
    ) );
    
    // Extras
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Extras - NEW!', 'crexis' ),
        'id'     => 'extras',
        'icon'   => 'fa fa-rocket',
        'fields' => array(
            array(
                'id'    => 'extras_info',
                    'type'  => 'info',
                    'title' => __('Essential Grid and Ultimate VC Addons are now included with the theme!', 'crexis'),
                    'style' => 'info',
                    'desc'  => __('It means that you can use those awesome, super popular premium plugins for free with Crexis! Please check <a href="https://veented.ticksy.com/article/8523/" target="_blank">this article</a> for more information. Kind Regards!', 'crexis')
                )
                
            
    	)
    ) );

    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */


    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {

            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'crexis' ),
                'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'crexis' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

