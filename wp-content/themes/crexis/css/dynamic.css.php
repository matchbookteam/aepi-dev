<?php

/**
 * Theme Dynamic Stylesheet
 *
 * @package Waxom
 * @since 1.0
 *
 */
 
header("Content-type: text/css;");

$crexis_options_accent_color = '#E54343';
$crexis_options_accent_color2 = '#f35050';
$crexis_options_accent_color3 = '#222222';

if(crexis_option('accent_color') ) {
   	$crexis_options_accent_color = esc_attr(crexis_option('accent_color'));
}
if(crexis_option('accent_color2') ) {
	$crexis_options_accent_color2 = esc_attr(crexis_option('accent_color2'));
}
if(crexis_option('accent_color3') ) {
	$crexis_options_accent_color3 = esc_attr(crexis_option('accent_color3'));
}



?>

<?php
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//		General
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
?>


/* Accent Text Colors */

	.colored,
	.team-boxes .member-details h5,
	ul.mobile-boxes li div.mobile-icon a,
	ul.mobile-boxes li .buttons span.second-icon,
	footer a:hover,
	.second-nav .nav-menu ul li.active > a,
	.relative-nav .nav a:hover,
	.relative-nav .nav li.active a,
	footer .copyright_link:hover,
	.white-nav .nav-menu ul.dropdown-menu li a:hover,
	.dark-nav .nav-menu ul.dropdown-menu .submenu_column a:hover,
	.navigation ul li a:hover,
	#team.type-2 .team-boxes .member-details .socials a:hover,
	#navigation-type2.dark-nav .nav-menu ul li a:hover,
	#navigation-type2.white-nav .nav-menu ul li a:hover,
	#navigation-type2.white-nav .nav-menu ul li.active a,
	#navigation-type2.dark-nav .nav-menu ul li.active a,
	.content .gray-bg-2 .texts p i,
	ul.moving-items .box:hover span.icon,
	footer.big-footer #latest_tweets ul li a,
	#page-header .page_header_inner a:hover,
	footer .copyright a,
	#shop .single_item_details .right .panel a:hover,
	.post .dates .details ul li a:hover,
	#blog.masonry-blog .item .item_button:hover,
	#shop .box .item_details .price,
	#features-mobile .f-collapse div p a:hover,
	.timelineFlat a.t_line_node.active,
	section#features .feature-boxes.type-3 .feature-box:hover .feature-icon,
	body.dark-layout .feature-boxes.type-3 .feature-box:hover .feature-text h4,
	.feature-boxes.type-3 .feature-box:hover .feature-icon,
	body.dark-layout .feature-boxes.type-3 .feature-box:hover .feature-icon,
	.feature-boxes.type-3 .feature-box:hover .feature-text h4,
	.btn.border-btn.btn-accent,
	.post-header a:hover,
	.news .box .details h3 a:hover,
	.vntd-testimonials-carousel h5,
	.pricing-box h1,
	.counter-color-accent .counter-value,
	.counter-color-accent .counter-title h6,
	.copyright a,
	.product-features .f-collapse div p a:hover,
	a.ajax-load-more-text:hover,
	.page-template-template-fullpage #navigation .nav-menu ul li a:hover,
	.page-template-template-fullpage #navigation .nav-menu ul li.active a,
	#woo-nav-cart.nav-cart-active > a,
	#latest_tweets ul li a,
	.vntd-team.type-2 .team-boxes .member-details .socials a:hover,
	.nav-menu ul.dropdown-menu a:hover,
	.vntd-team.type-1 .team-boxes .member-details p.member-position,
	.footer.black-bg a:hover,
	 .footer.big-footer.dark-footer .copyright a,
	 #page-content .pricing-box h1.price,
	 #mobile-nav li a:hover,
	 .not-found-big,
	 #vntd-twitter p a {
		color: <?php echo esc_attr( $crexis_options_accent_color ); ?>;
	}
	
	body:not(.page-template-template-onepager) .current_page_item > a,
	.current-menu-ancestor > a,
	.current-menu-parent > a,
	body:not(.page-template-template-onepager) .current-menu-item > a {
		color: <?php echo esc_attr( $crexis_options_accent_color ); ?> !important;
	}

/* Accent Background Colors */

	.colored-bg,
	.second-nav .nav-menu ul li:hover:after,
	body.dark-layout .news .box .details a.post_read_more_button:hover,
	.cbp-nav-pagination-active,
	.timeline-item-details .right-buttons a.read_more,
	.timelineFlat .item:before,
	.icon-box-big-centered-circle.box:hover .box-icon .changeable-icon,
	.icon-box-big-centered-square.box:hover .box-icon .changeable-icon:before,
	.cbp-l-filters-alignCenter .cbp-filter-item.cbp-filter-item-active,
	.category-boxes .owl-buttons div:hover,
	.icon-box-medium-left-circle .box-icon .changeable-icon,
	ul.mobile-boxes li:hover div.mobile-icon a,
	ul.mobile-boxes li .buttons span,
	ul.mobile-boxes li .buttons span.second-icon:hover,
	.pricing-box.active a.price_button,
	.pricing-box a.price_button:hover,
	.contact-block-horizontal a.box:hover .button:hover,
	.contact-block-vertical a.box:hover .button:hover,
	.timelineFlat .item_open .t_close:hover,
	.home_boxes .boxes.white-boxes .box:hover,
	#pageloader.colored .spinner div,
	.home_boxes .boxes.dark-boxes .box:hover,
	.box:hover .box-button,
	.icon-box-big-centered-square.box:hover .box-icon .changeable-icon,
	.box.icon-box-medium-left-square:hover .box-icon,
	.team-boxes .member-details .member-more:hover,
	.timelineFlat a.t_line_node.active:after,
	#member-modals .modal-dialog a.close:hover,
	.contact .contact_form .wpcf7-submit,
	.contact .contact_form.type-2 .wpcf7-submit:hover,
	.contact-block-horizontal a.box:hover .texts:before,
	.contact-block-horizontal a.box:hover .texts span.arrow:before,
	.contact-block-horizontal a.box:hover .texts span.arrow:after,
	.icon-box-medium-left-circle.box:hover .box-icon .changeable-icon.dark-icon,
	.news .box .details a.post_read_more_button:hover,
	#team.type-2 .team-boxes .member-details .member-more:hover,
	.colored-list li:before,
	#portfolio-items.colored-hover .item,
	.colored-hover .item,
	.load-more-button.circle-button:hover,
	#shop .box .item_image span.more:hover,
	#shop .single_item_details .right a:hover,
	.colored-bg-hover:hover,
	.widget ul.categories li:hover,
	.sidebar .widget a.tag:hover,
	.post .post-inner .post-more:hover,
	.dark-btn:hover,
	.colored-btn,
	.colored-icon,
	.dark-icon:hover,
	.icons-listing div:hover i,
	a.content-button:hover,
	body.dark-layout .contact .contact_form.type-2.white-form button:hover,
	#page-content .btn-accent,
	#page-content .btn-hover-accent:hover,
	#page-content .vc_progress-bar-color-accent .vc_bar,
	.widget .tagcloud a:hover,
	.widget.widget_recent_entries ul li:hover,
	.widget.widget_recent_comments ul li:hover,
	.widget.widget_archive ul li:hover,
	.widget.widget_categories ul li:hover,
	.widget ul li:hover,
	.reply-form #submit,
	#page-content .clients.testimonials .boxes .owl-pagination .owl-page.active,
	.counter-color-accent .counter-value:before,
	.vntd-team.type-2 .team-boxes .member-details .member-more:hover,
	#page-content .icon-box-medium-left-circle:hover .box-icon .changeable-icon.bg-accent2,
	#page-content .icon-box-medium-right-circle:hover .box-icon .changeable-icon.bg-accent2,
	.vntd-list-colored i.fa,
	.vntd-contact-form .wpcf7 .wpcf7-submit,
	ul.products .product-overlay:hover,
	input[type="submit"] {
		background-color: <?php echo esc_attr( $crexis_options_accent_color ); ?>;
	}

/* Border Colors */

	.colored-border,
	.timelineFlat a.t_line_node.active:after,
	ul.mobile-boxes li div.mobile-icon a,
	.icon-box-big-centered-square.box:hover .box-icon .changeable-icon:after,
	.contact *.error_warning,
	#shop .box .item_image span.more:hover:after,
	.sidebar .widget a.tag:hover,
	ul.mobile-boxes li:hover div.mobile-icon a:after,
	.icon-box-big-centered-circle .box-icon .changeable-icon:after,
	.widget .tagcloud a:hover,
	.contact .contact_form.type-2 .wpcf7-submit:hover {
		border-color:<?php echo esc_attr( $crexis_options_accent_color ); ?> !important;
	}
	
	.btn.border-btn.btn-accent,
	.btn-hover-accent:hover {
		border-color:<?php echo esc_attr( $crexis_options_accent_color ); ?>;
	}

	.tabs .nav-tabs li.active a,
	#page-content .vc_progress-bar-color-accent .vc_label_units:after,
	#page-content .vntd-contact-block.contact-block-horizontal a.box:hover .texts span.arrow {
		border-top-color:<?php echo esc_attr( $crexis_options_accent_color ); ?>;
	}
	
	.vntd-tour .nav-tabs li.active a,
	.icon-box-medium-left-square:hover .box-icon:after {
		border-left-color:<?php echo esc_attr( $crexis_options_accent_color ); ?>;
	}
	
	#page-content .vntd-contact-block.contact-block-horizontal a.box:hover .texts span.arrow {
		border-bottom-color:<?php echo esc_attr( $crexis_options_accent_color ); ?>;
	}
	
	
	
	<?php $bg_color = '#ffffff'; if(crexis_option('bg_color') ) { $bg_color = crexis_option('bg_color'); } ?>
	.spinner,
	.cbp-popup-singlePage .cbp-popup-loadingBox {
		border-top-color: <?php echo esc_attr( $crexis_options_accent_color ); ?>;
		border-left-color: <?php echo esc_attr( $crexis_options_accent_color ); ?>;
		border-right-color: <?php echo esc_attr( $crexis_options_accent_color ); ?>;
		border-bottom-color: transparent;
	}
	

<?php

//if($crexis_options_accent_color2 != '#f35050') { 
?>

/* Accent Color 2 (Default: Green) */

.color-accent2 {
	color: <?php echo esc_attr( $crexis_options_accent_color2 ); ?>;
}

.btn-accent2,
.btn-hover-accent2:hover,
input[type="submit"]:hover {
	background-color: <?php echo esc_attr( $crexis_options_accent_color2 ); ?>;
}

.btn-hover-accent2:hover {
	border-color: <?php echo esc_attr( $crexis_options_accent_color2 ); ?>;
}

#page-content .vc_progress-bar-color-accent .vc_bar {
	background: <?php echo esc_attr( $crexis_options_accent_color ); ?>;
	background: -webkit-gradient(linear, left top, right top, color-stop(0%,<?php echo esc_attr( $crexis_options_accent_color ); ?>), color-stop(100%,<?php echo esc_attr( $crexis_options_accent_color2 ); ?>));
}

#page-content .vc_progress-bar-color-accent .vc_label_units:after {
	border-top-color: <?php echo esc_attr( $crexis_options_accent_color2 ); ?>;
}

<?php 

?>

/* Accent Color 3 */

<!--#page-content .btn-accent2,-->

.vntd-post-nav a:hover {
	color: <?php echo esc_attr( $crexis_options_accent_color3 ); ?> !important;
}

#page-content .btn-accent3,
#page-content .btn-hover-accent3:hover,
#page-content .vc_progress-bar-color-accent3 .vc_bar,
.reply-form #submit:hover,
#page-content .bg-accent3,
#page-content .btn-hover-accent3:hover {
	background-color: <?php echo esc_attr( $crexis_options_accent_color3 ); ?>;
}

#page-content .btn-hover-accent3:hover {
	border-color: <?php echo esc_attr( $crexis_options_accent_color3 ); ?> !important;
}

#page-content .btn-hover-accent3:hover {
	border-color: <?php echo esc_attr( $crexis_options_accent_color3 ); ?>;
}

#page-content .vc_progress-bar-color-accent3 .vc_label_units:after {
	border-top-color: <?php echo esc_attr( $crexis_options_accent_color3 ); ?>;
}


<?php 

// Links

if( crexis_option( 'link_color' ) && crexis_option( 'link_color' ) != '' ) { 
	echo 'a, p > a { color:' . esc_attr( crexis_option( 'link_color' ) ) . '; }';
}

if( crexis_option( 'link_hover_color' ) && crexis_option( 'link_hover_color' ) != '#333333' ) { 
	echo 'a:hover, p > a:hover { color:' . esc_attr( crexis_option( 'link_hover_color' ) ) . '; }';
}

// Top Bar

if(crexis_option('topbar_bg_color') && crexis_option('topbar_bg_color') != '#fafafa') { 
	echo '#topbar,#pagetop { background-color:'.esc_attr(crexis_option('topbar_bg_color') ).'; }';
}

// Navigation

if(crexis_option('typography_navigation') && crexis_option('typography_navigation', 'color') != '#555555') { 
	echo '.nav-menu ul li a { color:'.esc_attr(crexis_option('typography_navigation', 'color') ).'; }';
}

if(crexis_option('typography_navigation') && crexis_option('typography_navigation', 'font-size') != '11px') { 
	echo '.nav-menu ul li a { font-size:'.esc_attr(crexis_option('typography_navigation', 'font-size') ).'; }';
}

if(crexis_option('header_bg_color') && crexis_option('header_bg_color') != '#ffffff') { 
	echo '#navigation,#navigation_sticky { background-color:'.esc_attr(crexis_option('header_bg_color') ).' !important; }';
}

// Page Title

if(crexis_option('typography_page_title') && crexis_option('typography_page_title', 'color') != '#363636') { 
	echo '#page-title h1 { color:'.esc_attr(crexis_option('typography_page_title', 'color') ).'; }';
}

$pagetitle_bg_color_start = crexis_option('pagetitle_bg_color_start');
$pagetitle_bg_color_end = crexis_option('pagetitle_bg_color_end');

if($pagetitle_bg_color_start && $pagetitle_bg_color_start != '#362f2d' || $pagetitle_bg_color_end  && $pagetitle_bg_color_end  != '#534741') { 
	echo '#page-title { background-color:'.esc_attr($pagetitle_bg_color_start).';
	background: -webkit-linear-gradient(left, '.$pagetitle_bg_color_start.', '.$pagetitle_bg_color_end.');
	background: -o-linear-gradient(left, '.$pagetitle_bg_color_start.', '.$pagetitle_bg_color_end.');
	background: -moz-linear-gradient(left, '.$pagetitle_bg_color_start.', '.$pagetitle_bg_color_end.');
	background: linear-gradient(left, '.$pagetitle_bg_color_start.', '.$pagetitle_bg_color_end.');
	 }';
}

if(crexis_option('breadcrumbs_color') && crexis_option('breadcrumbs_color') != '#959595') { 
	echo '.breadcrumbs a { color:'.esc_attr(crexis_option('breadcrumbs_color') ).'; }';
}

// Page Content

if(crexis_option('body_color') && crexis_option('body_color') != '#888888') { 
	echo 'body,.icon-description,.vntd-special-heading h6,#sidebar .bar ul li:before { color:'.esc_attr(crexis_option('body_color') ).'; }';
}

if(crexis_option('bg_color') && crexis_option('bg_color') != '#ffffff') { 
	echo '#pageloader,body { background-color:'.esc_attr(crexis_option('bg_color') ).'; }';
}

if(crexis_option('heading_color') && crexis_option('heading_color') != '#444444') {
	echo ' h1,h2,h3,h4,h5,h6 { color:'.esc_attr(crexis_option('heading_color') ).'; }';
	//echo '.icon-box-boxed-solid .icon-box-icon { background-color:'.$crexis_options_heading_color.'; }';
	//echo '#page-content .extra-meta-item .vntd-day, #page-content .extra-meta-item { background-color:'.crexis_option('heading_color').'; }';
}

if(crexis_option('special_heading_color') && crexis_option('special_heading_color') != '#6e6e6e') {
	echo 'h1.header { color:'.esc_attr(crexis_option('special_heading_color') ).'; }';
}


// Footer

if(crexis_option('footer_bg_color') && crexis_option('footer_bg_color') != '#202020' && crexis_option('footer_bg_color') != '#f6f6f6') { 
	echo '.footer_bottom { background-color:'.esc_attr(crexis_option('footer_bg_color') ).' !important; }';
}

if(crexis_option('footer_color') && crexis_option('footer_color') != '#666666' && crexis_option('footer_color') != '#aeaeae') { 
	echo '.copyright, #footer .vntd-social-icons a { color:'.esc_attr(crexis_option('footer_color') ).' !important; }';
	echo '#footer .vntd-social-icons a { border-color:'.esc_attr(crexis_option('footer_color') ).' !important; }';
}

if(crexis_option('footer_widgets_bg_color') && crexis_option('footer_widgets_bg_color') != '#252525' && crexis_option('footer_widgets_bg_color') != '#fafafa') { 
	echo '#footer-widgets-area { background-color:'.esc_attr(crexis_option('footer_widgets_bg_color') ).' !important; }';
}

?>

.vc_row { border: 0px; }
	
/* Font Sizes */

<?php 

if(class_exists('Crexis_Core') ) {

if(crexis_option("typography_body", "font-size") != "13px") {
	echo 'body { font-size:'.esc_attr(crexis_option("typography_body", "font-size") ).'; }';
}

if(crexis_option("typography_navigation", "font-size") != "14px") {
	echo ' .nav-menu ul li a { font-size:'.esc_attr(crexis_option("typography_navigation", "font-size") ).'; }';
}

if(crexis_option("typography_page_title", "font-size") != "30px") {
	echo ' #page-title h1 { font-size:'.esc_attr(crexis_option("typography_page_title", "font-size") ).'; }';
}

//if(crexis_option("typography_special_heading", "font-size") != "36px") {
//	echo ' #page-content .vntd-special-heading h1 { font-size:'.esc_attr(crexis_option("typography_special_heading", "font-size") ).'; }';
//}

if(crexis_option("typography_h1", "font-size") != "36px") {
	echo ' h1 { font-size:'.esc_attr(crexis_option("typography_h1", "font-size") ).'; }';
}

if(crexis_option("typography_h2", "font-size") != "30px") {
	echo ' h2 { font-size:'.esc_attr(crexis_option("typography_h2", "font-size") ).'; }';
}

if(crexis_option("typography_h3", "font-size") != "26px") {
	echo ' h3 { font-size:'.esc_attr(crexis_option("typography_h3", "font-size") ).'; }';
}

if(crexis_option("typography_h4", "font-size") != "24px") {
	echo ' h4 { font-size:'.esc_attr(crexis_option("typography_h4", "font-size") ).'; }';
}

if(crexis_option("typography_h5", "font-size") != "21px") {
	echo ' h5 { font-size:'.esc_attr(crexis_option("typography_h5", "font-size") ).'; }';
}

if(crexis_option("typography_h6", "font-size") != "18px") {
	echo ' h6 { font-size:'.esc_attr(crexis_option("typography_h6", "font-size") ).'; }';
}

//if(crexis_option("typography_h6", "font-size") != "13px") {
//	echo ' footer p, footer a { font-size:'.esc_attr(crexis_option("typography_h4", "font-size") ).'; }';
//}


/* Font Family */

$font_primary = crexis_option('typography_primary', "font-family");
$font_body = crexis_option('typography_body', "font-family");

if( $font_primary == 'Georgia, serif' ) {
	$font_primary = 'Georgia';
} elseif( $font_primary == 'Oswald' ) {
	echo ' h1, h2, h3, h4, h5, h6, .font-primary, .uppercase { -webkit-font-smoothing: antialiased; } ';
}

if( $font_body == 'Georgia, serif' ) {
	$font_body = 'Georgia';
}

if($font_primary && $font_primary != 'Raleway') {
	echo ' h1,h2,h3,h4,h5,h6,.font-primary { font-family:"'.esc_attr($font_primary).'", Open Sans, Helvetica, Arial, sans-serif !important; }';
}

if($font_body && $font_body != 'Open Sans') {
	echo ' body,h2.description,.vntd-cta-style-centered h1,.home-fixed-text,.font-secondary,.wpcf7-not-valid-tip,.testimonials h1,input,textarea { font-family:"'.esc_attr($font_body).'", Helvetica, Arial, sans-serif; }';
}

if( crexis_option('typography_navigation_font') == 'heading' ) { // If navigation font is set to primary font
	echo '.navigation .nav-menu { font-family:"' . esc_attr( $font_primary ) . '", Helvetica, Arial, sans-serif; }';
}

// Special Heading: 600, uppercase

if(crexis_option('typography_special_heading', "font-weight") && crexis_option('typography_special_heading', "font-weight") != "600") {
	echo " h1.header { font-weight:".esc_attr(crexis_option('typography_special_heading', "font-weight") )."; }";
}

if(crexis_option('typography_special_heading', "text-transform") && crexis_option('typography_special_heading', "text-transform") != "uppercase") {
	echo " h1.header { text-transform:".esc_attr(crexis_option('typography_special_heading', "text-transform") )."; }";
}

if(crexis_option('typography_special_heading', "font-size") && crexis_option('typography_special_heading', "font-size") != "30px") {
	echo " h1.header { font-size:".esc_attr(crexis_option('typography_special_heading', "font-size") )."; }";
}

// Text/Font Transform

if(crexis_option('typography_primary', "text-transform") && crexis_option('typography_primary', "text-transform") != 'none') {
	echo " h1,h2,h3,h4,h5,h6,.font-primary,.uppercase { text-transform:".esc_attr(crexis_option('typography_primary', "text-transform") )."; }";
}

if(crexis_option('typography_navigation', "text-transform") && crexis_option('typography_navigation', "text-transform") != 'none') {
	echo " ul.nav { text-transform:".esc_attr(crexis_option('typography_navigation', "text-transform") )."; }";
}

// Font Weight

if(crexis_option('typography_primary', "font-weight") && crexis_option('typography_primary', "font-weight") != "500") {
	echo " h1,h2,h3,h4,h5,h6,.font-primary,.w-option-set,#page-content .wpb_content_element .wpb_tabs_nav li,.vntd-pricing-box .properties { font-weight:".esc_attr(crexis_option('typography_primary', "font-weight") )."; }";
}

// Nav Font Weight

if(crexis_option('typography_navigation', "font-weight") && crexis_option('typography_navigation', "font-weight") != "500") {
	echo " ul.nav { font-weight:".esc_attr(crexis_option('typography_navigation', "font-weight") )."; }";
}

// Custom CSS

//if(crexis_option('footer_column_margin') ) {
//	echo ' .footer-widget-col-1 { margin-top: '. str_replace("px","",esc_attr(crexis_option('footer_column_margin') )) .'px; }';
//}

if(crexis_option('custom_css') ) {
	echo esc_textarea(crexis_option('custom_css') );
}

}

?>