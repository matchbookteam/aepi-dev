<?php 

if(function_exists('crexis_print_extra_content')) {
	crexis_print_extra_content();
}

$footer_style = 'classic';
$footer_classes = ' footer-classic t-left big-footer dark-footer';

if( crexis_option('footer_style') == 'centered' || get_post_meta(crexis_get_id(),'footer_style',TRUE) == 'centered' ) {
	$footer_style = 'centered';
	$footer_classes = ' footer-centered t-center black-bg';
}

?>

	</div>
	
	<?php
	
	if(crexis_option('footer_widgets') == true && is_active_sidebar('footer1') && get_post_meta(crexis_get_id(),'footer_widgets',TRUE) != 'disabled' || get_post_meta(crexis_get_id(),'footer_widgets',TRUE) == 'enabled' && is_active_sidebar('footer1')) { 
	
	?>
		
	<div id="footer-widgets-area" class="footer big-footer fullwidth dark-footer t-left">
	
		<div class="boxed footer_inner">
		
			<div class="footer-widgets-holder clearfix">
			
			<?php

			for($i=1;$i<=crexis_get_footer_cols();$i++) {
				if($i == crexis_get_footer_cols()) $last_class = ' vntd-span-last';				
				echo '<div class="'.crexis_get_footer_cols_class().'">';
				    if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer'.$i) );
				echo '</div>';
			}
							
			?>
			
			</div>
			
		</div>
		
	</div>
	
	<?php } ?>
	
	<!-- Footer -->
	<footer class="footer fullwidth <?php echo esc_attr( $footer_classes ); ?>">

		<?php if($footer_style == 'centered') { ?>
		<a href="#home" class="scroll top-button black-bg">
			<i class="fa fa-angle-double-up"></i>
		</a>
		<?php } ?>

		<div class="<?php if($footer_style == 'centered') { echo 'footer-inner relative t-center'; } else { echo 'footer_bottom'; } ?>">
			
			<?php if( $footer_style == 'centered' && crexis_option('footer_logo') ) { 
			
			$footer_logo_url = crexis_option('footer_logo', 'url');
			
			?>
			
			<div class="logo relative">
				<img src="<?php echo esc_url( $footer_logo_url ); ?>" alt="<?php echo get_bloginfo(); ?>">
			</div>
			
			<p class="copyright">
			<?php 
			
			if(crexis_option('copyright') != '') {
				echo str_replace("(/link)", "</a>", str_replace("(link)", '<a href="' . esc_url( crexis_option('copyright_url') ) . '">', esc_html( crexis_option('copyright') ) ) );
			} else {
				esc_html_e('Copyright 2017 Your Website.', 'crexis');
			}
			
			?>
			</p>
			
			<?php } else { ?>
			
			<!-- Bottom Inner -->
			<div class="boxed clearfix">
				<!-- Left, Copyright Area -->
				<div class="left f-left">
					<!-- Text and Link -->
					<p class="copyright">
						<?php 
						
						if(crexis_option('copyright') != '') {
							echo str_replace("(/link)", "</a>", str_replace("(link)", '<a href="' . esc_url( crexis_option('copyright_url') ) . '">', esc_html( crexis_option('copyright') ) ) );
						} else {
							esc_html_e('Copyright 2017 Your Website.', 'crexis');
						}
						
						?>
					</p>
				</div>
				<!-- End Left -->
				<!-- Right, Socials -->
				<div class="right f-right">
					<?php
					
					if(function_exists('crexis_print_social_icons')) {
						crexis_print_social_icons();
					}	
					
					?>
				</div>
				<!-- End Right -->
			</div>
			
			<?php } ?>
			<!-- End Inner -->
		</div>

	
	</footer>
	<!-- End Footer -->

	<!-- Back To Top Button -->

	<?php if(crexis_option('stt')) echo '<div id="back-top"><a href="#home" class="scroll t-center white"><i class="fa fa-angle-up"></i></a></div>'; ?>	
	
	<!-- End Back To Top Button -->

<?php wp_footer(); ?>

</body>
</html>