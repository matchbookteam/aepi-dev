<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'AEPIDATABASE');

/** MySQL database username */
define('DB_USER', 'DB USERNAME');

/** MySQL database password */
define('DB_PASSWORD', 'DB PASSWORD');

/** MySQL hostname */
define('DB_HOST', 'DB HOSTNAME');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'W|]7.Ls+I?[$~aBp2 xQ_z:Z>+u5hJ_M+HO7zWANBbH)ZU!`nuoRxdW%g{Qr?Uuw');
define('SECURE_AUTH_KEY',  'k{[DDo4<j5@|{hlNAapN70dS,-KKH%jU_LqKc7g8$;(G{`&3sYxR^k~g`lU*-3JY');
define('LOGGED_IN_KEY',    'DRIGjnkJNN`-P]8G5BZ8.2B*;1@G29MEf;.s]vPM.K$aS;~*1{&-c-YHIy!m0_`T');
define('NONCE_KEY',        '.o?y4F^og-4rC?Lnaud@rgCgyD7}FCO3VEE6M9p5$os3^m4&CWUo5,@8&BY%HIYI');
define('AUTH_SALT',        '+@dtNQ80.7XUf/9K8rs#UcFq!~F|T7h#bZ]Uje~YzM@ CZTb%~u_[hhXfc+4/7tE');
define('SECURE_AUTH_SALT', ':KC=I4;[~cEZrk>rsvQX! $2IoFZWG(LS,Lye<Ykx|a^?X>(=Zq[NYa7tURg4?2C');
define('LOGGED_IN_SALT',   'lc[84V@>.6;KC]QxCxYn8*{NOn/ W}P&u%3!h!S};F?)DT{wm@U|QQ6(m6Hw@wMR');
define('NONCE_SALT',       'uj;NA7:Z{bLPPdS=?]b +%g<Ml7VRSV28[Lx8Q=z4S7QXJO|oI~IXpe<^{Y2J|i-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
